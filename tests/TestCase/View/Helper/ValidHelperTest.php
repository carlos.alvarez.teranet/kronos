<?php
namespace App\Test\TestCase\View\Helper;

use App\View\Helper\ValidHelper;
use Cake\TestSuite\TestCase;
use Cake\View\View;

/**
 * App\View\Helper\ValidHelper Test Case
 */
class ValidHelperTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\View\Helper\ValidHelper
     */
    public $Valid;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $view = new View();
        $this->Valid = new ValidHelper($view);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Valid);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
