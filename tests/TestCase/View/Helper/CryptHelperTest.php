<?php
namespace App\Test\TestCase\View\Helper;

use App\View\Helper\CryptHelper;
use Cake\TestSuite\TestCase;
use Cake\View\View;

/**
 * App\View\Helper\CryptHelper Test Case
 */
class CryptHelperTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\View\Helper\CryptHelper
     */
    public $Crypt;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $view = new View();
        $this->Crypt = new CryptHelper($view);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Crypt);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
