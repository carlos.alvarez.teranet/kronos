<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\UroborosStatusesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\UroborosStatusesTable Test Case
 */
class UroborosStatusesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\UroborosStatusesTable
     */
    public $UroborosStatuses;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.uroboros_statuses'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('UroborosStatuses') ? [] : ['className' => UroborosStatusesTable::class];
        $this->UroborosStatuses = TableRegistry::get('UroborosStatuses', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->UroborosStatuses);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test defaultConnectionName method
     *
     * @return void
     */
    public function testDefaultConnectionName()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
