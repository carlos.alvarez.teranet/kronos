<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CriticismsCountriesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CriticismsCountriesTable Test Case
 */
class CriticismsCountriesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\CriticismsCountriesTable
     */
    public $CriticismsCountries;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.criticisms_countries'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('CriticismsCountries') ? [] : ['className' => CriticismsCountriesTable::class];
        $this->CriticismsCountries = TableRegistry::get('CriticismsCountries', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->CriticismsCountries);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test defaultConnectionName method
     *
     * @return void
     */
    public function testDefaultConnectionName()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
