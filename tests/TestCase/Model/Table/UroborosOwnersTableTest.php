<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\UroborosOwnersTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\UroborosOwnersTable Test Case
 */
class UroborosOwnersTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\UroborosOwnersTable
     */
    public $UroborosOwners;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.uroboros_owners'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('UroborosOwners') ? [] : ['className' => UroborosOwnersTable::class];
        $this->UroborosOwners = TableRegistry::get('UroborosOwners', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->UroborosOwners);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test defaultConnectionName method
     *
     * @return void
     */
    public function testDefaultConnectionName()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
