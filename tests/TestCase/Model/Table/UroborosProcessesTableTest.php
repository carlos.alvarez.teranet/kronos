<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\UroborosProcessesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\UroborosProcessesTable Test Case
 */
class UroborosProcessesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\UroborosProcessesTable
     */
    public $UroborosProcesses;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.uroboros_processes'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('UroborosProcesses') ? [] : ['className' => UroborosProcessesTable::class];
        $this->UroborosProcesses = TableRegistry::get('UroborosProcesses', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->UroborosProcesses);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test defaultConnectionName method
     *
     * @return void
     */
    public function testDefaultConnectionName()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
