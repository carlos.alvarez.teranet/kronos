<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\AttributesFilesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\AttributesFilesTable Test Case
 */
class AttributesFilesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\AttributesFilesTable
     */
    public $AttributesFiles;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.attributes_files'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('AttributesFiles') ? [] : ['className' => AttributesFilesTable::class];
        $this->AttributesFiles = TableRegistry::get('AttributesFiles', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->AttributesFiles);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test defaultConnectionName method
     *
     * @return void
     */
    public function testDefaultConnectionName()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
