<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\UroborosResolutionsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\UroborosResolutionsTable Test Case
 */
class UroborosResolutionsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\UroborosResolutionsTable
     */
    public $UroborosResolutions;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.uroboros_resolutions'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('UroborosResolutions') ? [] : ['className' => UroborosResolutionsTable::class];
        $this->UroborosResolutions = TableRegistry::get('UroborosResolutions', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->UroborosResolutions);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test defaultConnectionName method
     *
     * @return void
     */
    public function testDefaultConnectionName()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
