<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\UroborosProcessControlTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\UroborosProcessControlTable Test Case
 */
class UroborosProcessControlTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\UroborosProcessControlTable
     */
    public $UroborosProcessControl;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.uroboros_process_control'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('UroborosProcessControl') ? [] : ['className' => UroborosProcessControlTable::class];
        $this->UroborosProcessControl = TableRegistry::get('UroborosProcessControl', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->UroborosProcessControl);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test defaultConnectionName method
     *
     * @return void
     */
    public function testDefaultConnectionName()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
