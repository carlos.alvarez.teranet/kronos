<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * CriticismsTablesFixture
 *
 */
class CriticismsTablesFixture extends TestFixture
{

    /**
     * Table name
     *
     * @var string
     */
    public $table = 'criticas.Tables';

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'table_id' => ['type' => 'integer', 'length' => 10, 'autoIncrement' => true, 'null' => false, 'default' => null, 'precision' => null, 'comment' => null, 'unsigned' => null],
        'country_id' => ['type' => 'integer', 'length' => 10, 'null' => false, 'default' => null, 'precision' => null, 'comment' => null, 'unsigned' => null, 'autoIncrement' => null],
        'description' => ['type' => 'string', 'length' => 200, 'null' => false, 'default' => null, 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'database' => ['type' => 'string', 'length' => 20, 'null' => false, 'default' => null, 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        '_indexes' => [
            'XPKTablas_Critica' => ['type' => 'index', 'columns' => ['table_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['table_id'], 'length' => []],
            'R_3' => ['type' => 'foreign', 'columns' => ['country_id'], 'references' => ['Countries', 'country_id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'table_id' => 1,
            'country_id' => 1,
            'description' => 'Lorem ipsum dolor sit amet',
            'database' => 'Lorem ipsum dolor '
        ],
    ];
}
