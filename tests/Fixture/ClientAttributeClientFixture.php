<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ClientAttributeClientFixture
 *
 */
class ClientAttributeClientFixture extends TestFixture
{

    /**
     * Table name
     *
     * @var string
     */
    public $table = 'ClientAttribute_Client';

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'IdClient' => ['type' => 'integer', 'length' => 10, 'autoIncrement' => true, 'null' => false, 'default' => null, 'precision' => null, 'comment' => null, 'unsigned' => null],
        'Descricao' => ['type' => 'string', 'length' => 50, 'null' => false, 'default' => null, 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'DtCriacao' => ['type' => 'timestamp', 'length' => null, 'null' => false, 'default' => null, 'precision' => null, 'comment' => null],
        'DtAtualizacao' => ['type' => 'timestamp', 'length' => null, 'null' => false, 'default' => null, 'precision' => null, 'comment' => null],
        'FlgDesativacao' => ['type' => 'string', 'length' => 2, 'null' => false, 'default' => null, 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['IdClient'], 'length' => []],
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'IdClient' => 1,
            'Descricao' => 'Lorem ipsum dolor sit amet',
            'DtCriacao' => 1549549611,
            'DtAtualizacao' => 1549549611,
            'FlgDesativacao' => ''
        ],
    ];
}
