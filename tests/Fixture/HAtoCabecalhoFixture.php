<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * HAtoCabecalhoFixture
 *
 */
class HAtoCabecalhoFixture extends TestFixture
{

    /**
     * Table name
     *
     * @var string
     */
    public $table = 'dbo.HAto_Cabecalho';

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'IdHato_Cabecalho' => ['type' => 'biginteger', 'length' => 19, 'autoIncrement' => true, 'null' => false, 'default' => null, 'precision' => null, 'comment' => null, 'unsigned' => null],
        'Semana' => ['type' => 'tinyinteger', 'length' => 3, 'null' => false, 'default' => null, 'precision' => null, 'comment' => null, 'unsigned' => null],
        'idDomicilio' => ['type' => 'integer', 'length' => 10, 'null' => false, 'default' => null, 'precision' => null, 'comment' => null, 'unsigned' => null, 'autoIncrement' => null],
        'idIndividuo' => ['type' => 'tinyinteger', 'length' => 3, 'null' => false, 'default' => null, 'precision' => null, 'comment' => null, 'unsigned' => null],
        'Data_Compra' => ['type' => 'timestamp', 'length' => null, 'null' => false, 'default' => null, 'precision' => null, 'comment' => null],
        'idCanal' => ['type' => 'integer', 'length' => 10, 'null' => false, 'default' => null, 'precision' => null, 'comment' => null, 'unsigned' => null, 'autoIncrement' => null],
        'idArtigo' => ['type' => 'integer', 'length' => 10, 'null' => false, 'default' => null, 'precision' => null, 'comment' => null, 'unsigned' => null, 'autoIncrement' => null],
        'Data_Processamento' => ['type' => 'timestamp', 'length' => null, 'null' => true, 'default' => null, 'precision' => null, 'comment' => null],
        'Data_Criacao' => ['type' => 'timestamp', 'length' => null, 'null' => false, 'default' => null, 'precision' => null, 'comment' => null],
        'Data_Compra_Utilizada' => ['type' => 'timestamp', 'length' => null, 'null' => true, 'default' => null, 'precision' => null, 'comment' => null],
        'idProduto' => ['type' => 'integer', 'length' => 10, 'null' => false, 'default' => null, 'precision' => null, 'comment' => null, 'unsigned' => null, 'autoIncrement' => null],
        'idSub' => ['type' => 'integer', 'length' => 10, 'null' => false, 'default' => null, 'precision' => null, 'comment' => null, 'unsigned' => null, 'autoIncrement' => null],
        'idMarca' => ['type' => 'integer', 'length' => 10, 'null' => false, 'default' => null, 'precision' => null, 'comment' => null, 'unsigned' => null, 'autoIncrement' => null],
        'Quantidade' => ['type' => 'integer', 'length' => 10, 'null' => true, 'default' => null, 'precision' => null, 'comment' => null, 'unsigned' => null, 'autoIncrement' => null],
        'idConteudo' => ['type' => 'integer', 'length' => 10, 'null' => false, 'default' => null, 'precision' => null, 'comment' => null, 'unsigned' => null, 'autoIncrement' => null],
        'idPromocao' => ['type' => 'integer', 'length' => 10, 'null' => false, 'default' => null, 'precision' => null, 'comment' => null, 'unsigned' => null, 'autoIncrement' => null],
        'Preco_Unitario' => ['type' => 'decimal', 'length' => 9, 'precision' => 4, 'null' => false, 'default' => null, 'comment' => null, 'unsigned' => null],
        'Usuario' => ['type' => 'string', 'length' => 10, 'null' => false, 'default' => null, 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'Tipo_Ato' => ['type' => 'tinyinteger', 'length' => 3, 'null' => false, 'default' => null, 'precision' => null, 'comment' => null, 'unsigned' => null],
        'Usuario_Autorizacao' => ['type' => 'string', 'length' => 10, 'null' => true, 'default' => null, 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'CodBarr' => ['type' => 'string', 'length' => 20, 'null' => true, 'default' => null, 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'Desc_Prod' => ['type' => 'string', 'length' => 50, 'null' => true, 'default' => null, 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'Desc_Canal' => ['type' => 'string', 'length' => 20, 'null' => true, 'default' => null, 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'Peso' => ['type' => 'string', 'length' => 10, 'null' => true, 'default' => null, 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'Data_inv' => ['type' => 'string', 'length' => 10, 'null' => true, 'default' => null, 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'Id_Cabec' => ['type' => 'integer', 'length' => 10, 'null' => true, 'default' => null, 'precision' => null, 'comment' => null, 'unsigned' => null, 'autoIncrement' => null],
        'Id_Shop' => ['type' => 'integer', 'length' => 10, 'null' => true, 'default' => null, 'precision' => null, 'comment' => null, 'unsigned' => null, 'autoIncrement' => null],
        'Id_Item' => ['type' => 'integer', 'length' => 10, 'null' => true, 'default' => null, 'precision' => null, 'comment' => null, 'unsigned' => null, 'autoIncrement' => null],
        'Preco_Total' => ['type' => 'decimal', 'length' => 9, 'precision' => 2, 'null' => true, 'default' => null, 'comment' => null, 'unsigned' => null],
        'Forma_pagto' => ['type' => 'integer', 'length' => 10, 'null' => true, 'default' => null, 'precision' => null, 'comment' => null, 'unsigned' => null, 'autoIncrement' => null],
        'flgShopMis' => ['type' => 'boolean', 'length' => null, 'null' => true, 'default' => 0, 'precision' => null, 'comment' => null],
        'idPais' => ['type' => 'integer', 'length' => 10, 'null' => false, 'default' => null, 'precision' => null, 'comment' => null, 'unsigned' => null, 'autoIncrement' => null],
        'id_viaje' => ['type' => 'biginteger', 'length' => 19, 'null' => true, 'default' => null, 'precision' => null, 'comment' => null, 'unsigned' => null, 'autoIncrement' => null],
        'id_equipo' => ['type' => 'integer', 'length' => 10, 'null' => true, 'default' => null, 'precision' => null, 'comment' => null, 'unsigned' => null, 'autoIncrement' => null],
        'Data_Correcao' => ['type' => 'timestamp', 'length' => null, 'null' => true, 'default' => null, 'precision' => null, 'comment' => null],
        'Formato_Canal' => ['type' => 'integer', 'length' => 10, 'null' => true, 'default' => null, 'precision' => null, 'comment' => null, 'unsigned' => null, 'autoIncrement' => null],
        'idApp' => ['type' => 'integer', 'length' => 10, 'null' => true, 'default' => null, 'precision' => null, 'comment' => null, 'unsigned' => null, 'autoIncrement' => null],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['IdHato_Cabecalho'], 'length' => []],
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'IdHato_Cabecalho' => 1,
            'Semana' => 1,
            'idDomicilio' => 1,
            'idIndividuo' => 1,
            'Data_Compra' => 1625502008,
            'idCanal' => 1,
            'idArtigo' => 1,
            'Data_Processamento' => 1625502008,
            'Data_Criacao' => 1625502008,
            'Data_Compra_Utilizada' => 1625502008,
            'idProduto' => 1,
            'idSub' => 1,
            'idMarca' => 1,
            'Quantidade' => 1,
            'idConteudo' => 1,
            'idPromocao' => 1,
            'Preco_Unitario' => 1.5,
            'Usuario' => 'Lorem ip',
            'Tipo_Ato' => 1,
            'Usuario_Autorizacao' => 'Lorem ip',
            'CodBarr' => 'Lorem ipsum dolor ',
            'Desc_Prod' => 'Lorem ipsum dolor sit amet',
            'Desc_Canal' => 'Lorem ipsum dolor ',
            'Peso' => 'Lorem ip',
            'Data_inv' => 'Lorem ip',
            'Id_Cabec' => 1,
            'Id_Shop' => 1,
            'Id_Item' => 1,
            'Preco_Total' => 1.5,
            'Forma_pagto' => 1,
            'flgShopMis' => 1,
            'idPais' => 1,
            'id_viaje' => 1,
            'id_equipo' => 1,
            'Data_Correcao' => 1625502008,
            'Formato_Canal' => 1,
            'idApp' => 1
        ],
    ];
}
