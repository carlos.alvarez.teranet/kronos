<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * UroborosLogsFixture
 *
 */
class UroborosLogsFixture extends TestFixture
{

    /**
     * Table name
     *
     * @var string
     */
    public $table = 'WF_PROCESS_LOGS';

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'lid' => ['type' => 'integer', 'length' => 10, 'autoIncrement' => true, 'null' => false, 'default' => null, 'precision' => null, 'comment' => null, 'unsigned' => null],
        'eid' => ['type' => 'integer', 'length' => 10, 'null' => false, 'default' => null, 'precision' => null, 'comment' => null, 'unsigned' => null, 'autoIncrement' => null],
        'log_type' => ['type' => 'string', 'fixed' => true, 'length' => 1, 'null' => false, 'default' => null, 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null],
        'log_level' => ['type' => 'integer', 'length' => 10, 'null' => true, 'default' => null, 'precision' => null, 'comment' => null, 'unsigned' => null, 'autoIncrement' => null],
        'message' => ['type' => 'string', 'length' => 1000, 'null' => true, 'default' => null, 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'created_on' => ['type' => 'timestamp', 'length' => null, 'null' => true, 'default' => 'getdate', 'precision' => null, 'comment' => null],
        'detail' => ['type' => 'string', 'length' => 8000, 'null' => true, 'default' => null, 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'subject' => ['type' => 'string', 'length' => 500, 'null' => true, 'default' => null, 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'aid' => ['type' => 'integer', 'length' => 10, 'null' => true, 'default' => null, 'precision' => null, 'comment' => null, 'unsigned' => null, 'autoIncrement' => null],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['lid'], 'length' => []],
            'FK_WF_PROCESS_LOG_TYPE' => ['type' => 'foreign', 'columns' => ['log_type'], 'references' => ['WF_PROCESS_LOG_TYPE', 'code'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'FK_WF_PROCESS_LOG_CONTROL' => ['type' => 'foreign', 'columns' => ['eid'], 'references' => ['WF_PROCESS_CONTROL', 'eid'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'lid' => 1,
            'eid' => 1,
            'log_type' => 'Lorem ipsum dolor sit ame',
            'log_level' => 1,
            'message' => 'Lorem ipsum dolor sit amet',
            'created_on' => 1520519178,
            'detail' => 'Lorem ipsum dolor sit amet',
            'subject' => 'Lorem ipsum dolor sit amet',
            'aid' => 1
        ],
    ];
}
