<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * CriticismsLogsFixture
 *
 */
class CriticismsLogsFixture extends TestFixture
{

    /**
     * Table name
     *
     * @var string
     */
    public $table = 'criticas.Logs';

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'log_id' => ['type' => 'integer', 'length' => 10, 'autoIncrement' => true, 'null' => false, 'default' => null, 'precision' => null, 'comment' => null, 'unsigned' => null],
        'tabla_id' => ['type' => 'integer', 'length' => 10, 'null' => true, 'default' => null, 'precision' => null, 'comment' => null, 'unsigned' => null, 'autoIncrement' => null],
        'affected_id' => ['type' => 'integer', 'length' => 10, 'null' => true, 'default' => null, 'precision' => null, 'comment' => null, 'unsigned' => null, 'autoIncrement' => null],
        'field' => ['type' => 'string', 'fixed' => true, 'length' => 18, 'null' => true, 'default' => null, 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null],
        'action_id' => ['type' => 'integer', 'length' => 10, 'null' => false, 'default' => null, 'precision' => null, 'comment' => null, 'unsigned' => null, 'autoIncrement' => null],
        'old_value' => ['type' => 'string', 'length' => 20, 'null' => false, 'default' => null, 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'new_value' => ['type' => 'string', 'length' => 20, 'null' => false, 'default' => null, 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'date' => ['type' => 'timestamp', 'length' => null, 'null' => false, 'default' => null, 'precision' => null, 'comment' => null],
        '_indexes' => [
            'XPKCritica_logs' => ['type' => 'index', 'columns' => ['log_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['log_id'], 'length' => []],
            'R_5' => ['type' => 'foreign', 'columns' => ['action_id'], 'references' => ['Actions', 'action_id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'R_10' => ['type' => 'foreign', 'columns' => ['tabla_id'], 'references' => ['Tables', 'table_id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'log_id' => 1,
            'tabla_id' => 1,
            'affected_id' => 1,
            'field' => 'Lorem ipsum dolo',
            'action_id' => 1,
            'old_value' => 'Lorem ipsum dolor ',
            'new_value' => 'Lorem ipsum dolor ',
            'date' => 1621365496
        ],
    ];
}
