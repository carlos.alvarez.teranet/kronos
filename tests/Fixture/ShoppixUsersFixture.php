<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ShoppixUsersFixture
 *
 */
class ShoppixUsersFixture extends TestFixture
{

    /**
     * Table name
     *
     * @var string
     */
    public $table = 'AppUser';

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'Id' => ['type' => 'uuid', 'null' => false, 'default' => null, 'length' => null, 'precision' => null, 'comment' => null],
        'Email' => ['type' => 'string', 'length' => 320, 'null' => true, 'default' => null, 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'PasswordHash' => ['type' => 'string', 'length' => 1024, 'null' => false, 'default' => null, 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'TemporaryPasswordHash' => ['type' => 'string', 'length' => 1024, 'null' => true, 'default' => null, 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'TemporaryEmail' => ['type' => 'string', 'length' => 320, 'null' => true, 'default' => null, 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'IsUserDisabled' => ['type' => 'boolean', 'length' => null, 'null' => false, 'default' => 0, 'precision' => null, 'comment' => null],
        'UserDisabledDate' => ['type' => 'timestamp', 'length' => null, 'null' => true, 'default' => null, 'precision' => null, 'comment' => null],
        'IsAdminDisabled' => ['type' => 'boolean', 'length' => null, 'null' => false, 'default' => 0, 'precision' => null, 'comment' => null],
        'AdminDisabledDate' => ['type' => 'timestamp', 'length' => null, 'null' => true, 'default' => null, 'precision' => null, 'comment' => null],
        'ReferralCode' => ['type' => 'string', 'length' => 1024, 'null' => true, 'default' => null, 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'ReferrerCode' => ['type' => 'string', 'length' => 1024, 'null' => true, 'default' => null, 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'RegisteredDate' => ['type' => 'timestamp', 'length' => null, 'null' => false, 'default' => null, 'precision' => null, 'comment' => null],
        'RefreshToken' => ['type' => 'string', 'length' => 1024, 'null' => true, 'default' => null, 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'LastGeneralReferralsReminderDate' => ['type' => 'timestamp', 'length' => null, 'null' => true, 'default' => null, 'precision' => null, 'comment' => null],
        'LastCompleteProfileReminderDate' => ['type' => 'timestamp', 'length' => null, 'null' => true, 'default' => null, 'precision' => null, 'comment' => null],
        'IsFirstEmailConfirmed' => ['type' => 'boolean', 'length' => null, 'null' => false, 'default' => 0, 'precision' => null, 'comment' => null],
        'LoginAttemptsCount' => ['type' => 'integer', 'length' => 10, 'null' => false, 'default' => null, 'precision' => null, 'comment' => null, 'unsigned' => null, 'autoIncrement' => null],
        'BlockDateTime' => ['type' => 'timestamp', 'length' => null, 'null' => true, 'default' => null, 'precision' => null, 'comment' => null],
        'CreatedDateTime' => ['type' => 'timestamp', 'length' => null, 'null' => false, 'default' => null, 'precision' => null, 'comment' => null],
        'CreatedBy' => ['type' => 'string', 'length' => 300, 'null' => true, 'default' => null, 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'UpdatedDateTime' => ['type' => 'timestamp', 'length' => null, 'null' => false, 'default' => null, 'precision' => null, 'comment' => null],
        'UpdatedBy' => ['type' => 'string', 'length' => 300, 'null' => true, 'default' => null, 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'ShortId' => ['type' => 'integer', 'length' => 10, 'autoIncrement' => true, 'null' => false, 'default' => null, 'precision' => null, 'comment' => null, 'unsigned' => null],
        'ResetPasswordDate' => ['type' => 'timestamp', 'length' => null, 'null' => true, 'default' => null, 'precision' => null, 'comment' => null],
        'LastReceiptSubmissionDateTime' => ['type' => 'timestamp', 'length' => null, 'null' => true, 'default' => null, 'precision' => null, 'comment' => null],
        'EReceiptAccountId' => ['type' => 'string', 'length' => 100, 'null' => true, 'default' => null, 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'EReceiptRegistrationStatus' => ['type' => 'integer', 'length' => 10, 'null' => true, 'default' => null, 'precision' => null, 'comment' => null, 'unsigned' => null, 'autoIncrement' => null],
        'EReceiptRegistrationStatusChangedDateTime' => ['type' => 'timestamp', 'length' => null, 'null' => true, 'default' => null, 'precision' => null, 'comment' => null],
        'EReceiptRegistrationRenewalSent' => ['type' => 'boolean', 'length' => null, 'null' => true, 'default' => 0, 'precision' => null, 'comment' => null],
        'WebSiteRefreshToken' => ['type' => 'string', 'length' => 1024, 'null' => true, 'default' => null, 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'WebSiteBlockDateTime' => ['type' => 'timestamp', 'length' => null, 'null' => true, 'default' => null, 'precision' => null, 'comment' => null],
        'WebSiteLoginAttemptsCount' => ['type' => 'integer', 'length' => 10, 'null' => false, 'default' => null, 'precision' => null, 'comment' => null, 'unsigned' => null, 'autoIncrement' => null],
        'EReceiptRegistrationExpirationDate' => ['type' => 'timestamp', 'length' => null, 'null' => true, 'default' => null, 'precision' => null, 'comment' => null],
        'EReceiptRegistrationDateTime' => ['type' => 'timestamp', 'length' => null, 'null' => true, 'default' => null, 'precision' => null, 'comment' => null],
        'EReceiptRegistrationStatusUpdatedBy' => ['type' => 'string', 'length' => 300, 'null' => true, 'default' => null, 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'EReceiptConsent' => ['type' => 'boolean', 'length' => null, 'null' => true, 'default' => 0, 'precision' => null, 'comment' => null],
        'EReceiptConsentDateTime' => ['type' => 'timestamp', 'length' => null, 'null' => true, 'default' => null, 'precision' => null, 'comment' => null],
        'ConfirmationMailDateTime' => ['type' => 'timestamp', 'length' => null, 'null' => true, 'default' => null, 'precision' => null, 'comment' => null],
        'FaceBookId' => ['type' => 'string', 'length' => 200, 'null' => true, 'default' => null, 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'ActiveSubmissionEmail' => ['type' => 'string', 'length' => 150, 'null' => true, 'default' => null, 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'ActiveSubmissionAccess' => ['type' => 'boolean', 'length' => null, 'null' => true, 'default' => 0, 'precision' => null, 'comment' => null],
        '_indexes' => [
            'IX_AppUser_Email' => ['type' => 'index', 'columns' => ['Email'], 'length' => []],
            'IX_AppUser_ReferrerCode' => ['type' => 'index', 'columns' => ['ReferrerCode'], 'length' => []],
            'IX_AppUser_TemporaryEmail' => ['type' => 'index', 'columns' => ['TemporaryEmail'], 'length' => []],
            'IX_AppUser_Email_IsFirstEmailConfirmed' => ['type' => 'index', 'columns' => ['Email', 'IsFirstEmailConfirmed'], 'length' => []],
            'IX_AppUser_FacebookId_Id' => ['type' => 'index', 'columns' => ['FaceBookId', 'Id', 'AdminDisabledDate', 'BlockDateTime', 'ConfirmationMailDateTime', 'CreatedBy', 'CreatedDateTime', 'Email', 'IsAdminDisabled', 'IsFirstEmailConfirmed', 'IsUserDisabled', 'LastCompleteProfileReminderDate', 'LastGeneralReferralsReminderDate', 'LastReceiptSubmissionDateTime', 'LoginAttemptsCount', 'PasswordHash', 'ReferralCode', 'ReferrerCode', 'RefreshToken', 'RegisteredDate', 'ResetPasswordDate', 'ShortId', 'TemporaryEmail', 'TemporaryPasswordHash', 'UpdatedBy', 'UpdatedDateTime', 'UserDisabledDate', 'WebSiteBlockDateTime', 'WebSiteLoginAttemptsCount', 'WebSiteRefreshToken'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['Id'], 'length' => []],
            'UQ_AppUser_ShortId' => ['type' => 'unique', 'columns' => ['ShortId'], 'length' => []],
            'FK_AppUser_EReceiptRegistrationStatus' => ['type' => 'foreign', 'columns' => ['EReceiptRegistrationStatus'], 'references' => ['EReceiptRegistrationStatus', 'Id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'Id' => 'e946e499-1c47-47d8-b76e-e47a2d6144db',
            'Email' => 'Lorem ipsum dolor sit amet',
            'PasswordHash' => 'Lorem ipsum dolor sit amet',
            'TemporaryPasswordHash' => 'Lorem ipsum dolor sit amet',
            'TemporaryEmail' => 'Lorem ipsum dolor sit amet',
            'IsUserDisabled' => 1,
            'UserDisabledDate' => 1566855523,
            'IsAdminDisabled' => 1,
            'AdminDisabledDate' => 1566855523,
            'ReferralCode' => 'Lorem ipsum dolor sit amet',
            'ReferrerCode' => 'Lorem ipsum dolor sit amet',
            'RegisteredDate' => 1566855523,
            'RefreshToken' => 'Lorem ipsum dolor sit amet',
            'LastGeneralReferralsReminderDate' => 1566855523,
            'LastCompleteProfileReminderDate' => 1566855523,
            'IsFirstEmailConfirmed' => 1,
            'LoginAttemptsCount' => 1,
            'BlockDateTime' => 1566855523,
            'CreatedDateTime' => 1566855523,
            'CreatedBy' => 'Lorem ipsum dolor sit amet',
            'UpdatedDateTime' => 1566855523,
            'UpdatedBy' => 'Lorem ipsum dolor sit amet',
            'ShortId' => 1,
            'ResetPasswordDate' => 1566855523,
            'LastReceiptSubmissionDateTime' => 1566855523,
            'EReceiptAccountId' => 'Lorem ipsum dolor sit amet',
            'EReceiptRegistrationStatus' => 1,
            'EReceiptRegistrationStatusChangedDateTime' => 1566855523,
            'EReceiptRegistrationRenewalSent' => 1,
            'WebSiteRefreshToken' => 'Lorem ipsum dolor sit amet',
            'WebSiteBlockDateTime' => 1566855523,
            'WebSiteLoginAttemptsCount' => 1,
            'EReceiptRegistrationExpirationDate' => 1566855523,
            'EReceiptRegistrationDateTime' => 1566855523,
            'EReceiptRegistrationStatusUpdatedBy' => 'Lorem ipsum dolor sit amet',
            'EReceiptConsent' => 1,
            'EReceiptConsentDateTime' => 1566855523,
            'ConfirmationMailDateTime' => 1566855523,
            'FaceBookId' => 'Lorem ipsum dolor sit amet',
            'ActiveSubmissionEmail' => 'Lorem ipsum dolor sit amet',
            'ActiveSubmissionAccess' => 1
        ],
    ];
}
