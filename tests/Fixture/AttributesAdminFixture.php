<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * AttributesAdminFixture
 *
 */
class AttributesAdminFixture extends TestFixture
{

    /**
     * Table name
     *
     * @var string
     */
    public $table = 'A_ClientAttribute_Admin';

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'Admin' => ['type' => 'string', 'length' => 120, 'null' => false, 'default' => null, 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'AllowUpdate' => ['type' => 'boolean', 'length' => null, 'null' => false, 'default' => 0, 'precision' => null, 'comment' => null],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['Admin'], 'length' => []],
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'Admin' => 'c6f9a117-18e0-4413-9a57-a162ac3f2942',
            'AllowUpdate' => 1
        ],
    ];
}
