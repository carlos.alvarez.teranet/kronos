<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * CriticismsReviewsFixture
 *
 */
class CriticismsReviewsFixture extends TestFixture
{

    /**
     * Table name
     *
     * @var string
     */
    public $table = 'criticas.Reviews';

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'review_id' => ['type' => 'integer', 'length' => 10, 'autoIncrement' => true, 'null' => false, 'default' => null, 'precision' => null, 'comment' => null, 'unsigned' => null],
        'description' => ['type' => 'string', 'length' => 200, 'null' => false, 'default' => null, 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'created_for' => ['type' => 'string', 'length' => 100, 'null' => false, 'default' => null, 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'updated_for' => ['type' => 'string', 'length' => 100, 'null' => false, 'default' => null, 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'creation_date' => ['type' => 'timestamp', 'length' => null, 'null' => false, 'default' => null, 'precision' => null, 'comment' => null],
        'updated_date' => ['type' => 'timestamp', 'length' => null, 'null' => false, 'default' => null, 'precision' => null, 'comment' => null],
        'table_id' => ['type' => 'integer', 'length' => 10, 'null' => false, 'default' => null, 'precision' => null, 'comment' => null, 'unsigned' => null, 'autoIncrement' => null],
        '_indexes' => [
            'XPKCritica' => ['type' => 'index', 'columns' => ['review_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['review_id'], 'length' => []],
            'R_7' => ['type' => 'foreign', 'columns' => ['table_id'], 'references' => ['Tables', 'table_id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'review_id' => 1,
            'description' => 'Lorem ipsum dolor sit amet',
            'created_for' => 'Lorem ipsum dolor sit amet',
            'updated_for' => 'Lorem ipsum dolor sit amet',
            'creation_date' => 1620666032,
            'updated_date' => 1620666032,
            'table_id' => 1
        ],
    ];
}
