<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * CriticismsActionsFixture
 *
 */
class CriticismsActionsFixture extends TestFixture
{

    /**
     * Table name
     *
     * @var string
     */
    public $table = 'criticas.Actions';

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'action_id' => ['type' => 'integer', 'length' => 10, 'autoIncrement' => true, 'null' => false, 'default' => null, 'precision' => null, 'comment' => null, 'unsigned' => null],
        'field' => ['type' => 'string', 'length' => 20, 'null' => true, 'default' => null, 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'rule_id' => ['type' => 'integer', 'length' => 10, 'null' => false, 'default' => null, 'precision' => null, 'comment' => null, 'unsigned' => null, 'autoIncrement' => null],
        'action_dml' => ['type' => 'string', 'length' => 20, 'null' => false, 'default' => null, 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'value' => ['type' => 'string', 'length' => 20, 'null' => false, 'default' => null, 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        '_indexes' => [
            'XPKCritica_Acciones' => ['type' => 'index', 'columns' => ['action_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['action_id'], 'length' => []],
            'R_4' => ['type' => 'foreign', 'columns' => ['rule_id'], 'references' => ['Rules', 'rule_id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'R_6' => ['type' => 'foreign', 'columns' => ['rule_id'], 'references' => ['Rules', 'rule_id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'action_id' => 1,
            'field' => 'Lorem ipsum dolor ',
            'rule_id' => 1,
            'action_dml' => 'Lorem ipsum dolor ',
            'value' => 'Lorem ipsum dolor '
        ],
    ];
}
