<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * UroborosProcessesFixture
 *
 */
class UroborosProcessesFixture extends TestFixture
{

    /**
     * Table name
     *
     * @var string
     */
    public $table = 'WF_PROCESS';

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'pid' => ['type' => 'integer', 'length' => 10, 'autoIncrement' => true, 'null' => false, 'default' => null, 'precision' => null, 'comment' => null, 'unsigned' => null],
        'tid' => ['type' => 'integer', 'length' => 10, 'null' => false, 'default' => null, 'precision' => null, 'comment' => null, 'unsigned' => null, 'autoIncrement' => null],
        'cid' => ['type' => 'integer', 'length' => 10, 'null' => false, 'default' => null, 'precision' => null, 'comment' => null, 'unsigned' => null, 'autoIncrement' => null],
        'description' => ['type' => 'string', 'length' => 500, 'null' => true, 'default' => null, 'collate' => 'SQL_Latin1_General_CP1_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['pid'], 'length' => []],
            'FK_WF_PROCESS_CATEGORY' => ['type' => 'foreign', 'columns' => ['cid'], 'references' => ['WF_PROCESS_CATEGORY', 'cid'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'FK_WF_PROCESS_TEAM' => ['type' => 'foreign', 'columns' => ['tid'], 'references' => ['WF_TEAM', 'tid'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'pid' => 1,
            'tid' => 1,
            'cid' => 1,
            'description' => 'Lorem ipsum dolor sit amet'
        ],
    ];
}
