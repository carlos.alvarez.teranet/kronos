<?php

function saludar($nombre) {
    echo "¡Hola, $nombre!";
}

function nombreMes($idMes){
    $meses=array(
        '1'=>"Enero",
        '2'=>"Febrero",
        '3'=>"Marzo",
        '4'=>"Abril",
        '5'=>"Mayo",
        '6'=>"Junio",
        '7'=>"Julio",
        '8'=>"Agosto",
        '9'=>"Septiembre",
        '10'=>"Octubre",
        '11'=>"Noviembre",
        '12'=>"Diciembre",
    );
    return $meses[$idMes];

}

function getDateFormato($fecha,$format='Y-m-d'){
    if($fecha){
        return date($format,strtotime($fecha));
    }
}
?>