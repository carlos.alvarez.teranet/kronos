<?php
$this->Breadcrumbs->add([
    ['title' => 'Dashboard']
]);	
?>
<?= $this->Flash->render() ?>
<div class="row">
    <div class="col-xs-5 col-md-5">
        <div class="widget">
            <div class="widget-header ">
                <span class="widget-caption">Mis Datos</span>
            </div>
            <div class="widget-body">
	            <div class="row">
	            	<div class="col-sm-4">
	            		<center><?= $this->Html->image('profiles/'.$this->request->session()->read('Auth.User.id').'.jpg', ['width' => '100%', 'style' => 'max-width: 180px']); ?></center>
	            	</div>
	            	<div class="col-sm-8">
	            		<h3 style="margin-bottom: 20px; margin-top: 5px"><?= $this->request->session()->read('Auth.User.givenName') ?> <?= $this->request->session()->read('Auth.User.surname') ?></h3>
	            		<p style="margin-bottom: 4px"><strong>Title:</strong> <?= $this->request->session()->read('Auth.User.displayName') ?></p>
	            		<p style="margin-bottom: 4px"><strong>Title:</strong> <?= $this->request->session()->read('Auth.User.jobTitle') ?></p>
	            		<p style="margin-bottom: 4px"><strong>Email:</strong> <?= $this->request->session()->read('Auth.User.mail') ?></p>
	            		<p style="margin-bottom: 4px"><strong>Location:</strong> <?= $this->request->session()->read('Auth.User.officeLocation') ?></p>
	            		<p style="margin-bottom: 4px"><strong>Mobile:</strong> <?= $this->request->session()->read('Auth.User.mobilePhone') ?></p>
	            	</div>
	            </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-xs-5 col-md-5">
	    <div class="widget">
            <div class="widget-header ">
                <span class="widget-caption"><strong>Links de interés</strong></span>
            </div>
            <div class="widget-body" style="padding: 0px">
	            <div class="row">
		            <div class="col-sm-12">
			            <table class="table table-bordered table-responsive">
			            	<tr>
				            	<? foreach($links as $link) { ?>
				            	<td align="center"><a target="_blank" href="<?=$link['link'][$iptype] ?>"><?=$link['name'] ?></a></td>
				            	<? } ?>
			            	</tr>
			            </table>
		            </div>
	            </div>
            </div>
	    </div>
    </div>
</div>