<style>
	table.w th {
		border: 1px solid #acacac;
		border-collapse: collapse;
		border-spacing: 0px;		
	}
	table.w td {
		border: 1px solid #acacac;
		border-collapse: collapse;
		padding: 0px;
	}
	.w th {
		font-size: small !important;
	}
	
	.w td {
		font-size: small !important;
	}
</style>
<p>Estimad@s,</p>
<p>
	Se informa que los pesos para <strong><?= $country ?></strong> en el panel <strong><?= $panel ?></strong> en el periodo <strong><?= $period ?></strong>, fueron cargados exitosamente en <strong><?= $db ?>.<?= $table ?></strong>.<br>
	El resultado de los pesos de los últimos años, corresponde a:
</p>
<table class="w" cellpadding="0" cellspacing="0">
    <thead>
        <tr>
        	<th align="center" style="text-align: center; background-color: cornflowerblue; color: white" colspan="13">Pesos - <?= $db ?>.<?= $table ?> ('000)</th>
        </tr>
        <tr>
            <th align="center">Año</th>
            <th align="center">Mes 1</th>
            <th align="center">Mes 2</th>
            <th align="center">Mes 3</th>
            <th align="center">Mes 4</th>
            <th align="center">Mes 5</th>
            <th align="center">Mes 6</th>
            <th align="center">Mes 7</th>
            <th align="center">Mes 8</th>
            <th align="center">Mes 9</th>
            <th align="center">Mes 10</th>
            <th align="center">Mes 11</th>
            <th align="center">Mes 12</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach($weights as $weight) { ?>
        <tr>
            <td align="center"><?= $weight['Ano'] ?></td>
            <td align="right"><?= number_format($weight['Mes1']/1000, 0, ",", ".") ?></td>
            <td align="right"><?= number_format($weight['Mes2']/1000, 0, ",", ".") ?></td>
            <td align="right"><?= number_format($weight['Mes3']/1000, 0, ",", ".") ?></td>
            <td align="right"><?= number_format($weight['Mes4']/1000, 0, ",", ".") ?></td>
            <td align="right"><?= number_format($weight['Mes5']/1000, 0, ",", ".") ?></td>
            <td align="right"><?= number_format($weight['Mes6']/1000, 0, ",", ".") ?></td>
            <td align="right"><?= number_format($weight['Mes7']/1000, 0, ",", ".") ?></td>
            <td align="right"><?= number_format($weight['Mes8']/1000, 0, ",", ".") ?></td>
            <td align="right"><?= number_format($weight['Mes9']/1000, 0, ",", ".") ?></td>
            <td align="right"><?= number_format($weight['Mes10']/1000, 0, ",", ".") ?></td>
            <td align="right"><?= number_format($weight['Mes11']/1000, 0, ",", ".") ?></td>
            <td align="right"><?= number_format($weight['Mes12']/1000, 0, ",", ".") ?></td>
        </tr>
        <?php } ?> 
    </tbody>
</table>
<br>
<table class="w" cellpadding="0" cellspacing="0">
    <thead>
        <tr>
        	<th align="center" style="text-align: center; background-color: #eec264; color: black" colspan="13">Muestra - <?= $db ?>.<?= $table ?></th>
        </tr>
        <tr>
            <th align="center">Año</th>
            <th align="center">Mes 1</th>
            <th align="center">Mes 2</th>
            <th align="center">Mes 3</th>
            <th align="center">Mes 4</th>
            <th align="center">Mes 5</th>
            <th align="center">Mes 6</th>
            <th align="center">Mes 7</th>
            <th align="center">Mes 8</th>
            <th align="center">Mes 9</th>
            <th align="center">Mes 10</th>
            <th align="center">Mes 11</th>
            <th align="center">Mes 12</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach($samples as $sample) { ?>
        <tr>
            <td align="center"><?= $sample['Ano'] ?></td>
            <td align="right"><?= number_format($sample['Mes1'], 0, ",", ".") ?></td>
            <td align="right"><?= number_format($sample['Mes2'], 0, ",", ".") ?></td>
            <td align="right"><?= number_format($sample['Mes3'], 0, ",", ".") ?></td>
            <td align="right"><?= number_format($sample['Mes4'], 0, ",", ".") ?></td>
            <td align="right"><?= number_format($sample['Mes5'], 0, ",", ".") ?></td>
            <td align="right"><?= number_format($sample['Mes6'], 0, ",", ".") ?></td>
            <td align="right"><?= number_format($sample['Mes7'], 0, ",", ".") ?></td>
            <td align="right"><?= number_format($sample['Mes8'], 0, ",", ".") ?></td>
            <td align="right"><?= number_format($sample['Mes9'], 0, ",", ".") ?></td>
            <td align="right"><?= number_format($sample['Mes10'], 0, ",", ".") ?></td>
            <td align="right"><?= number_format($sample['Mes11'], 0, ",", ".") ?></td>
            <td align="right"><?= number_format($sample['Mes12'], 0, ",", ".") ?></td>
        </tr>
        <?php } ?> 
    </tbody>
</table>
<br>
<br>
<p>
	Atte.<br>
	ROC LatAm Web Services
</p>