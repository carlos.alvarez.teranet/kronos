<?php
$this->Breadcrumbs->add([
    ['title' => 'Shoppix'],
    ['title' => strtoupper($country)],
    ['title' => 'Participants'],
    ['title' => 'Summary']
]);	
?>
<?= $this->Flash->render() ?>
<div class="row">
	<div class="col-xs-12">
		<div class="dashboard-box">
			<div class="box-header">
				<div class="box-header">
                    <div class="deadline">
                        Remaining users: 1500
                    </div>
                </div>
			</div>
			<div class="box-progress">
                <div class="progress-handle">day 20</div>
                <div class="progress progress-xs progress-no-radius bg-whitesmoke">
                    <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 20%">
                    </div>
                </div>
            </div>
            <div class="box-tabbs">
            	<div class="tabbable">
                	<ul class="nav nav-tabs tabs-flat  nav-justified" id="myTab11">
                        <li class="active">
                            <a data-toggle="tab" href="#total_users" aria-expanded="true">
                                Total users
                            </a>
                        </li>
                        <li class="">
                            <a data-toggle="tab" href="#reffered" aria-expanded="false">
                                Referred
                            </a>
                        </li>

                        <li class="">
                            <a data-toggle="tab" id="confirmed" href="#bandwidth" aria-expanded="false">
                                Email confirmed
                            </a>
                        </li>
                        <li class="">
                            <a data-toggle="tab" href="#devices" aria-expanded="false">
                                Devices
                            </a>
                        </li>
                    </ul>
            	</div>
            	<div class="tab-content tabs-flat no-padding">
            		<div id="total_users" class="tab-pane padding-5 animated fadeInUp active">
	            		<?= $total_graph ?>
            		</div>
            		<div id="reffered" class="tab-pane padding-5 animated fadeInUp">
            		</div>
            		<div id="confirmed" class="tab-pane padding-5 animated fadeInUp">
            		</div>
            		<div id="devices" class="tab-pane padding-5 animated fadeInUp">
            		</div>
            	</div>
            </div>
			<div class="box-visits">
                <div class="row">
                    <div class="col-lg-4 col-sm-4 col-xs-12">
                        <div class="notification">
                            <div class="clearfix">
                                <div class="notification-icon">
                                    <i class="fa fa-gift palegreen bordered-1 bordered-palegreen"></i>
                                </div>
                                <div class="notification-body">
                                    <span class="title">Kate birthday party</span>
                                    <span class="description">08:30 pm</span>
                                </div>
                                <div class="notification-extra">
                                    <i class="fa fa-calendar palegreen"></i>
                                    <i class="fa fa-clock-o palegreen"></i>
                                    <span class="description">at home</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-sm-4 col-xs-12">
                        <div class="notification">
                            <div class="clearfix">
                                <div class="notification-icon">
                                    <i class="fa fa-check azure bordered-1 bordered-azure"></i>
                                </div>
                                <div class="notification-body">
                                    <span class="title">Hanging out with kids</span>
                                    <span class="description">03:30 pm - 05:15 pm</span>
                                </div>
                                <div class="notification-extra">
                                    <i class="fa fa-clock-o azure"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-sm-4 col-xs-12">
                        <div class="notification">
                            <div class="clearfix">
                                <div class="notification-icon">
                                    <i class="fa fa-phone bordered-1 bordered-orange orange"></i>
                                </div>
                                <div class="notification-body">
                                    <span class="title">Meeting with Patty</span>
                                    <span class="description">01:00 pm</span>
                                </div>
                                <div class="notification-extra">
                                    <i class="fa fa-clock-o orange"></i>
                                    <span class="description">office</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
		</div>
	</div>
</div>