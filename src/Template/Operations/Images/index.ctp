<?php
$this->Breadcrumbs->add([
    ['title' => 'Operations'],
    ['title' => 'Imágenes']
]);	
?>
<!--Page Related styles-->
<?= $this->Html->css('/assets/css/dataTables.bootstrap.css'); ?>
<?= $this->Flash->render() ?>
<div class="row">
	<div class="col-sm-12">
		<div class="tabbable">
            <ul class="nav nav-tabs" id="myTab">
                <li class="tab-green active">
                    <a data-toggle="tab" href="#latam">
                    	Búsqueda de imágenes
                    </a>
                </li>
            </ul>
            <div class="tab-content">
                <div id="latam" class="tab-pane active">
	                <div class="row">
		                <div class="col-sm-12">
		                    <h4><i class="fa fa-search"></i> Buscador</h4>
			                <hr>
		                </div>
	                </div>
                    <div class="row">
	                    <div class="col-sm-12">
		                    <form class="form-horizontal" method="post" action="<?= $this->Url->build(['action' => 'index', 'prefix' => 'operations']) ?>">
								<div class="form-group">
									<label for="country" class="col-sm-1 control-label no-padding-right">País</label>
		                            <div class="col-sm-3">
		                                <?= $this->Form->select('country', $countries, ['empty' => '(Seleccione País)', 'class' => 'form-control']); ?>
		                            </div>
								</div>
								<div class="form-group">
		                            <label for="idartigo" class="col-sm-1 control-label no-padding-right">idArtigo</label>
		                            <div class="col-sm-3">
			                            <?= $this->Form->input('idartigo', ['label' => false, 'class' => 'form-control']); ?>
		                            </div>
								</div>
								<!--
								<div class="form-group">
		                            <label for="codbar" class="col-sm-1 control-label no-padding-right">Codbar</label>
		                            <div class="col-sm-3">
		                                <?= $this->Form->input('codbar', ['label' => false, 'class' => 'form-control']); ?>
		                            </div>
		                        </div>
		                        -->
		                        <div class="form-group">
			                        <div class="col-sm-3 col-sm-offset-1">
		                           	 <?= $this->Form->submit('Buscar', ['class' => 'btn btn-primary']) ?>
			                        </div>
		                        </div>
		               		</form>
	                    </div>
                    </div>
                    <div class="row">
		                <div class="col-sm-12">
		                    <h4><i class="fa fa-table"></i> Resultados</h4>
			                <hr>
		                </div>
	                </div>
                    <div class="row">
	                    <div class="col-sm-12">
		                    <table class="table table-bordered table-condensed">
			                    <tr>
				                    <th>idArtigo</th>
				                    <th>Produto</th>
				                    <th>Sub</th>
				                    <th>Fabricante</th>
				                    <th>Marca</th>
				                    <th>Clas01</th>
				                    <th>Clas02</th>
				                    <th>Conteudo</th>
			                    </tr>
			                    <? foreach($rows as $row) { ?>
			                    <tr>
				                    <td><?= $row['IdArtigo'] ?></td>
				                    <td><?= $row['Produto'] ?></td>
				                    <td><?= $row['Sub'] ?></td>
				                    <td><?= $row['Fabricante'] ?></td>
				                    <td><?= $row['Marca'] ?></td>
				                    <td><?= $row['Clas01'] ?></td>
				                    <td><?= $row['Clas02'] ?></td>
				                    <td><?= $row['Conteudo'] ?></td>
			                    </tr>
			                    <? } ?>
		                    </table>
	                    </div>
                    </div>
                </div>
            </div>
		</div>
	</div>
</div>