<?php foreach($messages as $message) { ?>
	<?php if($this->request->session()->read('Auth.User.id') == $message['userId']) { ?>
    <div class="comment">
        <div class="comment-body" style="display: flex; justify-content: flex-end;">
            <div class="comment-text" style="text-align: right">
                <div class="comment-header" style="text-align: right">
                    <a href="#" title=""><span class="label label-primary"><?= $message['type'] ?></span> <?= $message['owner'] ?></a><span><?= $message['date']->format('d-m-Y h:i A') ?></span>
                </div>
                <?= $message['message'] ?>
            </div>
        </div>
    </div>
    <?php } else { ?>
    <div class="comment">
	    <?= $this->Html->image('profiles/'.(($message['userId']) ? $message['userId'] : 'no-image').'.jpg', ['class' => 'comment-avatar']); ?>
        <div class="comment-body" style="display: flex;">
            <div class="comment-text">
                <div class="comment-header">
                    <a href="#" title=""><span class="label label-primary"><?= $message['type'] ?></span> <?= $message['owner'] ?></a><span><?= $message['date']->format('d-m-Y h:i A') ?></span>
                </div>
                <?= $message['message'] ?>
            </div>
        </div>
    </div>
    <?php } ?>
<?php } ?>
<script lang="javascript" type="text/javascript">
$(document).ready(function() {

	$("#message_content").animate({ scrollTop: $('#message_content').prop("scrollHeight")}, 1000);

});
</script>