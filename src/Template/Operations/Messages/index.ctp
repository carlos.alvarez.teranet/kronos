<div id="message_content" class="tab-content tabs-flat" style="bottom: 150px; top: 0px; position: absolute; overflow: auto; width: 100%; padding: 5px 5px 5px 5px">
	<?php foreach($messages as $message) { ?>
		<?php if($this->request->session()->read('Auth.User.id') == $message['userId']) { ?>
	    <div class="comment">
	        <div class="comment-body" style="display: flex; justify-content: flex-end;">
	            <div class="comment-text" style="text-align: right">
	                <div class="comment-header" style="text-align: right">
	                    <a href="#" title=""><?= $message['owner'] ?></a><span><?= $message['date']->format('d-m-Y h:i A') ?></span>
	                </div>
	                <?= $message['message'] ?>
	            </div>
	        </div>
	    </div>
	    <?php } else { ?>
	    <div class="comment">
		    <?= $this->Html->image('profiles/'.(($message['userId']) ? $message['userId'] : 'no-image').'.jpg', ['class' => 'comment-avatar']); ?>
	        <div class="comment-body" style="display: flex;">
	            <div class="comment-text">
	                <div class="comment-header">
	                    <a href="#" title=""><?= $message['owner'] ?></a><span><?= $message['date']->format('d-m-Y h:i A') ?></span>
	                </div>
	                <?= $message['message'] ?>
	            </div>
	        </div>
	    </div>
	    <?php } ?>
    <?php } ?>
</div>

<!-- Page Footer -->
<div class="tab-content tabs-flat" style="position:fixed; left: 0; bottom:0; width:100%; height:150px; overflow: hidden; padding: 0px 5px 0px 5px">
    <div id="overview" class="tab-pane active">
        <div class="row profile-overview">
	        <form method="post" class="well" id="newMessageForm" action="<?= $this->Url->build(['controller' => 'messages', 'action' => 'create']); ?>" style="margin-bottom: 0px" onsubmit="return false;">
			    <span class="input-icon icon-right">
			        <textarea rows="2" id="summernote" name="message" class="form-control" placeholder="Enviar nuevo mensaje"></textarea>
			        <input type="hidden" name="type" value="pw"/>
			        <input type="hidden" name="reference" value="<?= $reference ?>"/>
			        <input type="hidden" name="period" value="<?= $period ?>"/>
			    </span>
			    <div class="row">
				    <div class="col-sm-8">
					    <div class="alert alert-info">Bitácora para <strong><?= $reference ?></strong> en periodo <strong><?= $period ?></strong></div>
				    </div>
				    <div class="col-sm-4">
					    <div class="padding-top-10 text-align-right">
					        <button type="submit" id="DAbtn" class="btn btn-sm btn-primary">
					            Enviar
					        </button>
					    </div>
				    </div>
			    </div>
			</form>
        </div>
    </div>
</div>
<script lang="javascript" type="text/javascript">

$(document).ready(function() {

	$("#message_content").animate({ scrollTop: $('#message_content').prop("scrollHeight")}, 1000);

});
$("#DAbtn").click(function (e) {
	$(this).prop('disabled', 'disabled');
	e.preventDefault();
	$.ajax({
		url: $('#newMessageForm').attr('action'),
		type: 'POST',
		dataType: 'json',
		data: $('#newMessageForm').serialize(),
		success: function (result, status, xhr) {
		    console.log('ok');
		},
		complete: function () {
			$('#message_content').load('<?= $this->Url->build(['controller' => 'messages', 'action' => 'content', $type, $reference, $period]) ?>');
			$('textarea[name=message]').val('');
			$("#DAbtn").removeAttr('disabled');			
		}
	});
});
</script>