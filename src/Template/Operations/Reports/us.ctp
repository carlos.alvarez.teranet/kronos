<?php
$this->Breadcrumbs->add([
    ['title' => 'Operations'],
    ['title' => 'Reports']
]);	
?>
<!--Page Related styles-->
<?= $this->Html->css('/assets/css/dataTables.bootstrap.css'); ?>
<?= $this->Flash->render() ?>
<div class="row">
	<div class="col-sm-12">
		<div class="tabbable">
            <ul class="nav nav-tabs" id="myTab">
                <li class="tab-green active">
                    <a data-toggle="tab" href="#latam">
                        Resumen LatAm
                    </a>
                </li>
                <?php foreach($inds as $ind) { ?>
                <?php if($ind['Country'] == 'LATAM') { continue; } ?>
                <li class="tab-blue">
                    <a data-toggle="tab" href="#<?= $ind['Country'] ?>">
                        <?= $ind['Country'] ?>
                    </a>
                </li>
                <?php } ?>
                <li class="tab-red">
               		<a data-toggle="tab" href="#pendants">
                        Pendientes <i class="fa fa-warning"></i>
                    </a>
                </li>
                 <li class="tab-purple">
               		<a style="color: red; font-weight: 600" data-toggle="tab" href="#unrelease">
                        Des-liberadas <i class="fa fa-clock-o"></i>
                    </a>
                </li>
            </ul>
            <div class="tab-content">
                <div id="latam" class="tab-pane active">
                    <div class="row">
	                    <div class="col-sm-12">
		                    <h4><i class="fa fa-line-chart"></i> Seguimiento Producción LatAm</h4>
		                    <hr>
		                    <table id="summary_table_inds" class="table table-condensed table-bordered">
			                    <thead>
				                    <tr>
					                    <th>Country</th>
					                    <th>Type</th>
					                    <th>Planned</th>
					                    <th>PW_DB</th>
					                    <th>Delivered</th>
					                    <th>Pending</th>
					                    <th align="center">Early</th>
					                    <th align="center">OnTime</th>
					                    <th align="center">Late_1</th>
					                    <th align="center">Late_2</th>
					                    <th align="center">Late_More</th>
				                    </tr>
			                    </thead>
			                    <tbody>
				                    <? foreach($inds as $ind) { ?>
				                    <?php if($ind['Country'] == 'LATAM') { continue; } ?>
				                    <tr>
					                    <td><?= $ind['Country'] ?></td>
						                <td><?= $ind['Type'] ?></td>
						                <td><?= $ind['Planned'] ?></td>
						                <td align="center"><?= $ind['PW_DB'] ?></td>
						                <td align="center"><?= $ind['Delivered'] ?></td>
						                <td align="center">
							                <?= $ind['Pending'] ?>
							                <?php if( $ind['Pending'] == 0 ) { ?> <i class="fa fa-circle text-success pull-right"></i> <? } ?>
							                <?php if( $ind['Diff_Pending'] > 0 and $ind['Pending'] > 0 ) { ?> <i class="fa fa-circle text-success pull-right"></i> <? } ?>
							                <?php if( $ind['Diff_Pending'] == 0 and $ind['Pending'] > 0 ) { ?> <i class="fa fa-circle text-warning pull-right"></i> <? } ?>
							                <?php if( $ind['Diff_Pending'] < 0 and $ind['Pending'] > 0) { ?> <i class="fa fa-circle text-danger pull-right"></i> <? } ?>
							            </td>
						                <td align="center" style="background-color: #0275d8; font-weight: 600!important; color: white">
							                <?= ($ind['Early'] == 0) ? '-' : number_format(($ind['Early'] / $ind['PW_DB'] * 100), 1, ',', '.').'%' ?>
							            </td>
						                <td align="center" style="background-color: #5cb85c; font-weight: 600!important; color: white">
							                <?= ($ind['OnTime'] == 0) ? '-' : number_format(($ind['OnTime'] / $ind['PW_DB'] * 100), 1, ',', '.').'%' ?>
							            </td>
						                <td align="center" style="background-color: #f0ad4e; font-weight: 600!important; color: white">
							                <?= ($ind['Late_1'] == 0) ? '-' : number_format(($ind['Late_1'] / $ind['PW_DB'] * 100), 1, ',', '.').'%' ?>
							            </td>
						                <td align="center" style="background-color: #d9534f; font-weight: 600!important; color: white">
							                <?= ($ind['Late_2'] == 0) ? '-' : number_format(($ind['Late_2'] / $ind['PW_DB'] * 100), 1, ',', '.').'%' ?>
							            </td>
						                <td align="center" style="background-color: #292b2c; font-weight: 600!important; color: white">
							                <?= ($ind['Late_More'] == 0) ? '-' : number_format(($ind['Late_More'] / $ind['PW_DB'] * 100), 1, ',', '.').'%' ?>
							            </td>
				                    </tr>
				                    <? } ?>
			                    </tbody>
			                    <tfoot>
				                    <? foreach($inds as $ind) { ?>
				                    <?php if($ind['Country'] <> 'LATAM') { continue; } ?>
				                    <tr style="color: white; background-color: #808080">
					                    <td><?= $ind['Country'] ?></td>
						                <td><?= $ind['Type'] ?></td>
						                <td><?= $ind['Planned'] ?></td>
						                <td align="center"><?= $ind['PW_DB'] ?></td>
						                <td align="center"><?= $ind['Delivered'] ?></td>
						                <td align="center">
							                <?= $ind['Pending'] ?>
							                <?php if( $ind['Diff_Pending'] > 0 and $ind['Pending'] > 0 ) { ?> <i class="fa fa-circle text-success pull-right"></i> <? } ?>
							                <?php if( $ind['Diff_Pending'] == 0 and $ind['Pending'] > 0 ) { ?> <i class="fa fa-circle text-warning pull-right"></i> <? } ?>
							                <?php if( $ind['Diff_Pending'] < 0 and $ind['Pending'] > 0) { ?> <i class="fa fa-circle text-danger pull-right"></i> <? } ?>
							            </td>
						                <td align="center" style="background-color: #0275d8; font-weight: 600!important; color: white">
							                <?= ($ind['Early'] == 0) ? '-' : number_format(($ind['Early'] / $ind['PW_DB'] * 100), 1, ',', '.').'%' ?>
							            </td>
						                <td align="center" style="background-color: #5cb85c; font-weight: 600!important; color: white">
							                <?= ($ind['OnTime'] == 0) ? '-' : number_format(($ind['OnTime'] / $ind['PW_DB'] * 100), 1, ',', '.').'%' ?>
							            </td>
						                <td align="center" style="background-color: #f0ad4e; font-weight: 600!important; color: white">
							                <?= ($ind['Late_1'] == 0) ? '-' : number_format(($ind['Late_1'] / $ind['PW_DB'] * 100), 1, ',', '.').'%' ?>
							            </td>
						                <td align="center" style="background-color: #d9534f; font-weight: 600!important; color: white">
							                <?= ($ind['Late_2'] == 0) ? '-' : number_format(($ind['Late_2'] / $ind['PW_DB'] * 100), 1, ',', '.').'%' ?>
							            </td>
						                <td align="center" style="background-color: #292b2c; font-weight: 600!important; color: white">
							                <?= ($ind['Late_More'] == 0) ? '-' : number_format(($ind['Late_More'] / $ind['PW_DB'] * 100), 1, ',', '.').'%' ?>
							            </td>
				                    </tr>
				                    <? } ?>
			                    </tfoot>
		                    </table>
	                    </div>
                    </div>
                </div>
                <?php foreach($inds as $ind) { ?>
                <?php if($ind['Country'] == 'LATAM') { continue; } ?>
                <div id="<?= $ind['Country'] ?>" class="tab-pane">
                    <div class="row">
	                    <div class="col-sm-12">
		                    <h4><i class="fa fa-table"></i> Detalle <?= $ind['Country'] ?></h4>
		                    <p>Periodo: <?= $details[0]['Period'] ?> | Grupo: <?= $details[0]['Grupo'] ?></p>
		                    <hr>
		                    <table class="table table-condensed table-bordered dataTable">
			                    <thead>
				                    <tr>
					                    <th>Nombre</th>
					                    <th>DA1 generado</th>
					                    <th>Actos liberados</th>
					                    <th>Acto armado</th>
					                    <th>Base generada</th>
					                    <th>Base cargada</th>
					                    <th>Base liberada</th>
					                    <th>Bitácora</th>
				                    </tr>
			                    </thead>
			                    <tbody>
				                    <?php foreach($details as $detail) { ?>
				                    <?php if($detail['Country'] == $ind['Country']) { ?>
				                    <tr valign="middle">
					                    <td><?= $detail['Nombre'] ?></td>
										<td><?= $detail['DA1_Exists'] ?></td>
										<td>
											<div class="progress" style="margin-bottom: 5px !important">
	                                            <div class="progress-bar progress-bar-<?= ($detail['Extraction'] == 100) ? 'success' : 'warning'  ?>" role="progressbar" aria-valuenow="<?= number_format($detail['Extraction'], 0, ',', '.') ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?= number_format($detail['Extraction'], 0, ',', '.') ?>%">
	                                                <span>
	                                                    <?= number_format($detail['Extraction'], 0, ',', '.') ?> %
	                                                </span>
	                                            </div>
	                                        </div>
	                                    </td>
										<td align="center">
											<?php if($detail['DA2_Generation'] == 1) { ?>
											<label class="label label-success">Acto creado</label>
											<?php } else { ?>
											<label class="label label-danger">Acto pendiente</label>
											<?php } ?>
										</td>
										<td align="center">
											<?php if($detail['LocalUser'] == 1) { ?>
											<label class="label label-success">Base creada</label>
											<?php } else { ?>
											<label class="label label-danger">Base pendiente</label>
											<?php } ?>
										</td>
										<td><?= (is_null($detail['uploaded_created'])) ? '-' : date('Y-m-d', strtotime($detail['uploaded_created'])) ?></td>
										<td>
											<?php if (is_null($detail['liberation_created']) and $detail['Release_DeadLine'] > 0 ) { ?>
											<label class="label label-danger">Retrasada</label>
											<?php } else if (is_null($detail['liberation_created']) and $detail['Release_DeadLine'] <= 0 ) { ?>
											-
											<?php } else if ($detail['UnRelease'] == 1) { ?>
											<label class="label label-purple">- Des-liberada -</label>
											<?php } else { ?>
											<?= date('Y-m-d', strtotime($detail['liberation_created'])) ?>
											<?php } ?>
										</td>
										<td>
											<a class="btn btn-xs btn-purple" data-pw="<?= $detail['Nombre'] ?>" data-period="<?= $detail['Period'] ?>" href="javascript:void(0);">Mensajes <i class="fa fa-envelope right"></i> 0</a>
										</td>
				                    </tr>
				                    <?php } ?>
				                    <?php } ?>
			                    </tbody>
		                    </table>
	                    </div>
                    </div>
                </div>
                <? } ?>
                <div id="pendants" class="tab-pane">
                    <div class="row">
	                    <div class="col-sm-12">
		                    <h4><i class="fa fa-table"></i> Detalle Pendientes</h4>
		                    <p>Periodo: <?= $details[0]['Period'] ?> | Grupo: PNC</p>
		                    <hr>
		                    <table id="pendants_table_inds" class="table table-condensed table-bordered">
			                    <thead>
				                    <tr>
					                    <th>Country</th>
					                    <th>Nombre</th>
					                    <th>DA1 generado</th>
					                    <th>Actos liberados</th>
					                    <th>Acto armado</th>
					                    <th>Base generada</th>
					                    <th>Base cargada</th>
					                    <th>Base liberada</th>
					                    <th>Bitácora</th>
				                    </tr>
			                    </thead>
			                    <tbody>
				                    <?php foreach($pendants as $pendant) { ?>
				                    <tr valign="middle">
					                    <td><?= $pendant['Country'] ?></td>
					                    <td><?= $pendant['Nombre'] ?></td>
										<td><?= $pendant['DA1_Exists'] ?></td>
										<td>
											<div class="progress" style="margin-bottom: 5px !important">
	                                            <div class="progress-bar progress-bar-<?= ($pendant['Extraction'] == 100) ? 'success' : 'warning'  ?>" role="progressbar" aria-valuenow="<?= number_format($pendant['Extraction'], 0, ',', '.') ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?= number_format($pendant['Extraction'], 0, ',', '.') ?>%">
	                                                <span>
	                                                    <?= number_format($pendant['Extraction'], 0, ',', '.') ?> %
	                                                </span>
	                                            </div>
	                                        </div>
	                                    </td>
										<td align="center">
											<?php if($pendant['DA2_Generation'] == 1) { ?>
											<label class="label label-success">Acto creado</label>
											<?php } else { ?>
											<label class="label label-danger">Acto pendiente</label>
											<?php } ?>
										</td>
										<td align="center">
											<?php if($pendant['LocalUser'] == 1) { ?>
											<label class="label label-success">Base creada</label>
											<?php } else { ?>
											<label class="label label-danger">Base pendiente</label>
											<?php } ?>
										</td>
										<td><?= (is_null($pendant['uploaded_created'])) ? '-' : date('Y-m-d', strtotime($pendant['uploaded_created'])) ?></td>
										<td>
											<?php if (is_null($pendant['liberation_created']) and $pendant['Release_DeadLine'] > 0 ) { ?>
											<label class="label label-danger">Retrasada</label>
											<?php } else if (is_null($pendant['liberation_created']) and $pendant['Release_DeadLine'] <= 0 ) { ?>
											-
											<?php } else if ($pendant['UnRelease'] == 1) { ?>
											<label class="label label-purple">- Des-liberada -</label>
											<?php } else { ?>
											<?= date('Y-m-d', strtotime($pendant['liberation_created'])) ?>
											<?php } ?>
										</td>
										<td>
											<a class="btn btn-xs btn-purple" href="javascript:void(0);">Mensajes <i class="fa fa-envelope right"></i> 0</a>
										</td>
				                    </tr>
				                    <?php } ?>
			                    </tbody>
		                    </table>
	                    </div>
                    </div>
                </div>
                <div id="unrelease" class="tab-pane">
                    <div class="row">
	                    <div class="col-sm-12">
		                    <h4><i class="fa fa-table"></i> Detalle Pendientes</h4>
		                    <p>Periodo: <?= $details[0]['Period'] ?> | Grupo: PNC</p>
		                    <hr>
		                    <table id="pendants_table_inds" class="table table-condensed table-bordered">
			                    <thead>
				                    <tr>
					                    <th>Country</th>
					                    <th>Nombre</th>
					                    <th>DA1 generado</th>
					                    <th>Actos liberados</th>
					                    <th>Acto armado</th>
					                    <th>Base generada</th>
					                    <th>Base cargada</th>
					                    <th>Base liberada</th>
					                    <th>Bitácora</th>
				                    </tr>
			                    </thead>
			                    <tbody>
				                    <?php foreach($details as $detail) { ?>
				                    <?php if ($detail['UnRelease'] == 0) { continue; }  ?>
				                    <tr valign="middle">
					                    <td><?= $detail['Country'] ?></td>
					                    <td><?= $detail['Nombre'] ?></td>
										<td><?= $detail['DA1_Exists'] ?></td>
										<td>
											<div class="progress" style="margin-bottom: 5px !important">
	                                            <div class="progress-bar progress-bar-<?= ($detail['Extraction'] == 100) ? 'success' : 'warning'  ?>" role="progressbar" aria-valuenow="<?= number_format($detail['Extraction'], 0, ',', '.') ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?= number_format($detail['Extraction'], 0, ',', '.') ?>%">
	                                                <span>
	                                                    <?= number_format($detail['Extraction'], 0, ',', '.') ?> %
	                                                </span>
	                                            </div>
	                                        </div>
	                                    </td>
										<td align="center">
											<?php if($detail['DA2_Generation'] == 1) { ?>
											<label class="label label-success">Acto creado</label>
											<?php } else { ?>
											<label class="label label-danger">Acto pendiente</label>
											<?php } ?>
										</td>
										<td align="center">
											<?php if($detail['LocalUser'] == 1) { ?>
											<label class="label label-success">Base creada</label>
											<?php } else { ?>
											<label class="label label-danger">Base pendiente</label>
											<?php } ?>
										</td>
										<td><?= (is_null($detail['uploaded_created'])) ? '-' : date('Y-m-d', strtotime($detail['uploaded_created'])) ?></td>
										<td>
											<?php if (is_null($detail['liberation_created']) and $detail['Release_DeadLine'] > 0 ) { ?>
											<label class="label label-danger">Retrasada</label>
											<?php } else if (is_null($detail['liberation_created']) and $detail['Release_DeadLine'] <= 0 ) { ?>
											-
											<?php } else if ($detail['UnRelease'] == 1) { ?>
											<label class="label label-purple">- Des-liberada -</label>
											<?php } else { ?>
											<?= date('Y-m-d', strtotime($detail['liberation_created'])) ?>
											<?php } ?>
										</td>
										<td>
											<a class="btn btn-xs btn-purple" href="javascript:void(0);">Mensajes <i class="fa fa-envelope right"></i> 0</a>
										</td>
				                    </tr>
				                    <?php } ?>
			                    </tbody>
		                    </table>
	                    </div>
                    </div>
                </div>
            </div>
		</div>
	</div>
</div>
<?= $this->Html->script('/assets/js/datatable/jquery.dataTables.min.js'); ?>
<?= $this->Html->script('/assets/js/datatable/ZeroClipboard.js'); ?>
<?= $this->Html->script('/assets/js/datatable/dataTables.tableTools.min.js'); ?>
<?= $this->Html->script('/assets/js/datatable/dataTables.bootstrap.min.js'); ?>
<script lang="kavascript" type="text/javascript">
    var oTable = $('.dataTable').dataTable({
        "sDom": "Tflt<'row DTTTFooter'<'col-sm-6'i><'col-sm-6'p>>",
        "aaSorting": [[3, 'asc']],
        "aLengthMenu": [
            [5, 15, 20, 50, 100, -1],
            [5, 15, 20, 50, 100, "All"]
        ],
        "iDisplayLength": 20,
        "language": {
            "search": "",
            "sLengthMenu": "_MENU_",
            "oPaginate": {
                "sPrevious": "Anterior",
                "sNext": "Siguiente"
            }
        },
        "aoColumnDefs": [
	    	{ "sWidth": "30px", "aTargets": [7] }
	    ]
    });
    
    var iTable = $('#summary_table_inds').dataTable({
	    "sDom": "lrt",
	    "bFilter": false, 
	    "bInfo": false,
	    "bPaginate": false,
	    "oLanguage": {"sLengthMenu": "\_MENU_"}
	});
	
	var pTable = $('#pendants_table_inds').dataTable({
        "sDom": "Tflt<'row DTTTFooter'<'col-sm-6'i><'col-sm-6'p>>",
        "aaSorting": [[3, 'asc']],
        "aLengthMenu": [
            [5, 15, 20, 50, 100, -1],
            [5, 15, 20, 50, 100, "All"]
        ],
        "iDisplayLength": 50,
        "language": {
            "search": "",
            "sLengthMenu": "_MENU_",
            "oPaginate": {
                "sPrevious": "Anterior",
                "sNext": "Siguiente"
            }
        }
    });
    
</script>