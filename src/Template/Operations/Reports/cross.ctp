<?php
$this->Breadcrumbs->add([
    ['title' => 'Operations'],
    ['title' => 'Reports']
]);	
?>
<!--Page Related styles-->
<?= $this->Html->css('/assets/css/dataTables.bootstrap.css'); ?>
<?= $this->Flash->render() ?>
<div class="row">
	<div class="col-sm-12">
		<div class="tabbable">
            <ul class="nav nav-tabs" id="myTab">
                <li class="tab-green active">
                    <a data-toggle="tab" href="#latam">
                        Resumen LatAm
                    </a>
                </li>
                <?php foreach($multies as $multy) { ?>
                <?php if($multy['Country'] == 'LATAM') { continue; } ?>
                <li class="tab-blue">
                    <a data-toggle="tab" href="#<?= $multy['Country'] ?>">
                        <?= $multy['Country'] ?>
                    </a>
                </li>
                <?php } ?>
                <li class="tab-red">
               		<a data-toggle="tab" href="#pendants">
                        Pendientes <i class="fa fa-warning"></i>
                    </a>
                </li>
            </ul>
            <div class="tab-content">
                <div id="latam" class="tab-pane active">
                    <div class="row">
	                    <div class="col-sm-12">
		                    <h4><i class="fa fa-line-chart"></i> Seguimiento Producción LatAm</h4>
		                    <hr>
		                    <table id="summary_table_inds" class="table table-condensed table-bordered">
			                    <thead>
				                    <tr>
					                    <th>Country</th>
					                    <th>Type</th>
					                    <th>Planned</th>
					                    <th>PW_DB</th>
					                    <th>Delivered</th>
					                    <th>Pending</th>
					                    <th align="center">Early</th>
					                    <th align="center">OnTime</th>
					                    <th align="center">Late_1</th>
					                    <th align="center">Late_2</th>
					                    <th align="center">Late_More</th>
				                    </tr>
			                    </thead>
			                    <tbody>
				                    <? foreach($multies as $multy) { ?>
				                    <tr>
					                    <td><?= $multy['Country'] ?></td>
						                <td><?= $multy['Type'] ?></td>
						                <td><?= $multy['Planned'] ?></td>
						                <td align="center"><?= $multy['PW_DB'] ?></td>
						                <td align="center"><?= $multy['Delivered'] ?></td>
						                <td align="center">
							                <?= $multy['Pending'] ?>
							                <?php if( $multy['Diff_Pending'] > 0 and $multy['Pending'] > 0 ) { ?> <i class="fa fa-circle text-success pull-right"></i> <? } ?>
							                <?php if( $multy['Diff_Pending'] == 0 and $multy['Pending'] > 0 ) { ?> <i class="fa fa-circle text-warning pull-right"></i> <? } ?>
							                <?php if( $multy['Diff_Pending'] < 0 and $multy['Pending'] > 0) { ?> <i class="fa fa-circle text-danger pull-right"></i> <? } ?>
							            </td>
						                <td align="center" style="background-color: #0275d8; font-weight: 600!important; color: white">
							                <?= ($multy['Early'] == 0) ? '-' : number_format(($multy['Early'] / $multy['PW_DB'] * 100), 1, ',', '.').'%' ?>
							            </td>
						                <td align="center" style="background-color: #5cb85c; font-weight: 600!important; color: white">
							                <?= ($multy['OnTime'] == 0) ? '-' : number_format(($multy['OnTime'] / $multy['PW_DB'] * 100), 1, ',', '.').'%' ?>
							            </td>
						                <td align="center" style="background-color: #f0ad4e; font-weight: 600!important; color: white">
							                <?= ($multy['Late_1'] == 0) ? '-' : number_format(($multy['Late_1'] / $multy['PW_DB'] * 100), 1, ',', '.').'%' ?>
							            </td>
						                <td align="center" style="background-color: #d9534f; font-weight: 600!important; color: white">
							                <?= ($multy['Late_2'] == 0) ? '-' : number_format(($multy['Late_2'] / $multy['PW_DB'] * 100), 1, ',', '.').'%' ?>
							            </td>
						                <td align="center" style="background-color: #292b2c; font-weight: 600!important; color: white">
							                <?= ($multy['Late_More'] == 0) ? '-' : number_format(($multy['Late_More'] / $multy['PW_DB'] * 100), 1, ',', '.').'%' ?>
							            </td>
				                    </tr>
				                    <? } ?>
			                    </tbody>
		                    </table>
	                    </div>
                    </div>
                </div>
                <?php foreach($multies as $multy) { ?>
                <?php if($multy['Country'] == 'LATAM') { continue; } ?>
                <div id="<?= $multy['Country'] ?>" class="tab-pane">
                    <div class="row">
	                    <div class="col-sm-12">
		                    <h4><i class="fa fa-table"></i> Detalle <?= $multy['Country'] ?></h4>
		                    <p>Periodo: <?= $details[0]['Period'] ?> | Grupo: <?= $details[0]['Grupo'] ?></p>
		                    <hr>
		                    <table class="table table-condensed table-bordered dataTable">
			                    <thead>
				                    <tr>
					                    <th>Nombre</th>
					                    <th># Dependientes</th>
					                    <th>DA1 generado</th>
					                    <th>Bases liberadas</th>
					                    <th>Acto armado</th>
					                    <th>Base generada</th>
					                    <th>Base cargada</th>
					                    <th>Base liberada</th>
					                    <th>Bitácora</th>
				                    </tr>
			                    </thead>
			                    <tbody>
				                    <?php foreach($details as $detail) { ?>
				                    <?php if($detail['Country'] == $multy['Country']) { ?>
				                    <tr valign="middle">
					                    <td><?= $this->Html->link($detail['Nombre'], ['action' => 'dependents', $details[0]['Period'], $detail['Nombre'], '_ext' => 'json'], ['class' => 'dmodal', 'data-info' => $detail['Nombre']]) ?></td>
					                    <td><?= $detail['Dependientes'] ?></td>
										<td><?= $detail['DA1_Exists'] ?></td>
										<td>
											<div class="progress" style="margin-bottom: 5px !important">
	                                            <div class="progress-bar progress-bar-<?= (($detail['Liberadas'] / $detail['Dependientes'] * 100) == 100) ? 'success' : 'warning'  ?>" role="progressbar" aria-valuenow="<?= number_format($detail['Extraction'], 0, ',', '.') ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?= number_format($detail['Liberadas'] / $detail['Dependientes'] * 100, 0, ',', '.') ?>%">
	                                                <span>
	                                                    <?= number_format($detail['Liberadas'] / $detail['Dependientes'] * 100, 0, ',', '.') ?> %
	                                                </span>
	                                            </div>
	                                        </div>
	                                    </td>
										<td align="center">
											<?php if($detail['DA2_Generation'] == 1) { ?>
											<label class="label label-success">Acto creado</label>
											<?php } else { ?>
											<label class="label label-danger">Acto pendiente</label>
											<?php } ?>
										</td>
										<td align="center">
											<?php if($detail['LocalUser'] == 1) { ?>
											<label class="label label-success">Base creada</label>
											<?php } else { ?>
											<label class="label label-danger">Base pendiente</label>
											<?php } ?>
										</td>
										<td><?= (is_null($detail['uploaded_created'])) ? '-' : date('Y-m-d', strtotime($detail['uploaded_created'])) ?></td>
										<td>
											<?php if (is_null($detail['liberation_created']) and $detail['Release_DeadLine'] > 0 ) { ?>
											<label class="label label-danger">Retrasada</label>
											<?php } else if (is_null($detail['liberation_created']) and $detail['Release_DeadLine'] <= 0 ) { ?>
											-
											<?php } else { ?>
											<?= date('Y-m-d', strtotime($detail['liberation_created'])) ?>
											<?php } ?>
										</td>
										<td>
											<a class="btn btn-xs btn-purple" href="javascript:void(0);">Mensajes <i class="fa fa-envelope right"></i> 0</a>
										</td>
				                    </tr>
				                    <?php } ?>
				                    <?php } ?>
			                    </tbody>
		                    </table>
	                    </div>
                    </div>
                </div>
                <? } ?>
                <div id="pendants" class="tab-pane">
                    <div class="row">
	                    <div class="col-sm-12">
		                    <h4><i class="fa fa-table"></i> Detalle Pendientes</h4>
		                    <p>Periodo: <?= $details[0]['Period'] ?> | Grupo: PNC</p>
		                    <hr>
		                    <table id="pendants_table_inds" class="table table-condensed table-bordered">
			                    <thead>
				                    <tr>
					                    <th>Country</th>
					                    <th>Nombre</th>
					                    <th>DA1 generado</th>
					                    <th>Actos liberados</th>
					                    <th>Acto armado</th>
					                    <th>Base generada</th>
					                    <th>Base cargada</th>
					                    <th>Base liberada</th>
					                    <th>Bitácora</th>
				                    </tr>
			                    </thead>
			                    <tbody>
				                    <?php foreach($pendants as $pendant) { ?>
				                    <tr valign="middle">
					                    <td><?= $pendant['Country'] ?></td>
					                    <td><?= $pendant['Nombre'] ?></td>
										<td><?= $pendant['DA1_Exists'] ?></td>
										<td>
											<div class="progress" style="margin-bottom: 5px !important">
	                                            <div class="progress-bar progress-bar-<?= ($pendant['Extraction'] == 100) ? 'success' : 'warning'  ?>" role="progressbar" aria-valuenow="<?= number_format($pendant['Extraction'], 0, ',', '.') ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?= number_format($pendant['Extraction'], 0, ',', '.') ?>%">
	                                                <span>
	                                                    <?= number_format($pendant['Extraction'], 0, ',', '.') ?> %
	                                                </span>
	                                            </div>
	                                        </div>
	                                    </td>
										<td align="center">
											<?php if($pendant['DA2_Generation'] == 1) { ?>
											<label class="label label-success">Acto creado</label>
											<?php } else { ?>
											<label class="label label-danger">Acto pendiente</label>
											<?php } ?>
										</td>
										<td align="center">
											<?php if($pendant['LocalUser'] == 1) { ?>
											<label class="label label-success">Base creada</label>
											<?php } else { ?>
											<label class="label label-danger">Base pendiente</label>
											<?php } ?>
										</td>
										<td><?= (is_null($pendant['uploaded_created'])) ? '-' : date('Y-m-d', strtotime($pendant['uploaded_created'])) ?></td>
										<td>
											<?php if (is_null($pendant['liberation_created']) and $pendant['Release_DeadLine'] > 0 ) { ?>
											<label class="label label-danger">Retrasada</label>
											<?php } else if (is_null($pendant['liberation_created']) and $pendant['Release_DeadLine'] <= 0 ) { ?>
											-
											<?php } else { ?>
											<?= date('Y-m-d', strtotime($pendant['liberation_created'])) ?>
											<?php } ?>
										</td>
										<td>
											<a class="btn btn-xs btn-purple" href="javascript:void(0);">Mensajes <i class="fa fa-envelope right"></i> 0</a>
										</td>
				                    </tr>
				                    <?php } ?>
			                    </tbody>
		                    </table>
	                    </div>
                    </div>
                </div>
            </div>
		</div>
	</div>
</div>
<div class="modal fade in modal-danger" tabindex="-1" id="dependents" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myLargeModalLabel_2">Bases dependientes <strong id="dependents_label"></strong></h4>
            </div>
            <div class="modal-body">
	            <table class="table table-condensed table-bordered" id="dependents_table">
	                <thead>
	                    <tr>
		                    <th>Nombre</th>
		                    <th>Descripción</th>
		                    <th>Grupo</th>
		                    <th>Release</th>
		                    <th>Item</th>
	                    </tr>
	                </thead>
	                <tbody>
		                <tr>
			                <td colspan="5">Cargando...</td>
		                </tr>
	                </tbody>
	            </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<?= $this->Html->script('/assets/js/datatable/jquery.dataTables.min.js'); ?>
<?= $this->Html->script('/assets/js/datatable/ZeroClipboard.js'); ?>
<?= $this->Html->script('/assets/js/datatable/dataTables.tableTools.min.js'); ?>
<?= $this->Html->script('/assets/js/datatable/dataTables.bootstrap.min.js'); ?>
<script lang="javascript" type="text/javascript">
    var oTable = $('.dataTable').dataTable({
        "sDom": "Tflt<'row DTTTFooter'<'col-sm-6'i><'col-sm-6'p>>",
        "aaSorting": [[3, 'asc']],
        "aLengthMenu": [
            [5, 15, 20, 50, 100, -1],
            [5, 15, 20, 50, 100, "All"]
        ],
        "iDisplayLength": 20,
        "language": {
            "search": "",
            "sLengthMenu": "_MENU_",
            "oPaginate": {
                "sPrevious": "Anterior",
                "sNext": "Siguiente"
            }
        },
        "aoColumnDefs": [
	    	{ "sWidth": "30px", "aTargets": [7] }
	    ]
    });
    
    var iTable = $('#summary_table_inds').dataTable({
	    "sDom": "lrt",
	    "bFilter": false, 
	    "bInfo": false,
	    "bPaginate": false,
	    "oLanguage": {"sLengthMenu": "\_MENU_"}
	});
	
	var pTable = $('#pendants_table_inds').dataTable({
        "sDom": "Tflt<'row DTTTFooter'<'col-sm-6'i><'col-sm-6'p>>",
        "aaSorting": [[3, 'asc']],
        "aLengthMenu": [
            [5, 15, 20, 50, 100, -1],
            [5, 15, 20, 50, 100, "All"]
        ],
        "iDisplayLength": 50,
        "language": {
            "search": "",
            "sLengthMenu": "_MENU_",
            "oPaginate": {
                "sPrevious": "Anterior",
                "sNext": "Siguiente"
            }
        }
    });
    
    $('.dmodal').click(function(e) {
	    e.preventDefault();
	    var source = $(this).attr("href");
	    $('#dependents_label').html($(this).data('info'));
	    $('#dependents').modal();
	    
	    $('#dependents_table').dataTable({
		    "processing": true,
		    "destroy": true,
		    "ajax": {
			    "url": source, 
			    "dataSrc": function ( json ) {
	                return json.list;
	            } 
			},
			"columns": [
	            { "data": "Nombre" },
	            { "data": "Descripcion" },
	            { "data": "Grupo" },
	            { "data": "liberation_created" },
	            { "data": "Item_Filtro" }
	        
	        ],
	        "sDom": "lrt",
		    "bFilter": false, 
		    "bInfo": false,
		    "bPaginate": false,
		    "oLanguage": {"sLengthMenu": "\_MENU_"}
	    });
    });
    
</script>