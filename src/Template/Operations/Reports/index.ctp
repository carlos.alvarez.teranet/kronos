<?php
$this->Breadcrumbs->add([
    ['title' => 'Operations'],
    ['title' => 'Reports / Periods']
]);	
?>
<!--Page Related styles-->
<?= $this->Html->css('/assets/css/dataTables.bootstrap.css'); ?>
<?= $this->Flash->render() ?>
<div class="row">
	<div class="col-sm-12">
		<div class="tabbable">
            <ul class="nav nav-tabs" id="myTab">
                <li class="tab-green active">
                    <a data-toggle="tab" href="#latam">
                        Resumen Periodos
                    </a>
                </li>
            </ul>
            <div class="tab-content">
                <div id="latam" class="tab-pane active">
                    <div class="row">
	                    <div class="col-sm-6">
		                    <h4><i class="fa fa-calendar"></i> Resumen Periodos</h4>
		                    <hr>
		                    <table class="table table-bordered table-condensed">
			                    <thead>
				                    <tr>
					                    <th>Periodo</th>
					                    <th># Individuales</th>
					                    <th># Cross</th>
					                    <th># OOH</th>
					                    <th># U&S</th>
				                    </tr>
			                    </thead>
			                    <tbody>
				                    <?php foreach($periods as $period) { ?>
				                    <tr>
					                    <td><?= $period['Period'] ?></td>
					                    <td><a href="<?= $this->Url->build(['controller' => 'reports', 'action' => 'individuals', $period['Period']]) ?>" class="btn btn-info btn-xs"><?= $period['Individuales'] ?></a></td>
					                    <td><a href="<?= $this->Url->build(['controller' => 'reports', 'action' => 'cross', $period['Period']]) ?>" class="btn btn-info btn-xs"><?= $period['Cross'] ?></a></td>
										<td><a href="<?= $this->Url->build(['controller' => 'reports', 'action' => 'ooh', $period['Period']]) ?>" class="btn btn-info btn-xs"><?= $period['OOH'] ?></a></td>
										<td><a href="<?= $this->Url->build(['controller' => 'reports', 'action' => 'us', $period['Period']]) ?>" class="btn btn-info btn-xs"><?= $period['US'] ?></a></td>
				                    </tr>
				                    <?php } ?>
			                    </tbody>
		                    </table>
	                    </div>
                    </div>
                </div>
            </div>
		</div>
	</div>
</div>