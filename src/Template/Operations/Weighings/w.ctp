<h4>
	<?= $config['countries'][$c_sel]['name'] ?> | <?= $config['countries'][$c_sel]['panels'][$p_sel]['name'] ?>
</h4>
<table class="table table-bordered table-condensed tr-th-center">
	<thead>
		<tr>
			<th>Año</th>
			<th>Mes</th>
			<th>idPeso</th>
			<th>Sample</th>
			<th>Weight</th>
		</tr>
	</thead>
	<tbody>
		<? if(empty($specials)) { ?>
		
			<tr>
				<td colspan="5" align="center"> Sin datos disponibles</td>
			</tr>
		
		<? } else { ?>
		
			<? foreach($specials as $key => $value) { ?>
			<tr>
				<td><?= $year ?></td>
				<td><?= $month ?></td>
				<td><?= $value['idPeso'] ?></td>
				<td><?= number_format($value['Samples'], 0, ",", ".") ?></td>
				<td><?= number_format($value['Weights'], 0, ",", ".") ?></td>
			</tr>
			<? } ?>
		
		<? } ?>
	</tbody>
</table>