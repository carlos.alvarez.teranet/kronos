<?php
$this->Breadcrumbs->add([
    ['title' => 'Operations'],
    ['title' => 'Pesajes']
]);	
?>
<!--Page Related styles-->
<?= $this->Html->css('/assets/css/dataTables.bootstrap.css'); ?>
<?= $this->Flash->render() ?>
<div class="row">
    <div class="col-lg-7 col-sm-7 col-xs-7">
    	<div class="widget radius-bordered">
	    	<div class="widget-header">
		    	<span class="widget-caption">Seleccione país</span>
	    	</div>
	    	<div class="widget-body">
		    	<div class="row">
			    	<div class="col-sm-12">
			    		<? foreach($config['countries'] as $key => $country) { ?>
				    	<a href="<?= $this->Url->build(['action' => 'c', $key]) ?>" class="btn btn-default" style="min-width: 80px">
				    		<span style="font-size: larger; font-weight: 800; font-family: sans-serif"><?= $country['code'];?></span>
							<br>
				    		<?= $country['name'];?>
				    	</a>
			    		<? } ?>
			    	</div>
		    	</div>
	    	</div>
    	</div>
	</div>
</div>