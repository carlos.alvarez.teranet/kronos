<?php
$this->Breadcrumbs->add([
    ['title' => 'Operations'],
    ['title' => 'Pesajes']
]);	
?>
<!--Page Related styles-->
<?= $this->Html->css('/assets/css/dataTables.bootstrap.css'); ?>
<?= $this->Flash->render() ?>
<div class="row">
    <div class="col-lg-7 col-sm-7 col-xs-7">
    	<div class="widget radius-bordered">
	    	<div class="widget-header">
		    	<span class="widget-caption">Seleccione país</span>
	    	</div>
	    	<div class="widget-body">
		    	<div class="row">
			    	<div class="col-sm-12">
			    		<? foreach($config['countries'] as $key => $country) { ?>
				    	<a href="<?= $this->Url->build(['action' => 'c', $key]) ?>" class="btn <?= ($c_sel == $key) ? 'btn-primary': 'btn-default'; ?>" style="min-width: 80px">
				    		<span style="font-size: larger; font-weight: 800; font-family: sans-serif"><?= $country['code'];?></span>
							<br>
				    		<?= $country['name'];?>
				    	</a>
			    		<? } ?>
			    	</div>
		    	</div>
	    	</div>
    	</div>
	</div>
	<div class="col-lg-5 col-sm-5 col-xs-5">
		<div class="widget radius-bordered">
	    	<div class="widget-header">
		    	<span class="widget-caption">Seleccione panel</span>
	    	</div>
	    	<div class="widget-body">
		    	<div class="row">
			    	<div class="col-sm-12" style="display: flex">
				    	<? foreach($config['countries'][$c_sel]['panels'] as $key => $panel) { ?>
				    	<a href="<?= $this->Url->build(['action' => 'p', $c_sel, $key]) ?>" class="btn btn-default" style="min-width: 80px">
				    		<span style="font-size: larger; font-weight: 800; font-family: sans-serif"><?= $panel['name'];?></span>
				    		<br>
				    		idPainel: <?= $panel['code'];?>
				    	</a>
			    		<? } ?>
			    	</div>
		    	</div>
	    	</div>
		</div>
	</div>
</div>