<?php
$this->Breadcrumbs->add([
    ['title' => 'Operations'],
    ['title' => 'Pesajes']
]);	
?>
<style>
.tr-th-center tr th {
	text-align: center;
	font-weight: 800;
}
.inline form {
	display: initial;
}
</style>
<!--Page Related styles-->
<?= $this->Html->css('/assets/css/dataTables.bootstrap.css'); ?>
<?= $this->Flash->render() ?>

<div class="modal fade in modal-danger" tabindex="-1" id="especiales" role="dialog" aria-labelledby="myLargeModalLabel_2" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myLargeModalLabel_2">Pesos especiales</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
            
<div class="modal fade in modal-danger" tabindex="-1" id="help" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myLargeModalLabel">Menu de ayuda</h4>
            </div>
            <div class="modal-body">
            	<h5>Para poder cargar correctamente los pesos, debes:</h5>
            	<ul>
	            	<li><strong>Poseer el archivo de pesos</strong> enviado por el área de Universos (Operaciones). El archivo es de tipo EXCEL <i class="fa fa-file-excel-o text-success"></i></li>
	            	<li>El archivo debe tener <strong>solo una "hoja" (sheet)</strong>, ya que el sistema lee la primera y única hoja</li>
	            	<li><strong>Seleccionar el Año y Mes</strong> donde se van a cargar los pesos</li>
	            	<li>El Año y Mes seleccionado debe <strong>corresponder</strong> al periodo que contenga el archivo a cargar (en sus columnas de Año y Mes</li>
	            	<li>El archivo a cargar debe contener las columnas mínimas requeridas por el sistema <i class="fa fa-hand-o-down"></i></li>
            	</ul>
            	<h5>Columnas mínimas que el archivo debe contener son:</h5>
            	<table class="table-responsive table-bordered table-condensed">
	            	<thead>
		            	<tr>
		            	<? foreach($help['csv']['columns'] as $column) { ?>
		            	<th><?= $column['from'] ?></th>
		            	<? } ?> 
		            	<tr>
	            	</thead>
            	</table>
            	<br>
            	<h5>
	            	Al momento de procesar el archivo, los datos del archivo quedarán en la tabla <strong><?= $help['weights']['db'] ?>.<?= $help['weights']['table'] ?></strong>.<br>
	            	Las columnas del archivo serán asignadas a las columnas de la tabla bajo el siguiente esquema:
            	</h5>
            	<table class="table-responsive table-bordered table-condensed">
	            	<thead>
		            	<tr>
			            	<th>Columna Archivo</th>
			            	<th><i class="fa fa-arrow-right"></i></th>
			            	<th>Tabla SQL</th>
		            	</tr>
	            	</thead>
	            	<tbody>
		            	<? foreach($help['csv']['columns'] as $column) { ?>
		            	<tr>
			            	<td><?= $column['from'] ?></td>
			            	<td><i class="fa fa-arrow-right"></i></td>
							<td><?= $column['to'] ?></td>
		            	</tr>
		            	<? } ?>
	            	</tbody>
            	</table>
            	<br>
            	<h5><strong>Pasos para cargar el archivo:</strong></h5>
            	<ol>
	            	<li>Seleccionar Año y Mes del periodo a cargar</li>
	            	<li>Presionar el botón <button class="btn btn-xs btn-success"><i class="fa fa-hand-o-right"></i> Seleccionar archivo</button> y seleccionar el archivo desde la computadora</li>
	            	<li>Presionar el botón <button class="btn btn-xs btn-info"><i class="fa fa-upload"></i> Subir</button> y esperar que suba el archivo. Se actualizará la página cuando termine</li>
	            	<li>Luego que el archivo se haya cargado, se mostrará en la lista inferior, donde se debe presionar el botón <button class="btn btn-xs btn-primary">Procesar</button></li>
            	</ol>
            	<p>* Si el archivo posee las columnas mínimas para procesar, la carga de los datos habrá sido exitosa. Caso contrario, indicará un error y deberá volver a cargar un archivo con las columnas correctas.</p>
            	<p>** El resultado de la carga del archivo se puede observar en la pestaña "Datos del panel"</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-7 col-sm-7 col-xs-7">
    	<div class="widget radius-bordered">
	    	<div class="widget-header">
		    	<span class="widget-caption">Seleccione país</span>
	    	</div>
	    	<div class="widget-body">
		    	<div class="row">
			    	<div class="col-sm-12">
			    		<? foreach($config['countries'] as $key => $country) { ?>
				    	<a href="<?= $this->Url->build(['action' => 'c', $key]) ?>" class="btn <?= ($c_sel == $key) ? 'btn-primary': 'btn-default'; ?>" style="min-width: 80px">
				    		<span style="font-size: larger; font-weight: 800; font-family: sans-serif"><?= $country['code'];?></span>
							<br>
				    		<?= $country['name'];?>
				    	</a>
			    		<? } ?>
			    	</div>
		    	</div>
	    	</div>
    	</div>
	</div>
	<div class="col-lg-5 col-sm-5 col-xs-5">
		<div class="widget radius-bordered">
	    	<div class="widget-header">
		    	<span class="widget-caption">Seleccione panel</span>
	    	</div>
	    	<div class="widget-body">
		    	<div class="row">
			    	<div class="col-sm-12" style="display: flex">
				    	<? foreach($config['countries'][$c_sel]['panels'] as $key => $panel) { ?>
				    	<a href="<?= $this->Url->build(['action' => 'p', $c_sel, $key]) ?>" class="btn <?= ($p_sel == $key) ? 'btn-primary': 'btn-default'; ?>" style="min-width: 80px">
				    		<span style="font-size: larger; font-weight: 800; font-family: sans-serif"><?= $panel['name'];?></span>
				    		<br>
				    		idPainel: <?= $panel['code'];?>
				    	</a>
			    		<? } ?>
			    	</div>
		    	</div>
	    	</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-sm-12">
		<div class="tabbable">
            <ul class="nav nav-tabs" id="myTab">
                <li class="tab-green">
                    <a data-toggle="tab" href="#summary" aria-expanded="true">
                        Datos del panel
                    </a>
                </li>
                <li class="tab-red active">
                    <a data-toggle="tab" href="#csv" aria-expanded="false">
                        Carga de pesos
                    </a>
                </li>
            </ul>
            <div class="tab-content">
                <div id="summary" class="tab-pane">
                    <div class="row">
	                    <div class="col-sm-12">
		                    <h4><i class="fa fa-line-chart"></i> Pesos / Universos de los últimos 5 años</h4>
		                    <table class="table table-bordered table-condensed tr-th-center">
			                    <thead>
				                    <tr>
				                    	<th align="center" style="text-align: center; background-color: cornflowerblue; color: white" colspan="13">Pesos - <?= $config['countries'][$c_sel]['panels'][$p_sel]['weights']['db'] ?>.<?= $config['countries'][$c_sel]['panels'][$p_sel]['weights']['table'] ?></th>
				                    </tr>
				                    <tr>
					                    <th align="center">Año</th>
					                    <th align="center">Mes 1</th>
					                    <th align="center">Mes 2</th>
					                    <th align="center">Mes 3</th>
					                    <th align="center">Mes 4</th>
					                    <th align="center">Mes 5</th>
					                    <th align="center">Mes 6</th>
					                    <th align="center">Mes 7</th>
					                    <th align="center">Mes 8</th>
					                    <th align="center">Mes 9</th>
					                    <th align="center">Mes 10</th>
					                    <th align="center">Mes 11</th>
					                    <th align="center">Mes 12</th>
				                    </tr>
			                    </thead>
			                    <tbody>
				                    <? foreach($weights as $weight) { ?>
				                    <tr>
					                    <td align="center"><?= $weight['Ano'] ?></td>
					                    <td align="right"><?= $this->Html->link( number_format($weight['Mes1'], 0, ",", ".") , ['action' => 'w', $c_sel, $p_sel, $weight['Ano'], 1], ['class' => 'especiales'] ) ?></td>
					                    <td align="right"><?= $this->Html->link( number_format($weight['Mes2'], 0, ",", ".") , ['action' => 'w', $c_sel, $p_sel, $weight['Ano'], 2], ['class' => 'especiales'] ) ?></td>
					                    <td align="right"><?= $this->Html->link( number_format($weight['Mes3'], 0, ",", ".") , ['action' => 'w', $c_sel, $p_sel, $weight['Ano'], 3], ['class' => 'especiales'] ) ?></td>
					                    <td align="right"><?= $this->Html->link( number_format($weight['Mes4'], 0, ",", ".") , ['action' => 'w', $c_sel, $p_sel, $weight['Ano'], 4], ['class' => 'especiales'] ) ?></td>
					                    <td align="right"><?= $this->Html->link( number_format($weight['Mes5'], 0, ",", ".") , ['action' => 'w', $c_sel, $p_sel, $weight['Ano'], 5], ['class' => 'especiales'] ) ?></td>
					                    <td align="right"><?= $this->Html->link( number_format($weight['Mes6'], 0, ",", ".") , ['action' => 'w', $c_sel, $p_sel, $weight['Ano'], 6], ['class' => 'especiales'] ) ?></td>
					                    <td align="right"><?= $this->Html->link( number_format($weight['Mes7'], 0, ",", ".") , ['action' => 'w', $c_sel, $p_sel, $weight['Ano'], 7], ['class' => 'especiales'] ) ?></td>
					                    <td align="right"><?= $this->Html->link( number_format($weight['Mes8'], 0, ",", ".") , ['action' => 'w', $c_sel, $p_sel, $weight['Ano'], 8], ['class' => 'especiales'] ) ?></td>
					                    <td align="right"><?= $this->Html->link( number_format($weight['Mes9'], 0, ",", ".") , ['action' => 'w', $c_sel, $p_sel, $weight['Ano'], 9], ['class' => 'especiales'] ) ?></td>
					                    <td align="right"><?= $this->Html->link( number_format($weight['Mes10'], 0, ",", ".") , ['action' => 'w', $c_sel, $p_sel, $weight['Ano'], 10], ['class' => 'especiales'] ) ?></td>
					                    <td align="right"><?= $this->Html->link( number_format($weight['Mes11'], 0, ",", ".") , ['action' => 'w', $c_sel, $p_sel, $weight['Ano'], 11], ['class' => 'especiales'] ) ?></td>
					                    <td align="right"><?= $this->Html->link( number_format($weight['Mes12'], 0, ",", ".") , ['action' => 'w', $c_sel, $p_sel, $weight['Ano'], 12], ['class' => 'especiales'] ) ?></td>
				                    </tr>
				                    <? } ?> 
			                    </tbody>
		                    </table>
	                    </div>
                    </div>
                    <br>
                    <div class="row">
	                    <div class="col-sm-12">
		                    <h4><i class="fa fa-users"></i> Muestra elegible de los últimos 5 años</h4>
		                    <table class="table table-bordered table-condensed tr-th-center">
			                    <thead>
				                    <tr>
				                    	<th align="center" style="text-align: center; background-color: #eec264; color: black" colspan="13">Muestra - <?= $config['countries'][$c_sel]['panels'][$p_sel]['weights']['db'] ?>.<?= $config['countries'][$c_sel]['panels'][$p_sel]['weights']['table'] ?></th>
				                    </tr>
				                    <tr>
					                    <th align="center">Año</th>
					                    <th align="center">Mes 1</th>
					                    <th align="center">Mes 2</th>
					                    <th align="center">Mes 3</th>
					                    <th align="center">Mes 4</th>
					                    <th align="center">Mes 5</th>
					                    <th align="center">Mes 6</th>
					                    <th align="center">Mes 7</th>
					                    <th align="center">Mes 8</th>
					                    <th align="center">Mes 9</th>
					                    <th align="center">Mes 10</th>
					                    <th align="center">Mes 11</th>
					                    <th align="center">Mes 12</th>
				                    </tr>
			                    </thead>
			                    <tbody>
				                    <? foreach($samples as $sample) { ?>
				                    <tr>
					                    <td align="center"><?= $sample['Ano'] ?></td>
					                    <td align="right"><?= number_format($sample['Mes1'], 0, ",", ".") ?></td>
					                    <td align="right"><?= number_format($sample['Mes2'], 0, ",", ".") ?></td>
					                    <td align="right"><?= number_format($sample['Mes3'], 0, ",", ".") ?></td>
					                    <td align="right"><?= number_format($sample['Mes4'], 0, ",", ".") ?></td>
					                    <td align="right"><?= number_format($sample['Mes5'], 0, ",", ".") ?></td>
					                    <td align="right"><?= number_format($sample['Mes6'], 0, ",", ".") ?></td>
					                    <td align="right"><?= number_format($sample['Mes7'], 0, ",", ".") ?></td>
					                    <td align="right"><?= number_format($sample['Mes8'], 0, ",", ".") ?></td>
					                    <td align="right"><?= number_format($sample['Mes9'], 0, ",", ".") ?></td>
					                    <td align="right"><?= number_format($sample['Mes10'], 0, ",", ".") ?></td>
					                    <td align="right"><?= number_format($sample['Mes11'], 0, ",", ".") ?></td>
					                    <td align="right"><?= number_format($sample['Mes12'], 0, ",", ".") ?></td>
				                    </tr>
				                    <? } ?> 
			                    </tbody>
		                    </table>
	                    </div>
                    </div>
                </div>
                <div id="csv" class="tab-pane active">
                    <div class="row">
	                	<div class="col-sm-6">
	                		<h4><i class="fa fa-file-excel-o text-success"></i> Cargar archivo de pesos (<?= $config['countries'][$c_sel]['code']; ?> / <?= $config['countries'][$c_sel]['panels'][$p_sel]['name']; ?>)</h4>
	                		<form class="form" enctype="multipart/form-data" method="post" action="<?= $this->Url->build(['action' => 'upload', $c_sel, $p_sel, 'prefix' => 'operations']) ?>">
		                		<div class="row">
			                		<div class="col-sm-6">
				                		<label for="p_year">Seleccione Año *</label>
				                		<div class="form-group">
					                		<?= $this->Form->select('p_year', $years, ['empty' => '(Seleccione Año)', 'class' => 'form-control', 'default' => $selected_year]) ?>
				                		</div>
			                		</div>
			                		<div class="col-sm-6">
				                		<label for="p_month">Seleccione Periodo/Mes *</label>
				                		<div class="form-group">
					                		<?= $this->Form->select('p_month', $months, ['empty' => '(Seleccione Periodo/Mes)', 'class' => 'form-control', 'default' => $selected_month * 1]) ?>
				                		</div>
			                		</div>
		                		</div>
		                		<div class="input-group">
					                <label class="input-group-btn">
					                    <span class="btn btn-success">
					                        <i class="fa fa-hand-o-right"></i> Seleccionar archivo <input name="excel" type="file" style="display: none;" multiple="">
					                    </span>
					                </label>
					                <input id="filename" type="text" class="form-control" readonly="">
					                <label class="input-group-btn">
					                	<button id="upload" disabled="" type="submit" class="btn btn-info"><i class="fa fa-upload"></i> Subir</button>
					                	<button id="remove" disabled="" type="reset" class="btn btn-danger"><i class="fa fa-times"></i> Remover</button>
					                </label>
					            </div>
	                		</form>
	                	</div>
	                	<div class="col-sm-6">
		                	<i class="text-primary fa fa-2x fa-question-circle pull-right" style="cursor: pointer" data-toggle="modal" data-target="#help"></i>
	                	</div>
	                </div>
	                <hr style="margin-top: 20px; margin-bottom: 20px">
	                <div class="row">
		                <div class="col-sm-12">
			                <h4><i class="fa fa-table"></i> Últimas cargas de archivos</h4>
		                    <table class="table table-bordered table-condensed table-hover tr-th-center">
			                    <thead>
				                    <tr>
					                    <th>Fecha</th>
					                    <th>Periodo</th>
					                    <th>Cargado por</th>
					                    <th>Nombre</th>
					                    <th>Tamaño</th>
					                    <th>Status</th>
										<th>Acciones</th>
				                    </tr>
			                    </thead>
			                    <tbody>
				                    <? if(empty($logs['items'])) { ?>
				                    <tr>
					                    <td colspan="7" style="text-align: center">
						                    <h5>No existen archivos cargados previamente</h5>
					                    </td>
				                    </tr>
				                    <? } ?>
				                    <? foreach($logs['items'] as $key => $log) { ?>
				                    <? if( isset($log['visible']) and $log['visible'] === false ) { continue; } ?>
				                	<tr>
					                	<td><?= $log['date'] ?></td>
					                	<td><?= $log['year'] ?> - <?= str_pad($log['month'], 2, '0', STR_PAD_LEFT) ?></td>
					                	<td><?= $log['user'] ?></td>
					                	<td><?= $this->Html->link($log['name'], ['action' => 'download', $c_sel, $p_sel, $key]) ?></td>
					                	<td><?= $this->Format->formatBytes($log['size']) ?></td>
					                	<td style="text-align: center">
						                	<?= (!array_key_exists('last_execution', $log) && !array_key_exists('start_execution', $log)) ? '<span class="label label-danger">No procesado</span>' : ''; ?>
						                	<?= (!array_key_exists('last_execution', $log) && array_key_exists('start_execution', $log)) ? '<span class="label label-info"><i class="fa fa-spin fa-spinner"></i> En Proceso</span>' : ''; ?>
						                	<?= (array_key_exists('last_execution', $log)) ? '<span class="label label-success">Procesado</span>' : ''; ?>
						                	<?= (array_key_exists('error', $log)) ? '<span class="label label-danger">E</span>' : ''; ?>
					                	</td>
					                	<? if(!array_key_exists('error', $log) and !array_key_exists('last_execution', $log) and !array_key_exists('start_execution', $log)) { ?>
					                	<td class="inline" style="text-align: center">
						                	<?= $this->Form->postButton(
											    'Procesar',
											    ['action' => 'process', $c_sel, $p_sel],
											    [
											    	'confirm' => 'Confirma el proceso de \n'.$log['name'], 
											    	'class' => 'btn btn-xs btn-primary',
											    	'data' => [
												    	'key' => $key
											    	]
											    ]);
											?>
											<?= $this->Form->postButton(
											    '<i class="fa fa-trash"></i>',
											    ['action' => 'delete', $c_sel, $p_sel],
											    [
											    	'confirm' => 'Confirma eliminación de archivo \n'.$log['name'].'\n* Este proceso solo elimina el archivo', 
											    	'class' => 'btn btn-xs btn-danger',
											    	'data' => [
												    	'key' => $key
											    	]
											    ]);
											?>
					                	</td>
					                	<? } else if(!array_key_exists('error', $log) and !array_key_exists('last_execution', $log) and array_key_exists('start_execution', $log)) { ?>
					                	<td style="text-align: center">
						                	Avance: <span class="progress_bar" data-csel="<?= $c_sel ?>" data-psel="<?= $p_sel ?>" data-key="<?= $key ?>">0%</span>
					                	</td>
					                	<? } else if(!array_key_exists('error', $log) and array_key_exists('last_execution', $log)) { ?>
					                	<td style="text-align: center">
						                	<button class="btn btn-xs btn-success" disabled="">Ya procesado</button>
					                	</td>
					                	<? } else { ?>
					                	<td class="inline" style="text-align: center">
						                	<button class="btn btn-xs btn-warning" disabled="">No procesable</button>
						                	<?= $this->Form->postButton(
											    '<i class="fa fa-trash"></i>',
											    ['action' => 'delete', $c_sel, $p_sel],
											    [
											    	'confirm' => 'Confirma eliminación de archivo \n'.$log['name'].'\n* Este proceso solo elimina el archivo', 
											    	'class' => 'btn btn-xs btn-danger',
											    	'data' => [
												    	'key' => $key
											    	]
											    ]);
											?>
					                	</td>
					                	<? } ?>
				                	</tr>
				                	<? } ?>
			                    </tbody>
		                    </table>
		                </div>
	                </div>
                </div>
            </div>
        </div>
	</div>
</div>
<script lang="javascript" type="text/javascript">
$('document').ready(function() {    
    
	$("input:file").change(function () {
		console.log('Seleccionando archivo');
    	var fileName = $(this).val();
    	
    	if(fileName) {
	    	$('#upload').removeAttr('disabled');
	    	$('#remove').removeAttr('disabled');
    	} else {
	    	$('#upload').prop('disabled', true);
	    	$('#remove').prop('disabled', true);
    	}
    	
    	var partsOfStr = fileName.split('\\');
		$("#filename").val(partsOfStr[2]);
    });
    
    setInterval(function() {
	    $(".progress_bar").each(function() { 
		    
		    var csel = $(this).data('csel');
		    var psel = $(this).data('psel');
		    var key  = $(this).data('key');
		    var $el  = $(this);
		    
			$.get("<?= $this->Url->build(['controller' => 'weighings', 'action' => 'status']) ?>/" + csel + "/" + psel + "/" + key, function(data, status) {
				if(data.status !== null) {
					$el.html(data.status.progress+"%");
				}
				if(data.status.progress == 100) {
					location.reload();
				}
			}, 'json');
	    });
    }, 2000);
    
    $(".especiales").click(function(e) {
	    e.preventDefault();
	    $("#especiales").modal();
	    var link = $(this).attr("href");
	    $("#especiales .modal-body").load(link, function() {
		    
	    });	    
    });
    
});
</script>
