<?php
$this->Breadcrumbs->add([
    ['title' => 'Operations'],
    ['title' => 'Distribución']
]);	
?>
<!--Page Related styles-->
<?= $this->Html->css('/assets/css/dataTables.bootstrap.css'); ?>
<?= $this->Flash->render() ?>
<div class="row">
    <div class="col-lg-12 col-sm-12 col-xs-12">
        <div class="tabbable">
            <ul class="nav nav-tabs nav-justified" id="myTab5">
	            <? foreach($countries as $country) { ?>
                <li class="tab tab-default">
                    <a data-toggle="tab" href="#home5">
                        <?= $country['Descricao'] ?>
                    </a>
                </li>
                <? } ?>
            </ul>
            <div class="tab-content">
	            <div id="home5" class="tab-pane in active">
	                <p>Raw denim you probably haven't heard of them jean shorts Austin.</p>
	            </div>
	
	            <div id="profile5" class="tab-pane">
	                <p>Food truck fixie locavore, accusamus mcsweeney's marfa nulla single-origin coffee squid.</p>
	            </div>
	        </div>
	    </div>
	</div>
</div>