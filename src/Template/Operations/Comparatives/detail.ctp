<?php
$this->Breadcrumbs->add([
    ['title' => 'Operations'],
    ['title' => 'Comparativos'],
    ['title' => $config['countries'][$country]['name']],
    ['title' => $database]
]);	
?>
<!--Page Related styles-->
<?= $this->Html->css('/assets/css/dataTables.bootstrap.css'); ?>
<?= $this->Flash->render() ?>
<div class="row">
    <div class="col-xs-12 col-md-12">
        <div class="widget">
            <div class="widget-header ">
                <span class="widget-caption">Información de base</span>
                <div class="widget-buttons">
                    <a href="#" data-toggle="maximize">
                        <i class="fa fa-expand"></i>
                    </a>
                    <a href="#" data-toggle="collapse">
                        <i class="fa fa-minus"></i>
                    </a>
                    <a href="#" data-toggle="dispose">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
             <div class="widget-body">
	             <div class="alert alert-info" style="margin-bottom: 0">Leyendo archivos desde \\KWSTAFIL001\Procesos\ZIbanz\Atos\*<?= $database ?>*.*</div>
	             <div class="row">
		             <div class="col-xs-12 col-md-12">
			             <h2><?= $database ?> | <?= $info['Panel'] ?></h2>
			             <p><?= $info['Descripcion'] ?> | <?= $info['Grupo'] ?> | <?= $info['Corte'] ?></p>
		             </div>
	             </div>
	             <div class="row">
		             <div class="col-xs-4 col-md-4">
			            <form method="post" name="comparative" action="<?= $this->Url->build(['action' => 'process', $country, $database], ['method' => 'post']) ?>">
			            <table class="table table-responsive table-bordered table-condensed">
				            <thead>
					            <tr>
						            <th>Archivo</th>
						            <th>Modificado</th>
						            <th>Tamaño</th>
						            <th align="center">Comparar</th>
					            </tr>
				            </thead>
				            <tbody>
					            <?php foreach($files as $file) { ?>
					            <tr>
						            <td><?= $file->name() ?>.<?= $file->ext() ?></td>
						            <td><?= date("d-m-Y H:i:s", $file->lastChange()) ?></td>
						            <td><?= number_format($file->size() / 1024, 0, ',', '.') ?> kb</td>
						            <td align="center">
										<div class="checkbox" style="margin-bottom: 0px; margin-top: 0px">
											<label>
												<?= $this->Form->checkbox('compare['.$file->ext().']', ['class' => 'checkbox']); ?>
												<span class="text checkbox"></span>
											</label>
										</div>
						            </td>
					            </tr>
					            <?php } ?>
				            </tbody>
				            <tfoot>
					            <tr>
						            <td align="right" colspan="4"><button class="btn btn-mini btn-primary" type="submit">Comparar</button></td>
					            </tr>
				            </tfoot>
			             </table>
			            </form>
		             </div>
		             <div class="col-xs-8 col-md-8">
			             Para comprar archivos, debes seleccionar solo 2 periodos (actos) y luego presionar el botón "Comparar".<br>
			             La comparación será realizada, entre los 2 periodos seleccionados, desde el mas antiguo hacia el mas actual.<br>
			             Posteriormente, el resultado de la comparación se mostrará en la tabla de mas abajo, en un archivo CSV, el que puede ser descargado y leído mediante EXCEL.<br>
			             La tabla de resultados tiene el siguiente esquema:<br>
			             <table class="table table-condensed table-bordered">
				             <tr>
					             <th>idAto</th>
					             <th>Year</th>
					             <th>Month</th>
					             <th>Database</th>
					             <th>Tipo</th>
					             <th>Campo</th>
					             <th>Valor Anterior</th>
					             <th>Valor Nuevo</th>
				             </tr>
			             </table>
			             <div class="alert alert-warning">La columna <strong>"Tipo"</strong> indica [Periodo], "Acto nuevo" o "Acto eliminado". En caso de indicar un periodo (Ej. 0922), indica que el acto existe en ambos archivos y que presenta diferencias en alguna columna. Dicha columna será indicada en <strong>"Campo"</strong> con el respectivo <string>"Valor anterior"</string> y <string>"Valor nuevo"</string></div>
		             </div>
	             </div>
	             <hr>
	             <div class="row">
		             <div class="col-xs-12 col-md-12">
			             <table class="table table-bordered table-condensed">
				             <tr>
					             <th>Archivo</th>
					             <th>Fecha</th>
					             <th>Tamaño</th>
					             <th style="text-align: center">Modificados</th>
					             <th style="text-align: center">Nuevos</th>
					             <th style="text-align: center">Eliminados</th>
					             <th>Accion</th>
				             </tr>
				             <?php foreach($acts as $r => $file) { ?>
				             <tr>
					             <td><?= $this->Html->link($file->name().".".$file->ext(), 'files/'.$file->name().".".$file->ext(), ['download' => $file->pwd()]); ?></td>
					             <td><?= date("d-m-Y H:i:s", $file->lastAccess()) ?></td>
					             <td><?= number_format($file->size() / 1024, 0, ',', '.') ?> kb</td>
					             <td style="text-align: center"><?= $stats[$r]['u'] ?></td>
					             <td style="text-align: center"><?= $stats[$r]['n'] ?></td>
					             <td style="text-align: center"><?= $stats[$r]['d'] ?></td>
					             <td>Accion</td>
				             </tr>
				             <?php } ?>
				             <?php if(empty($acts)) { ?>
				             <tr>
					             <td colspan="7">No existen archivos de comparación disponbles</td>
				             </tr>
				             <?php } ?>
			             </table>
		             </div>
	             </div>
             </div>
        </div>
    </div>
</div>
<script language="javascript" type="text/javascript">
	$(document).ready(function() {
		$("div.checkbox input.checkbox").click(function(evt) {
			var count = 0;
			$("input.checkbox").each(function() {
				count += $(this).is(':checked');
			});
			if(count > 2) {
				evt.preventDefault();
			}
		});
	});
</script>