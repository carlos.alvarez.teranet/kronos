<?php
$this->Breadcrumbs->add([
    ['title' => 'Operations'],
    ['title' => 'Comparativos'],
    ['title' => $config['countries'][$country]['name']]
]);	
?>
<!--Page Related styles-->
<?= $this->Html->css('/assets/css/dataTables.bootstrap.css'); ?>
<?= $this->Flash->render() ?>
<div class="row">
    <div class="col-xs-12 col-md-12">
        <div class="widget">
            <div class="widget-header ">
                <span class="widget-caption">Listado de bases de datos</span>
                <div class="widget-buttons">
                    <a href="#" data-toggle="maximize">
                        <i class="fa fa-expand"></i>
                    </a>
                    <a href="#" data-toggle="collapse">
                        <i class="fa fa-minus"></i>
                    </a>
                    <a href="#" data-toggle="dispose">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="widget-body">
	            <div class="alert alert-info">Listando bases de datos identificadas en <?= $config['countries'][$country]['dbname'] ?>.dbo.DBM_DA2</div>
			    <table class="table table-responsive table-bordered table-condensed dataTable">
				    <thead>
					    <tr>
						    <th>Database</th>
						    <th>Description</th>
						    <th>Type</th>
						    <th>Duration</th>
						    <th>Panel</th>
						    <th>Last Period</th>
					    </tr>
				    </thead>
				    <tbody>
					    <?php foreach($databases as $database) { ?>
					    <tr>
						    <td><?= $this->Html->link($database['Nombre'], ['action' => 'detail', $country, $database['Nombre']]) ?></td>
						    <td><?= $database['Descripcion'] ?></td>
						    <td><?= $database['Grupo'] ?></td>
						    <td><?= $database['Corte'] ?></td>
						    <td><?= $database['Panel'] ?></td>
						    <td><?= (array_key_exists($database['Nombre'], $elements)) ? max($elements[$database['Nombre']]) : 'Sin información'; ?></td>
					    </tr>
					    <?php } ?>
				    </tbody>
			    </table>
            </div>
        </div>
    </div>
</div>
<?= $this->Html->script('/assets/js/datatable/jquery.dataTables.min.js'); ?>
<?= $this->Html->script('/assets/js/datatable/ZeroClipboard.js'); ?>
<?= $this->Html->script('/assets/js/datatable/dataTables.tableTools.min.js'); ?>
<?= $this->Html->script('/assets/js/datatable/dataTables.bootstrap.min.js'); ?>
<script lang="javascript" type="text/javascript">
var oTable = $('.dataTable').dataTable({
    "sDom": "Tflt<'row DTTTFooter'<'col-sm-6'i><'col-sm-6'p>>",
    "aaSorting": [[0, 'asc']],
    "aLengthMenu": [
        [5, 15, 20, 50, 100, -1],
        [5, 15, 20, 50, 100, "All"]
    ],
    "iDisplayLength": 20,
    "language": {
        "search": "",
        "sLengthMenu": "_MENU_",
        "oPaginate": {
            "sPrevious": "Anterior",
            "sNext": "Siguiente"
        }
    }
});
</script>