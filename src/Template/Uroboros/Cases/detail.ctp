<?php
$this->Breadcrumbs->add([
    ['title' => 'Uroboros'],
    ['title' => 'Mis casos', 'url' => ['controller' => 'cases', 'action' => 'index', 'prefix' => 'uroboros']],
]);	
?>
<!--Page Related styles-->
<?= $this->Flash->render() ?>
<div class="row">
    <div class="col-xs-12 col-md-12">
	    <div class="widget">
            <div class="widget-header ">
                <span class="widget-caption">Detalle de caso <strong><?= $detail['eid']?></strong></span>
                <div class="widget-buttons">
                    <a href="#" data-toggle="maximize">
                        <i class="fa fa-expand"></i>
                    </a>
                    <a href="#" data-toggle="collapse">
                        <i class="fa fa-minus"></i>
                    </a>
                </div>
            </div>
            <div class="widget-body">
	            <div class="row">
		            <div class="col-sm-8">
			            <h3 style="margin-top: 10px"><?= $detail['uroboros_process']['description'] ?></h3>
			            <p style="margin: 1px 0px;"><strong>País:</strong> <?= $detail['params'] ?></p>
			            <p style="margin: 1px 0px;"><strong>Responsable:</strong> <?= $detail['uroboros_owner']['display'] ?></p>
			            <p style="margin: 1px 0px;"><strong>Fecha de ejecución:</strong> <?= $detail['start_date']->format('d-m-Y h:i a') ?></p>
		            </div>
					<div class="col-sm-4" style="text-align: right">
						<span style="margin-top: 10px;" class="label label-info"><?= $detail['uroboros_status']['name'] ?></span>
						<br>
						<div style="margin-top: 20px">
							<a href="<?= $this->Url->build(['action' => 'index', 'prefix' => 'uroboros']) ?>" class="btn btn-info"><i class="fa fa-reply"></i> Volver a listado</a>
							<?php if(!$detail['done']) { ?>
								<button data-toggle="modal" data-target=".modal-detail" type="button" class="btn btn-success"><i class="fa fa-check"></i> Marcar solucionado</button>
							<?php } else { ?>
								<button data-toggle="modal" data-target=".modal-detail" type="button" class="btn btn-primary"><i class="fa fa-edit"></i> Ver solución</button>
							<?php } ?>
						</div>
					</div>
	            </div>
	            <div class="row">
	            	<div class="col-sm-12">
	            		<table class="table table-responsive table-bordered table-hover table-striped">
	            			<thead>
		            			<tr>
			            			<th>lid</th>
			            			<th>type</th>
			            			<th>message</th>
			            			<th>date</th>
			            			<th width="100">detalle</th>
		            			</tr>
	            			</thead>
	            			<tbody>
		            			<?php foreach($detail['uroboros_logs'] as $log) { ?>
		            			<tr>
			            			<td><?= $log['lid'] ?></td>
			            			<td><?= $log['log_type'] ?></td>
			            			<td><?= $log['message'] ?></td>
			            			<td><?= $log['created_on']->format('d-m-Y h:i a') ?></td>
			            			<td>
				            			<button data-id="tr_<?= $log['lid'] ?>" type="button" class="btn btn-danger btn-xs detail_button"><i class="fa fa-search"></i> Ver detalle</button>
			            			</td>
		            			</tr>
		            			<tr class="tr_error" id="tr_<?= $log['lid'] ?>" style="display: none">
			            			<td colspan="5" style="background-color: #f3a9a9">
				            			<div style="margin: 0px" class="alert alert-danger"><?= $log['detail'] ?></div>
			            			</td>
		            			</tr>
		            			<?php } ?>
	            			</tbody>
	            		</table>
	            	</div>
	            </div>
            </div>
	    </div>
    </div>
</div>
<?php if(!$detail['done']) { ?>
<div class="modal modal-success fade modal-detail in" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="mySmallModalLabel">Confirmar solución</h4>
            </div>
            <div class="modal-body">
	            <?= $this->Form->create($resolution) ?>
	            <div class="form-group">
		            <label for="comments">Comentarios de solución para eid <strong><?= $detail['eid']?></strong></label>
		            <?= $this->Form->textarea('comments', ['label' => 'false', 'rows' => 5, 'class' => 'form-control', 'required']) ?>
	            </div>
	            <div class="form-group" style="text-align: right">
		            <button data-dismiss="modal" class="btn btn-danger"><i class="fa fa-times"></i> Cerrar</button>
		            <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Guardar</button>
	            </div>
            </div>
        </div>
    </div>
</div>
<?php } else { ?>
<div class="modal modal-primary fade modal-detail in" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="mySmallModalLabel">Detalle de solución aplicada</h4>
            </div>
            <div class="modal-body">
	            <table class="table">
	            	<tr>
		            	<th>Fecha de solución</th>
		            	<td><?= $detail['done_date']->format('d-m-Y'); ?></td>
	            	</tr>
	            	<tr>
		            	<th>Solucionado por</th>
		            	<td><?= $detail['done_user']; ?></td>
	            	</tr>
	            	<tr>
		            	<th colspan="2">Detalle solución</th>
	            	</tr>
	            	<tr>
		            	<td colspan="2" style="padding: 0px">
			            	<div class="block-content" style="border: 1px solid #ccc; padding: 10px;">
			            		<?= $detail['uroboros_resolution']['comments'] ?>
			            	</div>
			            </td>
	            	</tr>
	            </table>
	        </div>
	        <div class="modal-footer">
	        	<button data-dismiss="modal" class="btn btn-danger"><i class="fa fa-times"></i> Cerrar</button>
	        </div>
        </div>
    </div>
</div>
<?php } ?>
<script src="/assets/js/bootbox/bootbox.js"></script>
<script lang="javascript" type="text/javascript">
$('document').ready(function() {
	$('.detail_button').click(function() {
		var ref = $(this).data('id');
		$('#'+ref).toggle();
		
	});
});
</script>