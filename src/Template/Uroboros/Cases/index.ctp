<?php
$this->Breadcrumbs->add([
    ['title' => 'Uroboros'],
    ['title' => 'Mis casos']
]);	
?>
<!--Page Related styles-->
<?= $this->Flash->render() ?>
<div class="row">
    <div class="col-xs-12 col-md-12">
        <div class="tabbable">
            <ul class="nav nav-tabs" id="uroboros-cases">
                <li class="active tab-red">
                    <a data-toggle="tab" href="#panel1">
                        Pendientes
                        <span class="badge badge-danger"><?= count($pendants) ?></span>
                    </a>
                </li>

                <li class="tab-palegreen">
                    <a data-toggle="tab" href="#panel2">
                        Finalizados
                        <span class="badge badge-success"><?= count($finishes) ?></span>
                    </a>
                </li>
            </ul>

            <div class="tab-content">
                <div id="panel1" class="tab-pane in active">
                    <div class="row">
                    	<div class="col-sm-12">
                    		<table class="table table-striped table-bordered table-hover">
			                    <thead>
			                        <tr>
			                            <th>eid</th>
			                            <th>process</th>
			                            <th>params</th>
			                            <th>start_date</th>
			                            <th>end_date</th>
			                            <th>status</th>
			                            <th>lineas</th>
			                            <th width="100">Acciones</th>
			                        </tr>
			                    </thead>
			                    <tbody>
				                    <?php foreach($pendants as $pendant) { ?>
				                    <tr>
					                    <td><?= $pendant['eid'] ?></td>
					                    <td><?= $pendant['uroboros_process']['description'] ?></td>
					                    <td><?= $pendant['params'] ?></td>
										<td><?= $pendant['start_date']->format('d-m-Y h:i a') ?></td>
										<td><?= $pendant['end_date']->format('d-m-Y h:i a') ?></td>
										<td><?= $pendant['uroboros_status']['name'] ?></td>
										<td>
											<span class="badge badge-danger"><?= count($pendant['uroboros_logs']) ?></span>
										</td>
										<td>
											<a href="<?= $this->Url->build(['action' => 'detail', $pendant['eid'], 'prefix' => 'uroboros']) ?>" class="btn btn-xs btn-info"><i class="fa fa-info"></i> Ver detalle</a>
										</td>
				                    </tr>
				                    <?php } ?>
				                    <?php if(empty($pendants)) { ?>
				                    <tr>
					                    <td colspan="8">
						                    <center><h4>No existen registros disponibles</h4></center>
					                    </td>
				                    </tr>
				                    <?php } ?>
			                    </tbody>
                    		</table>
                    	</div>
                    </div>
                </div>

                <div id="panel2" class="tab-pane">
                    <div class="row">
                    	<div class="col-sm-12">
                    		<table class="table table-striped table-bordered table-hover">
			                    <thead>
			                        <tr>
			                            <th>eid</th>
			                            <th>process</th>
			                            <th>params</th>
			                            <th>start_date</th>
			                            <th>end_date</th>
			                            <th>status</th>
			                            <th>done_date</th>
			                            <th width="100">Acciones</th>
			                        </tr>
			                    </thead>
			                    <tbody>
				                    <?php foreach($finishes as $finish) { ?>
				                    <tr>
					                    <td><?= $finish['eid'] ?></td>
					                    <td><?= $finish['uroboros_process']['description'] ?></td>
					                    <td><?= $finish['params'] ?></td>
										<td><?= $finish['start_date']->format('d-m-Y h:i a') ?></td>
										<td><?= $finish['end_date']->format('d-m-Y h:i a') ?></td>
										<td><?= $finish['uroboros_status']['name'] ?></td>
										<td><?= $finish['done_date']->format('d-m-Y') ?></td>
										<td>
											<a href="<?= $this->Url->build(['action' => 'detail', $finish['eid'], 'prefix' => 'uroboros']) ?>" class="btn btn-xs btn-info"><i class="fa fa-info"></i> Ver detalle</a>
										</td>
				                    </tr>
				                    <?php } ?>
				                    <?php if(empty($finishes)) { ?>
				                    <tr>
					                    <td colspan="8">
						                    <center><h4>No existen registros disponibles</h4></center>
					                    </td>
				                    </tr>
				                    <?php } ?>
			                    </tbody>
                    		</table>
                    	</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>