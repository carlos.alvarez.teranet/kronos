<?php
$this->Breadcrumbs->add([
    ['title' => 'Client Services'],
    ['title' => 'Nomenclaturas'],
    ['title' => $config['countries'][$country]['name']]
]);	
?>
<!--Page Related styles-->
<?= $this->Html->css('/assets/css/dataTables.bootstrap.css'); ?>
<?= $this->Html->css('/assets/css/JQuery-File-Upload/jquery.fileupload.css'); ?>
<?= $this->Flash->render() ?>
<div class="modal fade in modal-primary" tabindex="-1" id="campoz" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myLargeModalLabel_2">Generación de Campo Z (Z_AR)</h4>
            </div>
            <div class="modal-body">
            	<form class="form" name="powrview" enctype="multipart/form-data" method="post" action="<?= $this->Url->build(['action' => 'upload', '_ext' => 'json']) ?>">
            		<div class="input-group">
		                <label class="input-group-btn">
		                    <span class="btn btn-success">
		                    	Seleccionar Base PW <input id="files" name="pw[]" type="file" style="display: none;" webkitdirectory directory multiple accept=".vl2">
		                    </span>
		                </label>
		                <input id="filename" name="filename" type="text" class="form-control" readonly="">
		                <label class="input-group-btn">
		                	<button id="upload" type="submit" class="btn btn-info"><i class="fa fa-upload"></i> Procesar Campo Z</button>
		                	<button id="remove" disabled="" type="reset" class="btn btn-danger"><i class="fa fa-times"></i> Remover</button>
		                </label>
		            </div>
        		</form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-xs-12 col-md-12">
	    <div class="widget">
	        <div class="widget-body">
	            <h3 style="margin-top: 10px; font-weight: 600 !important; text-transform: uppercase"><?=$name ?></h3>
	            <p style="margin-bottom: 0px">Listado de bases de datos PW por tipo de panel / grupo </p>
	        </div>
	    </div>
    </div>
</div>
<div class="row">
	<div class="col-sm-12">
		<div class="tabbable">
            <ul class="nav nav-tabs" id="myTab">
	            <? foreach($tabs as $key => $tab) { ?>
                <li class="tab-green <?= ($key == 0) ? 'active': ''; ?>">
                    <a data-toggle="tab" href="#<?= $tab ?>">
                        <?= $tab ?>
                    </a>
                </li>
                <? } ?>
            </ul>
            <div class="tab-content">
	            <? foreach($tabs as $key => $tab) { ?>
                <div id="<?= $tab ?>" class="tab-pane <?= ($key == 0) ? 'active' : ''; ?>">
                    <div class="row">
	                    <div class="col-sm-12">
		                    <h4><i class="fa fa-grid"></i> Bases activas para grupo de bases <strong><?= explode("_", $tab, 2)[1] ?></strong></h4>
		                    <hr>
		                    <table class="table table-bordered table-condensed table-hover dataTable">
		                    	<thead>
			                    	<tr>
				                    	<th width="10%">Name</th>
				                    	<th width="60%">Description</th>
				                    	<th width="10%"># Nomenclaturas</th>
				                    	<th width="10%">Action</th>
			                    	</tr>
		                    	</thead>
		                    	<tbody>
			                    	<? foreach($databases as $database) { ?>
			                    	<? if($database['Grupo'] == $tab) { ?>
			                    	<tr data-country="<?= $country ?>" data-database="<?= $database['Nombre'] ?>">
				                    	<td width="10%"><?= $database['Nombre'] ?></td>
				                    	<td width="55%"><?= $database['Descripcion'] ?></td>
				                    	<td width="10%" align="center"><?= (array_key_exists($database['Nombre'], $cantidad)) ? $cantidad[$database['Nombre']] : 0; ?></td>
				                    	<td width="15%">
					                    	<a class="btn btn-xs btn-primary" href="<?= $this->Url->build(['action' => 'detail', $country, $database['Nombre']]) ?>">Nomenclaturas</a>
					                    	<button class="btn btn-xs btn-purple" data-toggle="modal" data-target="#campoz">Campo Z</button>
					                    </td>
			                    	</tr>
			                    	<? } ?>
									<? } ?>
		                    	</tbody>
		                    </table>
	                    </div>
                    </div>
                </div>
                <? } ?>
            </div>
		</div>
	</div>
</div>
<?= $this->Html->script('/assets/js/datatable/jquery.dataTables.min.js'); ?>
<?= $this->Html->script('/assets/js/datatable/ZeroClipboard.js'); ?>
<?= $this->Html->script('/assets/js/datatable/dataTables.tableTools.min.js'); ?>
<?= $this->Html->script('/assets/js/datatable/dataTables.bootstrap.min.js'); ?>
<script lang="javascript" type="text/javascript">
$('document').ready(function() {
	$("input:file").change(function () {
		var fileBuffer = new DataTransfer();
    	var fileName = $(this)[0].files;
    	var pw = '';
    	//console.log(fileName);
    	$.each(fileName, function(key, element) {
	    	if(element.name == 'ZPRODVOL.vl2') {
		    	pw = element.webkitRelativePath.split('/')[0];
		    	$('#filename').val(pw);
		    	$('#pwname').val(pw);
		    	fileBuffer.items.add(element);
	    	}
	    	if(element.name == 'ZC') {
		    	fileBuffer.items.add(element);
	    	}
    	});
    	
    	document.getElementById("files").files = fileBuffer.files;
    });
    
    $( "form" ).on( "submit", function( event ) {
		event.preventDefault();
		
		var form = $("form[name=powrview]");
   		var formData = new FormData(form[0]);
		console.log(form[0]);
		
	});
});
var oTable = $('.dataTable').dataTable({
    "sDom": "Tflt<'row DTTTFooter'<'col-sm-6'i><'col-sm-6'p>>",
    "aLengthMenu": [
        [5, 15, 20, 50, 100, -1],
        [5, 15, 20, 50, 100, "All"]
    ],
    "iDisplayLength": -1,
    "language": {
        "search": "",
        "sLengthMenu": "_MENU_",
        "oPaginate": {
            "sPrevious": "Anterior",
            "sNext": "Siguiente"
        }
    },
    "bDestroy": true,
    "columns": [
		{ "width": "20%" },
		{ "width": "60%" },
		{ "width": "10%" },
		{ "width": "10%" }
	]
});
</script>