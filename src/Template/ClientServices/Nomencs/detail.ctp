<?php
$this->Breadcrumbs->add([
    ['title' => 'Client Services'],
    ['title' => 'Nomenclaturas'],
    ['title' => $config['countries'][$country]['name']]
]);	
?>
<!--Page Related styles-->
<?= $this->Html->css('/assets/css/dataTables.bootstrap.css'); ?>
<? $this->Html->css('/assets/css/dataTables.min.css'); ?>
<style>
.selected {
	background-color: gold;
	font-weight: 800;
}
.selected td:first-child {
	text-indent: 10px;
}
.dt-buttons.btn-group {
	position: absolute;
	right: 80px;
}
.selectable {
	cursor: pointer;	
}
.selectable {
	cursor: pointer;	
}
.selectable:hover {
	background: beige;
}
</style>
<?= $this->Flash->render() ?>
<div class="modal fade in modal-primary" tabindex="-1" id="idArtigos" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width: 1400px">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myLargeModalLabel_2"><span id="modal_pv"><?= $database['Nombre'] ?></span> | <span id="modal_nomenc"></span> | <span id="modal_id"></span> | <span id="modal_name"></span></h4>
            </div>
            <div class="modal-body">
	            <table style="font-size: smaller" class="table table-condensed table-bordered table-hover dataTable" id="table_artigos" width="100%">
		        	<thead>
			        	<tr>
				        	<th>idArtigo</th>
				        	<th>Produto</th>
				        	<th>Sub</th>
				        	<th>Fabricante</th>
				        	<th>Marca</th>
				        	<th>Clas01</th>
				        	<th>Clas02</th>
				        	<th>Clas03</th>
				        	<th>Clas04</th>
				        	<th>Clas05</th>
				        	<th>Conteudo</th>
							<th>Nro Actos</th>
							<th>CodBar1</th>
							<th>CodBar2</th>
							<th>CodBar3</th>
							<th>NroActosMAT</th>
							<th>NroBuyersMAT</th>
			        	</tr>
		        	</thead>
	            </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-xs-12 col-md-12">
	    <div class="widget">
	        <div class="widget-body">
	            <h3 style="margin-top: 10px"><?= $name ?> | <?= explode("_", $database['Grupo'])[1] ?> | <?= $database['Nombre'] ?></h3>
	            <h5><?= $database['Descripcion'] ?></h5>
	            <a href="javascript: history.back();" class="btn btn-xs btn-primary"><i class="fa fa-reply"></i> Volver</a>
	        </div>
	    </div>
    </div>
</div>
<div class="row">
	<div class="col-sm-12">
		<div class="widget">
	        <div class="widget-body">
	            <p style="margin-bottom: 0px">Listado de nomenclaturas mantenidas por ITM para la base de datos seleccionada</p>
	            <hr>
	            <div class="row">
		            <div class="col-sm-2">
			            <table class="table table-bordered table-condensed">
				            <thead>
					            <tr>
						            <th colspan="2" style="background-color: #2a2a28; color: #DBB451; font-weight: 800">NOMENCLATURA</th>
					            </tr>
				            </thead>
			            	<tbody>
				            	<? foreach($nomencs as $key => $nomenc) { ?>
				            	<tr class="selectable" data-key="<?= $key ?>" id="row_<?= $key ?>">
					            	<td><?= $nomenc[0] ?></td>
					            	<td align="center"><?= $nomenc[1] ?></td>
				            	</tr>
				            	<? } ?>
				            	<? if (count($nomencs) == 0) { ?>
				            	<tr>
					            	<td colspan="2"><i style="color: gold" class="fa fa-warning"></i> Sin nomenclaturas</td>
				            	</tr>
				            	<? } ?>
			            	</tbody>
			            </table>
		            </div>
		            <div class="col-sm-10">
			            <div class="row">
				            <div class="col-sm-12">
					            
				            </div>
			            </div>
			            <div id="loader" class="row" style="display: none">
				            <div class="col-sm-12">
					            <div class="well bordered-left bordered-yellow" style="background-color: #2a2a28; color: #DBB451">
                                    <h4 style="margin-bottom: 0px; text-align: center" class="block"><i class="fa fa-pulse fa-spinner"></i> Cargando Datos...</h4>
                                </div>
				            </div>
			            </div>
			            <div id="elements"></div>
		            </div>
	            </div>
	        </div>
	    </div>
	</div>
</div>
<? $this->Html->script('/assets/js/datatable/jquery.dataTables.min.js'); ?>
<? $this->Html->script('/assets/js/datatable/ZeroClipboard.js'); ?>
<? $this->Html->script('/assets/js/datatable/dataTables.tableTools.min.js'); ?>
<? $this->Html->script('/assets/js/datatable/dataTables.bootstrap.min.js'); ?>
<? $this->Html->script('/assets/js/datatable/datatables.min.js'); ?>

<script lang="javascript" type="text/javascript">
$('document').ready(function() {
	/*
	$('.selectable').click(function() {
		var key = $(this).data('key');
		$('.selectable').removeClass('selected');
		$('#row_' + key).addClass('selected');
		$('.table-detail').hide();
		$('#table_' + key).show();
	});
	*/
	$('.selectable').click(function() {
		var key = $(this).data('key');
		
		$('.selectable').removeClass('selected');
		$('#row_' + key).addClass('selected');
		
		$('#elements').hide();
		$('#loader').show();
		$.get('<?= $this->Url->build(['action' => 'elements', $country, $database['Nombre']]) ?>/' + key, function(response) {
			$('#loader').hide();
			$('#elements').show();
			$('#elements').html(response);
			
			$(".toggle-sql").unbind('click');
			$(".toggle-sql").click(function() {
				var id = $(this).data("id");
				$("#row_" + id).toggle();
			});
		});
	});
	
	$('#idArtigos').on('show.bs.modal', function (event) {
		var button 	= $(event.relatedTarget);
		var id		= button.data('id');
		var name	= button.data('name');
		var nomenc	= button.data('nomenc');
		var pw		= button.data('pw');
		
		var nomencid	= button.data('nomencid');
		var compoid		= button.data('compoid');
		
		$('#modal_id').html(id);
		$('#modal_name').html(name);
		$('#modal_nomenc').html(nomenc);
		
		$('#table_artigos').DataTable({
	        ajax: function (d, cb) {
            fetch("<?= $this->Url->build(['action' => 'artigos', $country]) ?>/" + nomencid + "/" + compoid + ".json")
                .then(response => response.json())
                .then(data => cb(data));
	        },
	        columns: [
	            { data: 'idArtigo' },
	            { data: 'Produto' },
	            { data: 'Sub' },
	            { data: 'Fabricante' },  
	            { data: 'Marca' },
	            { data: 'Clas01' },
	            { data: 'Clas02' },
	            { data: 'Clas03' },
	            { data: 'Clas04' },
	            { data: 'Clas05' },
	            { data: 'Conteudo' },
	            { data: 'NroActos' },
	            { data: 'CodBar' },
	            { data: 'CodBar2' },
	            { data: 'CodBar3' },
	            { data: 'NroActosMAT' },
	            { data: 'NroBuyersMAT' }
	            
	        ],
	        "sDom": "fBlt<'row DTTTFooter'<'col-sm-6'i><'col-sm-6'p>>",
			buttons: [
				'copy',
				'excel'
			],
		    "aLengthMenu": [
		        [5, 15, 20, 50, 100, -1],
		        [5, 15, 20, 50, 100, "All"]
		    ],
		    "bDestroy": true,
		    "iDisplayLength": 15,
		    "language": {
		        "search": "",
		        "sLengthMenu": "_MENU_",
		        "oPaginate": {
		            "sPrevious": "Anterior",
		            "sNext": "Siguiente"
		        }
		    },
		    createdRow: function(row, data, index) {
			    if (data.NroActosMAT >= 80) {
					$(row).css('background-color', '#00bfffbf');
				}
		    },
		    order: [[15, 'desc']],
	    });
	});
});
</script>