<table class="table table-bordered table-condensed table-hover table-detail">
    <thead>
        <tr>
            
            <th style="background-color: #2a2a28; color: #DBB451; font-weight: 800" align="center">#</th>
            <th style="background-color: #2a2a28; color: #DBB451; font-weight: 800" align="center">Nivel</th>
            <th style="background-color: #2a2a28; color: #DBB451; font-weight: 800">Code Desc (<?= $nomenc[0] ?>)</th>
            <th style="background-color: #2a2a28; color: #DBB451; font-weight: 800"># SKU's</th>
            <th style="background-color: #2a2a28; color: #DBB451; font-weight: 800">NroActos1Y</th>
            <th style="background-color: #2a2a28; color: #DBB451; font-weight: 800">Id</th>
            <th style="background-color: #2a2a28; color: #DBB451; font-weight: 800">Acciones</th>
        </tr>
    </thead>
    <tbody>
        <?php 
            $script = [];
            $linea = 2;
        ?>
        <tr>
            <td width="50px" align="center">1</td>
            <td width="50px" align="center">0</td>
            <td>NOT SPECIFIED</td>
            <td width="100px">N/A</td>
            <td width="100px">N/A</td>
            <td width="100px">N/A</td>
            <td width="150px"><button disabled class="btn btn-primary btn-xs">Ver Sku's</button> <button disabled class="btn btn-success btn-xs">Regla</button></td>
        </tr>
        <? foreach($elements as $element) { ?>
        <tr style="<?= ($element['Articulos'] == 0) ? 'color: red' : ''; ?>">
            <td width="50px" align="center"><?= $linea++ ?></td>
            <td width="50px" align="center"><?= $element['Nivel'] ?></td>
            <td><?= str_repeat('&nbsp;&nbsp;&nbsp;', $element['Nivel']) ?><?= $element['Linea'] ?></td>
            <td width="100px"><?= $element['Articulos'] ?></td>
            <td width="100px"><?= $element['NroActos1Y'] ?></td>
            <td width="100px"><?= $element['idComposicion'] ?></td>
            <td width="150px"><button <?= ($element['Articulos'] == 0) ? 'disabled' : ''; ?> data-pw="<?= $database['Nombre'] ?>" data-nomencid="<?= $element['idNomenclatura'] ?>" data-compoid="<?= $element['idComposicion'] ?>" data-nomenc="<?= $element['Nomenclatura'] ?>" data-id="<?= $element['idComposicion'] ?>" data-name="<?= $element['Linea'] ?>" data-toggle="modal" data-target="#idArtigos" class="btn btn-primary btn-xs">Ver Sku's</button> <button data-id="<?= $element['idComposicion'] ?>" class="btn toggle-sql btn-success btn-xs">Regla</button></td>
        </tr>
        <tr id="row_<?= $element['idComposicion'] ?>" style="display: none">
            <td colspan="5">
	            <?php
		            $script_out = [];
		            $script[$element['Nivel']] = $element['Script'];
		            foreach($script as $k => $v) {
			            if($k <= $element['Nivel']) {
				            $script_out[$k] = trim($v);
			            }
		            }
		        ?>
	            <pre><code class="sql"><?= str_replace(['[', ']', 'FROM', 'WHERE', 'AND'], ['', '', '<br>FROM', '<br>WHERE&emsp;', '<br>AND&emsp;'], implode(" AND ", $script_out)) ?></code></pre>
            </td>
        </tr>
        <? } ?>
    </tbody>
</table>