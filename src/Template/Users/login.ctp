<style>
i.fa {
  display: inline-block;
  border-radius: 50px;
  box-shadow: 0px 0px 2px rgb(139, 199, 77);
  padding: 0.5em 0.7em;

}
</style>
<div class="login-container animated fadeInDown">
    <div class="loginbox bg-white">
	    <? if($logged) { ?>
	    <div class="loginbox-title">Existe una sesión activa</div>
	    <div class="loginbox-social">
            <div class="social-title ">Sesión identificada correctamente</div>
        </div>
	    <? } else { ?>
        <div class="loginbox-title">Iniciar Sesión</div>
        <div class="loginbox-social">
            <div class="social-title ">Usa tu cuenta corporativa</div>
        </div>
        
        <? } ?>
        
        <? if($logged) { ?>
        
        <div style="padding: 5px">
	        <a href="<?= $this->Url->build(['controller' => 'Stats', 'action' => 'index', 'prefix' => false]); ?>" class="btn btn-block" style="padding: 5px; border: 1px solid #eee; text-align: center">
		        <div style="margin-top: 5px">
			        <?= $this->Html->image('profiles/'.$this->request->session()->read('Auth.User.id'), ['class' => 'avatar', 'width' => 96]); ?>
		        </div>
		        <div>
			        <h4 style="color: #000; margin-bottom: 0px"><?= $this->request->session()->read('Auth.User.displayName'); ?></h4>
			        <p style="color: #999"><?= $this->request->session()->read('Auth.User.jobTitle'); ?></p>
		        </div>
	        </a>
        </div>
        
        <div class="loginbox-or">
            <div class="or-line"></div>
            <div class="or">O</div>
        </div>
        
        <div style="padding: 5px; text-align: center">
	        <p><?= $this->Html->link('[Cerrar sesión]', ['action' => 'logout'], ['class' => 'btn btn-block btn-success']); ?></p>
        </div>
        
        <? } else { ?>
        
        <div style="padding: 5px">
	        <a href="<?= $this->Url->build(['action' => 'oauth', 'prefix' => false]) ?>" class="btn btn-block" style="overflow: hidden; padding: 5px; border: 1px solid #eee;">
		        <div class="pull-left">
			        <?= $this->Html->image('microsoft.png', ['width' => 50]); ?>
		        </div>
		        <div class="pull-left" style="margin-left: 5px; text-align: center">
			        <h4 style="color: #000; margin-bottom: 0px">Login with <strong>Microsoft</strong></h4>
			        <p style="color: #999">Ingresa con tu cuenta Microsoft</p>
		        </div>
	        </a>
        </div>
        
        <? } ?>
        
    </div>
    <?= $this->Flash->render() ?>
    <div class="logobox">
	    <?= $this->Html->image('logo_kantar.svg', ['style' => 'padding: 10px', 'width' => '100%']) ?>
    </div>
</div>