<!--Page Related styles-->
<?= $this->Html->css('/assets/css/dataTables.bootstrap.css'); ?>
<div class="row">
    <div class="col-lg-12 col-sm-12 col-xs-12">
        <div class="tabbable">
            <ul class="nav nav-tabs" id="countries">
	            <? foreach ($config['countries'] as $key => $country) { ?>
                <li class="<?= ($key == 0) ? 'active' : ''; ?>">
                    <a data-toggle="tab" href="#<?= $country['code'] ?>">
                        <?= $country['name'] ?>
                    </a>
                </li>
				<? } ?>
            </ul>

            <div class="tab-content">
	            <? foreach ($config['countries'] as $key => $country) { ?>
                <div id="<?= $country['code'] ?>" class="tab-pane <?= ($key == 0) ? 'in active' : ''; ?>">
                	<table class="datatable table table-striped table-bordered table-hover" id="table-<?= $country['code'] ?>">
						<thead>
							<tr>
								<th style="width: 100px">idProduto</th>
								<th>Descricao</th>
								<th>Painel</th>
								<th>DtCriacao</th>
								<th>FlgDesativacao</th>
								<th style="width: 250px">Acciones</th>
							</tr>
						</thead>
						<tbody>
							<? foreach ($categories[$country['base']] as $category) { ?>
							<tr>
								<td><?= $category['idProduto'] ?></td>
								<td><?= $category['Descricao'] ?></td>
								<td><?= substr($category['Painel'], 0, -1) ?></td>
								<td><?= date('d-m-Y', strtotime($category['DtCriacao'])) ?></td>
								<td><?= $category['FlgDesativacao'] ?></td>
								<td>
									<?= $this->Html->link('<i class="fa fa-edit"></i> Registrar cambios', ['action' => 'registry', $country['code'], $category['idProduto']], ['class' => 'btn btn-xs btn-primary', 'escape' => false]) ?>
									&nbsp;
									<?= $this->Html->link('<i class="fa fa-clock-o"></i> Ver historial', ['action' => 'history', $country['code'], $category['idProduto']], ['class' => 'btn btn-xs btn-info', 'escape' => false]) ?>
								</td>
							</tr>
							<? } ?>
						</tbody>
                	</table>
                </div>
                <? } ?> 
            </div>
        </div>
        <div class="horizontal-space"></div>

    </div>
</div>
<?= $this->Html->script('/assets/js/datatable/jquery.dataTables.min.js') ?>
<?= $this->Html->script('/assets/js/datatable/ZeroClipboard.js') ?>
<?= $this->Html->script('/assets/js/datatable/dataTables.tableTools.min.js') ?>
<?= $this->Html->script('/assets/js/datatable/dataTables.bootstrap.min.js') ?>
<?= $this->Html->script('/assets/js/jquery.cookie.js') ?>
<script language="javascript" type="text/javascript">
	$('document').ready(function(e) {
		$('.datatable').dataTable({
		    "sDom": "Tflt<'row DTTTFooter'<'col-sm-6'i><'col-sm-6'p>>",
		    "iDisplayLength": 100,
		    "aLengthMenu": [[50, 100, 250, 500, -1], [50, 100, 250, 500, "All"]],
		    "language": {
		        "search": "",
		        "sLengthMenu": "_MENU_",
		        "oPaginate": {
		            "sPrevious": "Anterior",
		            "sNext": "Siguiente"
		        }
		    },
		    "bStateSave": true,
		    "aaSorting": [],
		    "bAutoWidth": false,
		    "aoColumnDefs": [ { "bSortable": false, "aTargets": [ 5 ] } ],
		    "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
				if ( aData[4] == "AR" ) {
					$('tr', nRow).addClass('red-row');
				}
			}
		});
	});
</script>