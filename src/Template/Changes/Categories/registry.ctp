<style>
	.aranda-element {
		margin-bottom: 10px;
	}
	.aranda-element .title {
		display: block;
		font-weight: 800;
		font-family: 'Open Sans';
		font-size: smaller;
	}
	.aranda-element .aranda {
		display: block;
		font-size: medium;
	}
</style>
<div class="row">
	<div class="col-sm-6">
		<div class="well bordered-left bordered-yellow">
			<h2 class="block" style="font-weight: 800; padding-top: 0px; margin-bottom: 0px; padding-bottom:  0px"><?= $category['Descricao'] ?> (<?= $category['idProduto'] ?>)</h2>
			<p>
				<span class="sky" style="font-size: 18px"><?= $country['name'] ?></span> - <span class="carbon" style="font-size: 16px"><?= substr($category['Painel'], 0, -1) ?></span>
			</p>
		</div>
	</div>
</div>
<div class="row">
    <div class="col-lg-12 col-sm-12 col-xs-12">
        <div class="tabbable">
            <ul class="nav nav-tabs" id="groups">
	            <li class="active">
                    <a data-toggle="tab" href="#registry">
	                    Detalle del cambio
                    </a>
                </li>
                <li>
                    <a data-toggle="tab" href="#databases">
	                    Bases PW dependientes
                    </a>
                </li>
                <li>
                    <a data-toggle="tab" href="#asdk">
	                    Caso de Aranda
                    </a>
                </li>
            </ul>
            <div class="tab-content">
                <div id="registry" class="tab-pane in active">
	                <div id="registration-form">
	                    <form role="form">
	                        <div class="form-title">
	                            Favor complete el formulario lo mas detallado posible
	                        </div>
	                        <div class="row">
	                            <div class="col-sm-4">
	                                <div class="form-group">
	                                    <label for="email">Email de usuario (auto completado)</label>
	                                    <span class="input-icon icon-right">
	                                        <input type="email" class="form-control" value="<?= $this->request->session()->read('Auth.User.mail') ?>" readonly="readonly" id="email" placeholder="Ingrese Email">
	                                        <i class="fa fa-envelope blue"></i>
	                                    </span>
	                                </div>
	                            </div>
	                            <div class="col-sm-4">
	                                <div class="form-group">
	                                    <label for="date">Fecha de registro (auto completado)</label>
	                                    <span class="input-icon icon-right">
	                                        <input type="text" class="form-control" value="<?= date('d-m-Y') ?>" readonly="readonly" id="date" placeholder="Fecha de registro">
	                                        <i class="fa fa-calendar blue"></i>
	                                    </span>
	                                </div>
	                            </div>
	                            <div class="col-sm-2">
	                                <div class="form-group">
	                                    <label for="aranda">Caso aranda</label>
	                                    <span class="input-icon icon-right">
	                                        <input type="number" class="form-control" id="aranda" placeholder="Caso de aranda asociado">
	                                        <i class="fa fa-book blue"></i>
	                                    </span>
	                                </div>
	                            </div>
	                        </div>
	                        <div class="row">
	                            <div class="col-sm-2">
	                                <div class="form-group">
	                                    <label for="month">Mes productivo del cambio</label>
                                    	<select class="form-control" id="month">
	                                    	<option value>Seleccione mes</option>
	                                    	<option value="1">Enero</option>
	                                    	<option value="2">Febrero</option>
	                                    	<option value="3">Marzo</option>
	                                    	<option value="4">Abril</option>
	                                    	<option value="5">Mayo</option>
	                                    	<option value="6">Junio</option>
	                                    	<option value="7">Julio</option>
	                                    	<option value="8">Agosto</option>
	                                    	<option value="9">Septiembre</option>
	                                    	<option value="10">Octubre</option>
	                                    	<option value="11">Noviembre</option>
	                                    	<option value="12">Diciembre</option>
                                    	</select>
	                                </div>
	                            </div>
	                            <div class="col-sm-2">
	                                <div class="form-group">
	                                    <label for="year">Año productivo del cambio</label>
	                                    <?= $this->Form->select('years', $years, ['empty' => 'Seleccione año', 'class' => 'form-control']); ?>
	                                </div>
	                            </div>
	                            <div class="col-sm-4">
	                                <div class="form-group">
	                                    <label for="type">Tipo de cambio</label>
	                                    <?= $this->Form->select('types', $types, ['empty' => 'Seleccione tipo de cambio', 'class' => 'form-control']); ?>
	                                </div>
	                            </div>
	                        </div>
	                        <div class="row">
	                        <div class="col-lg-12 col-sm-12 col-xs-12">
	                            <div class="widget flat radius-bordered">
	                                <div class="widget-header bordered-bottom bordered-themeprimary">
	                                    <span class="widget-caption">Comentarios de los cambios efectuados - <b><u>Favor ser lo mas detallado posible</u></b></span>
	                                    <div class="widget-buttons">
	                                        <a href="#" data-toggle="maximize">
	                                            <i class="fa fa-expand"></i>
	                                        </a>
	                                    </div>
	                                </div>
	                                <div class="widget-body">
	                                    <div class="widget-main no-padding">
	                                        <div id="summernote"></div>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                    </div>
	                    </form>
	                </div>
                </div>
                <div id="databases" class="tab-pane">
	                <div class="row">
		                <div class="col-sm-12">
			                <h4>Listado de bases PW dependientes</h4>
		                </div>
	                </div>
	                <div class="row">
		                <div class="col-sm-12">
			                <table class="table table-condensed table-bordered">
				                <thead>
					                <tr>
					                <? foreach(array_keys($databases[0]) as $column) { ?>
					                <th><?= $column ?></th>
					                <? } ?>
					                </tr>
				                </thead>
				                <tbody>
					                <? foreach($databases as $database) { ?>
						                <? if($database['FlgActivo'] == 'AR') { ?>
						                <tr style="background-color: #ff9696; color: white">
						                <? } else { ?>
						                <tr>
						                <? } ?>
						                <? foreach(array_keys($database) as $column) { ?>
							                <? if(in_array($column, ['ENE', 'FEB', 'MAR', 'ABR', 'MAY', 'JUN', 'JUL', 'AGO', 'SEP', 'OCT', 'NOV', 'DIC'])) { ?>
							                <td align="center">
								                <? if($database[$column] == 1) { ?>
								                <i class="fa fa-check success"></i>
								                <? } else { ?>
								                <i class="fa fa-times danger"></i>
								                <? } ?>
							                </td>
							                <? } else { ?>
							                <td><?= $database[$column] ?></td>
							                <? } ?>
						                <? } ?>
					                </tr>
					                <? } ?>
				                </tbody>
			                </table>
		                </div>
	                </div>
                </div>
                <div id="asdk" class="tab-pane">
	                <div class="row">
		                <div class="col-sm-12">
			                <h4 class="orange"><strong>Código del Caso: <span class="aranda" data-field="CODE"></span></strong></h4>
		                </div>
	                </div>
	                <div class="row">
		                <div class="col-sm-2">
			                <div class="aranda-element">
				                <span class="title">Autor</span>
				                <span class="aranda" data-field="CLIENT"></span>
			                </div>
			                <div class="aranda-element">
				                <span class="title">País</span>
				                <span class="aranda" data-field="COUNTRY"></span>
			                </div>
			                <div class="aranda-element">
				                <span class="title">Especialista</span>
				                <span class="aranda" data-field="RESPONSIBLE"></span>
			                </div>
			                <div class="aranda-element">
				                <span class="title">Grupo de especialistas</span>
				                <span class="aranda" data-field="GROUP"></span>
			                </div>
		                </div>
			            <div class="col-sm-2">
			                <div class="aranda-element">
				                <span class="title">Impacto</span>
				                <span class="aranda" data-field="IMPACT"></span>
			                </div>
			                <div class="aranda-element">
				                <span class="title">Urgencia</span>
				                <span class="aranda" data-field="URGENCY"></span>
			                </div>
			                <div class="aranda-element">
				                <span class="title">Prioridad</span>
				                <span class="aranda" data-field="PRIORITY"></span>
			                </div>
		                </div>
		                <div class="col-sm-2">
			                <div class="aranda-element">
				                <span class="title">Estado</span>
				                <span class="aranda" data-field="STATUS"></span>
			                </div>
			                <div class="aranda-element">
				                <span class="title">Razón</span>
				                <span class="aranda" data-field="REASON"></span>
			                </div>
			                <div class="aranda-element">
				                <span class="title">Tipo de registro</span>
				                <span class="aranda" data-field="TYPE"></span>
			                </div>
		                </div>
		                <div class="col-sm-3">
			                <div class="aranda-element">
				                <span class="title">Categoría</span>
				                <span class="aranda" data-field="CATEGORY"></span>
			                </div>
			                <div class="aranda-element">
				                <span class="title">Servicio</span>
				                <span class="aranda" data-field="SERVICE"></span>
			                </div>
			                <div class="aranda-element">
				                <span class="title">Fecha de Apertura</span>
				                <span class="aranda" data-field="DATE"></span>
			                </div>
		                </div>
	                </div>
	                <div class="row" style="margin-top: 15px">
		                <div class="col-lg-9 col-sm-9 col-xs-12">
                            <div class="widget">
                                <div class="widget-header bordered-bottom bordered-sky">
                                    <span class="widget-caption">Descripción del caso</span>
                                </div><!--Widget Header-->
                                <div class="widget-body">
                                    <div class="aranda-element">
						                <span class="aranda" data-field="DESCRIPTION"></span>
					                </div>
                                </div><!--Widget Body-->
                            </div><!--Widget-->
                        </div>
	                </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--Summernote Scripts-->
<?= $this->Html->script('/assets/js/editors/summernote/summernote.js') ?>
<script>
    $('#summernote').summernote({ height: 250 });
</script>
<script language="javascript" type="text/javascript">
	$('#aranda').change(function() {
		$.get("<?= $this->Url->build(['controller' => 'services', 'action' => 'code']) ?>/" + $(this).val(), function(data) {
			$('span.aranda').each(function() {
				var field = $(this).data('field');
				if(field == 'DESCRIPTION') {
					$(this).html(data.detail[field]);
				} else {
					$(this).text(data.detail[field]);
				}
			});
		}, 'json');
	});
</script>