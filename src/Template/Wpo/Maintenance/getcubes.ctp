<?php
$this->Breadcrumbs->add([
    ['title' => 'WPO'],
    ['title' => 'Mantenedor']
]);	
?>


<style>
    .error-control{
        -webkit-box-shadow: 0px 0px 5px 0px rgba(255,0,0,1) !important;
-moz-box-shadow: 0px 0px 5px 0px rgba(255,0,0,1) !important;
box-shadow: 0px 0px 5px 0px rgba(255,0,0,1) !important;

        
    }
</style>
<!--Page Related styles-->
<?= $this->Html->css('/assets/css/dataTables.bootstrap.css'); ?>
<?= $this->Html->css('/assets/css/style-cubes.css'); ?>


<?= $this->Flash->render() ?>
<div class="row">
    

    <div class="col-xs-12 col-md-12">
        <div class="widget flat radius-bordered">
            <div class="widget-header bordered-bottom bordered-gray">
                <span class="widget-caption">
                    <h5>
                        <?=(isset($cubes)?'Formulario cubos ('.$cubes->id.')':'Crear Cubo');?> 
                        <input id="id" value='<?=$cubes->id;?>' style="display:none">
                    </h5>
                </span>
            </div>
	        <div class="widget-body">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="type">Cliente</label>
                            <input class="form-control obligacion" value="<?=$cubes['Client'];?>" id="cliente" list="autoCompleteCliente" >
                            <datalist id="autoCompleteCliente">
                                <?php
                                foreach ($arrCliente as $key => $c) {
                                ?>
                                    <option value="<?=$c;?>">
                                <?php
                                }
                                ?>
                            </datalist>
                    
                        </div>   
                    </div>

                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="type">Categoria</label>
                            <input class="form-control obligacion" value="<?=$cubes['Category'];?>" id="Categoria" >
                                
                        </div>   
                    </div>
                </div>

                
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="exampleInputEmail2">LCO <span style="color: red">*</span></label>
                            <span class="input-icon icon-right">
                                <?= $this->Form->text('LCO', ['label' => false,'value'=>$cubes->LCO,'id'=>'LCO', 'class' => 'form-control obligacion', 'placeholder' => 'LCO']) ?>
                                <i class="fa fa-info blue"></i>
                            </span>
                        </div> 
                    </div>

                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="exampleInputEmail2">PWDB <span style="color: red">*</span></label>
                            <span class="input-icon icon-right">
                                <?= $this->Form->text('PWDB', ['label' => false,'id'=>'PWDB', 'value'=>$cubes->PWDB,'class' => 'form-control obligacion', 'placeholder' => 'Ingrese descripción / nombre de dimensión']) ?>
                                <i class="fa fa-info blue"></i>
                            </span>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="type">Area</label>
                            <select class="form-control obligacion" id="area">
                                <option value="LOCAL">LOCAL</option>
                                <option value="GLOBAL">GLOBAL</option>
                            </select>
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="type">País</label>
                            <?= $this->Form->select('Pais', $objPaises, ['empty' => 'Seleccione País','id' => 'pais','default'=>$cubes->idCountry, 'class' => 'form-control obligacion']); ?>
                        </div>
                    </div>
                    

                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="type">Tipo</label>
                            <?= $this->Form->select('Tipo', $arrTipos, ['id'=>'Tipo','empty' => 'Seleccione Tipo','default'=>$cubes->idType, 'class' => 'form-control obligacion']); ?>
                        </div>
                    </div>

                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="type">Frecuencia</label>
                            
                            <?= $this->Form->select('Frecuencia', $arrFrecuency, ['disabled'=>'disabled','id'=>'Frecuencia','empty' => '','default'=>$cubes->Frecuency, 'class' => 'form-control obligacion']); ?>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="type">Loader_Files</label>
                            <div class="material-switch " style="margin-top:10px;">
                                <input id="Loader_Files" name="Loader_Files" <?=($cubes['Loader_Files']==1)?'checked="checked"':'';?> type="checkbox"/>
                                <label for="Loader_Files" class="label-success"></label>
                            </div>
                        </div>  
                    </div> 
                    <div class="col-sm-12">
                        <hr>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-1">
                        <div class="form-group">
                            <label for="type">M01</label>
                            <div class="material-switch " style="margin-top:10px;">
                                <input id="M01" class="switchChange" name="M01" <?=($cubes['M01']==1)?'checked="checked"':'';?> type="checkbox"/>
                                <label for="M01" class="label-success"></label>
                            </div>
                        </div>   
                    </div>
                   
                    <div class="col-sm-1">
                        <div class="form-group">
                            <label for="type">M02</label>
                            <div class="material-switch " style="margin-top:10px;">
                                <input id="M02" class="switchChange" name="M02" <?=($cubes['M02']==1)?'checked="checked"':'';?> type="checkbox"/>
                                <label for="M02" class="label-success"></label>
                            </div>
                        </div>   
                    </div>
                    <div class="col-sm-1">
                        <div class="form-group">
                            <label for="type">M03</label>
                            <div class="material-switch " style="margin-top:10px;">
                                <input id="M03" class="switchChange" name="M03" <?=($cubes['M03']==1)?'checked="checked"':'';?> type="checkbox"/>
                                <label for="M03" class="label-success"></label>
                            </div>
                        </div>   
                    </div>
                    <div class="col-sm-1">
                        <div class="form-group">
                            <label for="type">M04</label>
                            <div class="material-switch " style="margin-top:10px;">
                                <input id="M04" class="switchChange" name="M04" <?=($cubes['M04']==1)?'checked="checked"':'';?> type="checkbox"/>
                                <label for="M04" class="label-success"></label>
                            </div>
                        </div>   
                    </div>
                    <div class="col-sm-1">
                        <div class="form-group">
                            <label for="type">M05</label>
                            <div class="material-switch " style="margin-top:10px;">
                                <input id="M05" class="switchChange" name="M05" <?=($cubes['M05']==1)?'checked="checked"':'';?> type="checkbox"/>
                                <label for="M05" class="label-success"></label>
                            </div>
                        </div>   
                    </div>
                    <div class="col-sm-1">
                        <div class="form-group">
                            <label for="type">M06</label>
                            <div class="material-switch " style="margin-top:10px;">
                                <input id="M06" class="switchChange" name="M06" <?=($cubes['M06']==1)?'checked="checked"':'';?> type="checkbox"/>
                                <label for="M06" class="label-success"></label>
                            </div>
                        </div>   
                    </div>
                    <div class="col-sm-1">
                        <div class="form-group">
                            <label for="type">M07</label>
                            <div class="material-switch " style="margin-top:10px;">
                                <input id="M07" class="switchChange" name="M07" <?=($cubes['M07']==1)?'checked="checked"':'';?> type="checkbox"/>
                                <label for="M07" class="label-success"></label>
                            </div>
                        </div>   
                    </div>
                    <div class="col-sm-1">
                        <div class="form-group">
                            <label for="type">M08</label>
                            <div class="material-switch " style="margin-top:10px;">
                                <input id="M08" class="switchChange" name="M08" <?=($cubes['M08']==1)?'checked="checked"':'';?> type="checkbox"/>
                                <label for="M08" class="label-success"></label>
                            </div>
                        </div>   
                    </div>
                    <div class="col-sm-1">
                        <div class="form-group">
                            <label for="type">M09</label>
                            <div class="material-switch " style="margin-top:10px;">
                                <input id="M09" class="switchChange" name="M09" <?=($cubes['M09']==1)?'checked="checked"':'';?> type="checkbox"/>
                                <label for="M09" class="label-success"></label>
                            </div>
                        </div>   
                    </div>

                    <div class="col-sm-1">
                        <div class="form-group">
                            <label for="type">M10</label>
                            <div class="material-switch " style="margin-top:10px;">
                                <input id="M10" class="switchChange" name="M10" <?=($cubes['M10']==1)?'checked="checked"':'';?> type="checkbox"/>
                                <label for="M10" class="label-success"></label>
                            </div>
                        </div>   
                    </div>
                    <div class="col-sm-1">
                        <div class="form-group">
                            <label for="type">M11</label>
                            <div class="material-switch " style="margin-top:10px;">
                                <input id="M11" class="switchChange" name="M11" <?=($cubes['M11']==1)?'checked="checked"':'';?> type="checkbox"/>
                                <label for="M11" class="label-success"></label>
                            </div>
                        </div>   
                    </div>
                    <div class="col-sm-1">
                        <div class="form-group">
                            <label for="type">M12</label>
                            <div class="material-switch " style="margin-top:10px;">
                                <input id="M12" class="switchChange" name="M12" <?=($cubes['M12']==1)?'checked="checked"':'';?> type="checkbox"/>
                                <label for="M12" class="label-success"></label>
                            </div>
                        </div>   
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12">
                        <hr>
                        <div class="text-right">
                            <a class="btn btn-danger"  href="<?= $this->Url->build(['controller' => 'maintenance', 'action' => 'index', 'prefix' => 'wpo']); ?>">Volver</a>
                            <button class="btn btn-success" onclick="saveData()">Almacenar</button>
                            
                        </div>
                    </div>
                </div>
	        </div>
	    </div>
    </div>


<script>
    $(document).ready(function(){

        $('.switchChange').change(function (){
            let checkedCount=0;
            $('.switchChange').each(function (){
                if($(this).is(":checked")){
                    checkedCount++
                } 
            });
            
            


        })

    })

    
    function saveData(){
        let swError=0;
        $(".obligacion").each(function(){
            if(this.value==''){
                $(this).addClass("error-control")
                swError=1;
            }else{
                $(this).removeClass("error-control")
            }
        })

        if(swError==1){
            alerta("Error","Favor de revisar el formulario cubo, ya que hay datos por validar.")
        }else{

        

       

        const objSave={
            'id':$("#id").val(),
            'Client':$("#cliente").val(),
            'idClient':null,
            'Category':$("#Categoria").val(),
            'LCO':$("#LCO").val(),
            'PWDB':$("#PWDB").val(),
            'Area':$("#area").val(),
            'idCountry':$("#pais option:selected").val(),
            'Country':$("#pais option:selected").text(),
            'idType':$("#Tipo option:selected").val(),
            'Type':$("#Tipo option:selected").text(),
            'IdenFrecuencia':$("#Frecuencia option:selected").val(),
            'Frecuency':$("#Frecuencia option:selected").text(),
            'Loader_Files':($("#Loader_Files").is(":checked"))?1:0,
            'M01':($("#M01").is(":checked"))?1:0,
            'M02':$("#M02").is(":checked")?1:0,
            'M03':$("#M03").is(":checked")?1:0,
            'M04':$("#M04").is(":checked")?1:0,
            'M05':$("#M05").is(":checked")?1:0,
            'M06':$("#M06").is(":checked")?1:0,
            'M07':$("#M07").is(":checked")?1:0,
            'M08':$("#M08").is(":checked")?1:0,
            'M09':$("#M09").is(":checked")?1:0,
            'M10':$("#M10").is(":checked")?1:0,
            'M11':$("#M11").is(":checked")?1:0,
            'M12':$("#M12").is(":checked")?1:0,
        }


        $.ajax({
            url : '<?= $this->Url->build(['controller' => 'maintenance', 'action' => 'transaccubes', 'prefix' => 'wpo']); ?>',
            data : objSave,
            type : 'post',
            dataType : 'json',
            success : function(obj) {
                alerta('Aviso','Se almacena la información del cubo')
                 
            },
            error : function(xhr, status) {
                alert('Disculpe, existió un problema');
            },
            complete : function(xhr, status) {
                // alert('Petición realizada');
            }
        });
    }
    }   


    function alerta(title,body){
        $("#exampleModalLabel").html(title)

        $("#modal-body").html(body)
        $('#flipFlop').modal('show')
    }


</script>

<div class="modal fade" id="flipFlop" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header bg-success">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
            <h4 class="modal-title" style="color:#fff;" id="exampleModalLabel">Modal Title</h4>
         </div>
         <div class="modal-body" id="modal-body">
            Modal content...
         </div>
         <div class="modal-footer">
            <a  class="btn btn-success" href="<?= $this->Url->build(['controller' => 'maintenance', 'action' => '', 'prefix' => 'wpo']); ?>" >Aceptar</a>
         </div>
      </div>
   </div>
</div>