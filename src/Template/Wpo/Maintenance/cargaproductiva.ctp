<?php
$this->Breadcrumbs->add([
    ['title' => 'WPO'],
    ['title' => 'Carga Productiva Mensual'],
]);	

function getFechaSumaResta($fecha,$cant,$isSumaResta){
    return date("d-m-Y",strtotime($fecha.$isSumaResta." ".$cant." month")); 
}

$meses=array(
    '1'=>"Enero",
    '2'=>"Febrero",
    '3'=>"Marzo",
    '4'=>"Abril",
    '5'=>"Mayo",
    '6'=>"Junio",
    '7'=>"Julio",
    '8'=>"Agosto",
    '9'=>"Septiembre",
    '10'=>"Octubre",
    '11'=>"Noviembre",
    '12'=>"Diciembre",
);

$arrMonth=[];

// for($i=0;$i<=11;$i++){
//     $fecha=getFechaSumaResta(date('Y-m-d'),$i,'-');
//     $mes=getDateFormato($fecha,'m');
//     $MesName=$meses[$mes];
//     $anio=getDateFormato($fecha,'Y');
//     $arrMonth[$mes]=$MesName.' '.$anio;
//     // echo $MesName.' '.$anio.'<br>';
// }

foreach ($Periodo as $key => $m) {
    $mes=$m['Month'];
    $MesName=$meses[$mes];
    $anio=$m['Year'];
    $arrMonth[$m['IdPeriodo']]=$mes.' '.$anio;
}
// $arrMonth=array_reverse($arrMonth);
?>

<style>
    .fuente-meses{
        font-size: 10.5px;
        margin-left: 3px;
        background-color: greenyellow;
        padding: 4px;
        border-radius: 25px;
    }
    .fuente-meses-seleccionada{
        font-size: 10.5px;
        margin-left: 3px;
        background-color: #2c2cad;
        color:#fff;
        padding: 4px;
        border-radius: 25px;
    }

    .wrap-panel{
        border:1px solid #cecece;;
        padding:5px;
        border-radius: 3px;
        margin-top:5px;

    }
</style>


<!--Page Related styles-->
<?= $this->Html->css('/assets/css/dataTables.bootstrap.css'); ?>
<?= $this->Html->css('/assets/css/smart_wizard_all.css'); ?>
<?= $this->Html->css('/assets/css/dataTables.bootstrap.css'); ?>
<?= $this->Html->css('/assets/css/style-cubes.css'); ?>
<?= $this->Html->css('/assets/css/jquery-confirm.min.css'); ?>

<?= $this->Flash->render() ?>
<div class="row">
    

    <div class="col-lg-12 col-sm-12 col-xs-12">
        <div class="widget flat radius-bordered">
            <div class="widget-header bordered-bottom bordered-gray">
                <span class="widget-caption"><h5>Crear nueva carga productiva mensual</h5></span>
               
                
            </div>
            <div class="widget-body">
                <div class="widget-main no-padding">
                    
                <div id="smartwizard" >
                    <ul class="nav">
                        <li class="nav-item">
                        <a class="nav-link" href="#step-1">
                            <div class="num">1</div>
                            Periodo
                        </a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="#step-2">
                            <span class="num">2</span>
                            Cubos
                        </a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="#step-3">
                            <span class="num">3</span>
                            Confirmacion
                        </a>
                        </li>
                        
                    </ul>
                    
                        <div class="tab-content">
                            <div id="step-1" class="tab-pane" role="tabpanel" aria-labelledby="step-1">
                                <div class="row">
                                    <div class="col-md-12" style="min-height:200px;">

                                        <div class="row">
                                            <div class="col-md-3">
                                                Seleccionar Periodo:
                                                <select class="form-control" id="CmbPeriodo" onchange="getDataPeriodo(this.value)">
                                                    <option value="-1">Seleccionar Periodo</option>
                                                    <?php
                                                    foreach ($arrMonth as $key => $a) {
                                                        list($mes,$anio)=explode(" ",$a);
                                                    ?>
                                                    <option value="<?=$key;?>" data-ano='<?=$anio?>'  data-mes='<?=$mes?>'><?=$meses[$mes].' '.$anio;?></option>
                                                    <?php
                                                    }
                                                    ?>
                                                    

                                                </select>
                                            </div>

                                            <div class="col-md-1"></div>

                                            
                                            <div class="col-md-7" style="display:none" id="cargas-productivas-div">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                            Cargas productivas creadas para ese periodo:
                                                    </div>
                                           
                                                    <div id="result-cargas-productivas"></div>

                                                    <!-- <div class="col-md-7 wrap-panel">
                                                        <div class="row">
                                                            <div class="col-md-3 text-center"  style="margin-top:-15px;">
                                                                <h2>4000</h2>
                                                                <div class="" style="margin-top:-5px;">Seleccionados</div>

                                                            </div>
                                                            <div class="col-md-9">
                                                                Creada el día <strong>XX-XX-XXXX HH:MM:SS</strong>
                                                                <div><strong>Total cubos:</strong> 400</div>
                                                                <div class="text-right">
                                                                    <a href="" class="link-carga" style="font-size:11px">
                                                                        Ir carga productiva
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                       
                                                    </div> -->


                                                    
                                                
                                                
                                                </div>
                                            </div>
                                            
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="step-2" class="tab-pane" role="tabpanel" aria-labelledby="step-2">
                                <div class="row">
                                    <div class="col-md-12" >
                                        <h4>Listado de Cubos a incorporar</h4>
                                        <strong class="labelCuboSelect"></strong>
                                        
                                        <div id="tabla-cubo"></div>

                                    </div>
                                </div>                    
                                
                            </div>
                            <div id="step-3" class="tab-pane" style="min-height:200px" role="tabpanel" aria-labelledby="step-3">
                                <div class="row" >
                                    <div class="col-md-12" >
                                        <h4>Confirmación</h4>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-2">Periodo</div>
                                    <div class="col-md-2 text-right"><strong class="labelCuboSelect"></strong></div>
                                </div> 
                                <div class="row">
                                    <div class="col-md-2">Cubos totales</div>
                                    <div class="col-md-2 text-right"><strong class="labelCuboTotales"></strong> cubos</div>
                                </div> 
                                <div class="row">
                                    <div class="col-md-2">Cubos a general</div>
                                    <div class="col-md-2 text-right"><strong class="labelCuboaCrear"></strong> cubos</div>
                                </div> 

                                <div class="row">
                                    <div class="col-md-12">
                                        <hr>
                                    </div>
                                    <div class="col-md-12">
                                        <button class="btn btn-success" id="btn-genera" onclick="generarCargaProductiva()">
                                            <span class="carga"></span>
                                            Generar Carga Productiva
                                        </button>
                                    </div>
                                </div>
                            </div>
                           
                        </div>
                    
                        <!-- Include optional progressbar HTML -->
                        <div class="progress">
                        <div class="progress-bar" role="progressbar" style="width: 0%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                    </div>  



                </div>
            </div>
        </div>
    </div>
</div>

<!-- JavaScript -->
<?= $this->Html->script('/assets/js/jquery.smartWizard.min.js'); ?>
<?= $this->Html->script('/assets/js/datatable/jquery.dataTables.min.js'); ?>
<?= $this->Html->script('/assets/js/datatable/dataTables.tableTools.min.js'); ?>
<?= $this->Html->script('/assets/js/datatable/dataTables.bootstrap.min.js'); ?>
<?= $this->Html->script('/assets/js/jquery-confirm.min.js') ?>


<script>
     var oTable ='';
    $(document).ready(function(){
        $('#smartwizard').smartWizard({
            theme: 'arrows', // theme for the wizard, related css need to include for other than default theme
            lang: { // Language variables for button
                next: 'Siguiente',
                previous: 'Anterior'
            },
            justified: true, // Nav menu justification. true/false,
            autoAdjustHeight: true,
            enableUrlHash: true, // Enable selection of the step based on url hash

        });


        $('#smartwizard').on("leaveStep", function(e, anchorObject, stepNumber, stepDirection) {
            console.log(stepDirection)
            if(stepDirection==1){
                const idMes=$("#CmbPeriodo option:selected").attr("data-mes")
                if(idMes==-1){
                    alerta('Error','Favor de ingresar un periodo para revisar los Cubos.')
                }else{
                    getCubosPeriodo(idMes)
                }
                
            }else if(stepDirection==2){
                getResumen()
            }

            
        });
    })
    var hashLog=""

    function setDatable(hashLog){
        $('document').ready(function() {
            var oTable = $('#'+hashLog).dataTable({
                "sDom": "Tflt<'row DTTTFooter'<'col-sm-6'i><'col-sm-6'p>>",
                "aLengthMenu": [
                    [5, 10, 20, 50, 100, -1],
                    [5, 10, 20, 50, 100, "All"]
                ],
                "iDisplayLength": 10,
                "language": {
                    "search": "",
                    "sLengthMenu": "_MENU_",
                    "oPaginate": {
                        "sPrevious": "Anterior",
                        "sNext": "Siguiente"
                    }
                },
                "bDestroy": true
            });
            $(".tab-content").height('auto');
        });
    }

    var objData=[];
    var arrKey=[]
    function getResumen(){
        arrKey=[];
        let cont=0
        $('#'+hashLog).DataTable().rows().iterator('row', function(context, index){
            var node = $(this.row(index).node()); 
            let $input = $(node).find("input");
            $($input[0]).each(function(){
                if(this.checked==true){
                    cont++
                    arrKey.push(this.value)
                }
                
            })
               
        });
        $(".labelCuboaCrear").html(cont)
        console.log('cac',cont) 
        console.log('cac',arrKey) 
    }



    function generarCargaProductiva(){
        const idMes=$("#CmbPeriodo").val();
        const anio=$("#CmbPeriodo option:selected").attr("data-ano")
        let objSend={
            'IdPeriodo':parseInt(idMes),
            'anio':parseInt(anio),
            'Cubos':arrKey,
            'TotalCubos':parseInt($(".labelCuboTotales").html()),
            'TotalSeleccionados':parseInt($(".labelCuboaCrear").html()),
            
        }


        $(".carga").html('<i class="fa fa-spinner fa-spin"></i>')
        $("#btn-genera").prop("disabled",true)
        console.log(objSend);
        $.ajax({
            url : '<?= $this->Url->build(['controller' => 'maintenance', 'action' => 'createcargaproductiva', 'prefix' => 'wpo']); ?>',
            data :objSend,
            type : 'post',
            dataType : 'json',
            success : function(obj) {
               
                $.confirm({
                    title: 'Alerta',
                    content: 'Se creo la carga productiva con exito.',
                    type: 'green',
                    typeAnimated: true,
                    buttons: {
                        Ok: {
                            text: 'Aceptar',
                            btnClass: 'btn-green',
                            action: function(){
                                location.href="<?= $this->Url->build(['controller' => 'maintenance', 'action' =>'listcargaproductiva', 'prefix' => 'wpo']); ?>"
                            }
                        },
                        
                    }
                });

                console.log(obj);
                 
            },
            error : function(xhr, status) {
                alert('Disculpe, existió un problema');
            },
            complete : function(xhr, status) {
                $(".carga").html('')
                $("#btn-genera").prop("disabled",false)
            }
        });
    }
    

    
    function getDataPeriodo(Iden){
        $("#cargas-productivas-div").fadeIn()
        $("#result-cargas-productivas").html('<h1><i class="fa fa-spinner fa-spin"></i></h1>')

        // const idMes=$("#CmbPeriodo").val();
        // const anio=$("#CmbPeriodo option:selected").attr("data-ano")
       
        $(".sw-btn-next").prop('disabled',true);
      

        objData=[];
        $.ajax({
            url : '<?= $this->Url->build(['controller' => 'maintenance', 'action' => 'getdataperiodoparametro', 'prefix' => 'wpo']); ?>',
            data : {'IdPeriodo':Iden},
            type : 'post',
            dataType : 'json',
            success : function(obj) {
                console.log(obj.CargaProductiva.length)
                
                $(".tab-content").height('auto');
                
                let cadenaActual='';

                if(obj.CargaProductiva.length>0){
                    $.each(obj.CargaProductiva,function(index,val){
                        console.log(val)
                        cadenaActual+=`
                        <div class="col-md-8 wrap-panel">
                            <div class="row">
                                <div class="col-md-3 text-center"  style="margin-top:-15px;">
                                    <h2>${val.TotalSeleccionados}</h2>
                                    <div class="" style="margin-top:-5px;">Seleccionados</div>

                                </div>
                                <div class="col-md-9">
                                    Creada el día <strong>${formatoFecha(val.FechaCreacion)}</strong>
                                    <div><strong>Total cubos:</strong> ${val.TotalCubos}</div>
                                    <div class="text-right">
                                        <a class="link-carga" style="font-size:11px" href="<?= $this->Url->build(['controller' => 'maintenance', 'action' => 'detallecargaproductiva/', 'prefix' => 'wpo']); ?>/${val.IdCargaProductiva}">
                                            Ir carga productiva
                                        </a>
                                    </div>
                                </div>
                            </div>
                            
                        </div> 
                        `;
                    })
                    $("#cargas-productivas-div").fadeIn();

                    
                    $.confirm({
                        title: 'Error',
                        content: 'Ya existe una carga de trabajo para periodo seleccionado.',
                        type: 'red',
                        typeAnimated: true,
                        buttons: {
                            Ok: {
                                text: 'Aceptar',
                                btnClass: 'btn-red',
                                action: function(){
                                   
                                }
                            },
                            
                        }
                    });

                    
                }else{
                    $(".sw-btn-next").prop('disabled',false);
                    $("#cargas-productivas-div").hide()
                }

                $("#result-cargas-productivas").html(cadenaActual)
            },
            error : function(xhr, status) {
                alert('Disculpe, existió un problema');
            },
            complete : function(xhr, status) {
                // alert('Petición realizada');
            }
        });

    }
      

    function getCubosPeriodo(IdMes){
        $('#table_cubes').hide();
        $(".carga").html('<h1><i class="fa fa-spinner fa-spin"></i></h1>')
        $("#tabla-cubo").html('<h1><i class="fa fa-spinner fa-spin"></i></h1>')
        $(".labelCuboSelect").html($("#CmbPeriodo option:selected").text())
        $("#filas-cubos").html('')
        objData=[];
        $.ajax({
            url : '<?= $this->Url->build(['controller' => 'maintenance', 'action' => 'getcubosperiodo', 'prefix' => 'wpo']); ?>',
            data : {'idMes':IdMes},
            type : 'post',
            dataType : 'json',
            success : function(obj) {
              let cadenaBucle='';
              hashLog=obj.hashLog
              
              cadenaBucle+=`<table class="table table-bordered" id="${obj.hashLog}">
                    <thead>
                        <tr>
                            <th>PWDB</th>
                            <th>Pais</th>
                            <th>Cliente</th>
                            <th>Frecuencia</th>
                            <th>Periodo</th>
                            <th>Incluir</th>
                        </tr>
                    </thead>
                    <tbody >
                        
                    
                `;


              $.each(obj.getCubos,function(index,val){
                let meses=[];
                if(val.M01==1){
                    meses['1']=('Enero')
                }
                if(val.M02==1){
                    meses['2']=('Febrero')
                }
                if(val.M03==1){
                    meses['3']=('Marzo')
                }
                if(val.M04==1){
                    meses['4']=('Abril')
                }
                if(val.M05==1){
                     meses['5']=('Mayo')
                }
                if(val.M06==1){
                     meses['6']=('Junio')
                }
                if(val.M07==1){
                     meses['7']=('Julio')
                }
                if(val.M08==1){
                     meses['8']=('Agosto')
                }
                if(val.M09==1){
                     meses['9']=('Septiembre')
                }
                if(val.M10==1){
                     meses['10']=('Octubre')
                }
                if(val.M11==1){
                     meses['11']=('Noviembre')
                }
                if(val.M12==1){
                     meses['12']=('Diciembre')
                }

                let cadenaMes='';

                $.each(meses,function(index,val){
                    if(val){
                        let tag='<span class="[claseMes]">'+val+'</span>';

                        if(index==parseInt(IdMes)){
                            mes=tag.replace("[claseMes]","fuente-meses-seleccionada")
                        }else{
                            mes=tag.replace("[claseMes]","fuente-meses")    
                        }
                        
                        cadenaMes+=mes;
                    }
                })
                cadenaBucle+=`
                    <tr>
                        <td>${val.PWDB}</td>
                        <td>${val.Country}</td>
                        <td>${val.Client}</td>
                        <td>${val.Frecuency}</td>
                        <td>${cadenaMes}</td>
                        <td>
                            <div class="material-switch " style="margin-top:2px;">
                                <input checked="checked" id="isCubos${val.id}" class="isCuboSeleccionado" onclick="setCheckbox(this,${val.id})" name="isCubos${val.id}" value="${val.id}" type="checkbox">
                                <label for="isCubos${val.id}" class="label-success"></label>
                            </div>
                        
                        </td>
                    </tr>
                `; 
                objData.push(val.id);
              })

              cadenaBucle+=`
                    </tbody>
                </table>
            `;

             $("#tabla-cubo").html(cadenaBucle)
              
              $(".labelCuboTotales").html(parseInt(obj.getCubos.length))
              $(".carga").html('')
              $('#table_cubes').show();
              setDatable(obj.hashLog)
                 
            },
            error : function(xhr, status) {
                alert('Disculpe, existió un problema');
            },
            complete : function(xhr, status) {
                // alert('Petición realizada');
            }
        });

    }

    function setCheckbox(este,iden){
        if(este.checked){
            objData.push(iden)
        }else{
            const index = objData.indexOf(iden);
            if (index > -1) { // only splice objData when item is found
                objData.splice(index, 1); // 2nd parameter means remove one item only
            }
        }
    }

    function alerta(title,body){
        $("#exampleModalLabel").html(title)

        $("#modal-body").html(body)
        $('#flipFlop').modal('show')
    }

    function formatoFecha(fechaHora){
        console.log(fechaHora)
        let f=fechaHora.split('T');
        let fecha=f[0].split('-').reverse().join('-');
        let hora=f[1].substr(0,5);

        fechaHora=fecha+' '+hora;

        return (fechaHora)
    }

</script>

<div class="modal fade" id="flipFlop" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header bg-success">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
            <h4 class="modal-title" style="color:#fff;" id="exampleModalLabel">Modal Title</h4>
         </div>
         <div class="modal-body" id="modal-body">
            Modal content...
         </div>
         <div class="modal-footer">
         <button type="button" class="btn btn-secondary" data-dismiss="modal">Aceptar</button>
         </div>
      </div>
   </div>
</div>