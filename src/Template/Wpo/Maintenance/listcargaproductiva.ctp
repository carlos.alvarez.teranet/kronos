<?php
$this->Breadcrumbs->add([
    ['title' => 'WPO'],
    ['title' => 'Carga Productiva Mensual'],
]);	


// $arrMonth=array_reverse($arrMonth);
?>

<style>
    .col-card{
        width: 95%;
        padding-left: 14px;
        border-radius: 2px;
        border: 1px solid #bbb;
    }

    .periodo-title{
        font-size: 16px;
        font-weight: 560!important;
        color: #8f8f8f
       
    }
    .periodo-sub-title-asignado{
        font-size: 12px;

    }
    
</style>


<!--Page Related styles-->
<?= $this->Html->css('/assets/css/dataTables.bootstrap.css'); ?>
<?= $this->Html->css('/assets/css/smart_wizard_all.css'); ?>
<?= $this->Html->css('/assets/css/dataTables.bootstrap.css'); ?>
<?= $this->Html->css('/assets/css/style-cubes.css'); ?>
<?= $this->Html->css('/assets/css/jquery-confirm.min.css'); ?>

<?= $this->Flash->render() ?>
<div class="row">
    

    <div class="col-lg-12 col-sm-12 col-xs-12">
        <div class="widget flat radius-bordered">
            <div class="widget-header bordered-bottom bordered-gray">
                <div class="pull-right" style="margin-right:10px; margin-top:4px">
					<a class="btn btn-success btn-sm" href="<?= $this->Url->build(['controller' => 'maintenance', 'action' => 'cargaproductiva', 'prefix' => 'wpo']); ?>">
						<i class="fa fa-plus" aria-hidden="true"></i>Crear Carga productiva
					</a>
				</div>
                <span class="widget-caption">
                    <h5>Cargas productivas Cubos</h5>
                </span>
               
                
            </div>
            <div class="widget-body">
                <div class="widget-main no-padding" >

                    <div class="row">
                        <?php
                        
                        foreach ($CargaProductiva as $key => $c) {
                        ?>
                        <div class="col-md-4" style="margin-top:10px;">
                            
                            <div class="col-card">
                                <div class="row">
                                <div class="col-md-12">
                                        <h5 class="periodo-title"><?=nombreMes($c['Month']);?> <?=$c['Year'];?></h5>
                                    </div>

                                    <div class="col-md-12" style="margin-top:-18px">
                                        <h6 class="periodo-sub-title">Total Cubos habilitados: <?=$c['TotalCubos'];?> </h6>
                                    </div>

                                    <div class="col-md-12" style="margin-top:-18px">
                                        <h6 class="periodo-sub-title-asignado">Total Cubos asignados: <?=$c['TotalSeleccionados'];?> </h6>
                                    </div>

                                    <div class="col-md-12" style="margin-bottom:5px">
                                        <a class="btn btn-xs btn-primary"href="<?= $this->Url->build(['controller' => 'maintenance', 'action' => 'detallecargaproductiva/',$c['IdCargaProductiva'], 'prefix' => 'wpo']); ?>">
                                            <i class="fa fa-cogs" aria-hidden="true"></i>
                                            Administrar
                                        </a>
                                        
                                    </div>
                                </div>
                               
                            </div>
                        

                        </div>

                        <?php
                        }
                        ?>
                        
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>

<!-- JavaScript -->
<?= $this->Html->script('/assets/js/jquery.smartWizard.min.js'); ?>
<?= $this->Html->script('/assets/js/datatable/jquery.dataTables.min.js'); ?>
<?= $this->Html->script('/assets/js/datatable/dataTables.tableTools.min.js'); ?>
<?= $this->Html->script('/assets/js/datatable/dataTables.bootstrap.min.js'); ?>
<?= $this->Html->script('/assets/js/jquery-confirm.min.js') ?>


