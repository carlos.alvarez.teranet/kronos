<?php
$this->Breadcrumbs->add([
    ['title' => 'WPO'],
    ['title' => 'Mantenedor']
]);	
?>
<!--Page Related styles-->
<?= $this->Html->css('/assets/css/dataTables.bootstrap.css'); ?>
<?= $this->Html->css('/assets/css/jquery-confirm.min.css'); ?>

<?= $this->Flash->render() ?>
<div class="row">
    <div class="col-xs-12 col-md-12">
	    <div class="widget">
	        <div class="widget-body">
				<div class="pull-right">
					<a class="btn btn-success btn-sm" href="<?= $this->Url->build(['controller' => 'maintenance', 'action' => 'getcubes/', 'prefix' => 'wpo']); ?>">
						<i class="fa fa-plus" aria-hidden="true"></i>Add Cubo
					</a>
				</div>
				
	            <p style="margin-bottom: 0px">Listado de Cubos </p>
	        </div>
	    </div>
    </div>
</div>
<div class="row">
	<div class="col-sm-12">
		
		<table id="table_cubes" class="table table-condensed table-bordered table-hover">
			<thead>
				<tr>
					<th>Id</th>
					<th>Cliente</th>
					<th>Categoria</th>
					<th>LCO</th>
					<th>Tipo</th>
					<th>Base PW</th>
					<th>Frecuencia</th>
					<th>Acciones</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($cubes as $cube) { ?>
				<tr id="fila-<?= $cube['id'] ?>">
					<td><?= $cube['id'] ?></td>
					<td><?= $cube['Client'] ?></td>
					<td><?= $cube['Category'] ?></td>
					<td><?= $cube['LCO'] ?></td>
					<td><?= $cube['Type'] ?></td>
					<td><?= $cube['PWDB'] ?></td>
					<td><?= $cube['Frecuency'] ?></td>
					<td class="text-center">
						<a class="btn btn-xs btn-success" href="<?= $this->Url->build(['controller' => 'maintenance', 'action' => 'getcubes/'.$cube['id'], 'prefix' => 'wpo']); ?>">
							<i class="fa fa-pencil" aria-hidden="true"></i>
						</a>
						<button class="btn btn-xs btn-danger" onclick="deleteCubo(<?=$cube['id'];?>)">
							<i class="fa fa-trash-o" aria-hidden="true"></i>
						</button>
					</td>
				</tr>
				<?php } ?>
			</tbody>
		</table>
		
	</div>
</div>
<?= $this->Html->script('/assets/js/datatable/jquery.dataTables.min.js'); ?>
<?= $this->Html->script('/assets/js/datatable/ZeroClipboard.js'); ?>
<?= $this->Html->script('/assets/js/datatable/dataTables.tableTools.min.js'); ?>
<?= $this->Html->script('/assets/js/datatable/dataTables.bootstrap.min.js'); ?>
<?= $this->Html->script('/assets/js/jquery-confirm.min.js') ?>

<script lang="javascript" type="text/javascript">
$('document').ready(function() {
	var oTable = $('#table_cubes').dataTable({
	    "sDom": "Tflt<'row DTTTFooter'<'col-sm-6'i><'col-sm-6'p>>",
	    "aLengthMenu": [
	        [5, 15, 20, 50, 100, -1],
	        [5, 15, 20, 50, 100, "All"]
	    ],
	    "iDisplayLength": 20,
	    "language": {
	        "search": "",
	        "sLengthMenu": "_MENU_",
	        "oPaginate": {
	            "sPrevious": "Anterior",
	            "sNext": "Siguiente"
	        }
	    },
	    "bDestroy": true
	});
});

function deleteCubo(Iden){
	$.confirm({
		title: 'Consulta',
		content: 'Seguro que desea eliminar el cubo?',
		buttons: {
			
			
			somethingElse: {
				text: 'Si',
				btnClass: 'btn-success',
				// keys: ['enter', 'shift'],
				action: function(){
					
					$.ajax({
						url : '<?= $this->Url->build(['controller' => 'maintenance', 'action' => 'deletecubes', 'prefix' => 'wpo']); ?>',
						data : {'IdCubo':Iden},
						type : 'post',
						dataType : 'json',
						success : function(obj) {
							$("#fila-"+Iden).remove()
							
						},
						
					});
				}
			},
			somethingNo: {
				text: 'No',
				btnClass: 'btn-danger',
				// keys: ['enter', 'shift'],
				action: function(){
					// $.alert('Something else?');
				}
			}
		}
	});
}
</script>