<?php
$this->Breadcrumbs->add([
    ['title' => 'WPO'],
    ['title' => 'Detalle Carga Productiva Mensual'],
]);	

// $arrMonth=array_reverse($arrMonth);
?>




<!--Page Related styles-->
<?= $this->Html->css('/assets/css/dataTables.bootstrap.css'); ?>
<?= $this->Html->css('/assets/css/smart_wizard_all.css'); ?>
<?= $this->Html->css('/assets/css/dataTables.bootstrap.css'); ?>
<?= $this->Html->css('/assets/css/style-cubes.css'); ?>
<?= $this->Html->css('/assets/css/jquery-confirm.min.css'); ?>

<style>
    .fuente-meses{
        font-size: 10.5px;
        margin-left: 3px;
        background-color: greenyellow;
        padding: 4px;
        border-radius: 25px;
    }
    .fuente-meses-seleccionada{
        font-size: 10.5px;
        margin-left: 3px;
        background-color: #2c2cad;
        color:#fff;
        padding: 4px;
        border-radius: 25px;
    }

    .wrap-panel{
        border:1px solid #cecece;;
        padding:5px;
        border-radius: 3px;
        margin-top:5px;

    }
</style>
<?= $this->Flash->render() ?>
<div class="row">
    

    <div class="col-lg-12 col-sm-12 col-xs-12">
        <div class="widget flat radius-bordered">
            <div class="widget-header bordered-bottom bordered-gray">
                
                <span class="widget-caption">
                    <h5>Cargas productiva </h5>
                    
                </span>
               
                
            </div>
            <div class="widget-body">
                <div class="widget-main no-padding" >
                    <div id="smartwizard" >
                        <ul class="nav">
                            <li class="nav-item">
                            <a class="nav-link" href="#step-1">
                                <div class="num">1</div>
                                Carga productiva
                            </a>
                            </li>
                            <li class="nav-item">
                            <a class="nav-link" href="#step-2">
                                <span class="num">2</span>
                                Cargar Cubo
                            </a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div id="step-1" class="tab-pane" role="tabpanel" aria-labelledby="step-1">
                                <div class="row">
                                    <div class="col-md-3">
                                        <h4 style="text-decoration:underline">Detalle</h4>
                                        <strong>Periodo</strong>: <?=nombreMes($CargaProductiva['Month']);?> <?=$CargaProductiva['Year'];?><br>
                                        <strong>Total Cubos</strong>: <?=$CargaProductiva['TotalCubos'];?><br>
                                        <strong>Cubos Seleccionados</strong>: <?=$CargaProductiva['TotalSeleccionados'];?><br>
                                        <strong>Creación</strong>: <?=getDateFormato($CargaProductiva['FechaCreacion'],'d-m-Y H:i:s');?><br>
                                        <br>
                                    </div>


                                    <div class="col-md-9">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <h4 style="text-decoration:underline">Cubos Seleccionados</h4>

                                                
                                                <table class="table table-bordered" id="table-seleccionados">
                                                    <thead>
                                                        <tr>
                                                            <th>PWDB</th>
                                                            <th>Pais</th>
                                                            <th>Cliente</th>
                                                            <th>Frecuencia</th>
                                                            <th>Incluir</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody id="filas-cubos">
                                                        <?php
                                                        foreach ($CargaProductivaCubos as $key => $c) {
                                                        ?>
                                                        <tr id="fila-<?=$c['IdCargaProductivaCubo'];?>">
                                                            <td><?=$c['PWDB'];?></td>
                                                            <td><?=$c['Country'];?></td>
                                                            <td><?=$c['Client'];?></td>
                                                            <td><?=$c['Frecuency'];?></td>
                                                            <td class="text-center">
                                                                <button class="btn btn-xs btn-danger" onclick="deleteCubo(<?=$c['IdCargaProductivaCubo'];?>)">
                                                                    <i class="fa fa-trash-o" aria-hidden="true"></i>
                                                                </button>
                                                            </td>
                                                        </tr>


                                                        <?php
                                                        }


                                                        ?>
                                                        
                                                    </tbody>

                                                </table>

                                            </div>
                                        </div>
                                        
                                        
                                        
                                    </div>


                                

                                    <?php
                                    // echo $CargaProductiva;
                                
                                    ?>
                                    
                                </div>
                            </div>

                            <div id="step-2" class="tab-pane" role="tabpanel" aria-labelledby="step-2">
                                <div class="row">
                                    <div class="col-md-12" >
                                        <h4>Listado de Cubos a incorporar</h4>
                                        <strong class="labelCuboSelect"></strong>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="carga text-center"><h1><i class="fa fa-spinner fa-spin"></i></h1></div>
                                            </div>
                                        </div>
                                        <table class="table table-bordered" id="table_cubes_add">
                                            <thead>
                                                <tr>
                                                    <th>PWDB</th>
                                                    <th>Pais</th>
                                                    <th>Cliente</th>
                                                    <th>Frecuencia</th>
                                                    <th>Periodo</th>
                                                    <th>Incluir</th>
                                                </tr>
                                            </thead>
                                            <tbody id="filas-cubos-add">
                                                
                                            </tbody>

                                        </table>

                                    </div>
                                </div>                    
                                
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>

<!-- JavaScript -->
<?= $this->Html->script('/assets/js/jquery.smartWizard.min.js'); ?>
<?= $this->Html->script('/assets/js/datatable/jquery.dataTables.min.js'); ?>
<?= $this->Html->script('/assets/js/datatable/dataTables.tableTools.min.js'); ?>
<?= $this->Html->script('/assets/js/datatable/dataTables.bootstrap.min.js'); ?>
<?= $this->Html->script('/assets/js/jquery-confirm.min.js') ?>


<script>
     var oTable ='';
    $(document).ready(function(){
       
        $('#smartwizard').smartWizard({
            theme: 'arrows', // theme for the wizard, related css need to include for other than default theme
            lang: { // Language variables for button
                next: 'Siguiente',
                previous: 'Anterior'
            },
            justified: true, // Nav menu justification. true/false,
            autoAdjustHeight: true,
            enableUrlHash: true, // Enable selection of the step based on url hash
            hiddenSteps:true
        });

        $('#smartwizard').on("leaveStep", function(e, anchorObject, stepNumber, stepDirection) {
            console.log(stepDirection)
            if(stepDirection==1){
                const idMes=$("#CmbPeriodo").val();
                if(idMes==-1){
                    alerta('Error','Favor de ingresar un periodo para revisar los Cubos.')
                }else{
                    getCubosPeriodo('<?=(strlen($CargaProductiva['Month'])==1)?'0'.$CargaProductiva['Month']:$CargaProductiva['Month'];?>')
                }
                
            }
            
        });
        setDatable('table-seleccionados')



    })


    function getCubosPeriodo(idMes){
        $('#table_cubes_add').hide();
        $(".carga").html('<h1><i class="fa fa-spinner fa-spin"></i></h1>')
        $(".labelCuboSelect").html($("#CmbPeriodo option:selected").text())
        objData=[];
        $.ajax({
            url : '<?= $this->Url->build(['controller' => 'maintenance', 'action' => 'getcubosperiodonoselect', 'prefix' => 'wpo']); ?>',
            data : {idMes:idMes,'IdCargaProductiva':<?=$c['IdCargaProductiva'];?>},
            type : 'post',
            dataType : 'json',
            success : function(obj) {
              let cadenaBucle='';
              $.each(obj.getCubos,function(index,val){
                let meses=[];
                if(val.M01==1){
                    meses['1']=('Enero')
                }
                if(val.M02==1){
                    meses['2']=('Febrero')
                }
                if(val.M03==1){
                    meses['3']=('Marzo')
                }
                if(val.M04==1){
                    meses['4']=('Abril')
                }
                if(val.M05==1){
                     meses['5']=('Mayo')
                }
                if(val.M06==1){
                     meses['6']=('Junio')
                }
                if(val.M07==1){
                     meses['7']=('Julio')
                }
                if(val.M08==1){
                     meses['8']=('Agosto')
                }
                if(val.M09==1){
                     meses['9']=('Septiembre')
                }
                if(val.M10==1){
                     meses['10']=('Octubre')
                }
                if(val.M11==1){
                     meses['11']=('Noviembre')
                }
                if(val.M12==1){
                     meses['12']=('Diciembre')
                }

                let cadenaMes='';

                $.each(meses,function(index,val){
                    if(val){
                        let tag='<span class="[claseMes]">'+val+'</span>';

                        if(index==parseInt(idMes)){
                            mes=tag.replace("[claseMes]","fuente-meses-seleccionada")
                        }else{
                            mes=tag.replace("[claseMes]","fuente-meses")    
                        }
                        
                        cadenaMes+=mes;
                    }
                })
                cadenaBucle+=`
                    <tr>
                        <td>${val.PWDB}</td>
                        <td>${val.Country}</td>
                        <td>${val.Client}</td>
                        <td>${val.Frecuency}</td>
                        <td>${cadenaMes}</td>
                        <td>
                            <div class="material-switch " style="margin-top:2px;">
                                <input checked="checked" id="isCubos${val.id}" class="isCuboSeleccionado" onclick="setCheckbox(this,${val.id})" name="isCubos${val.id}" value="${val.id}" type="checkbox">
                                <label for="isCubos${val.id}" class="label-success"></label>
                            </div>
                        
                        </td>
                    </tr>
                `; 
                objData.push(val.id);
              })
              
              $(".labelCuboTotales").html(parseInt(obj.getCubos.length))
              $("#filas-cubos-add").html(cadenaBucle)
              $(".carga").html('')
              $('#table_cubes_add').show();
              setDatable('table_cubes_add')
                 
            },
            error : function(xhr, status) {
                alert('Disculpe, existió un problema');
            },
            complete : function(xhr, status) {
                // alert('Petición realizada');
            }
        });

    }

    function setDatable(Iden){
        $('document').ready(function() {
            var oTable = $('#'+Iden).dataTable({
                "sDom": "Tflt<'row DTTTFooter'<'col-sm-6'i><'col-sm-6'p>>",
                "aLengthMenu": [
                    [5, 10, 20, 50, 100, -1],
                    [5, 10, 20, 50, 100, "All"]
                ],
                "iDisplayLength": 10,
                "language": {
                    "search": "",
                    "sLengthMenu": "_MENU_",
                    "oPaginate": {
                        "sPrevious": "Anterior",
                        "sNext": "Siguiente"
                    }
                },
                "bDestroy": true
            });
            $(".tab-content").height('auto');
        });
    }

    function deleteCubo(Iden){
	$.confirm({
		title: 'Consulta',
		content: 'Seguro que desea eliminar el cubo de <?=$titleCargaProductiva;?>?',
		buttons: {
			
			
			somethingElse: {
				text: 'Si',
				btnClass: 'btn-success',
				// keys: ['enter', 'shift'],
				action: function(){
					
					$.ajax({
						url : '<?= $this->Url->build(['controller' => 'maintenance', 'action' => 'deletecubescargaproductiva', 'prefix' => 'wpo']); ?>',
						data : {'IdCargaProductivaCubo':Iden,'IdCargaProductiva':<?=$c['IdCargaProductiva'];?>},
                        
						type : 'post',
						dataType : 'json',
						success : function(obj) {
							$("#fila-"+Iden).remove()
							
						},
						
					});
				}
			},
			somethingNo: {
				text: 'No',
				btnClass: 'btn-danger',
				// keys: ['enter', 'shift'],
				action: function(){
					// $.alert('Something else?');
				}
			}
		}
	});
}

var heightScreen=480;
function createFrame(UrlFrame,Titulo){
    
    $("#myModalLabe-frame").html(Titulo)
    

    $("#modal-body-frame").html('<iframe src="'+UrlFrame+'" width="100%" height="'+heightScreen+'" scrolling="auto" />')
    $('#myModal-frame').modal('show')  
}

</script>

<style type="text/css">
    .modal-wide .modal-dialog  {
        
        width: 95%;
        height: 100%;
        padding: 0;
    }
    

</style>


<div class="modal fade bs-example-modal-lg modal-wide" id="myModal-frame" tabindex="-1" role="dialog" aria-labelledby="myModalLabel-frame">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" style="font-size:20px;">&times;</span></button>
                <h4 class="modal-title" id="TitleFrame"></h4>
            </div>
            <div class="modal-body" id="modal-body-frame"></div>
        </div>
    </div>
</div>
