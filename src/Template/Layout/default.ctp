<!DOCTYPE html>
<!--
BeyondAdmin - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.6
Version: 1.6.0
Purchase: https://wrapbootstrap.com/theme/beyondadmin-adminapp-angularjs-mvc-WB06R48S4
-->

<html xmlns="http://www.w3.org/1999/xhtml">

<!-- Head -->
<head>
    <?= $this->Html->charset() ?>
    <title>
    <?= $this->fetch('title') ?>
    </title>

    <meta name="description" content="blank page" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />  
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <?= $this->Html->meta('icon') ?>

    <!--Basic Styles-->
    <?= $this->Html->css('/assets/css/bootstrap.min.css') ?>
    <?= $this->Html->css('/assets/css/font-awesome.min.css') ?>
    <?= $this->Html->css('/assets/css/weather-icons.min.css') ?>
    <link id="bootstrap-rtl-link" href="" rel="stylesheet" />

    <!--Fonts-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,600,700,300"
          rel="stylesheet" type="text/css">

    <!--Beyond styles-->
    <?= $this->Html->css('/assets/css/beyond.min.css', ['id' => 'beyond-link']) ?>
    <?= $this->Html->css('/assets/css/demo.min.css') ?>
    <?= $this->Html->css('/assets/css/typicons.min.css') ?>
    <?= $this->Html->css('/assets/css/animate.min.css') ?>
    <link id="skin-link" href="" rel="stylesheet" type="text/css" />

    <!--Skin Script: Place this script in head to load scripts for skins and rtl support-->
    <?= $this->Html->script('/assets/js/skins.js') ?>
    
    <!--Basic Scripts-->
    <?= $this->Html->script('/assets/js/jquery.min.js') ?>
    <?= $this->Html->script('/assets/js/bootstrap.min.js') ?>
    <?= $this->Html->script('/assets/js/slimscroll/jquery.slimscroll.min.js') ?>
    
    <?= $this->Html->css('custom.css') ?>
    
    
    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>
<!-- /Head -->
<!-- Body -->
<body>
    <!-- Loading Container -->
    <div class="loading-container">
        <div class="loader"></div>
    </div>
    <!--  /Loading Container -->
    <?php if(0 == 1) { ?>
    <!-- Navbar -->
    <div class="navbar">
        <div class="navbar-inner" style="background-color: #2a2a28">
            <?= $this->element('navbar') ?>
        </div>
    </div>
    <!-- /Navbar -->
    <?php } ?>
    <!-- Main Container -->
    <div class="main-container container-fluid">
        <!-- Page Container -->
        <div class="page-container">
            <!-- Page Sidebar -->
            <?= $this->element('sidebar') ?>
            <!-- /Page Sidebar -->
            <!-- Page Content -->
            <div class="page-content">
                <!-- Page Breadcrumb -->
                <?= $this->element('breadcrumbs') ?>
                <!-- /Page Breadcrumb -->
                <!-- Page Header -->
                <?= $this->element('header') ?>
                <!-- /Page Header -->
                <!-- Page Body -->
                <div class="page-body">
                    <!-- Your Content Goes Here -->
                    <?= $this->fetch('content') ?>
                </div>
                <!-- /Page Body -->
            </div>
			<!-- /Page Content -->
        </div>
        <!-- /Page Container -->
        <!-- Main Container -->

    </div>
    <!--Beyond Scripts-->
    <?= $this->Html->script('/assets/js/beyond.min.js') ?>

	<!-- Modals -->
	<div id="modal_loader"></div>
    <!--Page Related Scripts-->
    
</body>
<!--  /Body -->
</html>
