<!DOCTYPE html>
<!--
BeyondAdmin - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.6
Version: 1.6.0
Purchase: https://wrapbootstrap.com/theme/beyondadmin-adminapp-angularjs-mvc-WB06R48S4
-->

<html xmlns="http://www.w3.org/1999/xhtml">
<!--Head-->
<head>
    <?= $this->Html->charset() ?>
    <title>
    <?= $this->fetch('title') ?>
    </title>

    <meta name="description" content="login page" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="theme-color" content="#4ea8e9" />
    <meta name="apple-mobile-web-app-status-bar-style" content="#4ea8e9">

    <?= $this->Html->meta('icon') ?>

    <!--Basic Styles-->
    <?= $this->Html->css('/assets/css/bootstrap.min.css') ?>
    <?= $this->Html->css('/assets/css/font-awesome.min.css') ?>
    <link id="bootstrap-rtl-link" href="" rel="stylesheet" />

    <!--Fonts-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,600,700,300" rel="stylesheet" type="text/css">
    
    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>
<!--Head Ends-->
<!--Body-->
<body style="width: 100%; background-color: #f9f9f9; padding: 0px; margin: 0px;">
    
    <?= $this->fetch('content'); ?>

    <!--Basic Scripts-->
    <?= $this->Html->script('/assets/js/jquery.min.js') ?>
    <?= $this->Html->script('/assets/js/bootstrap.min.js') ?>

    
</body>
<!--Body Ends-->
</html>
