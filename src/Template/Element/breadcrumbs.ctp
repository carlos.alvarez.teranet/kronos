<?php
$this->Breadcrumbs->templates([
    'wrapper' => '{{content}}',
]);	
?>
<div class="page-breadcrumbs">
    <ul class="breadcrumb">
        <li>
            <i class="fa fa-home"></i>
            <a href="#">Home</a>
        </li>
        <? 
        echo $this->Breadcrumbs->render(
			['class' => 'breadcrumbs-trail']
		); 
		?>
    </ul>
</div>
