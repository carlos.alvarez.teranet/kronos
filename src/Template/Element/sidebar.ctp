<div class="page-sidebar" id="sidebar">
    <!-- Page Sidebar Header-->
    <div class="sidebar-header-wrapper" style="display: none">
        <input type="text" class="searchinput" />
        <i class="searchicon fa fa-search"></i>
        <div class="searchhelper">Búsqueda no disponible</div>
    </div>
    <div class="sidebar-header-wrapper" style="height: 100px">
	    <small style="top: 5px;">
            <img style="width: auto; height: 50px; z-index: 1; position: relative; transform: translateY(-50%); top: 50%; margin-left: 15px" src="/img/logoroc.png" alt="" />
        </small>
    </div>
    <!-- /Page Sidebar Header -->
    <!-- Sidebar Menu -->
    <ul class="nav sidebar-menu">
        <!--Dashboard-->
        <li class="<?= ($this->request->params['controller'] == 'Stats' and !isset($this->request->params['prefix'])) ? 'active' : '' ?>">
            <a href="<?= $this->Url->build(['controller' => 'stats', 'prefix' => false]); ?>">
                <i class="menu-icon glyphicon glyphicon-home"></i>
                <span class="menu-text"> Dashboard </span>
            </a>
        </li>

        <!--UI Elements-->
        <?php if($this->Valid->checkPermission('Fusion')) { ?>
        <li class="<?= (in_array($this->request->params['controller'], ['Generations'])) ? 'open' : '' ?>">
            <a href="#" class="menu-dropdown">
                <i class="menu-icon fa fa-bookmark"></i>
                <span class="menu-text"> Proyecto Fusión </span>

                <i class="menu-expand"></i>
            </a>

            <ul class="submenu">
                <li class="<?= (in_array($this->request->params['controller'], ['Generations'])) ? 'active' : '' ?>">
                    <a href="<?= $this->Url->build(['controller' => 'generations', 'action' => 'index', 'prefix' => 'fusion']); ?>">
                        <span class="menu-text">Generación</span>
                    </a>
                </li>
            </ul>
        </li>
        <?php } ?>
        
        <?php if($this->Valid->checkPermission('Operations')) { ?>
        <li class="<?= (in_array($this->request->params['controller'], ['Weighings', 'Comparatives', 'Reports', 'Images'])) ? 'open' : '' ?>">
            <a href="#" class="menu-dropdown">
                <i class="menu-icon fa fa-calculator"></i>
                <span class="menu-text"> Operaciones </span>

                <i class="menu-expand"></i>
            </a>

            <ul class="submenu">
                <li class="<?= (in_array($this->request->params['controller'], ['Weighings'])) ? 'active' : '' ?>">
                    <a href="<?= $this->Url->build(['controller' => 'weighings', 'action' => 'index', 'prefix' => 'operations']); ?>">
                        <span class="menu-text">Pesaje</span>
                    </a>
                </li>
                <li class="<?= (in_array($this->request->params['controller'], ['Reports'])) ? 'active' : '' ?>">
                    <a href="<?= $this->Url->build(['controller' => 'reports', 'action' => 'index', 'prefix' => 'operations']); ?>">
                        <span class="menu-text">Seguimiento Bases</span>
                    </a>
                </li>
                <li class="<?= (in_array($this->request->params['controller'], ['Comparatives']) and  $this->request->params['prefix'] == 'operations') ? 'open' : '' ?>">
	                <a href="#" class="menu-dropdown">
	                    <span class="menu-text">Comparativos Acts</span>
	                </a>
	                <?php
		                $string = file_get_contents( CONFIG . DS . "platform" . DS . "comparatives.json" );
						$config = json_decode($string, true);
		            ?>
	                <ul class="submenu">
		                <? foreach($config['countries'] as $key => $detail) { ?>
		                <li class="<?= (in_array($this->request->params['controller'], ['Comparatives']) and $this->request->params['pass'][0] == $key) ? 'active' : '' ?>">
		                    <a href="<?= $this->Url->build(['controller' => 'comparatives', 'action' => 'index',  $key, 'prefix' => 'operations']); ?>">
		                        <span class="menu-text"><?= $detail['name'] ?></span>
		                    </a>
		                </li>
		                <? } ?>
	                </ul>
                </li>
                <li class="<?= (in_array($this->request->params['controller'], ['Images'])) ? 'active' : '' ?>">
                    <a href="<?= $this->Url->build(['controller' => 'images', 'action' => 'index', 'prefix' => 'operations']); ?>">
                        <span class="menu-text">Banco de imágenes</span>
                    </a>
                </li>
            </ul>
        </li>
        <?php } ?>
        
        <!-- Bitácora -->
        <?php if($this->Valid->checkPermission('Changes')) { ?>
        <li class="<?= (in_array($this->request->params['controller'], ['Categories', 'Parameters', 'Acts', 'Process', 'Cubes'])) ? 'open' : '' ?>">
            <a href="#" class="menu-dropdown">
                <i class="menu-icon fa fa-compass"></i>
                <span class="menu-text"> Bitácora de cambios</span>

                <i class="menu-expand"></i>
            </a>

            <ul class="submenu">
	            <!--
	            <li class="<?= (in_array($this->request->params['controller'], ['Generals'])) ? 'active' : '' ?>">
                    <a href="<?= $this->Url->build(['controller' => 'generals', 'action' => 'index', 'prefix' => 'changes']); ?>">
                        <span class="menu-text">- Consolidado -</span>
                    </a>
                </li>
	            <li class="<?= (in_array($this->request->params['controller'], ['Collections'])) ? 'active' : '' ?>">
                    <a href="<?= $this->Url->build(['controller' => 'collections', 'action' => 'index', 'prefix' => 'changes']); ?>">
                        <span class="menu-text">Data Collection</span>
                    </a>
                </li>
                -->
                <li class="<?= (in_array($this->request->params['controller'], ['Categories'])) ? 'active' : '' ?>">
                    <a href="<?= $this->Url->build(['controller' => 'categories', 'action' => 'index', 'prefix' => 'changes']); ?>">
                        <span class="menu-text">Masterfile</span>
                    </a>
                </li>
                <!--
                <li class="<?= (in_array($this->request->params['controller'], ['Parameters'])) ? 'active' : '' ?>">
                    <a href="<?= $this->Url->build(['controller' => 'parameters', 'action' => 'index', 'prefix' => 'changes']); ?>">
                        <span class="menu-text">Itemization</span>
                    </a>
                </li>
                <li class="<?= (in_array($this->request->params['controller'], ['Validations'])) ? 'active' : '' ?>">
                    <a href="<?= $this->Url->build(['controller' => 'validations', 'action' => 'index', 'prefix' => 'changes']); ?>">
                        <span class="menu-text">Data Validation</span>
                    </a>
                </li>
                <li class="<?= (in_array($this->request->params['controller'], ['Process'])) ? 'active' : '' ?>">
                    <a href="<?= $this->Url->build(['controller' => 'Process', 'action' => 'index', 'prefix' => 'changes']); ?>">
                        <span class="menu-text">Data Processing</span>
                    </a>
                </li>
                <li class="<?= (in_array($this->request->params['controller'], ['Cubes'])) ? 'active' : '' ?>">
                    <a href="<?= $this->Url->build(['controller' => 'cubes', 'action' => 'index', 'prefix' => 'changes']); ?>">
                        <span class="menu-text">Worldpanel Online</span>
                    </a>
                </li>
                -->
            </ul>
        </li>
        <?php } ?>
        
        <!-- Mirror -->
        <!--
        <?php if($this->Valid->checkPermission('Mirror')) { ?>
        <li class="<?= (in_array($this->request->params['controller'], ['Eligibilities']) and  $this->request->params['prefix'] == 'mirror') ? 'open' : '' ?>">
            <a href="#" class="menu-dropdown">
                <i class="menu-icon fa fa-image"></i>
                <span class="menu-text"> Mirror </span>

                <i class="menu-expand"></i>
            </a>

            <ul class="submenu">
                <li class="<?= (in_array($this->request->params['controller'], ['Eligibilities'])) ? 'active' : '' ?>">
                    <a href="<?= $this->Url->build(['controller' => 'eligibilities', 'action' => 'index', 'prefix' => 'mirror']); ?>">
                        <span class="menu-text">Elegiblidad</span>
                    </a>
                </li>
            </ul>
        </li>
        <?php } ?>
        -->
        
        
         <!-- Data Validation -->
         <!--
        <?php if($this->Valid->checkPermission('data_validation')) { ?>
		<li class="<?= (in_array($this->request->params['controller'], ['Countries']) and  $this->request->params['prefix'] == 'validation') ? 'open' : '' ?>">
        	<a href="#" class="menu-dropdown">
            	<i class="menu-icon fa fa-check"></i>
				<span class="menu-text"> Data Validation </span>

				<i class="menu-expand"></i>
        	</a>
			<ul class="submenu">
	            <li class="<?= (in_array($this->request->params['controller'], ['Countries']) and  $this->request->params['prefix'] == 'validation') ? 'open' : '' ?>">
	                <a href="#" class="menu-dropdown">
	                    <span class="menu-text">Configuración</span>
	                </a>
	                <ul class="submenu">
	                    <li class="<?= ($this->request->params['controller'] == 'Countries') ? 'active' : '' ?>">
		                    <a href="<?= $this->Url->build(['controller' => 'Countries', 'action' => 'index', 'prefix' => 'validation']); ?>">
		                        <span class="menu-text">Paises</span>
		                    </a>
		                </li>
	                </ul>
	            </li>
			</ul>
		</li>
		<?php } ?>
		-->
		
        <?php if($this->Valid->checkPermission('client_attribute')) { ?>
        <li class="<?= (isset($this->request->params['prefix']) and in_array($this->request->params['prefix'], ['client_attribute'])) ? 'open' : '' ?>">
        	<a href="#" class="menu-dropdown">
                <i class="menu-icon fa fa-bookmark"></i>
                <span class="menu-text"> Client Attribute</span>

                <i class="menu-expand"></i>
            </a>

            <ul class="submenu">
                <li class="<?= ($this->request->params['controller'] == 'AssignmentsCountries' and $this->request->params['prefix'] == 'client_attribute') ? 'active' : '' ?>">
                    <a href="<?= $this->Url->build(['controller' => 'AssignmentsCountries', 'action' => 'index', 'prefix' => 'client_attribute']); ?>">
                        <span class="menu-text">Por país</span>
                    </a>
                </li>
                <li class="<?= ($this->request->params['controller'] == 'AssignmentsClients' and $this->request->params['prefix'] == 'client_attribute') ? 'active' : '' ?>">
                    <a href="<?= $this->Url->build(['controller' => 'AssignmentsClients', 'action' => 'index', 'prefix' => 'client_attribute']); ?>">
                        <span class="menu-text">Por cliente</span>
                    </a>
                </li>
            </ul>
        </li>
        <?php } ?>
        
        <?php //if($this->Valid->checkPermission('client_services')) { ?>
        <li class="<?= (isset($this->request->params['prefix']) and in_array($this->request->params['prefix'], ['client_services'])) ? 'open' : '' ?>">
        	<a href="#" class="menu-dropdown">
                <i class="menu-icon fa fa-users"></i>
                <span class="menu-text"> Client Services</span>

                <i class="menu-expand"></i>
            </a>

            <ul class="submenu">
                <li class="<?= ($this->request->params['controller'] == 'Nomencs' and $this->request->params['prefix'] == 'client_services') ? 'active' : '' ?>">
                    <a href="#" class="menu-dropdown">
                        <span class="menu-text">Nomenclaturas</span>
                    </a>
                    <ul class="submenu">
	                    <?php
			                $string = file_get_contents( CONFIG . DS . "platform" . DS . "countries.json" );
							$config = json_decode($string, true);
			            ?>
			            <? foreach($config['countries'] as $key => $detail) { ?>
	                    <li class="<?= ($this->request->params['controller'] == 'Nomencs' and $this->request->params['prefix'] == 'client_services' and $this->request->params['pass'][0] == $key) ? 'active' : '' ?>">
		                    <a href="<?= $this->Url->build(['controller' => 'Nomencs', 'action' => 'index', $key, 'prefix' => 'client_services']); ?>">
		                        <span class="menu-text"><?= $detail['name'] ?></span>
		                    </a>
		                </li>
		                <? } ?>
	                </ul>
                </li>
            </ul>
        </li>
        <?php //} ?>
        
        <li class="<?= (isset($this->request->params['prefix']) and in_array($this->request->params['prefix'], ['wpo'])) ? 'open' : '' ?>">
        	<a href="#" class="menu-dropdown">
                <i class="menu-icon fa fa-bar-chart"></i>
                <span class="menu-text"> Control de Cubos</span>

                <i class="menu-expand"></i>
            </a>

            <ul class="submenu">
                <li class="<?= (in_array($this->request->params['controller'], ['Maintenance'])) ? 'active' : '' ?>">
                    <a href="<?= $this->Url->build(['controller' => 'maintenance', 'action' => 'index', 'prefix' => 'wpo']); ?>">
                        <span class="menu-text">Mantenedor</span>
                    </a>
                </li>

                <li class="<?= (in_array($this->request->params['controller'], ['Maintenance'])) ? 'active' : '' ?>">
                    <a href="<?= $this->Url->build(['controller' => 'maintenance', 'action' => 'listcargaproductiva', 'prefix' => 'wpo']); ?>">
                        <span class="menu-text">Carga productiva</span>
                    </a>
                </li>
            </ul>
        </li>
        
        <!-- Shoppix -->
        <!--
        <?php if($this->Valid->checkPermission('shoppix')) { ?>
        <li class="<?= (isset($this->request->params['prefix']) and in_array($this->request->params['prefix'], ['shoppix'])) ? 'open' : '' ?>">
        	<a href="#" class="menu-dropdown">
                <i class="menu-icon fa fa-bookmark"></i>
                <span class="menu-text"> Shoppix</span>

                <i class="menu-expand"></i>
            </a>

            <ul class="submenu">
	            <li class="<?= (in_array($this->request->params['controller'], ['Participants']) and $this->request->params['prefix'] == 'shoppix') ? 'open' : '' ?>">
	                <a href="#" class="menu-dropdown">
	                    <span class="menu-text">Brasil</span>
	                </a>
	                <ul class="submenu">
	                    <li class="<?= ($this->request->params['controller'] == 'Participants') ? 'active' : '' ?>">
		                    <a href="<?= $this->Url->build(['controller' => 'Participants', 'action' => 'index', 'br', 'prefix' => 'shoppix']); ?>">
		                        <span class="menu-text">Participants</span>
		                    </a>
		                </li>
	                </ul>
	            </li>
            </ul>
        </li>
        <?php } ?>
        -->
        


        <!-- Uroboros -->
        <?php if($this->Valid->checkPermission('uroboros')) { ?>
        <li class="<?= (isset($this->request->params['prefix']) and in_array($this->request->params['prefix'], ['uroboros'])) ? 'open' : '' ?>">
            <a href="#" class="menu-dropdown">
                <i class="menu-icon fa fa-envelope"></i>
                <span class="menu-text"> Uroboros</span>

                <i class="menu-expand"></i>
            </a>

            <ul class="submenu">
                <li class="<?= ($this->request->params['controller'] == 'Cases' and $this->request->params['action'] == 'index' and $this->request->params['prefix'] == 'uroboros') ? 'active' : '' ?>">
                    <a href="<?= $this->Url->build(['controller' => 'cases', 'prefix' => 'uroboros']); ?>">
                        <span class="menu-text">Mis casos</span>
                    </a>
                </li>
                <li class="<?= ($this->request->params['controller'] == 'Cases' and $this->request->params['action'] == 'all' and $this->request->params['prefix'] == 'uroboros') ? 'active' : '' ?>">
                    <a href="<?= $this->Url->build(['controller' => 'cases', 'action' => 'all', 'prefix' => 'uroboros']); ?>">
                        <span class="menu-text">Todos los casos</span>
                    </a>
                </li>
            </ul>

        </li>
        <?php } ?>


        <!-- MCM -->
        <?php if($this->Valid->checkPermission('mcm')) { ?>
        <li class="<?= (isset($this->request->params['prefix']) and in_array($this->request->params['prefix'], ['uroboros'])) ? 'open' : '' ?>">
            <a href="#" class="menu-dropdown">
                <i class="menu-icon fa fa-envelope"></i>
                <span class="menu-text"> MCM - Master File</span>

                <i class="menu-expand"></i>
            </a>

            <ul class="submenu">
                <li class="<?= ($this->request->params['controller'] == 'Cases' and $this->request->params['action'] == 'index' and $this->request->params['prefix'] == 'uroboros') ? 'active' : '' ?>">
                    <a href="<?= $this->Url->build(['controller' => 'cases', 'prefix' => 'uroboros']); ?>">
                        <span class="menu-text">Gestion Ruims</span>
                    </a>
                </li>
                <li class="<?= ($this->request->params['controller'] == 'Cases' and $this->request->params['action'] == 'all' and $this->request->params['prefix'] == 'uroboros') ? 'active' : '' ?>">
                    <a href="<?= $this->Url->build(['controller' => 'cases', 'action' => 'all', 'prefix' => 'uroboros']); ?>">
                        <span class="menu-text">Gestión Categorías</span>
                    </a>
                </li>
            </ul>

        </li>
        <?php } ?>
        

        <!-- Configuration -->
        <?php if($this->Valid->checkPermission('Configurations')) { ?>
		<li class="<?= (in_array($this->request->params['controller'], ['Clients', 'Markets', 'Flags', 'Dimensions', 'Attributes', 'Parameters', 'Clusters', 'Countries', 'Fields']) and in_array($this->request->params['prefix'], ['client_attribute', 'fusion'])) ? 'open' : '' ?>">
        	<a href="#" class="menu-dropdown">
            	<i class="menu-icon fa fa-cogs"></i>
				<span class="menu-text"> Configuraciones </span>

				<i class="menu-expand"></i>
        	</a>
			<ul class="submenu">
	            <li class="<?= (in_array($this->request->params['controller'], ['Clients', 'Markets', 'Flags', 'Dimensions', 'Attributes'])) ? 'open' : '' ?>">
	                <a href="#" class="menu-dropdown">
	                    <span class="menu-text">Client Attributes</span>
	                </a>
	                <ul class="submenu">
	                    <li class="<?= ($this->request->params['controller'] == 'Attributes') ? 'active' : '' ?>">
		                    <a href="<?= $this->Url->build(['controller' => 'Attributes', 'action' => 'index', 'prefix' => 'client_attribute']); ?>">
		                        <span class="menu-text">Atributos</span>
		                    </a>
		                </li>
		                <li class="<?= ($this->request->params['controller'] == 'Clients') ? 'active' : '' ?>">
		                    <a href="<?= $this->Url->build(['controller' => 'Clients', 'action' => 'index', 'prefix' => 'client_attribute']); ?>">
		                        <span class="menu-text">Clientes</span>
		                    </a>
		                </li>
		                <li class="<?= ($this->request->params['controller'] == 'Markets') ? 'active' : '' ?>">
		                    <a href="<?= $this->Url->build(['controller' => 'Markets', 'action' => 'index', 'prefix' => 'client_attribute']); ?>">
		                        <span class="menu-text">Mercados</span>
		                    </a>
		                </li>
		                <li class="<?= ($this->request->params['controller'] == 'Dimensions') ? 'active' : '' ?>">
		                    <a href="<?= $this->Url->build(['controller' => 'Dimensions', 'action' => 'index', 'prefix' => 'client_attribute']); ?>">
		                        <span class="menu-text">Dimensiones</span>
		                    </a>
		                </li>
		                <li class="<?= ($this->request->params['controller'] == 'Flags') ? 'active' : '' ?>">
		                    <a href="<?= $this->Url->build(['controller' => 'Flags', 'action' => 'index', 'prefix' => 'client_attribute']); ?>">
		                        <span class="menu-text">Flags</span>
		                    </a>
		                </li>
		            </ul>
	            </li>
	            <li class="<?= (in_array($this->request->params['controller'], ['Clusters', 'Countries', 'Fields']) and  $this->request->params['prefix'] == 'fusion') ? 'open' : '' ?>">
	                <a href="#" class="menu-dropdown">
	                    <span class="menu-text">Fusion OOH + U&S</span>
	                </a>
	                <ul class="submenu">
	                    <li class="<?= ($this->request->params['controller'] == 'Countries' and  $this->request->params['prefix'] == 'fusion') ? 'active' : '' ?>">
		                    <a href="<?= $this->Url->build(['controller' => 'Countries', 'action' => 'index', 'prefix' => 'fusion']); ?>">
		                        <span class="menu-text">Paises</span>
		                    </a>
		                </li>
		                <li class="<?= ($this->request->params['controller'] == 'Fields' and  $this->request->params['prefix'] == 'fusion') ? 'active' : '' ?>">
		                    <a href="<?= $this->Url->build(['controller' => 'Fields', 'action' => 'index', 'prefix' => 'fusion']); ?>">
		                        <span class="menu-text">Campos</span>
		                    </a>
		                </li>
		                <li class="<?= ($this->request->params['controller'] == 'Clusters' and  $this->request->params['prefix'] == 'fusion') ? 'active' : '' ?>">
		                    <a href="<?= $this->Url->build(['controller' => 'Clusters', 'action' => 'index', 'prefix' => 'fusion']); ?>">
		                        <span class="menu-text">Clústers</span>
		                    </a>
		                </li>
		            </ul>
	            </li>
        	</ul>
		</li>
		<?php } ?>
    </ul>
    <!-- /Sidebar Menu -->
</div>
