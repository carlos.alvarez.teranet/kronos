<div class="navbar-container">
    <!-- Navbar Barnd -->
    <div class="navbar-header pull-left">
        <a href="#" class="navbar-brand">
            <small style="position: absolute; top: 5px">
            	<?= $this->Html->image('logoroc.png', ['width' => 50]); ?>
            </small>
        </a>
    </div>
    <!-- /Navbar Barnd -->
    <!-- Sidebar Collapse -->
    <div class="sidebar-collapse" id="sidebar-collapse">
        <i class="collapse-icon fa fa-bars"></i>
    </div>
    <!-- /Sidebar Collapse -->
    <!-- Account Area and Settings --->
    <div class="navbar-header pull-right">
        <div class="navbar-account">
            <ul class="account-area">
	            <!--
                <li>
                    <a class="wave in" id="chat-link" title="Chat" href="#">
                        <i class="icon glyphicon glyphicon-comment"></i>
                    </a>
                </li>
                -->
                <li>
                    <a class="login-area dropdown-toggle" data-toggle="dropdown">
                        <div class="avatar" title="View your public profile">
	                        <?= $this->Html->image('profiles/'.$this->request->session()->read('Auth.User.id').'.jpg', ['width' => 48]); ?>
                        </div>
                        <section>
                            <h2><span class="profile"><span><?= $this->request->session()->read('Auth.User.displayName'); ?></span></span></h2>
                        </section>
                    </a>
                    <!--Login Area Dropdown-->
                    <ul class="pull-right dropdown-menu dropdown-arrow dropdown-login-area">
                        <li class="username"><a><?= $this->request->session()->read('Auth.User.name'); ?></a></li>
                        <li class="email"><a><?= $this->request->session()->read('Auth.User.jobTitle'); ?></a></li>
                        <!--Avatar Area-->
                        <li>
                            <div class="avatar-area">
                                <?= $this->Html->image('profiles/'.$this->request->session()->read('Auth.User.id').'.jpg', ['width' => 128]); ?>
                                <span class="caption">Change Photo</span>
                            </div>
                        </li>
                        <!--Avatar Area-->
                        <li class="edit">
                            <a href="profile.html" class="pull-left">Profile</a>
                            <a href="#" class="pull-right">Setting</a>
                        </li>
                        <!--/Theme Selector Area-->
                        <li class="dropdown-footer">
                            <a href="<?= $this->Url->build(['controller' => 'Users', 'action' => 'logout', 'prefix' => false]); ?>">
                                Sign out
                            </a>
                        </li>
                    </ul>
                    <!--/Login Area Dropdown-->
                </li>
                <!-- /Account Area -->
                <!--Note: notice that setting div must start right after account area list.
                no space must be between these elements-->
                <!-- Settings -->
            </ul>
            <!-- Settings -->
        </div>
    </div>
    <!-- /Account Area and Settings -->
</div>