<?php
$this->Breadcrumbs->add([
    ['title' => 'Configuración'],
    ['title' => 'Clientes']
]);	
?>
<!--Page Related styles-->
<?= $this->Html->css('/assets/css/dataTables.bootstrap.css'); ?>
<?= $this->Flash->render() ?>
<div class="row">
    <div class="col-xs-12 col-md-12">
        <div class="widget">
            <div class="widget-header ">
                <span class="widget-caption">Listado de clientes</span>
                <div class="widget-buttons">
                    <a href="#" data-toggle="maximize">
                        <i class="fa fa-expand"></i>
                    </a>
                    <a href="#" data-toggle="collapse">
                        <i class="fa fa-minus"></i>
                    </a>
                    <a href="#" data-toggle="dispose">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="widget-body">
                <table class="table table-striped table-bordered table-hover" id="simpledatatable">
                    <thead>
                        <tr>
                            <th>
                                IdClient
                            </th>
                            <th>
                                Descricao
                            </th>
                            <th>
                                DtCriacao
                            </th>
                            <th>
                                DtAtualizacao
                            </th>
                            <th>
                                FlgDesativacao
                            </th>
                            <th>
	                            Acciones
                            </th>
                        </tr>
                    </thead>
                    <tbody>
	                    <?php foreach($clients as $client) { ?>
	                    <tr>
		                    <td><?= $client['IdClient'] ?></td>
		                    <td><?= $client['Descricao'] ?></td>
		                    <td><?= $client['DtCriacao']->format('d M Y - h:i A') ?></td>
		                    <td><?= $client['DtAtualizacao']->format('d M Y - h:i A') ?></td>
		                    <td><?= $client['FlgDesativacao'] ?> (<?= $client['flag']['description'] ?>)</td>
		                    <td>
			                    <?= $this->Html->link('<i class="fa fa-edit"></i> Editar', ['action' => 'edit', $client['IdClient']], ['class' => 'btn btn-xs btn-info', 'escape' => false] ); ?>
		                    </td>
	                    </tr>
	                    <?php } ?>
                    </tbody>
                </table>
                <div class="row" style="margin-top: 10px">
                	<div class="col-sm-12">
	                	<?= $this->Html->link('<i class="fa fa-plus"></i> Agregar Cliente', ['controller' => 'clients', 'action' => 'add'], ['class' => 'btn btn-blue', 'escape' => false]) ?>
                	</div>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->Html->script('/assets/js/datatable/jquery.dataTables.min.js') ?>
<?= $this->Html->script('/assets/js/datatable/ZeroClipboard.js') ?>
<?= $this->Html->script('/assets/js/datatable/dataTables.tableTools.min.js') ?>
<?= $this->Html->script('/assets/js/datatable/dataTables.bootstrap.min.js') ?>
<script language="javascript" type="text/javascript">
var oTable = $('#simpledatatable').dataTable({
    "sDom": "Tflt<'row DTTTFooter'<'col-sm-6'i><'col-sm-6'p>>",
    "iDisplayLength": 10,
    "oTableTools": {
        "aButtons": [
            "copy", "csv", "xls", "pdf", "print"
        ],
        "sSwfPath": "assets/swf/copy_csv_xls_pdf.swf"
    },
    "language": {
        "search": "",
        "sLengthMenu": "_MENU_",
        "oPaginate": {
            "sPrevious": "Anterior",
            "sNext": "Siguiente"
        }
    },
    "aaSorting": []
});
</script>