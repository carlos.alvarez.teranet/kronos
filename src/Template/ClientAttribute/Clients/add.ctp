<?php
$this->Breadcrumbs->add([
    ['title' => 'Configuración'],
    ['title' => 'Clientes', 'url' => ['controller' => 'clients', 'action' => 'index' ]],
    ['title' => 'Agregar']
]);	
?>
<?= $this->Flash->render() ?>
<div class="row">
    <div class="col-lg-6 col-sm-6 col-xs-12">
        <div class="widget flat">
            <div class="widget-header bg-blue">
                <span class="widget-caption"><i class="fa fa-plus"></i> Formulario nuevo cliente</span>
            </div>
            <div class="widget-body">
                <div id="registration-form">
	                <?= $this->Form->create($client) ?>
                    <form role="form">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="exampleInputEmail2">Descricao <span style="color: red">*</span></label>
                                    <span class="input-icon icon-right">
                                    	<?= $this->Form->text('Descricao', ['label' => false, 'class' => 'form-control', 'placeholder' => 'Ingrese descripción / nombre de cliente']) ?>
                                        <i class="fa fa-info blue"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="exampleInputEmail2">Flag Desactivacion</label>
                                    <span class="input-icon icon-right">
                                    	<?= $this->Form->select('FlgDesativacao', $flags, ['label' => false, 'class' => 'form-control']) ?>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="exampleInputEmail2">Comentarios</label>
                                    <span class="input-icon icon-right">
                                    	<?= $this->Form->textarea('Comentarios', ['label' => false, 'rows' => 5, 'class' => 'form-control']) ?>
                                        <i class="fa fa-bars blue"></i>
                                    </span>
                                    <p class="help-block">Incorpore un comentario que haga referencia al cliente</p>
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-blue">Crear cliente</button>
                        <?= $this->Html->link('<i class="fa fa-reply"></i> Volver', ['controller' => 'clients', 'action' => 'index'], ['class' => 'btn btn-info', 'escape' => false]); ?>
                    </form>
                    <?= $this->Form->end() ?>
                </div>
            </div>
        </div>
    </div>
</div>