<?php
$this->Breadcrumbs->add([
    ['title' => 'Client Attribute']
]);	
?>
<!--Page Related styles-->
<?= $this->Html->css('/assets/css/dataTables.bootstrap.css'); ?>
<?= $this->Flash->render() ?>
<div class="row">
	<? foreach($clients as $client) { ?>
	<? if(!array_key_exists($client['IdClient'], $available)) { continue; } ?>
	<div class="col-xs-3 col-md-3">
        <div class="widget">
            <div class="widget-body">
	            <div class="row">
                	<div class="col-sm-8">
	                	<h2 class="pull-left"><i class="fa fa-circle <?= (empty($client['attributes_pendants_details'])) ? 'success' : 'warning'; ?>"></i> <?=$this->Html->link($client['Descricao'], ['controller' => 'assignments-clients', 'action' => 'display', md5($client['Descricao'])]); ?></h2>
	                	<div class="row">
			                <div class="col-sm-12">
				                <hr style="margin-top: 0px; margin-bottom: 0px">
			                </div>
		                </div>
                	</div>
                	<div class="col-sm-4">
						<div class="pull-right">
		                	<center>
		                		<h3><?= (array_key_exists($client['IdClient'], $pendants)) ? $pendants[$client['IdClient']] : 0 ?></h3>
	                			<!-- <h3><?= ($client['attributes_pendants_details']) ? $client['attributes_pendants_details'][0]['Pendants'] : 0 ?></h3> -->
	                			<h6>Asignaciones<br>Pendientes</h6>
		                	</center>
	                	</div>
					</div>
                </div>
            </div>
        </div>
    </div>
    <? } ?>
</div>