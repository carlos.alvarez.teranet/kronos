<?php
$this->Breadcrumbs->add([
    ['title' => 'Client Attribute', 'url' => ['controller' => 'assignments-clients', 'action' => 'index', 'prefix' => 'client_attribute' ]],
    ['title' => $attribute['client']['Descricao'], 'url' => ['controller' => 'assignments-clients', 'action' => 'display', md5($attribute['client']['Descricao']), 'prefix' => 'client_attribute']],
    ['title' => $attribute['Descricao']." - ".$country['Descricao']]
]);	
?>
<?= $this->Html->css('/assets/css/dataTables.bootstrap.css'); ?>
<div id="render">
	<?= $this->Flash->render() ?>
</div>
<div class="row">
    <div class="col-lg-12 col-sm-12 col-xs-12">
        <div class="widget flat">
            <div class="widget-body">
	            <h2>
		            <i class="fa fa-circle success"></i>&nbsp;
		            <?=$attribute['Descricao'] ?>
		        </h2>
	            <p>Market: <?= $attribute['market']['Descricao'] ?> / Client: <?= $attribute['client']['Descricao'] ?> / Country: <?= $country['Descricao'] ?></p>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12 col-sm-12 col-xs-12">
        <div class="tabbable">
            <ul class="nav nav-tabs" id="myTab">
                <li class="active">
                    <a data-toggle="tab" href="#tab2">
                        Pendientes <span class="badge badge-danger" id="span-badge"><?= $pendants ?></span>
                    </a>
                </li>
                <li>
                    <a data-toggle="tab" href="#tab3">
                        <i class="fa fa-file-excel-o" aria-hidden="true"></i> Excel <span class="badge badge-danger"></span>
                    </a>
                </li>
            </ul>
            <div class="tab-content">
                <div id="tab2" class="tab-pane in active">
	                <form class="form" method="post" id="form-pendant">
		                <div class="row">
		                	<div class="col-sm-12">
				                <table class="table table-striped table-bordered table-hover" id="pendant_table">
				                    <thead>
				                        <tr>
					                        <th>
						                        <label>
	                                                <input id="check-all" type="checkbox">
	                                                <span class="text"></span>
	                                            </label>
					                        </th>
					                        <? if(in_array('IdArtigo', $select)) { ?>
					                        	<th>IdArtigo</th>
					                        	<th>Barcode</th>
					                        	<th>Produto</th>
					                        	<th>Sub</th>
					                        	<th>Fabricante</th>
					                        	<th>Marca</th>
					                        	<th>Características</th>
					                        <? } else { ?>
						                        <? foreach($select as $item) { ?>
						                        <? if(in_array($item, $function)) { continue; } ?>
						                        <th><?= $item ?></th>
						                        <? } ?>
						                    <? } ?>
						                       <th>Value</th>
				                        </tr>
				                    </thead>
				                    <tbody>
					                    <? foreach($results as $result) { ?>
						                    <tr>
							                    <td>
								                    <label>
	                                                    <input class="checkbox" type="checkbox">
	                                                    <span class="text"></span>
	                                                </label>
							                    </td>
							                    <? if(in_array('IdArtigo', $select)) { ?>
													<td><?= $result['IdArtigo'] ?></td>
													<td><?= $result['CodBar'] ?></td>
													<td><?= $result['Produto'] ?> (<?= $result['IdProduto'] ?>)</td>
													<td><?= $result['Sub'] ?> (<?= $result['IdSub'] ?>)</td>
													<td><?= $result['Fabricante'] ?> (<?= $result['IdFabricante'] ?>)</td>
													<td><?= $result['Marca'] ?> (<?= $result['IdMarca'] ?>)</td>
													<td style="font-size: smaller; width: 30%">
														<?	
															$attrs = []; 
															foreach( ['01', '02', '03', '04', '05', '06', '07', '08', '09'] as $attr) {
																if($result['Clas'.$attr]) {
																	$attrs[] = $result['Clas'.$attr];
																}
															}
														?>
														<?= implode(" | ", $attrs) ?>
													</td>
												<? } else { ?>
								                    <? foreach($select as $key => $item) { ?>
								                    <? if(in_array($item, $function)) { continue; } ?>
								                    <td><?= $result[$item] ?> (<?= $result[$select[($key-1)]] ?>)</td>
								                    <? } ?>
							                    <? } ?>
							                    <td style="padding: 3px;">
								                    <?
									                $datas = [
										                'class' => 'form-control',
										                'empty' => 'Seleccione valor'
									                ];
									                foreach($forms as $key => $element) { 
										                echo $this->Form->hidden('Config.'.$result['Id'].'.Dim0'.$element['Orden'], ['value' => $result[$element['Descricao']]]);
										            }
									                ?>
							                    	<?= $this->Form->select('Config.'.$result['Id'].'.IdValue', $values, $datas); ?>
							                    </td>
						                    </tr>
						                    <? $result['Id']++; ?>
					                    <? } ?>
				                    </tbody>
					            </table>
		                	</div>
		                </div>
			            <div class="row" style="margin-top: 10px;">
			            	<div class="col-sm-12">
				            	<button class="btn btn-blue pull-right" type="submit"><i class="fa fa-save"></i> Guardar</button>
				            	<button disabled="" class="btn btn-blue pull-right" style="display: none" type="button"><i class="fa fa-spin fa-spinner"></i> Guardando</button>
			            	</div>
			            </div>
	                </form>
                </div>
                <div id="tab3" class="tab-pane in">
	                <div class="row">
	                	<div class="col-sm-6">
	                		<p>Descarga de archivo para edición masiva</p>
	                		<a id="prepare_download_1" href="<?= $this->Url->build(['controller' => 'assignments-countries', 'action' => 'download', $country['Id'], $attribute['IdClientAttribute'], true, 'prefix' => 'client_attribute', '_ext' => 'xlsx']) ?>" class="btn btn-primary"><i class="fa fa-download" aria-hidden="true"></i> Descarga de archivo (Pendientes)</a>
	                		<a id="prepare_download_2" href="<?= $this->Url->build(['controller' => 'assignments-countries', 'action' => 'download', $country['Id'], $attribute['IdClientAttribute'], false, 'prefix' => 'client_attribute', '_ext' => 'xlsx']) ?>" class="btn btn-default"><i class="fa fa-download" aria-hidden="true"></i> Descarga de archivo (Completo)</a>
							<button style="display: none" id="loading_download" disabled="" class="btn btn-primary"><i class="fa fa-pulse fa-spinner" aria-hidden="true"></i> Descargando</button>
	                	</div>
	                </div>
	                <hr>
	                <div class="row">
	                	<div class="col-sm-6">
	                		<p>Subir nueva configuración masiva</p>
	                		<form class="form" enctype="multipart/form-data" method="post" action="<?= $this->Url->build(['action' => 'upload', $country['Id'], $attribute['IdClientAttribute'], 'prefix' => 'client_attribute']) ?>">
		                		<div class="input-group">
					                <label class="input-group-btn">
					                    <span class="btn btn-success">
					                        ... Seleccionar archivo <input name="excel" type="file" style="display: none;" multiple="">
					                    </span>
					                </label>
					                <input id="filename" type="text" class="form-control" readonly="">
					                <label class="input-group-btn">
					                	<button id="upload" disabled="" type="submit" class="btn btn-info"><i class="fa fa-upload"></i> Subir</button>
					                	<button id="remove" disabled="" type="reset" class="btn btn-danger"><i class="fa fa-times"></i> Remover</button>
					                </label>
					            </div>
	                		</form>
	                	</div>
	                </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade bs-pendant-modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="mySmallModalLabel">Asignar valor seleccionados</h4>
            </div>
            <div class="modal-body">
	            <div class="form-group">
		            <?= $this->Form->select('MassivePendantValue', $values, ['class' => 'form-control', 'empty' => 'Seleccione valor']); ?>
	            </div>
                <div class="form-group">
                	<button class="btn btn-warning btn-block" id="MassivePendantButton" type="button">Asignar valor</button>
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<script lang="javascript" type="text/javascript">
$('document').ready(function() {    
    
	$("input:file").change(function () {
		console.log('Seleccionando archivo');
    	var fileName = $(this).val();
    	
    	if(fileName) {
	    	$('#upload').removeAttr('disabled');
	    	$('#remove').removeAttr('disabled');
    	} else {
	    	$('#upload').prop('disabled', true);
	    	$('#remove').prop('disabled', true);
    	}
    	
    	var partsOfStr = fileName.split('\\');
		$("#filename").val(partsOfStr[2]);
    });
    
    $('#prepare_download_1').click(function(e) {
	    console.log('Inicia descarga de archivo');
	    blockUIForDownload();
    });
    $('#prepare_download_2').click(function(e) {
	    console.log('Inicia descarga de archivo');
	    blockUIForDownload();
    });
    
    var fileDownloadCheckTimer;
	function blockUIForDownload() {
		//$.cookie('fileDownloadToken', 'pendant', { path: '/' });
		$('#prepare_download_1').hide();
		$('#prepare_download_2').hide();
		$('#loading_download').show();
		fileDownloadCheckTimer = window.setInterval(function () {
			var cookieValue = $.cookie('fileDownloadToken');
			if (cookieValue == 'finish')
			finishDownload();
		}, 1000);
	}
	
	function finishDownload() {
		console.log('Archivo finalizado');
		window.clearInterval(fileDownloadCheckTimer);
		$.removeCookie('fileDownloadToken'); //clears this cookie value
		$('#prepare_download_1').show();
		$('#prepare_download_2').show();
		$('#loading_download').hide();
	}
	 	
});
</script>
<?= $this->Html->script('/assets/js/datatable/jquery.dataTables.min.js') ?>
<?= $this->Html->script('/assets/js/datatable/ZeroClipboard.js') ?>
<?= $this->Html->script('/assets/js/datatable/dataTables.tableTools.min.js') ?>
<?= $this->Html->script('/assets/js/datatable/dataTables.bootstrap.min.js') ?>
<?= $this->Html->script('/assets/js/jquery.cookie.js') ?>
<script language="javascript" type="text/javascript">
var oTable = $('table').dataTable({
    "sDom": "Tflt<'row DTTTFooter'<'col-sm-6'i><'col-sm-6'p>>",
    "iDisplayLength": 100,
    "aLengthMenu": [[50, 100, 250, 500, -1], [50, 100, 250, 500, "All"]],
    "oTableTools": {
        "aButtons": [
            "copy", "csv", "xls", "pdf", "print",
            {
                "sExtends":    "text",
                "sButtonText": "Modificar seleccionados",
                "fnClick": function ( nButton, oConfig, oFlash ) {
	                $('.bs-pendant-modal').modal('show');                    
                },
                "sButtonClass": "hide pendant_button btn-warning"
            }
        ],
        "sSwfPath": "assets/swf/copy_csv_xls_pdf.swf"
    },
    "language": {
        "search": "",
        "sLengthMenu": "_MENU_",
        "oPaginate": {
            "sPrevious": "Anterior",
            "sNext": "Siguiente"
        }
    },
    "bStateSave": true,
    "aaSorting": [],
    "aoColumnDefs": [ { "bSortable": false, "aTargets": [ 0 ] } ]
});

$('document').ready(function(e) {
	// Check All
	$('#check-all').click(function() {
		var status = $(this).is(":checked");
		if(status) {
			$('.checkbox').prop('checked', true);
		} else {
			$('.checkbox').prop('checked', false);
		}
		$('.checkbox').change();
	});
	
	oTable.$('.checkbox').change(function(e) {
		var total = 0;
		oTable.$('.checkbox').each(function() {
			var status = $(this).is(":checked");
			if(status) {
				total++;
			}
		});
		if(total > 0) {
			$('.pendant_button').removeClass('hide').show();
			$('.pendant_button').html('Modificar seleccionados ['+total+']');
		} else {
			$('.pendant_button').addClass('hide').hide();
		}
	});
	
	$('#MassivePendantButton').click(function() {
		var val = $('select[name=MassivePendantValue]').val();
		oTable.$('.checkbox:checked').each(function() {
			$(this).closest('tr').find('select').val(val);
		});
		$('.bs-pendant-modal').modal('hide'); 
	});
	
	//Capturar submit
	$('#form-pendant').on('submit', function(e) {
		
		e.preventDefault();
		changes = false;
		
		$submit = $(this).find('button:submit');
		$button = $(this).find('button:submit').next();
		
		$submit.hide();
		$button.show();
		
        var data = oTable.$('input, select').serialize();

        // console.log('Submitting data', data);

        // Submit form data via ajax
        $.ajax({
			type: "POST",
			url: '<?= $this->Url->build(['action' => 'update', $country['Id'], $attribute['IdClientAttribute'], '_ext' => 'json'], true) ?>',
			data: data,
			success: function(data){
				if(data.response.error === true) {
					$('#render').html('<div onclick="this.classList.add(\'hidden\')" class="fade in alert alert-'+data.response.type+'"><button class="close" data-dismiss="alert">×</button>'+data.response.message+'</div>');
					$submit.show();
					$button.hide();
				} else {
					location.reload();
				}
			}
        });

        // Prevent form submission
        return false;
    } );
	
	// Prevent Unsave
	var changes = false;
	$("select").change(function() {
		changes = false;
		oTable.$('select').each(function() {
			if($(this).val().length > 0) {
				changes = true;
			}
		});
	});
	
	$(window).bind('beforeunload', function(e) {
		if(changes) {
			return false;
		}
	});
	
});
</script>