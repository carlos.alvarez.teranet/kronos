<?php
$this->Breadcrumbs->add([
    ['title' => 'Client Attribute', 'url' => ['controller' => 'assignments-clients', 'action' => 'index', 'prefix' => 'client_attribute' ]],
    ['title' => $attributes->first()['Client']]
]);	
?>
<!--Page Related styles-->
<?= $this->Html->css('/assets/css/dataTables.bootstrap.css'); ?>
<?= $this->Flash->render() ?>
<div class="row">
    <div class="col-xs-12 col-md-12">
        <div class="widget">
            <div class="widget-header ">
                <span class="widget-caption">Listado de asignaciones</span>
            </div>
            <div class="widget-body">
                <table class="table table-striped table-bordered table-hover" id="simpledatatable">
                    <thead>
                        <tr>
                            <th>
                                Status
                            </th>
                            <th>
                                Descricao
                            </th>
                            <th>
                                Market
                            </th>
                            <th>
                                Pais
                            </th>
                            <th>
                                Pendants
                            </th>
                        </tr>
                    </thead>
                    <tbody>
	                    <? foreach($attributes as $attribute) { ?>
	                    <tr>
		                    <td>
			                    <? if($attribute['Pendants'] == 0) { ?>
			                    <i class="fa fa-2x fa-circle success"></i>
			                    <? } else { ?>
			                    <i class="fa fa-2x fa-circle warning"></i>
			                    <? } ?>
		                    </td>
		                    <td><?= $this->Html->link($descriptions[$attribute['Country_id'].'-'.$attribute['ClientAttribute_id']]['name'] . " (".$attribute['ClientAttribute_id'].")" , ['action' => 'detail', $attribute['Country_id'], $attribute['ClientAttribute_id'], 'prefix' => 'client_attribute']); ?></td>
		                    <td><?= $descriptions[$attribute['Country_id'].'-'.$attribute['ClientAttribute_id']]['market'] ?></td>
		                    <td><?= $countries[$attribute['Country_id']] ?></td>
		                    <td><?= $attribute['Pendants'] ?></td>
	                    </tr>
	                    <? } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<?= $this->Html->script('/assets/js/datatable/jquery.dataTables.min.js') ?>
<?= $this->Html->script('/assets/js/datatable/ZeroClipboard.js') ?>
<?= $this->Html->script('/assets/js/datatable/dataTables.tableTools.min.js') ?>
<?= $this->Html->script('/assets/js/datatable/dataTables.bootstrap.min.js') ?>
<script language="javascript" type="text/javascript">
var oTable = $('#simpledatatable').dataTable({
    "sDom": "Tflt<'row DTTTFooter'<'col-sm-6'i><'col-sm-6'p>>",
    "iDisplayLength": 10,
    "oTableTools": {
        "aButtons": [
            "copy", "csv", "xls", "pdf", "print"
        ],
        "sSwfPath": "assets/swf/copy_csv_xls_pdf.swf"
    },
    "language": {
        "search": "",
        "sLengthMenu": "_MENU_",
        "oPaginate": {
            "sPrevious": "Anterior",
            "sNext": "Siguiente"
        }
    },
    "aaSorting": [[ 3, "asc" ], [ 1, "asc" ]],
    "bPaginate": false,
    "bInfo": false
});
</script>