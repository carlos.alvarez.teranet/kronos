<?php
$this->Breadcrumbs->add([
    ['title' => 'Configuración'],
    ['title' => 'Flags', 'url' => ['controller' => 'flags', 'action' => 'index' ]],
    ['title' => 'Editar']
]);	
$myTemplates = [
	'radio' => '<input type="radio" name="{{name}}" value="{{value}}"{{attrs}}><span class="text"></span>',
	'radioWrapper' => '<div class="radio">{{label}}</div>'
]; 
$this->Form->templates($myTemplates);
?>
<?= $this->Flash->render() ?>
<div class="row">
    <div class="col-lg-6 col-sm-6 col-xs-12">
        <div class="widget flat">
            <div class="widget-header bg-blue">
                <span class="widget-caption"><i class="fa fa-plus"></i> Formulario edición flag</span>
            </div>
            <div class="widget-body">
                <div id="registration-form">
	                <?= $this->Form->create($flag) ?>
                    <form role="form">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="code">Código <span style="color: red">*</span></label>
                                    <span class="input-icon icon-right">
                                    	<?= $this->Form->text('code', ['label' => false, 'class' => 'form-control', 'placeholder' => 'Ingrese código']) ?>
                                        <i class="fa fa-info blue"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="description">Descripción <span style="color: red">*</span></label>
                                    <span class="input-icon icon-right">
                                    	<?= $this->Form->text('description', ['label' => false, 'class' => 'form-control', 'placeholder' => 'Ingrese descripcción del flag']) ?>
                                        <i class="fa fa-info blue"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
	                        <div class="col-sm-12">
		                        <div class="control-group">
			                        <label for="active">Status inicial <span style="color: red">*</span></label>
									<?= $this->Form->radio('active', [
										['value' => 1, 'text' => 'Activo', 'class' => 'colored-green'],
										['value' => 0, 'text' => 'Inactivo', 'class' => 'colored-red']
									]); ?>
		                        </div>
	                        </div>
                        </div>
                        <button type="submit" class="btn btn-blue">Editar flag</button>
                        <?= $this->Html->link('<i class="fa fa-reply"></i> Volver', ['controller' => 'flags', 'action' => 'index'], ['class' => 'btn btn-info', 'escape' => false]); ?>
                    </form>
                    <?= $this->Form->end() ?>
                </div>
            </div>
        </div>
    </div>
</div>