<?php
$this->Breadcrumbs->add([
    ['title' => 'Configuración'],
    ['title' => 'Atributos', 'url' => ['controller' => 'dimensions', 'action' => 'index' ]],
    ['title' => 'Agregar']
]);	
?>
<?= $this->Flash->render() ?>
<div class="row">
    <div class="col-lg-6 col-sm-6 col-xs-12">
        <div class="widget flat">
            <div class="widget-header bg-blue">
                <span class="widget-caption"><i class="fa fa-plus"></i> Formulario nuevo atributo</span>
            </div>
            <div class="widget-body">
                <div id="registration-form">
	                <?= $this->Form->create($attribute) ?>
                    <form role="form">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="exampleInputEmail2">Descricao <span style="color: red">*</span></label>
                                    <span class="input-icon icon-right">
                                    	<?= $this->Form->text('Descricao', ['label' => false, 'class' => 'form-control', 'placeholder' => 'Ingrese descripción / nombre de dimensión']) ?>
                                        <i class="fa fa-info blue"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="exampleInputEmail2">Cliente</label>
                                    <span class="input-icon icon-right">
                                    	<?= $this->Form->select('IdClient', $clients, ['label' => false, 'class' => 'form-control', 'empty' => 'Seleccione cliente']) ?>
                                    </span>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="exampleInputEmail2">Mercado</label>
                                    <span class="input-icon icon-right">
                                    	<?= $this->Form->select('IdMarket', $markets, ['label' => false, 'class' => 'form-control', 'empty' => 'Seleccione mercado']) ?>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="exampleInputEmail2">Flag Desactivacion</label>
                                    <span class="input-icon icon-right">
                                    	<?= $this->Form->select('FlgDesativacao', $flags, ['label' => false, 'class' => 'form-control']) ?>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-blue">Crear atributo de cliente</button>
                        <?= $this->Html->link('<i class="fa fa-reply"></i> Volver', ['controller' => 'attributes', 'action' => 'index'], ['class' => 'btn btn-info', 'escape' => false]); ?>
                    </form>
                    <?= $this->Form->end() ?>
                </div>
            </div>
        </div>
    </div>
</div>