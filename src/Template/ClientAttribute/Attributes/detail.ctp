<?php
$this->Breadcrumbs->add([
    ['title' => 'Configuración'],
    ['title' => 'Atributos', 'url' => ['controller' => 'attributes', 'action' => 'index' ]],
    ['title' => 'Detalle']
]);	
?>
<?= $this->Flash->render() ?>
<div class="row">
    <div class="col-lg-12 col-sm-12 col-xs-12">
        <div class="widget flat">
            <div class="widget-body">
	            <h2>
		            <i class="fa fa-circle success"></i>&nbsp;
		            <?=$attribute['Descricao'] ?>
		        </h2>
	            <p><strong>Cliente:</strong> <?= $attribute['client']['Descricao'] ?> - <strong>Mercado:</strong> <?= $attribute['market']['Descricao'] ?></p>
            </div>
        </div>
    </div>
</div>
<div class="row">
	<div class="col-lg-6 col-sm-6 col-xs-6">
		<div class="widget">
			<div class="widget-body">
				<h4>Agregar nueva dimensión</h4>
				<div class="row">
		            <?= $this->Form->create($attribute); ?>
                    <div class="col-lg-4 col-md-6">
                        <div class="form-group">
                            <label for="">Dimension <span style="color: red">*</span></label>
                            <span class="input-icon icon-right">
                            	<?= $this->Form->select('IdDimension', $dimensions, ['label' => false, 'class' => 'form-control select2']) ?>
                            </span>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
	                    <button style="margin-top: 23px;" class="btn btn-success"><i class="fa fa-plus"></i> Agregar</button>
                    </div>
                    <?= $this->Form->end(); ?>
                </div>
			</div>
            <div class="widget-header ">
                <span class="widget-caption">
                	Dimensiones utilizadas
                </span>
            </div>
            <div class="widget-body">
	            <table class="table table-responsive table-bordered table-hover table-striped">
	            	<thead>
		            	<tr>
			            	<th width="40px">
                                <label>
                                    <input name="check-subs" type="checkbox">
                                    <span class="text"></span>
                                </label>
			            	</th>
			            	<th>Orden</th>
			            	<th>IdDimension</th>
			            	<th>Dimension</th>
		            	</tr>
	            	</thead>
	            	<tbody id="draggable">
		            	<? foreach($attribute['dimensions'] as $dimension) { ?>
		            	<tr id="item-<?= $dimension['IdDimension'] ?>">
			            	<td width="40px">
				            	<label>
                                    <input name="check-cd[]" value="" type="checkbox">
                                    <span class="text"></span>
                                </label>
			            	</td>
			            	<td><?= $dimension['_joinData']['Orden'] ?></td>
			            	<td><?= $dimension['IdDimension'] ?></td>
			            	<td><?= $dimension['Descricao'] ?></td>
		            	</tr>
		            	<? } ?>
	            	</tbody>
	            </table>
            </div>
		</div>
	</div>
</div>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script lang="javascript" type="text/javascript">
$('document').ready(function(e) {
	
	$("#draggable").sortable({
		helper: fixWidthHelper,
		axis: 'y',
		update: function (event, ui) {
	        var data = $(this).sortable('serialize');
			$.post('<?= $this->Url->build(['action' => 'order', $attribute['IdClientAttribute'], '_ext' => 'json']); ?>', data, function(response) {
				if(response.error === false) {
					location.reload();
				}
			}, 'json');
	    }
	});
	
	function fixWidthHelper(e, ui) {
	    ui.children().each(function() {
	        $(this).width($(this).width());
	    });
	    return ui;
	}
	
});
</script>