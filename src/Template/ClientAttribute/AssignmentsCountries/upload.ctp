<?php
$this->Breadcrumbs->add([
    ['title' => 'Client Attribute', 'url' => ['controller' => 'assignments', 'action' => 'index', 'prefix' => 'client_attribute' ]],
    ['title' => $country['Descricao'], 'url' => ['controller' => 'assignments', 'action' => 'display', $country['Id'], 'prefix' => 'client_attribute']],
    ['title' => $attribute['Descricao'], 'url' => ['controller' => 'assignments', 'action' => 'detail', $country['Id'], $attribute['IdClientAttribute'], 'prefix' => 'client_attribute']],
    ['title' => 'Upload Excel']
]);	
?>
<?= $this->Html->css('/assets/css/dataTables.bootstrap.css'); ?>
<div id="render">
	<?= $this->Flash->render() ?>
</div>
<div class="row">
    <div class="col-lg-12 col-sm-12 col-xs-12">
        <div class="widget flat">
            <div class="widget-body">
	            <h2>
		            <i class="fa fa-circle success"></i>&nbsp;
		            <?=$attribute['Descricao'] ?>
		        </h2>
	            <p>Market: <?= $attribute['market']['Descricao'] ?> / Client: <?= $attribute['client']['Descricao'] ?> / Country: <?= $country['Descricao'] ?></p>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-xs-12 col-md-12">
        <div class="widget">
            <div class="widget-header ">
                <span class="widget-caption">Listado de cambios detectados</span>
                <div class="widget-buttons">
                    <a href="#" data-toggle="maximize">
                        <i class="fa fa-expand"></i>
                    </a>
                    <a href="#" data-toggle="collapse">
                        <i class="fa fa-minus"></i>
                    </a>
                </div>
            </div>
            <div class="widget-body">
	            <div class="row">
		            <div class="col-lg-12">
			            <?php if(empty($data)) { ?>
			            <table class="table table-responsive table-striped table-bordered table-hover">
		                    <tbody>
			                    <tr>
				                    <td>
					                    <center><h4>No existen cambios detectados en el archivo</h4></center>
				                    </td>
			                    </tr>
		                    </tbody>
			            </table>
			            <?php } else { ?>
			            <table class="table table-responsive table-striped table-bordered table-hover">
		                    <thead>
			                    <tr>
				                    <? if(in_array('IdArtigo', array_keys($data[0]))) { ?>
			                        	<th>IdArtigo</th>
			                        	<th>Barcode</th>
			                        	<th>Produto</th>
			                        	<th>Sub</th>
			                        	<th>Fabricante</th>
			                        	<th>Marca</th>
			                        	<th>Características</th>
			                        	<th>Value</th>
			                        	<th>Original</th>
			                        <? } else { ?>
				                        <?php foreach($data[0] as $header => $values) { ?>
						                    <?php if($header == 'Change') continue; ?>
						                    <th><?= $header ?></th>
					                    <? } ?>
				                    <? } ?>
				                    
				                    
			                    </tr>
		                    </thead>
		                    <tbody>
			                    <?php foreach($data as $item) { ?>
			                    <tr>
				                    <?php if(in_array('IdArtigo', array_keys($data[0]))) { ?>
				                    	<td><?=$item['IdArtigo'] ?></td>
				                    	<td><?=$item['CodBar'] ?></td>
				                    	<td><?=$item['Produto'] ?></td>
				                    	<td><?=$item['Sub'] ?></td>
				                    	<td><?=$item['Fabricante'] ?></td>
				                    	<td><?=$item['Marca'] ?></td>
				                    	<td style="font-size: smaller; width: 30%">
					                    	<?	
												$attrs = []; 
												foreach( ['01', '02', '03', '04', '05', '06', '07', '08', '09'] as $attr) {
													if($item['Clas'.$attr]) {
														$attrs[] = $item['Clas'.$attr];
													}
												}
											?>
											<?= implode(" | ", $attrs) ?>
				                    	</td>
				                    	<td style="background-color: #ffd402"><?=$item['Value'] ?></td>
				                    	<td style="background-color: #00adff61"><?=$item['Original'] ?></td>
				                    <?php } else { ?>
					                    <?php foreach($item as $index => $element) { ?>
					                    <?php if($index == 'Change') continue; ?>
					                    <?php
							                if($index == 'Value') {
								                $color = '#ffd402';
							                } else if ($index == 'Original') {
								                $color = '#00adff61';
							                } else {
								                $color = 'white';
							                }
						                ?>
					                    <td style="background-color: <?= $color ?>"><?= ($element) ? $element : "(Empty)"; ?></td>
					                    <?php } ?>
									<?php } ?>
			                    </tr>
			                    <?php } ?>
		                    </tbody>
			            </table>
			            <?php } ?>
		            </div>
	            </div>
            </div>
            <div class="widget-body">
	            <?php if(count($data)) { ?>
	            <a href="<?= $this->Url->build(['controller' => 'assignments-countries', 'action' => 'process', $this->Crypt->toLetters($save['Id']), 'prefix' => 'client_attribute']) ?>" class="btn btn-success"><i class="fa fa-save"></i> Aplicar cambios</a>
	            <?php } ?>
	            <a href="<?= $this->Url->build(['controller' => 'assignments-countries', 'action' => 'detail', $country['Id'], $attribute['IdClientAttribute'], 'prefix' => 'client_attribute']) ?>" class="btn btn-danger"><i class="fa fa-reply"></i> Volver</a>
            </div>
        </div>
    </div>
</div>