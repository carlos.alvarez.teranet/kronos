<?php
function num2alpha($n)
{
    for($r = ""; $n >= 0; $n = intval($n / 26) - 1)
        $r = chr($n%26 + 0x41) . $r;
    return $r;
}

$this->PhpExcel->getActiveSheet()->setCellValue('A2', $attribute['Descricao']);
$this->PhpExcel->getActiveSheet()->getStyle("A2")->getFont()->setSize(18);

$this->PhpExcel->getActiveSheet()->setCellValue('A3', 'Market: '.$attribute['market']['Descricao']);
$this->PhpExcel->getActiveSheet()->getStyle("A3")->getFont()->setSize(12);

$this->PhpExcel->getActiveSheet()->setCellValue('A4', 'Client: '.$attribute['client']['Descricao']);
$this->PhpExcel->getActiveSheet()->getStyle("A4")->getFont()->setSize(12);

$this->PhpExcel->getActiveSheet()->setCellValue('D3', 'Fecha: '.date('d-m-Y h:i a'));
$this->PhpExcel->getActiveSheet()->getStyle("D3")->getFont()->setSize(12);

$this->PhpExcel->getActiveSheet()->setCellValue('D4', 'País: '.$country['Descricao']);
$this->PhpExcel->getActiveSheet()->getStyle("D4")->getFont()->setSize(12);


// Poblado de tabla
$row = 7;
if($results) {
	foreach($results as $key => $result) {
		
		foreach($result as $index => $value) {
			
			if($index == 'Value') {
				
				// Lista validada
				$objValidation = $this->PhpExcel->getActiveSheet()->getCellByColumnAndRow( array_search($index, array_keys($result)) , ($key + $row))->getDataValidation();
			    $objValidation->setType(PHPExcel_Cell_DataValidation::TYPE_LIST);
			    $objValidation->setErrorStyle(PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
			    $objValidation->setAllowBlank(true);
			    $objValidation->setShowDropDown(true);
			    $objValidation->setErrorTitle('Input error');
			    $objValidation->setError('Value is not in list');
			    $objValidation->setFormula1('\'List_Sheet\'!$A$1:$A$'.count($values));
			    
			    // Formato condicional
			    $objConditional1 = new PHPExcel_Style_Conditional();
			    $objConditional1->setConditionType(PHPExcel_Style_Conditional::CONDITION_EXPRESSION)
	                ->setOperatorType(PHPExcel_Style_Conditional::OPERATOR_EQUAL)
	                ->addCondition( "=" . num2alpha(array_search($index, array_keys($result))) . ($key + $row) . " <> " . num2alpha(array_search($index, array_keys($result)) + 1) . ($key + $row) )
					->getStyle()->getFont()->getColor()->setARGB(PHPExcel_Style_Color::COLOR_RED);
			    
			    $conditionalStyles = $this->PhpExcel->getActiveSheet()->getStyle((string)num2alpha(array_search($index, array_keys($result))) . ($key + $row))->getConditionalStyles();
			    array_push($conditionalStyles, $objConditional1);
			    $this->PhpExcel->getActiveSheet()->getStyle((string)num2alpha(array_search($index, array_keys($result))) . ($key + $row))->setConditionalStyles($conditionalStyles);
					    
			    $value = $values[$value];
			    
			    // Crear valor oculto para comprar
			    $this->PhpExcel->getActiveSheet()->setCellValueByColumnAndRow( array_search($index, array_keys($result)) + 1 , ($key + $row), $value);
			    
			    $this->PhpExcel->getActiveSheet()->getStyle((string)num2alpha(array_search($index, array_keys($result))) . ($key + $row))->applyFromArray(
				    array(
				        'fill' => array(
				            'type' => PHPExcel_Style_Fill::FILL_SOLID,
				            'color' => array('rgb' => 'DDDDDD')
				        )
				    )
				);
					
			}
			
			$this->PhpExcel->getActiveSheet()->setCellValueByColumnAndRow( array_search($index, array_keys($result)) , ($key + $row), $value);
		}
		
	}

	foreach($result as $index => $value) {
		$this->PhpExcel->getActiveSheet()->setCellValueByColumnAndRow( array_search($index, array_keys($result)) , ($row - 1), $index);
	}
} else {
	$result = [];
}
// Set autofilter
if($result) {
	$this->PhpExcel->getActiveSheet()->setAutoFilter('A6:'.num2alpha(count($result) - 1).'6');
	
	
	// Protect CELLS
	$this->PhpExcel->getActiveSheet()
	    ->getProtection()->setSheet(true);
	$this->PhpExcel->getActiveSheet()->getStyle(num2alpha(count($result) - 1).'7:'.num2alpha(count($result) - 1).($key + $row))
	    ->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
	$this->PhpExcel->getActiveSheet()->getProtection()->setPassword('K4nt4r');
	    
	// Ocultar ultima columna
	$this->PhpExcel->getActiveSheet()->getColumnDimension((string)num2alpha(count($result)))->setVisible(false);
	
	// Bordes
	$this->PhpExcel->getActiveSheet()->getStyle("A6:".num2alpha(count($result) - 1).($key + $row))->applyFromArray(
	    array(
	        'borders' => array(
	            'allborders' => array(
	                'style' => PHPExcel_Style_Border::BORDER_THIN,
	                'color' => array('rgb' => 'CCCCCC')
	            )
	        )
	    )
	);
	
	// Fit Columns
	foreach(range('B', num2alpha(count($result) - 1)) as $columnID) {
	    $this->PhpExcel->getActiveSheet()->getColumnDimension($columnID)
	        ->setAutoSize(true);
	}
}

// Hide grilines
$this->PhpExcel->getActiveSheet()->setShowGridlines(false);


// Hoja de validaciones
$this->PhpExcel->createSheet(1);
$this->PhpExcel->setActiveSheetIndex(1)->setTitle("List_Sheet");

$position = 1;
foreach($values as $key => $value) {
	//$this->PhpExcel->getActiveSheet()->setCellValueByColumnAndRow( 0 , $key, $value);
	$this->PhpExcel->getActiveSheet()->setCellValueByColumnAndRow( 0 , $position, $value);
	$position++;
}

// Termino de archivo
$this->PhpExcel->getActiveSheet()->getProtection()->setPassword('K4nt4r');
$this->PhpExcel->getActiveSheet()->setSheetState(PHPExcel_Worksheet::SHEETSTATE_HIDDEN);
$this->PhpExcel->setActiveSheetIndex(0);