<?php
$this->Breadcrumbs->add([
    ['title' => 'Client Attribute', 'url' => ['controller' => 'assignments-countries', 'action' => 'index', 'prefix' => 'client_attribute' ]],
    ['title' => $country['Descricao']]
]);	
?>
<!--Page Related styles-->
<?= $this->Html->css('/assets/css/dataTables.bootstrap.css'); ?>
<?= $this->Flash->render() ?>
<div class="row">
    <div class="col-xs-12 col-md-12">
        <div class="widget">
            <div class="widget-header ">
                <span class="widget-caption">Listado de asignaciones</span>
                <div class="widget-buttons" style="background: #300073; color: white; padding-left: 10px; padding-right: 10px">
                    <strong>Cierre Masterfile:</strong> <?= date('d/m/Y', strtotime($sprend['Date'])) ?>
                </div>
            </div>
            <div class="widget-body">
                <table class="table table-striped table-bordered table-hover" id="simpledatatable">
                    <thead>
                        <tr>
                            <th>
                                Status
                            </th>
                            <th>
                                Descricao
                            </th>
                            <th>
                                Market
                            </th>
                            <th>
                                Client
                            </th>
                            <th>
                                Pendants
                            </th>
                        </tr>
                    </thead>
                    <tbody>
	                    <? foreach($attributes as $attribute) { ?>
	                    <tr>
		                    <td>
			                    <? if($updating) { ?>
			                    	<i class="fa fa-2x fa-exclamation-circle warning"></i>
			                    <? } else { ?>
				                    <? if($pendants[$attribute['IdClientAttribute']] == 0) { ?>
				                    <i class="fa fa-2x fa-circle success"></i>
				                    <? } else { ?>
				                    <i class="fa fa-2x fa-circle warning"></i>
				                    <? } ?>
			                    <? } ?>
		                    </td>
		                    <? if($updating) { ?>
		                    	<td><?= $attribute['Descricao'] . " (".$attribute['IdClientAttribute'].")" ?></td>
		                    <? } else { ?>
		                    	<td><?= $this->Html->link($attribute['Descricao'] . " (".$attribute['IdClientAttribute'].")" , ['action' => 'detail', $country['Id'], $attribute['IdClientAttribute'], 'prefix' => 'client_attribute']); ?></td>
		                    <? } ?>
		                    <td><?= $attribute['market']['Descricao'] ?></td>
		                    <td><?= $attribute['client']['Descricao'] ?></td>
		                    <td>
			                <? if($updating) { ?>
			                    <i class="fa fa-spinner fa-pulse"></i>
			                <? } else { ?>
			                	<?= $pendants[$attribute['IdClientAttribute']] ?>
			                <? } ?>
			                </td>
	                    </tr>
	                    <? } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<?= $this->Html->script('/assets/js/datatable/jquery.dataTables.min.js') ?>
<?= $this->Html->script('/assets/js/datatable/ZeroClipboard.js') ?>
<?= $this->Html->script('/assets/js/datatable/dataTables.tableTools.min.js') ?>
<?= $this->Html->script('/assets/js/datatable/dataTables.bootstrap.min.js') ?>
<script language="javascript" type="text/javascript">
var oTable = $('#simpledatatable').dataTable({
    "sDom": "Tflt<'row DTTTFooter'<'col-sm-6'i><'col-sm-6'p>>",
    "iDisplayLength": 10,
    "oTableTools": {
        "aButtons": [
            "copy", "csv", "xls", "pdf", "print"
        ],
        "sSwfPath": "assets/swf/copy_csv_xls_pdf.swf"
    },
    "language": {
        "search": "",
        "sLengthMenu": "_MENU_",
        "oPaginate": {
            "sPrevious": "Anterior",
            "sNext": "Siguiente"
        }
    },
    "aaSorting": [[ 3, "asc" ], [ 1, "asc" ]],
    "bPaginate": false,
    "bInfo": false
});
</script>