<?php
$this->Breadcrumbs->add([
    ['title' => 'Client Attribute']
]);	
?>
<!--Page Related styles-->
<?= $this->Html->css('/assets/css/dataTables.bootstrap.css'); ?>
<?= $this->Flash->render() ?>
<div class="row">
	<? foreach($countries as $country) { ?>
    <? //if($pendants[$country['Id']] == 0) { continue; } ?>
    <? if($availables[$country['Id']] === false) { continue; } ?>
    <div class="col-xs-3 col-md-3">
        <div class="widget">
            <div class="widget-body">
	            <div class="row">
                	<div class="col-sm-8">
	                	<h2 class="pull-left"><i class="fa fa-circle <?= ($pendants[$country['Id']] == 0) ? 'success' : 'warning'; ?>"></i> <?=$this->Html->link($country['Descricao'], ['controller' => 'assignments-countries', 'action' => 'display', $country['Id']]); ?></h2>
	                	<div class="row">
			                <div class="col-sm-12">
				                Cierre Masterfile: <?= date('d/m/Y', strtotime($sprend[$country['Id']]['Date'])) ?><br>
				                Last Update: <?= date('d/m/y H:i', strtotime(explode(".", end(explode("/", $country['lu'][0])))[0])) ?>
				                <hr style="margin: 10px 0px">
				                <?php if(in_array($country['Id'], $update)) { ?>
							        <button style="display: <?= (in_array($country['Id'], $files)) ? 'inline' : 'none'; ?>" data-country="<?= $country['Id'] ?>" disabled class="btn btn-xs btn-info load-<?= $country['Id'] ?>-working"><i class="fa fa-pulse fa-spinner"></i> Actualizando...</button>      
					                <button style="display: <?= (in_array($country['Id'], $files)) ? 'none' : 'inline'; ?>" data-country="<?= $country['Id'] ?>" class="btn btn-xs btn-info refresh load-<?= $country['Id'] ?>"><i class="fa fa-refresh"></i> Actualizar</button>
								<?php } else { ?>
									<button disabled class="btn btn-xs btn-info refresh"><i class="fa fa-refresh"></i> Actualizar</button>
								<?php } ?>
			                </div>
		                </div>
                	</div>
					<div class="col-sm-4">
						<div class="pull-right">
		                	<center>
		                		<div class="load-<?= $country['Id'] ?>-working" style="display: <?= (in_array($country['Id'], $files)) ? 'inline' : 'none'; ?>">
		                			<h3><i class="fa fa-pulse fa-spinner"></i></h3>
		                			<h6>Calculando<br>Pendientes</h6>
		                		</div>
		                		<div class="load-<?= $country['Id'] ?>" style="display: <?= (in_array($country['Id'], $files)) ? 'none' : 'inline'; ?>">
		                			<h3><?= $pendants[$country['Id']] ?></h3>
		                			<h6>Asignaciones<br>Pendientes</h6>
		                		</div>
		                	</center>
	                	</div>
					</div>
                </div>
                
            </div>
        </div>
    </div>
    <? } ?>
</div>
<?php if(count($update) > 0) { ?>
<script lang="javascript" type="text/javascript">
$('document').ready(function() {
	
	function updateFile(country) {
		var i = setInterval(function() {
			$.post('<?= $this->Url->build(['controller' => 'assignments-countries', 'action' => 'readFile', 'prefix' => 'client_attribute']) ?>/' + country, function(r) {
				if(r == "END") {
					clearInterval(i);
					location.reload();
				}
			});
		}, 2000);
	}
	
	$('.refresh').click(function(e) {
		
		if(!confirm('¿Desea actualizar pendientes?')) {
			return false;
		}
		
		var country = $(this).data('country');
		
		$.post('<?= $this->Url->build(['controller' => 'assignments-countries', 'action' => 'refresh', 'prefix' => 'client_attribute']) ?>/' + country + '.json', function(r) {
			//updateFile(country);
			$('.load-'+country).hide();
			$('.load-'+country+'-working').show();
		},'json');
	});
});
</script>
<? } ?>