<?php
$this->Breadcrumbs->add([
    ['title' => 'Configuración'],
    ['title' => 'Mercados', 'url' => ['controller' => 'markets', 'action' => 'index' ]],
    ['title' => 'Detalle']
]);	
?>
<?= $this->Flash->render() ?>
<div class="row">
    <div class="col-lg-12 col-sm-12 col-xs-12">
        <div class="widget flat">
            <div class="widget-body">
	            <h2>
		            <i class="fa fa-circle success"></i>&nbsp;
		            <?=$market['Descricao'] ?>
		        </h2>
	            <p><?= (empty($market['Comentarios'])) ? '[No existe una descripción detallada del mercado]' : $market['Comentarios'] ?></p>
            </div>
        </div>
    </div>
</div>
<div class="row">
	<div class="col-lg-12 col-sm-12 col-xs-12">
		<div class="widget">
            <div class="widget-body">
	            <h4>Agregar nuevos elementos</h4>
	            <div class="row">
		            <?= $this->Form->create($market, ['id' => 'assign-form']); ?>
		            <?= $this->Form->hidden('IdMarket', ['value' => $market['IdMarket']]); ?>
                    <div class="col-lg-3 col-md-4">
                        <div class="form-group">
                            <label for="">Produto <span style="color: red">*</span></label>
                            <span class="input-icon icon-right">
                            	<?= $this->Form->select('IdProduto', $products, ['label' => false, 'class' => 'form-control select2', 'empty' => 'Seleccione Produto']) ?>
                            </span>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4">
                        <div class="form-group">
                            <label for="">Subproduto <span style="color: red">*</span></label>
                            <span class="input-icon icon-right">
                            	<?= $this->Form->select('IdSub', [], ['label' => false, 'class' => 'form-control select2', 'multiple' => 'multiple']) ?>
                            </span>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4">
	                    <button style="margin-top: 23px;" class="btn btn-success"><i class="fa fa-plus"></i> Agregar</button>
                    </div>
                    <?= $this->Form->end(); ?>
                </div>
            </div>
            <div class="widget-header ">
                <span class="widget-caption">
                	Productos y Subs asociados (<?= count($market['subs'])?> registros)
                </span>
                <div class="widget-buttons">
	                <button id="remove-all" style="margin-right: 10px; display: none" type="button" class="btn btn-xs btn-danger"><i class="fa fa-check"></i> Remover seleccionados</button>
                </div>
            </div>
        	<div class="widget-body">
	        	<form name="remove-form" id="remove-form" action="<?= $this->Url->build(['controller' => 'markets', 'action' => 'remove', '_ext' => 'json']) ?>" method="post">
		            <table class="table table-responsive table-bordered table-hover table-striped">
		            	<thead>
			            	<tr>
				            	<th width="40px">
	                                <label>
	                                    <input name="check-subs" type="checkbox">
	                                    <span class="text"></span>
	                                </label>
				            	</th>
				            	<th>Produto</th>
				            	<th>Subproduto</th>
				            	<th>Created</th>
				            	<th>Created By</th>
				            	<th>Actions</th>
			            	</tr>
		            	</thead>
		            	<tbody>
			            	<? foreach($market['subs'] as $sub) { ?>
			            	<tr>
				            	<td>
	                                <label>
	                                    <input name="check-sub[]" value="<?= $sub['_joinData']['Id'] ?>" type="checkbox">
	                                    <span class="text"></span>
	                                </label>
				            	</td>
				            	<td><?=$sub['product']['Descricao'] ?></td>
				            	<td><?=$sub['Descricao'] ?></td>
				            	<td><?=$sub['_joinData']['DtCriacao']->format('d M Y - h:i A') ?></td>
				            	<td><?= (empty($sub['_joinData']['CreatedBy']))? '[Sin registro]' : $sub['_joinData']['CreatedBy'] ?></td>
				            	<td>
					            	<?=
					            	$this->Html->link(
									    '<i class="fa fa-times"></i>Remover',
									    ['controller' => 'Markets', 'action' => 'remove', $sub['_joinData']['Id']],
									    ['confirm' => '¿Seguro que desea eliminar esta asignación?', 'class' => 'btn btn-xs btn-danger', 'escape' => false]
									);
									?>
				            	</td>
			            	</tr>
			            	<? } ?>
			            	<? if(!$market['subs']) {?>
			            		<tr>
				            		<td colspan="6">
					            		<center>No existen registros</center>
				            		</td>
			            		</tr>
			            	<? } ?>
		            	</tbody>
		            </table>
	        	</form>
        	</div>
		</div>
	</div>
</div>
<?= $this->Html->script('/assets/js/select2/select2.min.js') ?>
<script lang="javascript" type="text/javascript">
$('document').ready(function(e) {
	$('select[name="IdSub[]"]').attr('disabled', 'disabled');
	$('.select2').val('').trigger('change.select2');
	$('.select2').select2();
	
	// On Change Products
	$('select[name="IdProduto"]').change(function () {
		var IdProduto = $(this).val();
		$('select[name="IdSub[]"]').empty();
		$('select[name="IdSub[]"]').removeAttr('disabled');
		$.post('<?= $this->Url->build(['controller' => 'Subs', 'action' => 'getbyidproduto', 'prefix' => 'client_attribute', '_ext' => 'json']) ?>', {IdProduto: IdProduto}, function(response) {
			$('select[name="IdSub"]').select2('destroy');
			if(Object.keys(response.subs).length) {
				$.each(response.subs, function(k, v) {
				    $('select[name="IdSub[]"]').append($("<option />").val(k).text(v));
				});
			} else {
				$('select[name="IdSub[]"]').append($("<option />").val('-2').text('Problemas al conectar a BD'));
				$('select[name="IdSub[]"]').attr('disabled', 'disabled');
			}
			$('select[name="IdSub[]"]').select2({multiple: true, placeholder: "Seleccione Subproducto"});
		}, 'json');
	});
	
	// Checkboxes
	$('input[name="check-subs"]').click(function() {
		$('input[name="check-sub[]"]').prop('checked', this.checked);
	});
	
	$("input[type='checkbox']").change(function() {
		var total = 0;
	    $('input[name="check-sub[]"]').each(function() {
		    if(this.checked) {
			    total++;
		    }
	    });
		if(total > 0) {
			$('#remove-all').show();
		} else {
			$('#remove-all').hide();
		}
	});
	
	$('#remove-all').click(function() {
		var total = 0;
	    $('input[name="check-sub[]"]').each(function() {
		    if(this.checked) {
			    total++;
		    }
	    });
	    if(total > 0) {
		    var response = confirm("¿Seguro que desea eliminar estas "+total+" asignaciones?");
		    if(response) {
			    $('#remove-form').submit();
		    }
	    }
	});
	
});
</script>