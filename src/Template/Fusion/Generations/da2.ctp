<?php
$this->Breadcrumbs->add([
    ['title' => 'Generación'],
    ['title' => 'Fusión', 'url' => ['controller' => 'generations', 'action' => 'index', 'prefix' => 'fusion']],
    ['title' => $country['Descricao'], 'url' => ['controller' => 'generations', 'action' => 'da2', $country['Id'], 'prefix' => 'fusion']]
]);	
?>
<?= $this->Flash->render() ?>
<div class="row">
    <div class="col-lg-12 col-sm-12 col-xs-12">
        <div class="widget flat">
            <div class="widget-body">
	            <h2>
		            <i class="fa fa-calendar success"></i> Meses procesados
		        </h2>
	            <p>Listado de meses procesados, para extraer DA2</p>
	            <div class="row">
		            <? foreach($logs as $log) { ?>
		            <div class="col-sm-2">
			            <label style="padding: 10px; border: 1px solid #ddd; margin: 3px; display: block; font-size: 14px">
                            <input name="months" type="checkbox">
                            <span class="text">&nbsp;<?= $log['Executions']['Descricao'] ?></span>
                        </label>
		            </div>
		            <? } ?>
	            </div>
            </div>
            <div class="widget-body">
	            <h2>
		            <i class="fa fa-list success"></i> Categorías habilitadas
		        </h2>
	            <p>Listado de categorías habilitadas para extracción</p>
	            <div class="row">
		            <? foreach($categories as $category) { ?>
		            <div class="col-sm-2">
			            <label style="padding: 10px; border: 1px solid #ddd; margin: 3px; display: block; font-size: 14px">
                            <input value="<?= $category['Id'] ?>" name="categories" type="checkbox">
                            <span class="text">&nbsp;<?= $category['Description'] ?></span>
                        </label>
		            </div>
		            <? } ?>
	            </div>
            </div>
        </div>
    </div>
</div>
