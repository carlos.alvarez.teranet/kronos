<?php
$this->Breadcrumbs->add([
    ['title' => 'Generación'],
    ['title' => 'Fusión', 'url' => ['controller' => 'generations', 'action' => 'index', 'prefix' => 'fusion']],
    ['title' => $execution['country']['Descricao'], 'url' => ['controller' => 'generations', 'action' => 'country', $execution['country']['Id'], 'prefix' => 'fusion']],
    ['title' => $execution['Descricao']]
]);	
?>
<?= $this->Flash->render() ?>
<div class="row">
    <div class="col-lg-12 col-sm-12 col-xs-12">
        <div class="widget flat">
            <div class="widget-body">
	            <h2>
		            <i class="fa fa-circle success"></i>&nbsp;
		            <?=$execution['country']['Descricao'] ?> / <?=$execution['Descricao'] ?>
		        </h2>
	            <p>Listado de procesos para código de ejecución</p>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12 col-sm-12 col-xs-12">
        <div class="tabbable">
            <ul class="nav nav-tabs" id="myTab">
                <li class="active">
                    <a data-toggle="tab" href="#tab1">
                        1. Homologación
                    </a>
                </li>
                <li>
                    <a data-toggle="tab" href="#tab2">
                        2. Fusionar
                    </a>
                </li>
                <li>
                    <a data-toggle="tab" href="#tab3">
                        3. Imputar Actos
                    </a>
                </li>
                <li>
                    <a data-toggle="tab" href="#tab4">
                        4. Calculo Consumo
                    </a>
                </li>
            </ul>
            <div class="tab-content">
                <div id="tab1" class="tab-pane in active">
                    <div class="row">
	                    <div class="col-sm-6">
		                    <div class="well with-header">
			                    <? if(empty($execution['country']['logs']['homologacion']['ooh'])) { ?>
			                    <p><code>
			                    	Aún no ha sido ejecutado
			                    </code></p>
			                    <? } else { ?>
				                <pre>La tabla <strong><?= $execution['country']['parameters']['DbIndOOH']['Value'] ?>.dbo.Fusion_Web_Homologacion_OOH_<?= $execution['Id'] ?></strong> existe</pre>
				                <table class="table table-condensed table-responsive table-bordered">
					                <thead>
						                <tr>
							                <th>Fecha</th>
							                <th>Descripción</th>
						                </tr>
					                </thead>
					                <tbody>
						                <? foreach($execution['country']['logs']['homologacion']['ooh'] as $item) { ?>
						                <tr>
							                <td><?= $item['DtCriacao']->format('d/m/Y H:i:s') ?></td>
							                <td><?= $item['Descricao'] ?></td>
						                </tr>
						                <? } ?>
					                </tbody>
				                </table>
				                <br>
			                    <? } ?>
			                    <div class="header bordered-palegreen">Query - Homologación OOH</div>
			                    <button class="btn btn-default" data-toggle="modal" data-target=".modal-homologacion-ooh">Ver query OOH</button>
			                    <button data-section='homologacion' data-type="ooh" data-execution="<?=$execution['Id'] ?>" type="button" class="btn execute btn-blue">Ejecutar script</button>
                            </div>
	                    </div>
	                    <div class="col-sm-6">
		                    <div class="well with-header">
			                    <? if(empty($execution['country']['logs']['homologacion']['us'])) { ?>
			                    <p><code>
			                    	Aún no ha sido ejecutado
			                    </code></p>
			                    <? } else { ?>
				                <pre>La tabla <strong><?= $execution['country']['parameters']['DbIndUS']['Value'] ?>.dbo.Fusion_Web_Homologacion_US_<?= $execution['Id'] ?></strong> existe</pre>
				                <table class="table table-condensed table-responsive table-bordered">
					                <thead>
						                <tr>
							                <th>Fecha</th>
							                <th>Descripción</th>
						                </tr>
					                </thead>
					                <tbody>
						                <? foreach($execution['country']['logs']['homologacion']['us'] as $item) { ?>
						                <tr>
							                <td><?= $item['DtCriacao']->format('d/m/Y H:i:s') ?></td>
							                <td><?= $item['Descricao'] ?></td>
						                </tr>
						                <? } ?>
					                </tbody>
				                </table>
				                <br>
			                    <? } ?>
			                    <div class="header bordered-azure">Query - Homologación U&S PNC</div>
			                    <button class="btn btn-default" data-toggle="modal" data-target=".modal-homologacion-us">Ver query PNC U&S</button>
			                    <button data-section='homologacion' data-type="us" data-execution="<?=$execution['Id'] ?>" type="button" class="btn execute btn-blue">Ejecutar script</button>
                            </div>
	                    </div>
                    </div>
                </div>
                <div id="tab2" class="tab-pane in">
	                <div class="row">
	                    <div class="col-sm-12">
		                    <div class="well with-header">
			                    <div class="header bordered-palegreen">Fusion Pesos Imputados OOH - PNC U&S</div>
			                    <? if(empty($execution['country']['logs']['fusionar']['both'])) { ?>
			                    <p><code>
			                    	Aún no ha sido ejecutado
			                    </code></p>
			                    <? } else { ?>
				                <pre>La tabla <strong><?= $execution['country']['parameters']['DbIndUS']['Value'] ?>.dbo.Fusion_Web_Pesos_Imputado_OOH_US_<?= $execution['Id'] ?></strong> existe</pre>
				                <table class="table table-condensed table-responsive table-bordered">
					                <thead>
						                <tr>
							                <th>Fecha</th>
							                <th>Descripción</th>
						                </tr>
					                </thead>
					                <tbody>
						                <? foreach($execution['country']['logs']['fusionar']['both'] as $item) { ?>
						                <tr>
							                <td><?= $item['DtCriacao']->format('d/m/Y H:i:s') ?></td>
							                <td><?= $item['Descricao'] ?></td>
						                </tr>
						                <? } ?>
					                </tbody>
				                </table>
				                <br>
			                    <? } ?>
			                    <button data-section='fusionar' data-type="both" data-execution="<?=$execution['Id'] ?>" type="button" class="btn execute btn-blue">Ejecutar fusion (batch)</button>
		                    </div>
	                    </div>
	                </div>
                </div>
                <div id="tab3" class="tab-pane in">
	                <div class="row">
	                    <div class="col-sm-12">
		                    <div class="well with-header">
			                    <div class="header bordered-palegreen">Imputar actos de OOH a comportamiento de Fusion</div>
			                    <? if(empty($execution['country']['logs']['imputar']['ooh'])) { ?>
			                    <p><code>
			                    	Aún no ha sido ejecutado
			                    </code></p>
			                    <? } else { ?>
				                <pre>Este proceso ha sido ejecutado anteriormente. En caso de ejecutarlo nuevamente, perderá los datos previamente ejecutados.</pre>
				                <table class="table table-condensed table-responsive table-bordered">
					                <thead>
						                <tr>
							                <th>Fecha</th>
							                <th>Descripción</th>
						                </tr>
					                </thead>
					                <tbody>
						                <? foreach($execution['country']['logs']['imputar']['ooh'] as $item) { ?>
						                <tr>
							                <td><?= $item['DtCriacao']->format('d/m/Y H:i:s') ?></td>
							                <td><?= $item['Descricao'] ?></td>
						                </tr>
						                <? } ?>
					                </tbody>
				                </table>
				                <br>
			                    <? } ?>
			                    <button class="btn btn-default" data-toggle="modal" data-target=".modal-imputar-ooh">Ver query imputación</button>
			                    <button data-section='imputar' data-type="ooh" data-execution="<?=$execution['Id'] ?>" type="button" class="btn execute btn-blue">Ejecutar script</button>
		                    </div>
	                    </div>
	                </div>
                </div>
                <div id="tab4" class="tab-pane in">
	                <div class="row">
	                    <div class="col-sm-12">
		                    <div class="well with-header">
			                    <div class="header bordered-palegreen">Calcular consumo de PNC U&S y OOH</div>
			                    <? if(empty($execution['country']['logs']['consume']['us'])) { ?>
			                    <p><code>
			                    	Aún no ha sido ejecutado
			                    </code></p>
			                    <? } else { ?>
				                <pre>Este proceso ha sido ejecutado anteriormente. En caso de ejecutarlo nuevamente, perderá los datos previamente ejecutados.</pre>
				                <table class="table table-condensed table-responsive table-bordered">
					                <thead>
						                <tr>
							                <th>Fecha</th>
							                <th>Descripción</th>
						                </tr>
					                </thead>
					                <tbody>
						                <? foreach($execution['country']['logs']['consume']['us'] as $item) { ?>
						                <tr>
							                <td><?= $item['DtCriacao']->format('d/m/Y H:i:s') ?></td>
							                <td><?= $item['Descricao'] ?></td>
						                </tr>
						                <? } ?>
					                </tbody>
				                </table>
				                <br>
			                    <? } ?>
			                    <button class="btn btn-default" data-toggle="modal" data-target=".modal-consume-us">Ver query consumo</button>
			                    <button data-section='consume' data-type="us" data-execution="<?=$execution['Id'] ?>" type="button" class="btn execute btn-blue">Ejecutar script</button>
		                    </div>
	                    </div>
	                </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade modal-homologacion-ooh" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="">Detalle de query</h4>
            </div>
            <div class="modal-body">
            	<pre>
	            	<code class="sql"><?= $queries['homologation']['ooh'] ?></code>
	            </pre>
            </div>
        </div>
    </div>
</div>
<div class="modal fade modal-homologacion-us" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="">Detalle de query</h4>
            </div>
            <div class="modal-body">
            	<pre>
	            	<code class="sql"><?= $queries['homologation']['us'] ?></code>
				</pre>
            </div>
        </div>
    </div>
</div>
<div class="modal fade modal-imputar-ooh" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="">Detalle de query</h4>
            </div>
            <div class="modal-body">
            	<pre>
	            	<code class="sql"><?= $queries['imputar']['ooh'] ?></code>
				</pre>
            </div>
        </div>
    </div>
</div>
<div class="modal fade modal-consume-us" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="">Detalle de query</h4>
            </div>
            <div class="modal-body">
            	<pre>
	            	<code class="sql"><?= $queries['consume']['us'][0] ?></code>
	            	<code class="sql"><?= $queries['consume']['us'][1] ?></code>
	            	<code class="sql"><?= $queries['consume']['us'][2] ?></code>
	            	<code class="sql"><?= $queries['consume']['us'][3] ?></code>
	            	<code class="sql"><?= $queries['consume']['us'][4] ?></code>
	            	<code class="sql"><?= $queries['consume']['us'][5] ?></code>
				</pre>
            </div>
        </div>
    </div>
</div>
<script lang="javascript" type="text/javascript">
$('document').ready(function() {
	
	$('.execute').click(function() {
		
		$button = $(this);
		$button.prop('disabled', true).html('<i class="fa fa-spinner fa-pulse"></i> Ejecutando...');
		
		var section = $(this).data('section');
		var type = $(this).data('type');
		var execution = $(this).data('execution');
		var country = <?= $execution['Country_id'] ?>;
		
		$.post("<?= $this->Url->build(['controller' => 'generations', 'action' => 'run', 'prefix' => 'fusion', '_ext' => 'json']) ?>", {execution_id: execution, section: section, type: type, country_id: country}, function(r) {
			if(r.response.error) {
				$button.removeAttr('disabled').html('Ejecutar script');
			} else {
				location.reload();
			}
		}, 'json');
		
	});
	
});
</script>