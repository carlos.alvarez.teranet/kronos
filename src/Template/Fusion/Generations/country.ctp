<?php
$this->Breadcrumbs->add([
    ['title' => 'Generación'],
    ['title' => 'Fusión', 'url' => ['controller' => 'generations', 'action' => 'index', 'prefix' => 'fusion']],
    ['title' => $country['Descricao']]
]);	
?>
<?= $this->Flash->render() ?>
<div class="row">
    <div class="col-lg-12 col-sm-12 col-xs-12">
        <div class="widget flat">
            <div class="widget-body">
	            <h2>
		            <i class="fa fa-circle success"></i>&nbsp;
		            <?=$country['Descricao'] ?>
		        </h2>
	            <p>Generación de procesos para Fusión</p>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12 col-sm-12 col-xs-12">
        <div class="widget flat">
            <div class="widget-body">
	            <div class="row">
		            <div class="col-sm-12">
	                    <table class="table table-condensed table-bordered table-hover">
	                    	<thead>
		                    	<tr>
			                    	<th>Código</th>
			                    	<th>Descripción</th>
			                    	<th>Periodo</th>
			                    	<th>Fecha creación</th>
			                    	<th>Acciones</th>
		                    	</tr>
	                    	</thead>
	                    	<tbody>
		                    	<? if(empty($country['executions'])) { ?>
		                    	<tr>
			                    	<td colspan="5"><center>No existen ejecuciones para este país</center></td>
		                    	</tr>
		                    	<? } else { ?>
		                    	<? foreach($country['executions'] as $item) { ?>
		                    	<tr>
			                    	<td><?= $item['Id'] ?></td>
			                    	<td><?= $item['Descricao'] ?></td>
			                    	<td><?= $item['Ano']."-".str_pad($item['Mes'], 2, '0', STR_PAD_LEFT) ?></td>
			                    	<td><?= $item['DtCriacao']->format('d M Y - h:i A') ?></td>
			                    	<td>
				                    	<?= $this->Html->link('<i class="fa fa-cogs"></i> Configuración', ['action' => 'detail', $item['Id']], ['class' => 'btn btn-success btn-xs', 'escape' => false]) ?>
			                    	</td>
		                    	</tr>
		                    	<? } ?>
		                    	<? } ?>
	                    	</tbody>
	                    </table>
		            </div>
	            </div>
	            <div class="row" style="margin-top: 10px">
		            <div class="col-sm-12">
		            	<button class="btn btn-blue" data-toggle="modal" data-target=".bs-example-modal-sm"><i class="fa fa-plus"></i> Crear nueva ejecución</button>
		            </div>
	            </div>
            </div>
        </div>
    </div>
</div>
<!--Small Modal Templates-->
<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="mySmallModalLabel">Nuevo código de ejecución</h4>
            </div>
            <div class="modal-body">
	            <?= $this->Form->create($execution); ?>
                <div class="form-group">
                    <label for="">Descripción de código de ejecución <span style="color: red">*</span></label>
                    <span class="input-icon icon-right">
                    	<?= $this->Form->hidden('Country_id', ['value' => $country['Id']]); ?>
                    	<?= $this->Form->text('Descricao', ['label' => false, 'class' => 'form-control', 'placeholder' => 'Ej. Ejecución Dic 2017']) ?>
                        <i class="fa fa-tags blue"></i>
                    </span>
                </div>
                <div class="row">
                	<div class="col-sm-6">
	                	<div class="form-group">
							<label for="">Año <span style="color: red">*</span></label>
							<?= $this->Form->select('Ano', [date('Y')-4 => date('Y')-4, date('Y')-3 => date('Y')-3, date('Y')-2 => date('Y')-2, date('Y')-1 => date('Y')-1, date('Y') => date('Y')], ['class' => 'form-control', 'empty' => 'Seleccione año']) ?>
                		</div>
                	</div>
                	<div class="col-sm-6">
	                	<div class="form-group">
							<label for="">Mes <span style="color: red">*</span></label>
							<?= $this->Form->select('Mes', [1 => 'Enero',2 => 'Febrero',3 => 'Marzo', 4 => 'Abril', 5 => 'Mayo', 6 => 'Junio', 7 => 'Julio', 8 => 'Agosto', 9 => 'Septiembre', 10 => 'Octubre', 11 => 'Noviembre', 12 => 'Diciembre'], ['class' => 'form-control', 'empty' => 'Seleccione mes']) ?>
                		</div>
                	</div>
                </div>
                <div class="form-group">
	                <button class="btn btn-blue" type="submit">Crear</button>
	                <button class="btn btn-danger" data-dismiss="modal" type="button">Cancelar</button>
                </div>
                <?= $this->Form->end() ?>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<!--End Small Modal Templates-->