<?php
$this->Breadcrumbs->add([
    ['title' => 'Generación'],
    ['title' => 'Fusión'],
    ['title' => 'Países']
]);	
?>
<!--Page Related styles-->
<?= $this->Html->css('/assets/css/dataTables.bootstrap.css'); ?>
<?= $this->Flash->render() ?>
<div class="row">
	<? foreach($countries as $country) { ?>
    <div class="col-xs-3 col-md-3">
        <div class="widget">
            <div class="widget-body">
	            <div class="row">
                	<div class="col-sm-12">
	                	<h2><i class="fa fa-circle success"></i> <?=$country['Descricao'] ?></h2>
	                </div>
                </div>
                <div class="row">
	                <div class="col-sm-12">
		                <?=$this->Html->link('<i class="fa fa-calendar"></i> Procesar Meses', ['controller' => 'generations', 'action' => 'country', $country['Id']], ['class' => 'btn btn-xs btn-info', 'escape' => false]); ?>
		                <!--
		                <?= $this->Html->link('<i class="fa fa-download"></i> Extraer DA2', ['controller' => 'generations', 'action' => 'da2', $country['Id']], ['class' => 'btn btn-xs btn-warning', 'escape' => false]); ?>
		                -->
	                </div>
                </div>
            </div>
        </div>
    </div>
    <? } ?>
</div>