<?php
$this->Breadcrumbs->add([
    ['title' => 'Configuración'],
    ['title' => 'Fusión'],
    ['title' => 'Parametros']
]);	
?>
<!--Page Related styles-->
<?= $this->Html->css('/assets/css/dataTables.bootstrap.css'); ?>
<?= $this->Flash->render() ?>
<?= $this->Form->create(); ?>
<div class="row">
    <div class="col-xs-12 col-md-6">
        <div class="widget">
            <div class="widget-header ">
                <span class="widget-caption">Listado de parámetros</span>
            </div>
            <div class="widget-body">
	            <div class="form form-horizontal">
		            <? foreach($parameters as $key => $parameter) { ?>
	            	<div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="<?=$parameter['Code'] ?>" class="col-sm-4 control-label no-padding-right"><?=$parameter['Description'] ?></label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="<?=$parameter['Code'] ?>" value="<?= ($parameter['Value']) ? $parameter['Value'] : ''; ?>" placeholder="Ingrese parámetro">
                                    <!-- <p class="help-block"></p> -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <? } ?>
	            </div>
            </div>
            <div class="widget-body">
                <button class="btn btn-blue"><i class="fa fa-save"></i> Guardar</button>
            </div>
        </div>
    </div>
</div>