<?php
$this->Breadcrumbs->add([
    ['title' => 'Configuración'],
    ['title' => 'Fusión'],
    ['title' => 'Clústers', 'url' => ['controller' => 'clusters', 'action' => 'index', 'prefix' => 'fusion']],
    ['title' => 'Editar']
]);	
?>
<?= $this->Flash->render() ?>
<div class="row">
    <div class="col-lg-6 col-sm-6 col-xs-12">
        <div class="widget flat">
            <div class="widget-header bg-blue">
                <span class="widget-caption"><i class="fa fa-plus"></i> Formulario edición clúster</span>
            </div>
            <div class="widget-body">
                <div id="registration-form">
	                <?= $this->Form->create($cluster) ?>
                    <form role="form">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="exampleInputEmail2">Descricao <span style="color: red">*</span></label>
                                    <span class="input-icon icon-right">
                                    	<?= $this->Form->text('Descricao', ['label' => false, 'class' => 'form-control', 'placeholder' => 'Ingrese descripción / nombre de clúster']) ?>
                                        <i class="fa fa-info blue"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="exampleInputEmail2">Cantidad de grupos <span style="color: red">*</span></label>
                                    <span class="input-icon icon-right">
                                    	<?= $this->Form->number('Groups', ['label' => false, 'class' => 'form-control', 'placeholder' => 'Numero de cantidad de grupos']) ?>
                                        <i class="fa fa-tags blue"></i>
                                    </span>
                                </div>
                            </div>
                            <div class="col-sm-8">
                                <div class="form-group">
                                    <label for="">País <span style="color: red">*</span></label>
                                    <?= $this->Form->select('Country_id', $countries, ['label' => false, 'class' => 'form-control', 'empty' => 'Seleccione país']) ?>
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-blue">Actualizar clúster de agrupación</button>
                        <?= $this->Html->link('<i class="fa fa-reply"></i> Volver', ['controller' => 'clusters', 'action' => 'index', 'prefix' => 'fusion'], ['class' => 'btn btn-info', 'escape' => false]); ?>
                    </form>
                    <?= $this->Form->end() ?>
                </div>
            </div>
        </div>
    </div>
</div>