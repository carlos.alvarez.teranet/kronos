<?php
$this->Breadcrumbs->add([
    ['title' => 'Configuración'],
    ['title' => 'Clústers', 'url' => ['controller' => 'clusters', 'action' => 'index' ]],
    ['title' => 'Detalle']
]);	
?>
<?= $this->Flash->render() ?>
<div class="row">
    <div class="col-lg-12 col-sm-12 col-xs-12">
        <div class="widget flat">
            <div class="widget-body">
	            <h2>
		            <i class="fa fa-circle success"></i>&nbsp;
		            <?=$cluster['Descricao'] ?> / <?=$cluster['country']['Descricao'] ?>
		        </h2>
	            <p>Clúster de agrupación, creado por <?= ($cluster['CreatedBy']) ? $cluster['CreatedBy'] : 'No especificado'; ?></p>
            </div>
        </div>
    </div>
</div>
<div class="row">
	<div class="col-lg-8 col-sm-8 col-xs-12">
		<div class="widget">
			<div class="widget-header bg-blueberry">
				<span class="widget-caption">
                	Agregar nueva agrupación (U&S PNC)
                </span>
			</div>
			<div class="widget-body">
				<form method="post" name="US">
					<input type="hidden" name="IdCluster" value="<?=$cluster['Id'] ?>">
					<div class="row">
			            <div class="col-lg-3 col-md-6">
	                        <div class="form-group">
	                            <label for="">Grupo <span style="color: red">*</span></label>
	                            <span class="input-icon icon-right">
	                            	<?= $this->Form->select('group', array_combine(range(1, $cluster['Groups']), range(1, $cluster['Groups'])), ['label' => false, 'class' => 'form-control select2', 'empty' => 'Seleccione grupo', 'required']) ?>
	                            </span>
	                        </div>
	                    </div>
			            <div class="col-lg-3 col-md-6">
	                        <div class="form-group">
	                            <label for="">Campo / Columna <span style="color: red">*</span></label>
	                            <span class="input-icon icon-right">
	                            	<?= $this->Form->select('field', $lists['ColsUS'], ['label' => false, 'class' => 'form-control select2 special', 'empty' => 'Seleccione campo / columna', 'data-origin' => 'US', 'required']) ?>
	                            </span>
	                        </div>
	                    </div>
	                    <div class="col-lg-3 col-md-6">
	                        <div class="form-group">
	                            <label for="">Valor <span style="color: red">*</span></label>
	                            <span class="input-icon icon-right">
	                            	<?= $this->Form->select('values[]', [], ['id' => 'val_US', 'label' => false, 'class' => 'form-control select2', 'empty' => 'Seleccione columna', 'disabled', 'multiple', 'required']) ?>
	                            </span>
	                        </div>
	                    </div>
	                    <div class="col-lg-3 col-md-6">
		                    <button name="submit" value="US" style="margin-top: 23px;" class="btn btn-success"><i class="fa fa-plus"></i> Agregar</button>
	                    </div>
	                </div>
				</form>
				<table class="table table-responsive table-hover table-bordered">
	                <thead>
		                <tr>
			                <th>Id</th>
			                <th>Grupo</th>
			                <th>Campo</th>
			                <th>Valor</th>
			                <th>Creado por</th>
			                <th>Acciones</th>
		                </tr>
	                </thead>
	                <tbody>
		                <? if(!$cluster['uss']) { ?>
		                <tr>
			                <td colspan="6">
				                <center>No existen registros</center>
				            </td>
		                </tr>
		                <? } else { ?>
		                <? foreach($cluster['uss'] as $us) { ?>
		                <tr>
			                <td><?=$us['Id'] ?></td>
			                <td><?=$us['NGroup'] ?></td>
			                <td><?=$us['Field'] ?></td>
			                <td><?=$us['Valor'] ?></td>
			                <td><?=$us['CreatedBy'] ?></td>
			                <td>
				                <?= $this->Html->link('<button class="btn btn-xs btn-danger"><i class="fa fa-times"></i> Eliminar</button>', ['controller' => 'clusters', 'action' => 'removeAssoc', $us['Id'], 'prefix' => 'fusion'], ['confirm' => '¿Deseas eliminar este registro?', 'escape' => false]); ?>
			                </td>
		                </tr>
		                <? } ?>
		                <? } ?>
	                </tbody>
                </table>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-lg-8 col-sm-8 col-xs-12">
		<div class="widget">
			<div class="widget-header bg-blueberry">
				<span class="widget-caption">
                	Agregar nueva agrupación (OOH)
                </span>
			</div>
			<div class="widget-body">
				<form method="post" name="OOH">
					<input type="hidden" name="IdCluster" value="<?=$cluster['Id'] ?>">
					<div class="row">
			            <div class="col-lg-3 col-md-6">
	                        <div class="form-group">
	                            <label for="">Grupo <span style="color: red">*</span></label>
	                            <span class="input-icon icon-right">
	                            	<?= $this->Form->select('group', array_combine(range(1, $cluster['Groups']), range(1, $cluster['Groups'])), ['label' => false, 'class' => 'form-control select2', 'empty' => 'Seleccione grupo', 'required']) ?>
	                            </span>
	                        </div>
	                    </div>
			            <div class="col-lg-3 col-md-6">
	                        <div class="form-group">
	                            <label for="">Campo / Columna <span style="color: red">*</span></label>
	                            <span class="input-icon icon-right">
	                            	<?= $this->Form->select('field', $lists['ColsOOH'], ['label' => false, 'class' => 'form-control select2 special', 'empty' => 'Seleccione campo / columna', 'data-origin' => 'OOH', 'required']) ?>
	                            </span>
	                        </div>
	                    </div>
	                    <div class="col-lg-3 col-md-6">
	                        <div class="form-group">
	                            <label for="">Valor <span style="color: red">*</span></label>
	                            <span class="input-icon icon-right">
	                            	<?= $this->Form->select('values[]', [], ['id' => 'val_OOH', 'label' => false, 'class' => 'form-control select2', 'empty' => 'Seleccione columna', 'disabled', 'multiple', 'required']) ?>
	                            </span>
	                        </div>
	                    </div>
	                    <div class="col-lg-3 col-md-6">
		                    <button name="submit" value="OOH" style="margin-top: 23px;" class="btn btn-success"><i class="fa fa-plus"></i> Agregar</button>
	                    </div>
	                </div>
				</form>
				<table class="table table-responsive table-hover table-bordered">
	                <thead>
		                <tr>
			                <th>Id</th>
			                <th>Grupo</th>
			                <th>Campo</th>
			                <th>Valor</th>
			                <th>Creado por</th>
			                <th>Acciones</th>
		                </tr>
	                </thead>
	                <tbody>
		                <? if(!$cluster['oohs']) { ?>
		                <tr>
			                <td colspan="6">
				                <center>No existen registros</center>
				            </td>
		                </tr>
		                <? } else { ?>
		                <? foreach($cluster['oohs'] as $ooh) { ?>
		                <tr>
			                <td><?=$ooh['Id'] ?></td>
			                <td><?=$ooh['NGroup'] ?></td>
			                <td><?=$ooh['Field'] ?></td>
			                <td><?=$ooh['Valor'] ?></td>
			                <td><?=$ooh['CreatedBy'] ?></td>
			                <td>
				                <?= $this->Html->link('<button class="btn btn-xs btn-danger"><i class="fa fa-times"></i> Eliminar</button>', ['controller' => 'clusters', 'action' => 'removeAssoc', $ooh['Id'], 'prefix' => 'fusion'], ['confirm' => '¿Deseas eliminar este registro?', 'escape' => false]); ?>
			                </td>
		                </tr>
		                <? } ?>
		                <? } ?>
	                </tbody>
                </table>
			</div>
		</div>
	</div>
</div>
<?= $this->Html->script('/assets/js/select2/select2.min.js') ?>
<script lang="javascript" type="text/javascript">
$('document').ready(function(e) {
	$('.select2').select2({
		placeholder: "Seleccione valores"
	});
	
	$('.special').change(function() {
		var group = $(this).data('origin');
		var send = {field: $(this).val(), group: group, country_id: <?=$cluster['Country_id']?>};
	
		var $dropdown = $("#val_"+group);
		$dropdown.select2('destroy');
		$dropdown.empty();
		$dropdown.prop('disabled', 'disabled');
		$dropdown.select2({
			placeholder: "Cargando datos..."
		});
		
		$.post('<?= $this->Url->build(['controller' => 'clusters', 'action' => 'fields', 'prefix' => 'fusion']) ?>', send, function(response) {
				if(response.response.length > 0) {
				$dropdown.select2('destroy');
				$dropdown.empty();
				$.each(response.response, function(key, val) {
				    $dropdown.append($("<option />").val(val[0]).text(val[0]));
				});
				$dropdown.removeAttr('disabled');
				$dropdown.select2({
					placeholder: "Seleccione valores"
				});
			}
		}, 'json');
	});
	
	$('form').submit(function() {
		var data = $(this).serializeArray();
		if(data.length < 3) {
			return false;
		}
	});
	
	var OOH = $('.special[data-origin=OOH]').val();
	var US	= $('.special[data-origin=US]').val();
	
	if(OOH) {
		$('.special[data-origin=OOH]').change();
	}
	if(US) {
		$('.special[data-origin=US]').change();
	}
	
});
</script>