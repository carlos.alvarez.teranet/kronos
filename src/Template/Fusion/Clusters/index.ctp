<?php
$this->Breadcrumbs->add([
    ['title' => 'Configuración'],
    ['title' => 'Fusión'],
    ['title' => 'Clústers']
]);	
?>
<!--Page Related styles-->
<?= $this->Html->css('/assets/css/dataTables.bootstrap.css'); ?>
<?= $this->Flash->render() ?>
<div class="row">
    <div class="col-xs-12 col-md-12">
        <div class="widget">
            <div class="widget-header ">
                <span class="widget-caption">Listado de clústeres</span>
                <div class="widget-buttons">
                    <a href="#" data-toggle="maximize">
                        <i class="fa fa-expand"></i>
                    </a>
                    <a href="#" data-toggle="collapse">
                        <i class="fa fa-minus"></i>
                    </a>
                    <a href="#" data-toggle="dispose">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="widget-body">
                <table class="table table-striped table-bordered table-hover" id="simpledatatable">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Nombre</th>
                            <th>País</th>
                            <th>Grupos</th>
                            <th>DtCriacao</th>
                            <th>DtAtualizacao</th>
                            <th>Creado por</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
	                    <? if($clusters->isEmpty()) { ?>
	                    <tr>
		                    <td colspan="7" align="center">No existen registros</td>
	                    </tr>
	                    <? } else { ?>
	                    <? foreach($clusters as $cluster) { ?>
	                    <tr>
		                    <td><?=$cluster['Id'] ?></td>
		                    <td><?=$cluster['Descricao'] ?></td>
		                    <td><?=$cluster['country']['Descricao'] ?></td>
		                    <td><?=$cluster['Groups'] ?></td>
		                    <td><?=$cluster['DtCriacao']->format('d M Y - h:i A') ?></td>
		                    <td><?= ($cluster['DtAtualizacao']) ? $cluster['DtAtualizacao']->format('d M Y - h:i A') : '' ?></td>
		                    <td><?= ($cluster['CreatedBy']) ? $cluster['CreatedBy'] : 'No especificado'; ?></td>
		                    <td>
			                    <?= $this->Html->link('<i class="fa fa-edit"></i> Editar', ['action' => 'edit', $cluster['Id'], 'prefix' => 'fusion'], ['class' => 'btn btn-xs btn-info', 'escape' => false] ); ?>
			                    <?= $this->Html->link('<i class="fa fa-cogs"></i> Configurar', ['action' => 'detail', $cluster['Id'], 'prefix' => 'fusion'], ['class' => 'btn btn-xs btn-warning', 'escape' => false] ); ?>
		                    </td>
	                    </tr>
	                    <? } ?>
	                    <? } ?>
                    </tbody>
                </table>
                <div class="row" style="margin-top: 10px">
                	<div class="col-sm-12">
	                	<?= $this->Html->link('<i class="fa fa-plus"></i> Agregar clúster de agrupación', ['controller' => 'clusters', 'action' => 'add', 'prefix' => 'fusion'], ['class' => 'btn btn-blue', 'escape' => false]) ?>
                	</div>
                </div>
            </div>
        </div>
    </div>
</div>