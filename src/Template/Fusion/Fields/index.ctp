<?php
$this->Breadcrumbs->add([
    ['title' => 'Configuración'],
    ['title' => 'Fusión'],
    ['title' => 'Países']
]);	
?>
<!--Page Related styles-->
<?= $this->Html->css('/assets/css/dataTables.bootstrap.css'); ?>
<?= $this->Flash->render() ?>
<div class="row">
	<? foreach($countries as $country) { ?>
    <div class="col-xs-3 col-md-3">
        <div class="widget">
            <div class="widget-body">
	            <div class="row">
                	<div class="col-sm-12">
	                	<h2><i class="fa fa-circle success"></i> <?=$country['Descricao'] ?></h2>
	                </div>
                </div>
                <div class="row">
	                <div class="col-sm-12">
		                <?=$this->Html->link('<i class="fa fa-check"></i> Seleccionar país', ['controller' => 'fields', 'action' => 'detail', $country['Id']], ['class' => 'btn btn-xs btn-info', 'escape' => false]); ?>
	                </div>
                </div>
            </div>
        </div>
    </div>
    <? } ?>
</div>