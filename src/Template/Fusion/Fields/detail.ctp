<?php
$this->Breadcrumbs->add([
    ['title' => 'Configuración'],
    ['title' => 'Campos', 'url' => ['controller' => 'fields', 'action' => 'index' ]],
    ['title' => $country['Descricao']]
]);	
?>
<?= $this->Flash->render() ?>
<div class="row">
    <div class="col-lg-12 col-sm-12 col-xs-12">
        <div class="widget flat">
            <div class="widget-body">
	            <h2>
		            <i class="fa fa-circle success"></i>&nbsp;
		            <?=$country['Descricao'] ?>
		        </h2>
	            <p>Listado de campos homologados para crear tablas temporales</p>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12 col-sm-12 col-xs-12">
        <div class="tabbable">
            <ul class="nav nav-tabs" id="myTab">
	            <? foreach($country['fields_groups'] as $group) { ?>
                <li class="<?= ($group['Status'] == 1) ? 'active' : ''; ?>">
                    <a data-toggle="tab" href="#<?=$group['Code'] ?>">
                        <?= $group['Description'] ?>
                    </a>
                </li>
                <? } ?>
            </ul>
            <div class="tab-content">
	            <? $cont = 0; ?>
	            <? foreach($country['fields_groups'] as $group) { ?>
                <div id="<?=$group['Code'] ?>" class="tab-pane in <?= ($group['Status'] == 1) ? 'active' : ''; ?>">
                    <p><?=$group['Detail'] ?></p>
                    <?=$this->Form->create() ?>
                    <div class="row">
	                    <div class="col-sm-12">
		                    <table class="table table-bordered table-hover">
			                    <thead>
				                    <tr>
					                    <th>Id</th>
					                    <th>Campo</th>
					                    <th>Comentario</th>
					                    <th>OOH</th>
					                    <th>PNC U&S</th>
					                    
				                    </tr>
			                    </thead>
			                    <tbody>
				                    <? if(empty($group['fields'])) { ?>
				                    <tr>
					                    <td colspan="5"><center>No existen registros</center></td>
				                    </tr>
				                    <? } else { ?>
				                    <? foreach($group['fields'] as $key => $field) { ?>
				                    <tr>
					                    <td><?=$field['Id'] ?></td>
					                    <td><?=$field['Description'] ?></td>
					                    <td><?=$field['Comments'] ?></td>
					                    <td>
						                    <?=$this->Form->hidden($cont.'.Id', ['value' => $field['Id']]); ?>
						                    <?=$this->Form->select($cont.'.OOH', $lists['OOH'][$group['Code']], ['label' => false, 'class' => 'form-control', 'empty' => 'Seleccione campo', 'default' => $field['OOH']]) ?>
						                </td>
					                    <td>
						                    <?=$this->Form->select($cont.'.US', $lists['US'][$group['Code']], ['label' => false, 'class' => 'form-control', 'empty' => 'Seleccione campo', 'default' => $field['US']]) ?>
						                    <? $cont++; ?>
						                </td>
				                    </tr>
				                    <? } ?>
				                    <? } ?>
			                    </tbody>
		                    </table>
	                    </div>
                    </div>
                    <div class="row" style="margin-top: 10px">
	                    <div class="col-sm-12">
                    		<button type="submit" class="btn btn-blue"><i class="fa fa-save"></i> Guardar</button>
	                    </div>
                    </div>
                    <?= $this->Form->end(); ?>
                </div>
                <? } ?>
            </div>
        </div>
    </div>
</div>