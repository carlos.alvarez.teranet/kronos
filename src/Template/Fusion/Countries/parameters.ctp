<?php
$this->Breadcrumbs->add([
    ['title' => 'Configuración'],
    ['title' => 'Fusión'],
    ['title' => 'Países', 'url' => ['controller' => 'countries', 'action' => 'index', 'prefix' => 'fusion']],
    ['title' => 'Parametros '.$country['Descricao']]
]);	
?>
<!--Page Related styles-->
<?= $this->Html->css('/assets/css/dataTables.bootstrap.css'); ?>
<?= $this->Flash->render() ?>
<div class="row">
    <div class="col-lg-12 col-sm-12 col-xs-12">
        <div class="widget flat">
            <div class="widget-body">
	            <h2>
		            <i class="fa fa-circle success"></i>&nbsp;
		            <?=$country['Descricao'] ?>
		        </h2>
	            <p>País de fusión, creado por <?= ($country['CreatedBy']) ? $country['CreatedBy'] : 'No especificado'; ?></p>
            </div>
        </div>
    </div>
</div>
<?= $this->Form->create(); ?>
<div class="row">
	<? foreach($country['parameters'] as $key => $parameter) { ?>
    <div class="col-xs-12 col-md-4">
        <div class="widget">
            <div class="widget-header ">
                <span class="widget-caption"><?=$key ?></span>
            </div>
            <div class="widget-body" style="min-height: 300px">
	            <div class="form form-horizontal">
		            <? foreach($parameter as $item) { ?>
	            	<div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="<?=$item['Code'] ?>" class="col-sm-4 control-label no-padding-right"><?=$item['Description'] ?></label>
                                <div class="col-sm-8">
	                                
	                                <? if($item['Code'] == 'CiDatUS') {
	                                	echo $this->Form->select('CiDatUS', $lists['ColsUS'], ['class' => 'form-control select2 special', 'data-origin' => 'US', 'empty' => 'Seleccione opciones', 'value' => $item['Value']]);
	                                } else if ($item['Code'] == 'CiDatOOH') {
	                                	echo $this->Form->select('CiDatOOH', $lists['ColsOOH'], ['class' => 'form-control select2 special', 'data-origin' => 'OOH', 'empty' => 'Seleccione opciones', 'value' => $item['Value']]);
	                                } else if ($item['Code'] == 'ClDatUS') {
		                                echo $this->Form->select('ClDatUS', [], ['class' => 'form-control select2', 'empty' => 'Seleccione ciudades', 'id' => 'ClDatUS', 'multiple' => true, 'value' => array_combine(explode(',', $item['Value']), explode(',', $item['Value']))]);
		                            } else if ($item['Code'] == 'ClDatOOH') {
			                            echo $this->Form->select('ClDatOOH', [], ['class' => 'form-control select2', 'empty' => 'Seleccione ciudades', 'id' => 'ClDatOOH', 'multiple' => true, 'value' => array_combine(explode(',', $item['Value']), explode(',', $item['Value']))]);
	                                } else if ($item['Code'] == 'EdDatOOH') {
		                                echo $this->Form->select('EdDatOOH', $lists['ColsOOH'], ['class' => 'form-control select2', 'empty' => 'Seleccione opciones', 'id' => 'EdDatOOH', 'value' => $item['Value']]);
		                            } else if ($item['Code'] == 'EdDatUS') {
		                                echo $this->Form->select('EdDatUS', $lists['ColsUS'], ['class' => 'form-control select2', 'empty' => 'Seleccione opciones', 'id' => 'EdDatUS', 'value' => $item['Value']]);
		                            } else { ?>
                                    <input type="text" class="form-control" name="<?=$item['Code'] ?>" value="<?= ($item['Value']) ? $item['Value'] : ''; ?>" placeholder="Ingrese parámetro">
                                    <!-- <p class="help-block"></p> -->
                                    <? } ?>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    <? } ?>
	            </div>
            </div>
            <div class="widget-body">
                <button class="btn btn-blue"><i class="fa fa-save"></i> Guardar</button>
            </div>
        </div>
    </div>
    <? } ?>
</div>
<?= $this->Html->script('/assets/js/select2/select2.min.js') ?>
<script lang="javascript" type="text/javascript">
$('document').ready(function(e) {
	$('.select2').select2({
		placeholder: "Seleccione valores"
	});
	
	$('.special').change(function() {
		var group = $(this).data('origin');
		var send = {field: $(this).val(), group: group, country_id: <?=$country['Id']?>};
	
		var $dropdown = $("#ClDat"+group);
		$dropdown.select2('destroy');
		$dropdown.empty();
		$dropdown.prop('disabled', 'disabled');
		$dropdown.select2({
			multiple: true,
			placeholder: "Cargando datos..."
		});
		
		$.post('<?= $this->Url->build(['controller' => 'clusters', 'action' => 'fields', 'prefix' => 'fusion']) ?>', send, function(response) {
				if(response.response.length > 0) {
				$dropdown.select2('destroy');
				$dropdown.empty();
				$.each(response.response, function(key, val) {
					if(val[1] == 1) {
				    	$dropdown.append($("<option />").val(val[0]).text(val[0]).prop('selected', true));
				    } else {
					    $dropdown.append($("<option />").val(val[0]).text(val[0]));
				    }
				});
				$dropdown.removeAttr('disabled');
				$dropdown.select2({
					placeholder: "Seleccione valores"
				});
			}
		}, 'json');
	});
	
	$('form').submit(function() {
		var data = $(this).serializeArray();
		if(data.length < 3) {
			return false;
		}
	});
	
	var OOH = $('.special[data-origin=OOH]').val();
	var US	= $('.special[data-origin=US]').val();
	
	if(OOH) {
		$('.special[data-origin=OOH]').change();
	}
	if(US) {
		$('.special[data-origin=US]').change();
	}
	
});
</script>