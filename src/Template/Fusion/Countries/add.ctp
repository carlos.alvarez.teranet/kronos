<?php
$this->Breadcrumbs->add([
    ['title' => 'Configuración'],
    ['title' => 'Fusión'],
    ['title' => 'Países', 'url' => ['controller' => 'countries', 'action' => 'index', 'prefix' => 'fusion']],
    ['title' => 'Agregar']
]);	
?>
<?= $this->Flash->render() ?>
<div class="row">
    <div class="col-lg-6 col-sm-6 col-xs-12">
        <div class="widget flat">
            <div class="widget-header bg-blue">
                <span class="widget-caption"><i class="fa fa-plus"></i> Formulario nuevo país</span>
            </div>
            <div class="widget-body">
                <div id="registration-form">
	                <?= $this->Form->create($country) ?>
                    <form role="form">
                        <div class="row">
                            <div class="col-sm-8">
                                <div class="form-group">
                                    <label for="exampleInputEmail2">Descricao <span style="color: red">*</span></label>
                                    <span class="input-icon icon-right">
                                    	<?= $this->Form->text('Descricao', ['label' => false, 'class' => 'form-control', 'placeholder' => 'Ingrese descripción / nombre de país']) ?>
                                        <i class="fa fa-info blue"></i>
                                    </span>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="exampleInputEmail2">Nombre corto <span style="color: red">*</span></label>
                                    <span class="input-icon icon-right">
                                    	<?= $this->Form->text('Short', ['label' => false, 'class' => 'form-control', 'placeholder' => 'Ingrese abreviación']) ?>
                                        <i class="fa fa-tags blue"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="exampleInputEmail2">Flag Desactivacion</label>
                                    <span class="input-icon icon-right">
                                    	<?= $this->Form->select('FlgDesativacao', $flags, ['label' => false, 'class' => 'form-control']) ?>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-blue">Crear país</button>
                        <?= $this->Html->link('<i class="fa fa-reply"></i> Volver', ['controller' => 'clusters', 'action' => 'index', 'prefix' => 'fusion'], ['class' => 'btn btn-info', 'escape' => false]); ?>
                    </form>
                    <?= $this->Form->end() ?>
                </div>
            </div>
        </div>
    </div>
</div>