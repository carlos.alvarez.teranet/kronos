<?php
$this->Breadcrumbs->add([
    ['title' => 'Configuración'],
    ['title' => 'Fusión'],
    ['title' => 'Países', 'url' => ['controller' => 'countries', 'action' => 'index', 'prefix' => 'fusion']],
    ['title' => 'Joins '.$country['Descricao']]
]);	
?>
<!--Page Related styles-->
<?= $this->Html->css('/assets/css/dataTables.bootstrap.css'); ?>
<?= $this->Flash->render() ?>
<div class="row">
    <div class="col-lg-12 col-sm-12 col-xs-12">
        <div class="widget flat">
            <div class="widget-body">
	            <h2>
		            <i class="fa fa-circle success"></i>&nbsp;
		            <?=$country['Descricao'] ?>
		        </h2>
	            <p>País de fusión, creado por <?= ($country['CreatedBy']) ? $country['CreatedBy'] : 'No especificado'; ?></p>
            </div>
        </div>
    </div>
</div>
<div class="row">
	<div class="col-xs-6 col-md-6">
		 <div class="widget">
            <div class="widget-header ">
                <span class="widget-caption">Llaves de union JOIN (OOH)</span>
            </div>
            <div class="widget-body">
	            <?= $this->Form->create($join_OOH) ?>
		            <?= $this->Form->hidden('Country_id', ['value' => $country['Id']]) ?>
		            <?= $this->Form->hidden('IdType', ['value' => 'OOH']) ?>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="exampleInputEmail2">Campo Individuos <span style="color: red">*</span></label>
                                <?= $this->Form->select('Field_Ind', $lists['OOH']['Ind'], ['label' => false, 'class' => 'form-control', 'placeholder' => 'Ingrese descripción / nombre de país', 'empty' => 'Seleccione campo']) ?>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="exampleInputEmail2">Operador <span style="color: red">*</span></label>
                                <?= $this->Form->select('Operator', $lists['OOH']['Op'], ['label' => false, 'class' => 'form-control', 'placeholder' => 'Ingrese descripción / nombre de país', 'empty' => 'Seleccione operador']) ?>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="exampleInputEmail2">Campo Panel <span style="color: red">*</span></label>
                                <?= $this->Form->select('Field_Pan', $lists['OOH']['Pan'], ['label' => false, 'class' => 'form-control', 'placeholder' => 'Ingrese descripción / nombre de país', 'empty' => 'Seleccione campo']) ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
	                        <button class="btn btn-info" type="submit"><i class="fa fa-save"></i> Guardar</button>
                        </div>
                    </div>
	            <?= $this->Form->end(); ?>
            </div>
            <div class="widget-body ">
	            <table class="table table-responsive table-bordered table-hover table-striped table-condensed">
		            <thead>
			            <tr>
				            <th>Id</th>
				            <th>Campo Ind</th>
				            <th>Operator</th>
				            <th>Campo Panel</th>
				            <th>Acciones</th>
			            </tr>
		            </thead>
		            <tbody>
			            <? if($joins['OOH']) { ?>
			            <? foreach($joins['OOH'] as $join) { ?>
			            <tr>
				            <td><?=$join['Id'] ?></td>
				            <td><?=$join['Field_Ind'] ?></td>
				            <td><?=$join['Operator'] ?></td>
				            <td><?=$join['Field_Pan'] ?></td>
				            <td>
					            <?= $this->Html->link('<i class="fa fa-times"></i> Eliminar', ['controller' => 'countries', 'action' => 'remove', 'prefix' => 'fusion', $country['Id'], $join['Id']], ['escape' => false, 'class' => 'btn btn-xs btn-danger', 'confirm' => 'Desea eliminar este registro?']) ?>
				            </td>
			            </tr>
			            <? } ?>
			            <? } else { ?>
			            <tr>
				            <td colspan="5"><center>No existen registros</center></td>
			            </tr>
			            <? } ?>
		            </tbody>
	            </table>
            </div>
		</div>
	</div>
	<div class="col-xs-6 col-md-6">
		 <div class="widget">
            <div class="widget-header ">
                <span class="widget-caption">Llaves de union JOIN (PNC U&S)</span>
            </div>
            <div class="widget-body">
	            <?= $this->Form->create($join_US) ?>
		            <?= $this->Form->hidden('Country_id', ['value' => $country['Id']]) ?>
		            <?= $this->Form->hidden('IdType', ['value' => 'US']) ?>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="exampleInputEmail2">Campo Individuos <span style="color: red">*</span></label>
                                <?= $this->Form->select('Field_Ind', $lists['US']['Ind'], ['label' => false, 'class' => 'form-control', 'placeholder' => 'Ingrese descripción / nombre de país', 'empty' => 'Seleccione campo']) ?>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="exampleInputEmail2">Operador <span style="color: red">*</span></label>
                                <?= $this->Form->select('Operator', $lists['US']['Op'], ['label' => false, 'class' => 'form-control', 'placeholder' => 'Ingrese descripción / nombre de país', 'empty' => 'Seleccione operador']) ?>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="exampleInputEmail2">Campo Panel <span style="color: red">*</span></label>
                                <?= $this->Form->select('Field_Pan', $lists['US']['Pan'], ['label' => false, 'class' => 'form-control', 'placeholder' => 'Ingrese descripción / nombre de país', 'empty' => 'Seleccione campo']) ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
	                        <button class="btn btn-info" type="submit"><i class="fa fa-save"></i> Guardar</button>
                        </div>
                    </div>
	            <?= $this->Form->end(); ?>
            </div>
            <div class="widget-body ">
	            <table class="table table-responsive table-bordered table-hover table-striped table-condensed">
		            <thead>
			            <tr>
				            <th>Id</th>
				            <th>Campo Ind</th>
				            <th>Operator</th>
				            <th>Campo Panel</th>
				            <th>Acciones</th>
			            </tr>
		            </thead>
		            <tbody>
			            <? if($joins['US']) { ?>
			            <? foreach($joins['US'] as $join) { ?>
			            <tr>
				            <td><?=$join['Id'] ?></td>
				            <td><?=$join['Field_Ind'] ?></td>
				            <td><?=$join['Operator'] ?></td>
				            <td><?=$join['Field_Pan'] ?></td>
				            <td>
					            <?= $this->Html->link('<i class="fa fa-times"></i> Eliminar', ['controller' => 'countries', 'action' => 'remove', 'prefix' => 'fusion', $country['Id'], $join['Id']], ['escape' => false, 'class' => 'btn btn-xs btn-danger', 'confirm' => 'Desea eliminar este registro?']) ?>
				            </td>
			            </tr>
			            <? } ?>
			            <? } else { ?>
			            <tr>
				            <td colspan="5"><center>No existen registros</center></td>
			            </tr>
			            <? } ?>
		            </tbody>
	            </table>
            </div>
		</div>
	</div>
</div>