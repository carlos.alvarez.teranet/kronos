<?php
$this->Breadcrumbs->add([
    ['title' => 'Configuración'],
    ['title' => 'Fusión'],
    ['title' => 'Países']
]);	
?>
<!--Page Related styles-->
<?= $this->Html->css('/assets/css/dataTables.bootstrap.css'); ?>
<?= $this->Flash->render() ?>
<div class="row">
    <div class="col-xs-12 col-md-12">
        <div class="widget">
            <div class="widget-header ">
                <span class="widget-caption">Listado de países</span>
                <div class="widget-buttons">
                    <a href="#" data-toggle="maximize">
                        <i class="fa fa-expand"></i>
                    </a>
                    <a href="#" data-toggle="collapse">
                        <i class="fa fa-minus"></i>
                    </a>
                    <a href="#" data-toggle="dispose">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="widget-body">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Nombre</th>
                            <th>Abreviación</th>
                            <th>DtCriacao</th>
                            <th>DtAtualizacao</th>
                            <th>Creado por</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
	                    <? if($countries->isEmpty()) { ?>
	                    <tr>
		                    <td colspan="7" align="center">No existen registros</td>
	                    </tr>
	                    <? } else { ?>
	                    <? foreach($countries as $country) { ?>
	                    <tr>
		                    <td><?=$country['Id'] ?></td>
		                    <td><?=$country['Descricao'] ?></td>
		                    <td><?=$country['Short'] ?></td>
		                    <td><?=$country['DtCriacao']->format('d M Y - h:i A') ?></td>
		                    <td><?= ($country['DtAtualizacao']) ? $country['DtAtualizacao']->format('d M Y - h:i A') : '' ?></td>
		                    <td><?= ($country['CreatedBy']) ? $country['CreatedBy'] : 'No especificado'; ?></td>
		                    <td>
			                    <?= $this->Html->link('<i class="fa fa-edit"></i> Editar', ['action' => 'edit', $country['Id'], 'prefix' => 'fusion'], ['class' => 'btn btn-xs btn-info', 'escape' => false] ); ?>
			                    <?= $this->Html->link('<i class="fa fa-cogs"></i> Parámetros', ['action' => 'parameters', $country['Id'], 'prefix' => 'fusion'], ['class' => 'btn btn-xs btn-warning', 'escape' => false] ); ?>
			                    <?= $this->Html->link('<i class="fa fa-tags"></i> Joins', ['action' => 'join', $country['Id'], 'prefix' => 'fusion'], ['class' => 'btn btn-xs btn-blue', 'escape' => false] ); ?>
		                    </td>
	                    </tr>
	                    <? } ?>
	                    <? } ?>
                    </tbody>
                </table>
                <div class="row" style="margin-top: 10px">
                	<div class="col-sm-12">
	                	<?= $this->Html->link('<i class="fa fa-plus"></i> Agregar pais', ['controller' => 'countries', 'action' => 'add', 'prefix' => 'fusion'], ['class' => 'btn btn-blue', 'escape' => false]) ?>
                	</div>
                </div>
            </div>
        </div>
    </div>
</div>