<?php
namespace App\Shell;

use Cake\Console\Shell;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;
use Cake\Datasource\ConnectionManager;


class NacFotoShell extends Shell
{
	
	public function initialize()
    {
        parent::initialize();
    }
    
    public function main($dbcode = null) 
    {
	    
	    $countries = [
			'BR_KWP',
			'CAM_KWP',
			'ECU_KWP',
			'PE_KWP',
			'BO_KWP',
			'CL_KWP',
			'CO_KWP',
			'MX_KWP'  
	    ];
	    
	    $dir 		= '/mnt/NACImagem/in';
	    $out		= '/mnt/NACImagem';

		$this->info('Leyendo directorio...');
		$output = 	shell_exec(sprintf("find %s -maxdepth 1 -type f -name '*.jpg' > %s", $dir, TMP . 'images' . DS . 'images.txt'));
		$file	=	fopen(TMP . 'images' . DS . 'images.txt', 'r');
	    
	    foreach($countries as $country) {
		    
		    $this->info('Procesando Pais: ' . $country);		    
		    $connection = ConnectionManager::get($country);
			
			$rows = $connection->execute("	SELECT	n.codigoNac, n.IdAto, n.Barcode
											FROM	dbo.MFA_Temp t
											JOIN	dbo.NAC_Ato n ON n.idAto = t.ActCode")->fetchAll('assoc');
			
			while(!feof($file)) {
				
				$line 	= trim(fgets($file));
				$array 	= explode('/', $line);
				$name	= $array[4];
				$nac	= explode('_', $name)[1];
				
				if(explode('_', $name)[0] != 'IMG') {
					continue;
				}
				
				foreach($rows as $row) {
					if($row['codigoNac'] == $nac) {
						$this->out('Código NAC encontrado: ' . $nac);
						
						$insert	= $connection->execute("INSERT INTO dbo.NACFoto (IdAto, NomeArquivo, Barcode, DataCriacao, idEntrevistador)
														VALUES
														(:IdAto, :NomeArquivo, :Barcode, GETDATE(), 888)",
														['IdAto' => $row['IdAto'], 'NomeArquivo' => $name, 'Barcode' => $row['Barcode']],
														['integer', 'string', 'string']);
														
						shell_exec(sprintf('mv %s %s', $dir . DS . $name, $out . DS . explode('_', $country)[0] . DS . $name));
						
					}
				}
				
			}
		    
	    }
	    
	    
    }
    
    public function setup($dbcode = null) 
    {
	    
	    $dir 		= '/mnt/NACImagem/in';
	    $out		= '/mnt/NACImagem';

		$this->info('Leyendo directorio...');
		$output = 	shell_exec(sprintf("find %s -maxdepth 1 -type f -name '*.jpg' > %s", $dir, TMP . 'images' . DS . 'images.txt'));
		$file	=	fopen(TMP . 'images' . DS . 'images.txt', 'r');
	    
		    
	    $this->info('Procesando Pais: ' . $dbcode);		    
	    $connection = ConnectionManager::get($dbcode);
		
		$rows = $connection->execute("	SELECT	n.codigoNac, n.IdAto, n.Barcode
										FROM	dbo.MFA_Temp t
										JOIN	dbo.NAC_Ato n ON n.idAto = t.ActCode")->fetchAll('assoc');
		
		while(!feof($file)) {
			
			$line 	= trim(fgets($file));
			$array 	= explode('/', $line);
			$name	= $array[4];
			$nac	= explode('_', $name)[1];
			
			if(explode('_', $name)[0] != 'IMG') {
				continue;
			}
			
			foreach($rows as $row) {
				if($row['codigoNac'] == $nac) {
					$this->out('Código NAC encontrado: ' . $nac);													
					shell_exec(sprintf('mv %s %s', $dir . DS . $name, $out . DS . explode('_', $dbcode)[0] . DS . $name));
					
				}
			}
			
		}
	    
	    
    }
    
    public function optimize($dbcode = null)
    {
	    
	     $countries = [
			'BR_KWP',
			'CAM_KWP',
			'ECU_KWP',
			'PE_KWP',
			'BO_KWP',
			'CL_KWP',
			'CO_KWP',
			'MX_KWP'  
	    ];
	    
	    foreach($countries as $country) {
	    
		    $connection = ConnectionManager::get($country);
		    
		    $dir 		= '/mnt/NACImagem/' . explode('_', $country)[0];
		    $out		= $dir . DS . 'OLD';
		    $output 	= shell_exec(sprintf("find %s -maxdepth 1 -type f -name '*.jpg' > %s", $dir, TMP . 'images' . DS . $country . '.txt'));
		    $file		= fopen(TMP . 'images' . DS . $country . '.txt', 'r');
		    
		    $rows = $connection->execute("	SELECT	n.codigoNac, n.IdAto, n.Barcode
											FROM	dbo.MFA_Temp t
											JOIN	dbo.NAC_Ato n ON n.idAto = t.ActCode")->fetchAll('assoc');
		    
		    while(!feof($file)) {
			    
			    $line 	= trim(fgets($file));
				$array 	= explode('/', $line);
				$name	= $array[4];
				$nac	= explode('_', $name)[1];
				
				$check = 0;
				foreach($rows as $row) {
					if($row['codigoNac'] == $nac) {
						$check = 1;
					}
				}
				if($check == 0) {
					shell_exec(sprintf('mv %s %s', $dir . DS . $name, $out . DS . $name));
				}
			    
			}
	    
	    }
	    
    }
	
}
