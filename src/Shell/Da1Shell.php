<?php
namespace App\Shell;

use Cake\Console\Shell;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;
use Cake\Datasource\ConnectionManager;


class Da1Shell extends Shell
{

    public function initialize()
    {
        parent::initialize();
    }

	public function main() {
		
		$file 		= fopen(TMP . "da1" . DS . "BR_PNI_FUS.da1", 'w');
		
		$connection = ConnectionManager::get('BR_KWP');
		$inds		= $connection->execute("SELECT RIGHT('0000000' + CAST(PNI_HouseHold_Id as VARCHAR), 7) + RIGHT('000' + CAST(PNI_Individual_Id as VARCHAR), 3) as idDomicilio FROM dbo.Fusion_Portal_Results_OOH_PNI GROUP by PNI_HouseHold_Id, PNI_Individual_Id ORDER by CAST(PNI_HouseHold_Id as INTEGER) ASC ,CAST(PNI_Individual_Id as INTEGER) ASC")->fetchAll('assoc');
		
		$connection = ConnectionManager::get('BR_SPRI_PNI');
		$years		= [2020, 2019, 2018, 2017];
		$layout 	= $connection->execute("SELECT Nombre, spVariable, Largo, PosInicial FROM dbo.RG_Panelis_Variables ORDER by PosInicial ASC")->fetchAll('assoc');
		
		foreach($inds as $ind) {
			
			$main_row = [];
			
			$query	= $connection->execute("SELECT * FROM dbo.RG_Panelis WHERE idDomicilio = :idDomicilio AND ANO IN (2020, 2019, 2018, 2017) ORDER by ANO DESC", ['idDomicilio' => $ind['idDomicilio']]);
			$rows	= $query->fetchAll('assoc');
			
			foreach($years as $year) {
				
				$key 	= array_search($year, array_column($rows, 'ANO'));
				$line = '';
				
				if($key !== false) {
			
					foreach($layout as $element) {
						$line .= str_pad($rows[$key][$element['Nombre']], $element['Largo'], "0", STR_PAD_LEFT);
					}
					
				} else {
									
					foreach($layout as $element) {
						if(in_array($element['Nombre'], ['FM01', 'FM02', 'FM03', 'FM04', 'FM05', 'FM06', 'FM07', 'FM08', 'FM09', 'FM10', 'FM11', 'FM12'])) {
							$line .= str_pad("0", $element['Largo'], "0", STR_PAD_LEFT);
						} else {
							$line .= str_pad($rows[0][$element['Nombre']], $element['Largo'], "0", STR_PAD_LEFT);	
						}
					}
					
				}
				
				$main_row[] = str_pad($line, 500, "0", STR_PAD_RIGHT);
				
			}
			
			$text = implode("", $main_row);
			fwrite($file, str_pad($text, 2000, "0", STR_PAD_RIGHT) . PHP_EOL);
			
		}
		
	}
    
}