<?php
namespace App\Shell;

use Cake\Console\Shell;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;
use Cake\Datasource\ConnectionManager;


class OohShell extends Shell
{
	
	

    public function initialize()
    {
        parent::initialize();
    }

    public function main()
    {
	    
	    $connection = ConnectionManager::get('BR_KWP');
	    
	    $ciclos = $this->getCiclos($connection);
	    
	    foreach($ciclos as $ciclo) {
		    
		    $relations 	= $this->getRelationsAvailable($ciclo, $connection);
		    $oohs		= $this->getOOHs($ciclo, $connection);
		    
		    // Insertar registros utilizando relaciones anteriores
		    foreach($relations as $relation) {
			    
			    $oohs 	= $this->insertPreviousRelation($ciclo, $relation, $oohs, $connection);
			    
		    }
		    
		    // Insertar relaciones nuevas
		    foreach($oohs as $key => $ooh) {
			    
			    $result = $this->insertNewRelations($ciclo, $ooh, $connection);
			    
		    }
		    
	    }
	    
	    	    
	}
	
	private function insertNewRelations($ciclo = null, $ooh = null, $connection = null) {
		
		$this->out(sprintf('Procesando individuo: %s (veces: %s, conteo: %s)', $ooh['IdIndividuo'], $ooh['veces'], $ooh['conteo']));
		
		if($ooh['conteo'] == $ooh['veces']) {
			return true;
		}
		
		do {
			
			$candidate = $connection->execute('SELECT	hin.*
												FROM	dbo.Fusion_Homologacion_IN hin
												LEFT	JOIN dbo.Fusion_Pesos_Imputado_OOH_US ios
														ON ios.Dom_Equi_US = hin.iddomicilio
														AND	ios.Ind_Equi_US = hin.idindividuo
														AND ios.ano = :ano
														AND ios.mes = :mes
												WHERE	hin.ano = :ano2
												AND		hin.messem = :mes2
												AND		hin.region_fusion = :region
												AND		hin.tramo_fusion = :tramo
												AND		hin.sexo = :sexo
												AND		hin.nse_fusion = :nse
												AND		ios.Dom_Equi_US IS NULL
												AND		ios.Ind_Equi_US IS NULL',
												[
													'ano' => $ciclo['ano'],
													'mes' => $ciclo['mes'],
													'ano2' => $ciclo['ano'],
													'mes2' => $ciclo['mes'],
													'region' => $ciclo['region'],
													'tramo' => $ciclo['tramo'],
													'sexo' => $ciclo['sexo'],
													'nse' => $ciclo['nse']
												])->fetch('assoc');
												
			$insert = $connection->execute('INSERT	INTO dbo.Fusion_Pesos_Imputado_OOH_US
													(Ano, mes, IdIndividuo, Region, Tramo_Fusion, Sexo, NSE_Fusion, Valor, New_Peso, Dom_Equi_US, Ind_Equi_US)
											VALUES	(:ano, :mes, :idindividuo, :region, :tramo, :sexo, :nse, :valor, :new_peso, :dom, :ind)',
											[
												'ano' => $ciclo['ano'],
												'mes' => $ciclo['mes'],
												'idindividuo' => $ooh['IdIndividuo'],
												'region' => $ciclo['region'],
												'tramo' => $ciclo['tramo'],
												'sexo' => $ciclo['sexo'],
												'nse' => $ciclo['nse'],
												'valor' => 0,
												'new_peso' => 0,
												'dom' => $candidate['iddomicilio'],
												'ind' => $candidate['idindividuo']
											]);
			
			$this->info(sprintf('Asociando individuo %s [%s de %s] (Dom: %s - Ind: %s)', $ooh['IdIndividuo'], $ooh['conteo'], $ooh['veces'], $candidate['iddomicilio'], $candidate['idindividuo']));
											
			$ooh['conteo'] = $ooh['conteo'] + 1;
			
		} while ($ooh['conteo'] < $ooh['veces']);
		
		return true;
		
	}
	
	private function insertPreviousRelation($ciclo = null, $relation = null, $oohs = null, $connection = null) {
		
		// Si ya se completo la cuota
		$key = array_search($relation['idindividuo'], array_column($oohs, 'IdIndividuo'));
		if($oohs[$key]['conteo'] == $oohs[$key]['veces']) {
			return $oohs;
		}
		
		// Si ya ha sido asignado previamente
		$exists	= $connection->execute('SELECT	*
										FROM	dbo.Fusion_Pesos_Imputado_OOH_US
										WHERE	ano = :ano
										AND		mes = :mes
										AND		region = :region
										AND		tramo_fusion = :tramo
										AND		sexo = :sexo
										AND		nse_fusion = :nse
										AND		Dom_Equi_US = :dom
										AND		Ind_Equi_US = :ind',
										[
											'ano' => $ciclo['ano'],
											'mes' => $ciclo['mes'],
											'region' => $ciclo['region'],
											'tramo' => $ciclo['tramo'],
											'sexo' => $ciclo['sexo'],
											'nse' => $ciclo['nse'],
											'dom' => $relation['Dom_Equi_US'],
											'ind' => $relation['Ind_Equi_US']
										])->fetchAll('assoc');
										
		if(count($exists) > 0) {
			return $oohs;
		}
		
		//$this->warn(sprintf('Conteo: %s - Veces: %s', $oohs[$key]['conteo'], $oohs[$key]['veces']));
		
		$insert = $connection->execute('INSERT	INTO dbo.Fusion_Pesos_Imputado_OOH_US
												(Ano, mes, IdIndividuo, Region, Tramo_Fusion, Sexo, NSE_Fusion, Valor, New_Peso, Dom_Equi_US, Ind_Equi_US)
										VALUES	(:ano, :mes, :idindividuo, :region, :tramo, :sexo, :nse, :valor, :new_peso, :dom, :ind)',
										[
											'ano' => $ciclo['ano'],
											'mes' => $ciclo['mes'],
											'idindividuo' => $relation['idindividuo'],
											'region' => $ciclo['region'],
											'tramo' => $ciclo['tramo'],
											'sexo' => $ciclo['sexo'],
											'nse' => $ciclo['nse'],
											'valor' => 0,
											'new_peso' => 0,
											'dom' => $relation['Dom_Equi_US'],
											'ind' => $relation['Ind_Equi_US']
										]);
										
		if($insert) {
			$oohs[$key]['conteo']++;
		}
		
		return $oohs;
		
	}
	
	private function getRelationsAvailable($ciclo = null, $connection = null) {
	
		$relations = $connection->execute('SELECT 	mix.idindividuo, mix.Dom_Equi_US, mix.Ind_Equi_US, MAX(mix.mes) as mes, MAX(mix.ano) as ano
											FROM	dbo.Fusion_pesos_Imputado_OOH_US mix
											JOIN	dbo.Fusion_Homologacion_IN hin 
													ON hin.iddomicilio = mix.Dom_Equi_US
													AND hin.idindividuo = mix.Ind_Equi_US
													AND hin.messem = :mes
													AND hin.ano = :ano
													AND hin.region_fusion = :region
													AND hin.tramo_fusion = :tramo
													AND hin.sexo = :sexo
													AND hin.nse_fusion = :nse
											JOIN	dbo.Fusion_homologacion_OOH hoo
													ON hoo.idindividuo = mix.idindividuo
													AND hoo.mes = :mes2
													AND hoo.ano = :ano2
													AND hoo.region = :region2
													AND hoo.tramo_fusion = :tramo2
													AND hoo.sexo = :sexo2
													AND hoo.nse_fusion = :nse2
											GROUP	by mix.idindividuo, mix.Dom_Equi_US, mix.Ind_Equi_US
											ORDER	by MAX(mix.mes) ASC',[
														'mes' => $ciclo['mes'],
														'ano' => $ciclo['ano'],
														'region' => $ciclo['region'],
														'tramo' => $ciclo['tramo'],
														'sexo' => $ciclo['sexo'],
														'nse' => $ciclo['nse'],
														'mes2' => $ciclo['mes'],
														'ano2' => $ciclo['ano'],
														'region2' => $ciclo['region'],
														'tramo2' => $ciclo['tramo'],
														'sexo2' => $ciclo['sexo'],
														'nse2' => $ciclo['nse']
													]);
											
		return $relations;
		
	}
	
	private function getCiclos($connection = null) {
		
		$ciclos = $connection->execute('SELECT	ano, messem as mes, region_fusion as region, Tramo_Fusion as tramo, sexo, Nse_Fusion as nse, COUNT(*) as Cantidad
										FROM	dbo.Fusion_Homologacion_IN
										GROUP	by ano, messem, region_fusion, tramo_fusion, sexo, nse_fusion
										ORDER	by ano, messem, region_fusion, tramo_fusion, sexo, nse_fusion');
										
		return $ciclos;
		
	}
	
	private function getOOHs($ciclo = null, $connection = null) {
		
		$oohs = $connection->execute('SELECT 	*, 0 as veces, 0 as conteo
									  FROM 		dbo.Fusion_Homologacion_OOH 
									  WHERE 	ano = :ano 
									  AND 		mes = :mes 
									  AND 		region = :region 
									  AND 		tramo_fusion = :tramo 
									  AND 		sexo = :sexo 
									  AND 		nse_fusion = :nse 
									  ORDER 	by idindividuo',
									  [
										  'ano' => $ciclo['ano'],
										  'mes' => $ciclo['mes'],
										  'region' => $ciclo['region'],
										  'tramo' => $ciclo['tramo'],
										  'sexo' => $ciclo['sexo'],
										  'nse' => $ciclo['nse']
									  ])->fetchAll('assoc');
									  
		$total_oohs	= count($oohs);
		$division	= intval($ciclo['Cantidad'] / $total_oohs);
		$especials	= $ciclo['Cantidad'] - ($division * $total_oohs); 
		
		foreach($oohs as $key => $ooh) {
			
			if($key < $especials) {
				$veces = $division + 1;
			} else {
				$veces = $division;
			}
			
			$oohs[$key]['veces'] = $veces;
			
		}
									  
		return $oohs;
		
	}
}
?>