<?php
namespace App\Shell;

use Cake\Console\Shell;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;
use Cake\Datasource\ConnectionManager;


class Da1EcuadorExpConBbShell extends Shell
{

    public function initialize()
    {
        parent::initialize();
    }

	public function main() {
		
		$file 		= fopen(TMP . "da1" . DS . "Pesos2305_Ecuador_P.da1", 'w');
		
		$connection = ConnectionManager::get('ECU_KWP');
		//$inds		= $connection->execute("SELECT RIGHT('00000000' + CAST(idDomicilio as VARCHAR), 8) as idDomicilio FROM dbo.RG_Domicilios_Pesos GROUP by idDomicilio ORDER by CAST(idDomicilio as INTEGER) ASC")->fetchAll('assoc');
		
		$weights	= $connection->execute("SELECT 	RIGHT('00000000' + CAST(idDomicilio as VARCHAR), 8) as idDomicilio,
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2019 AND MesSem = 1 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2019 AND MesSem = 2 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2019 AND MesSem = 3 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2019 AND MesSem = 4 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2019 AND MesSem = 5 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2019 AND MesSem = 6 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2019 AND MesSem = 7 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2019 AND MesSem = 8 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2019 AND MesSem = 9 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2019 AND MesSem = 10 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2019 AND MesSem = 11 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2019 AND MesSem = 12 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2020 AND MesSem = 1 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2020 AND MesSem = 2 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2020 AND MesSem = 3 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2020 AND MesSem = 4 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2020 AND MesSem = 5 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2020 AND MesSem = 6 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2020 AND MesSem = 7 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2020 AND MesSem = 8 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2020 AND MesSem = 9 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2020 AND MesSem = 10 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2020 AND MesSem = 11 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2020 AND MesSem = 12 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2021 AND MesSem = 1 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2021 AND MesSem = 2 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2021 AND MesSem = 3 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2021 AND MesSem = 4 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2021 AND MesSem = 5 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2021 AND MesSem = 6 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2021 AND MesSem = 7 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2021 AND MesSem = 8 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2021 AND MesSem = 9 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2021 AND MesSem = 10 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2021 AND MesSem = 11 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2021 AND MesSem = 12 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2022 AND MesSem = 1 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2022 AND MesSem = 2 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2022 AND MesSem = 3 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2022 AND MesSem = 4 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2022 AND MesSem = 5 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2022 AND MesSem = 6 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2022 AND MesSem = 7 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2022 AND MesSem = 8 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2022 AND MesSem = 9 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2022 AND MesSem = 10 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2022 AND MesSem = 11 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2022 AND MesSem = 12 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2023 AND MesSem = 1 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2023 AND MesSem = 2 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2023 AND MesSem = 3 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2023 AND MesSem = 4 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2023 AND MesSem = 5 THEN Valor ELSE 0 END) as VARCHAR), 6)
													as [pesos]
											FROM 	dbo.RG_Domicilios_Pesos_Expansion_Con_BB
											WHERE	idPeso = 1
											GROUP	by RIGHT('00000000' + CAST(idDomicilio as VARCHAR), 8)
											ORDER	by 1 ASC")->fetchAll('assoc');
													
		$connection = ConnectionManager::get('ECU_KWP');
		$years		= [2023, 2022, 2021, 2020, 2019];
		$layout 	= $connection->execute("SELECT Nombre, spVariable, Largo, PosInicial FROM dbo.RG_Panelis_Variables ORDER by PosInicial ASC")->fetchAll('assoc');
		
		$n = 0;
		$this->verbose("Cantidad de Domicilios a procesar: " . count($weights));
		foreach($weights as $ind) {
			
			$n++;
			$this->verbose("Procesando domiciio: " . $n);
			$main_row = [];
			
			$query	= $connection->execute("SELECT [Ano],[PAIS],[NPAN],[EDAC],[EDPSH],[NIPSH],[NSE_LOC],[NI],[NI12],[IME],[CIUDAD],[TZON],[NET],[CV],[EDBEQ1],[EDBEQ2],[EDBEQ3],[EDBEQ4],[NSEREG],[NIME18],[KSL01],[KSL02],[GSL01],[GSL02],[GSL03],[GLP01],[GLP02],[GLP03],[GLP04],[GLP05],[GLP06],[GLP07],[GLP08],[GLP09],[GLP10],[GLP11],[GLP12] FROM dbo.RG_Panelis WHERE NPAN = :idDomicilio AND Ano IN (2023, 2022, 2021, 2020, 2019) ORDER by ANO DESC", ['idDomicilio' => $ind['idDomicilio']]);
			$rows	= $query->fetchAll('assoc');
			
			foreach($years as $year) {
				
				$key 	= array_search($year, array_column($rows, 'Ano'));
				$line = '';
				
				if($key !== false) {
			
					foreach($layout as $element) {
						//$this->abort(print_r($rows[$key], true));
						$line .= str_pad($rows[$key][$element['Nombre']], $element['Largo'], "0", STR_PAD_LEFT);
					}
					
				} else {
									
					foreach($layout as $element) {
						$line .= str_pad($rows[0][$element['Nombre']], $element['Largo'], "0", STR_PAD_LEFT);	
					}
					
				}
				
				$main_row[] = str_pad($line, 200, "0", STR_PAD_RIGHT);
				
			}
			
			//$clave = array_search($ind['idDomicilio'], array_column($weights, 'idDomicilio'));
			$pesosPeriodos = $ind['pesos'];
			
			
			$text = implode("", $main_row).$pesosPeriodos;
			//fwrite($file, str_pad($text, 2806, "0", STR_PAD_RIGHT) . PHP_EOL);
			fwrite($file, str_pad($text, ( (5 * 200) + strlen($ind['pesos']) ), "0", STR_PAD_RIGHT) . PHP_EOL);
			
		}
		
	}
    
}