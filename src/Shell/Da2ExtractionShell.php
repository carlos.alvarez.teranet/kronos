<?php
namespace App\Shell;

use Cake\Console\Shell;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;
use Cake\Datasource\ConnectionManager;


class Da2ExtractionShell extends Shell
{

    public function initialize()
    {
        parent::initialize();
        $this->countries = [
	        'MX_KWP' => 'MEXICO'
        ];
    }

	public function main($country = null, $layout = null, $idProduto = null, $desde = null, $hasta = null) {
		
		$source		= "/mnt/procesos/ACTOS/" . $this->countries[$country] . "/" . substr($hasta, 0, 4) . "_" . substr($hasta, -2) . "/ATO" . str_pad($idProduto, 3, '0', STR_PAD_LEFT) . "." . substr($hasta, -2) . substr($hasta, 3, 1) . "_Quetzal";
		$file 		= fopen($source, 'wa+');
		
		$this->out($source);

		$connection = ConnectionManager::get($country);
		
		$query		= $connection->execute("SELECT	DA2_217
											FROM	$layout
											WHERE	idProduto = :idProduto
											AND		Ano * 100 + Mes BETWEEN :desde AND :hasta
											AND		PesoMensual > 0", 
												['idProduto' => $idProduto, 'desde' => $desde, 'hasta' => $hasta]
											);
		$total 	= $query->count();
		$actual = 0;
		
		$this->info("Cantidad de lineas a procesar: " . $total);
		
		die();
											
		while($results = $query->fetch('assoc')) {
			
			$actual++;
			$text = implode("", $results);
			$this->verbose("Procesando linea: " . $actual . " (" . ($actual / $total * 100) . ")");
			fwrite($file, $text . PHP_EOL);
			
			
		}
		
		fclose($file);
		
		
	}
    
}