<?php
namespace App\Shell;

use Cake\Console\Shell;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;
use Cake\Datasource\ConnectionManager;


class Da1ColombiaShell extends Shell
{

    public function initialize()
    {
        parent::initialize();
    }

	public function main() {

		$file 		= fopen(TMP . "da1" . DS . "Pesos2305_Colombia_P.da1", 'w');

		$connection = ConnectionManager::get('CO_KWP');
		//$inds		= $connection->execute("SELECT RIGHT('00000000' + CAST(idDomicilio as VARCHAR), 8) as idDomicilio FROM dbo.RG_Domicilios_Pesos WHERE Ano >= 2019 AND idPeso = 1 GROUP by idDomicilio ORDER by CAST(idDomicilio as INTEGER) ASC")->fetchAll('assoc');

		$weights	= $connection->execute("SELECT 	RIGHT('00000000' + CAST(rgd.idDomicilio as VARCHAR), 8) as idDomicilio,
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2019 AND rgd.MesSem = 1 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2019 AND rgd.MesSem = 2 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2019 AND rgd.MesSem = 3 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2019 AND rgd.MesSem = 4 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2019 AND rgd.MesSem = 5 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2019 AND rgd.MesSem = 6 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2019 AND rgd.MesSem = 7 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2019 AND rgd.MesSem = 8 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2019 AND rgd.MesSem = 9 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2019 AND rgd.MesSem = 10 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2019 AND rgd.MesSem = 11 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2019 AND rgd.MesSem = 12 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2020 AND rgd.MesSem = 1 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2020 AND rgd.MesSem = 2 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2020 AND rgd.MesSem = 3 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2020 AND rgd.MesSem = 4 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2020 AND rgd.MesSem = 5 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2020 AND rgd.MesSem = 6 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2020 AND rgd.MesSem = 7 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2020 AND rgd.MesSem = 8 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2020 AND rgd.MesSem = 9 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2020 AND rgd.MesSem = 10 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2020 AND rgd.MesSem = 11 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2020 AND rgd.MesSem = 12 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2021 AND rgd.MesSem = 1 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2021 AND rgd.MesSem = 2 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2021 AND rgd.MesSem = 3 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2021 AND rgd.MesSem = 4 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2021 AND rgd.MesSem = 5 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2021 AND rgd.MesSem = 6 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2021 AND rgd.MesSem = 7 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2021 AND rgd.MesSem = 8 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2021 AND rgd.MesSem = 9 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2021 AND rgd.MesSem = 10 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2021 AND rgd.MesSem = 11 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2021 AND rgd.MesSem = 12 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2022 AND rgd.MesSem = 1 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 1 AND rgc.idPeso = 2 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 1 AND rgc.idPeso = 3 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 1 AND rgc.idPeso = 4 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 1 AND rgc.idPeso = 5 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 1 AND rgc.idPeso = 6 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 1 AND rgc.idPeso = 7 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 1 AND rgc.idPeso = 8 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 1 AND rgc.idPeso = 20 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2022 AND rgd.MesSem = 2 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 2 AND rgc.idPeso = 2 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 2 AND rgc.idPeso = 3 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 2 AND rgc.idPeso = 4 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 2 AND rgc.idPeso = 5 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 2 AND rgc.idPeso = 6 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 2 AND rgc.idPeso = 7 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 2 AND rgc.idPeso = 8 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 2 AND rgc.idPeso = 20 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2022 AND rgd.MesSem = 3 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 3 AND rgc.idPeso = 2 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 3 AND rgc.idPeso = 3 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 3 AND rgc.idPeso = 4 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 3 AND rgc.idPeso = 5 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 3 AND rgc.idPeso = 6 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 3 AND rgc.idPeso = 7 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 3 AND rgc.idPeso = 8 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 3 AND rgc.idPeso = 20 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2022 AND rgd.MesSem = 4 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 4 AND rgc.idPeso = 2 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 4 AND rgc.idPeso = 3 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 4 AND rgc.idPeso = 4 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 4 AND rgc.idPeso = 5 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 4 AND rgc.idPeso = 6 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 4 AND rgc.idPeso = 7 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 4 AND rgc.idPeso = 8 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 4 AND rgc.idPeso = 20 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2022 AND rgd.MesSem = 5 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 5 AND rgc.idPeso = 2 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 5 AND rgc.idPeso = 3 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 5 AND rgc.idPeso = 4 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 5 AND rgc.idPeso = 5 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 5 AND rgc.idPeso = 6 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 5 AND rgc.idPeso = 7 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 5 AND rgc.idPeso = 8 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 5 AND rgc.idPeso = 20 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2022 AND rgd.MesSem = 6 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 6 AND rgc.idPeso = 2 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 6 AND rgc.idPeso = 3 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 6 AND rgc.idPeso = 4 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 6 AND rgc.idPeso = 5 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 6 AND rgc.idPeso = 6 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 6 AND rgc.idPeso = 7 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 6 AND rgc.idPeso = 8 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 6 AND rgc.idPeso = 20 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2022 AND rgd.MesSem = 7 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 7 AND rgc.idPeso = 2 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 7 AND rgc.idPeso = 3 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 7 AND rgc.idPeso = 4 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 7 AND rgc.idPeso = 5 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 7 AND rgc.idPeso = 6 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 7 AND rgc.idPeso = 7 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 7 AND rgc.idPeso = 8 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 7 AND rgc.idPeso = 20 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2022 AND rgd.MesSem = 8 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 8 AND rgc.idPeso = 2 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 8 AND rgc.idPeso = 3 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 8 AND rgc.idPeso = 4 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 8 AND rgc.idPeso = 5 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 8 AND rgc.idPeso = 6 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 8 AND rgc.idPeso = 7 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 8 AND rgc.idPeso = 8 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 8 AND rgc.idPeso = 20 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2022 AND rgd.MesSem = 9 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 9 AND rgc.idPeso = 2 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 9 AND rgc.idPeso = 3 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 9 AND rgc.idPeso = 4 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 9 AND rgc.idPeso = 5 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 9 AND rgc.idPeso = 6 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 9 AND rgc.idPeso = 7 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 9 AND rgc.idPeso = 8 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 9 AND rgc.idPeso = 20 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2022 AND rgd.MesSem = 10 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 10 AND rgc.idPeso = 2 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 10 AND rgc.idPeso = 3 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 10 AND rgc.idPeso = 4 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 10 AND rgc.idPeso = 5 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 10 AND rgc.idPeso = 6 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 10 AND rgc.idPeso = 7 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 10 AND rgc.idPeso = 8 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 10 AND rgc.idPeso = 20 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2022 AND rgd.MesSem = 11 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 11 AND rgc.idPeso = 2 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 11 AND rgc.idPeso = 3 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 11 AND rgc.idPeso = 4 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 11 AND rgc.idPeso = 5 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 11 AND rgc.idPeso = 6 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 11 AND rgc.idPeso = 7 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 11 AND rgc.idPeso = 8 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 11 AND rgc.idPeso = 20 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2022 AND rgd.MesSem = 12 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 12 AND rgc.idPeso = 2 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 12 AND rgc.idPeso = 3 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 12 AND rgc.idPeso = 4 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 12 AND rgc.idPeso = 5 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 12 AND rgc.idPeso = 6 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 12 AND rgc.idPeso = 7 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 12 AND rgc.idPeso = 8 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 12 AND rgc.idPeso = 20 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2023 AND rgd.MesSem = 1 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 1 AND rgc.idPeso = 2 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 1 AND rgc.idPeso = 3 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 1 AND rgc.idPeso = 4 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 1 AND rgc.idPeso = 5 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 1 AND rgc.idPeso = 6 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 1 AND rgc.idPeso = 7 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 1 AND rgc.idPeso = 8 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 1 AND rgc.idPeso = 20 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2023 AND rgd.MesSem = 2 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 2 AND rgc.idPeso = 2 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 2 AND rgc.idPeso = 3 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 2 AND rgc.idPeso = 4 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 2 AND rgc.idPeso = 5 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 2 AND rgc.idPeso = 6 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 2 AND rgc.idPeso = 7 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 2 AND rgc.idPeso = 8 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 2 AND rgc.idPeso = 20 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2023 AND rgd.MesSem = 3 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 3 AND rgc.idPeso = 2 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 3 AND rgc.idPeso = 3 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 3 AND rgc.idPeso = 4 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 3 AND rgc.idPeso = 5 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 3 AND rgc.idPeso = 6 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 3 AND rgc.idPeso = 7 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 3 AND rgc.idPeso = 8 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 3 AND rgc.idPeso = 20 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2023 AND rgd.MesSem = 4 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 4 AND rgc.idPeso = 2 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 4 AND rgc.idPeso = 3 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 4 AND rgc.idPeso = 4 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 4 AND rgc.idPeso = 5 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 4 AND rgc.idPeso = 6 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 4 AND rgc.idPeso = 7 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 4 AND rgc.idPeso = 8 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 4 AND rgc.idPeso = 20 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2023 AND rgd.MesSem = 5 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 5 AND rgc.idPeso = 2 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 5 AND rgc.idPeso = 3 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 5 AND rgc.idPeso = 4 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 5 AND rgc.idPeso = 5 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 5 AND rgc.idPeso = 6 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 5 AND rgc.idPeso = 7 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 5 AND rgc.idPeso = 8 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 5 AND rgc.idPeso = 20 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6)
											as [pesos]
									FROM 	dbo.RG_Domicilios_Pesos rgd
									LEFT JOIN	dbo.RG_Domicilios_Pesos_CSV rgc ON rgc.Iddomicilio = rgd.Iddomicilio AND rgc.Ano = rgd.Ano AND rgc.MesSem = rgd.messem
									WHERE	rgd.idPeso = 1
									AND		rgd.Ano >= 2019
									GROUP	by RIGHT('00000000' + CAST(rgd.idDomicilio as VARCHAR), 8)
									ORDER	by 1 ASC")->fetchAll('assoc');

		$connection = ConnectionManager::get('CO_KWP');
		$years		= [2023, 2022, 2021, 2020, 2019];
		$layout 	= $connection->execute("SELECT Nombre, spVariable, Largo, PosInicial FROM dbo.RG_Panelis_Variables ORDER by PosInicial ASC")->fetchAll('assoc');

		$n = 0;
		$this->verbose("Cantidad de Domicilios a procesar: " . count($weights));
		foreach($weights as $ind) {

			$n++;
			$this->verbose("Procesando Domicilio: " . $n);
			$main_row = [];

		$query	= $connection->execute("SELECT [PAIS],[NPAN],[EDAC],[ANAC],[SEXAC],[SLAC],[NIAC],[ACPSH],[EDPSH],[SEXPSH],[SLPSH],[NIPSH],[NSE_LOC],[PNSE_LOC],[PLSM],[CLSM],[PNSELP],[CNSELP],[NI],[NIMA18],[NI15],[NI12],[NI5],[NI3],[N_4_5],[N_6_12],[N_10_15],[N_13_17],[N_16_19],[ADOL],[PHIJO],[HIJOS],[IME],[IMA],[FEM],[EDFEMMEN],[EDFEMMAY],[MASC],[EDMASCMEN],[EDMASCMAY],[ZONA],[CIUDAD],[DIST],[R5],[FRAC],[RAD],[TZON],[AUTOS],[LLV],[LLF],[LN],[PC],[NET],[ANOS_BA],[ANOS_TE],[EMP_NET],[NTEL],[TVCOLOR],[VD],[DVD],[REFRIG],[LVJ],[VCAB],[TVSA],[TVBN],[CELU],[CG],[MICR],[ASP],[ENC],[EQM],[PAVA],[OLLA],[PLV],[RLJ],[FILM],[FOTO_ROL],[FOTO_DIG],[FAX],[HTHE],[SV],[GOS],[GAT],[ET],[THOG],[EDMA],[TIPH],[TOIL],[AMB],[DORM],[RADI],[TRAN],[LR],[LD],[CANA],[SIND1],[AIND1],[PIND1],[CEL1],[SIND2],[AIND2],[PIND2],[CEL2],[SIND3],[AIND3],[PIND3],[CEL3],[SIND4],[AIND4],[PIND4],[CEL4],[SIND5],[AIND5],[PIND5],[CEL5],[SIND6],[AIND6],[PIND6],[CEL6],[SIND7],[AIND7],[PIND7],[CEL7],[SIND8],[AIND8],[PIND8],[CEL8],[SIND9],[AIND9],[PIND9],[CEL9],[SIND10],[AIND10],[PIND10],[CEL10],[SIND11],[AIND11],[PIND11],[CEL11],[SIND12],[AIND12],[PIND12],[CEL12],[SIND13],[AIND13],[PIND13],[CEL13],[SIND14],[AIND14],[PIND14],[CEL14],[SIND15],[AIND15],[PIND15],[CEL15],[SIND16],[AIND16],[PIND16],[CEL16],[PER_RAZ],[PER_SRA],[PER_GIG],[PER_GRA],[PER_MED],[PER_PEQ],[PER_MIN],[GAT_RAZ],[GAT_SRA],[CMAS],[CV],[COMEDOR],[LIVING],[DORMPPAL],[DORMIT],[BAÑOPPAL],[COCINA],[TERRAZA],[KIDS12],[NSE_REG],[CLOROX],[EDBEQ1],[EDBEQ2],[EDBEQ3],[EDBEQ4],[N_2_6],[N_7_10],[N_11_14],[N_13_18],[N_19_25],[ST_PAN],[ST_HYS],[SEGMENTO_SHAMPO],[SPECTRUM],[LAUNDRY],[CW2009_P2],[CW2009_P4],[CW2009_P8],[CW2009_imc],[N_0],[N_1],[N_2],[N_3_5],[N_6_10],[N_11_mas],[ECAC],[CGREEN],[HIJOSSH],[CS2009_SH],[PNSE2011],[NSE2011],[LAV],[GL_Tiene],[GL_Compra],[GL_Tiras],[GL_Lac],[GL_Marca],[C123PG],[Cluster_Think],[Cluster_Saude],[Shopper_Seg],[Cluster_Imcama],[shop_trib],[Cuadrante],[Hog_IBS],[Meses_Bebes],[A_C_Trabajadora],[SSeg_Nacional],[NSEREG],[NIME18],[EDJF],[FEM13_23],[FEM24_49],[FEM50_99],[FEM18_45],[FEM13_49],[SLIMA],[SLIME],[CMEDIA],[BICICLETA],[CONGELADOR],[CONSOLAS],[EMPLEADADO],[EMPLEADOMEN],[IPADTABLET],[LAVADORASINSECA],[LAVADORACONSECA],[LAVADORASEMI],[MOTOCICLETA],[NOTEBOOK],[REFRIGERADORUNA],[REFRIGERADORDUPLEX],[SECADORA],[TELEFONOSINAC],[TELEFONOCONAC],[TELEVISIONACOLOR],[BANDAANCHA],[ANTEPARABO],[BATIDOELEC],[CALENTADORAGUA],[CAFETERA],[CENTRIFUGA],[COCINA2],[FILTROAGUA],[FREIDORAELEC],[HERVIDOR],[PLANCHAELEC],[TELEFIJO],[TVPAGA],[WIFI],[ELECTRICIDAD],[GAS_CANALIZADO],[CALEFACCION],[AIREACON],[INODORO],[AGUACANALIZADA],[CALLEPAVIM],[REGION],[ESTRATO],[CUADRANTENEW],[NEW_SAMPLE],[IMEQNew],[REGIONew],[ESTRATONew],[Origen],[MIRROR01],[MIRROR02],[MIRROR03],[MIRROR04],[MIRROR05],[MIRROR06],[MIRROR07],[MIRROR08],[MIRROR09],[MIRROR10],[MIRROR11],[MIRROR12],[IME_NEW_M01],[IME_NEW_M02],[IME_NEW_M03],[IME_NEW_M04],[IME_NEW_M05],[IME_NEW_M06],[IME_NEW_M07],[IME_NEW_M08],[IME_NEW_M09],[IME_NEW_M10],[IME_NEW_M11],[IME_NEW_M12],[Ano] FROM dbo.RG_Panelis WHERE idDomicilio = :idDomicilio AND Ano IN (2023, 2022, 2021, 2020, 2019) ORDER by ANO DESC", ['idDomicilio' => $ind['idDomicilio']]);
			$rows	= $query->fetchAll('assoc');

			foreach($years as $year) {

				$key 	= array_search($year, array_column($rows, 'Ano'));
				$line = '';

				if($key !== false) {

					foreach($layout as $element) {
						//$this->out($rows[$key][$element['Nombre']] . " - " . $element['Nombre']);
						$rows[$key][$element['Nombre']] = (!is_numeric((int)$rows[$key][$element['Nombre']])) ? '0' : $rows[$key][$element['Nombre']];
						$line .= str_pad($rows[$key][$element['Nombre']] * 1, $element['Largo'], "0", STR_PAD_LEFT);
						//$this->info($rows[$key][$element['Nombre']] . " - " . $element['Nombre']);						
					}

				} else {

					foreach($layout as $element) {
						//$this->out($rows[0][$element['Nombre']] . " - " . $element['Nombre']);
						$rows[0][$element['Nombre']] = (!is_numeric((int)$rows[0][$element['Nombre']])) ? '0' : $rows[0][$element['Nombre']];
						$line .= str_pad($rows[0][$element['Nombre']] * 1, $element['Largo'], "0", STR_PAD_LEFT);
						//$this->info($rows[0][$element['Nombre']] . " - " . $element['Nombre']);
					}

				}

				$main_row[] = str_pad($line, 500, "0", STR_PAD_RIGHT);
				//$this->abort("END");

			}

			//$clave = array_search($ind['idDomicilio'], array_column($weights, 'idDomicilio'));
			$pesosPeriodos = $ind['pesos'];


			$text = implode("", $main_row).$pesosPeriodos;
			//fwrite($file, str_pad($text, 2926, "0", STR_PAD_RIGHT) . PHP_EOL);
			fwrite($file, str_pad($text, ( (5 * 500) + strlen($ind['pesos']) ), "0", STR_PAD_RIGHT) . PHP_EOL);


		}

	}

}