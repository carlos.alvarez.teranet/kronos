<?php
namespace App\Shell;

use Cake\Console\Shell;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;
use Cake\Datasource\ConnectionManager;


class Da1BrasilOohShell extends Shell
{

    public function initialize()
    {
        parent::initialize();
    }

	public function main() {
		
		$file 		= fopen(TMP . "da1" . DS . "Pesos2305_Brasil_OOH.da1", 'w');
		
		$connection = ConnectionManager::get('BR_KWP');
		//$inds		= $connection->execute("SELECT RIGHT('000000000' + CAST(idIndividuo as VARCHAR), 9) as idIndividuo FROM dbo.RG_Individuos_Pesos_OOH WHERE Ano >= 2019 GROUP by idIndividuo ORDER by CAST(idIndividuo as VARCHAR) ASC")->fetchAll('assoc');
		
		$weights	= $connection->execute("SELECT RIGHT('000000000' + CAST(rgd.idIndividuo as VARCHAR), 9) as idIndividuo,
											RIGHT('00000000' + CAST(MAX(CASE WHEN rgd.Ano = 2019 AND rgd.MesSem = 1 THEN rgd.Valor ELSE 0 END) as VARCHAR), 8) +
											RIGHT('00000000' + CAST(MAX(CASE WHEN rgd.Ano = 2019 AND rgd.MesSem = 2 THEN rgd.Valor ELSE 0 END) as VARCHAR), 8) +
											RIGHT('00000000' + CAST(MAX(CASE WHEN rgd.Ano = 2019 AND rgd.MesSem = 3 THEN rgd.Valor ELSE 0 END) as VARCHAR), 8) +
											RIGHT('00000000' + CAST(MAX(CASE WHEN rgd.Ano = 2019 AND rgd.MesSem = 4 THEN rgd.Valor ELSE 0 END) as VARCHAR), 8) +
											RIGHT('00000000' + CAST(MAX(CASE WHEN rgd.Ano = 2019 AND rgd.MesSem = 5 THEN rgd.Valor ELSE 0 END) as VARCHAR), 8) +
											RIGHT('00000000' + CAST(MAX(CASE WHEN rgd.Ano = 2019 AND rgd.MesSem = 6 THEN rgd.Valor ELSE 0 END) as VARCHAR), 8) +
											RIGHT('00000000' + CAST(MAX(CASE WHEN rgd.Ano = 2019 AND rgd.MesSem = 7 THEN rgd.Valor ELSE 0 END) as VARCHAR), 8) +
											RIGHT('00000000' + CAST(MAX(CASE WHEN rgd.Ano = 2019 AND rgd.MesSem = 8 THEN rgd.Valor ELSE 0 END) as VARCHAR), 8) +
											RIGHT('00000000' + CAST(MAX(CASE WHEN rgd.Ano = 2019 AND rgd.MesSem = 9 THEN rgd.Valor ELSE 0 END) as VARCHAR), 8) +
											RIGHT('00000000' + CAST(MAX(CASE WHEN rgd.Ano = 2019 AND rgd.MesSem = 10 THEN rgd.Valor ELSE 0 END) as VARCHAR), 8) +
											RIGHT('00000000' + CAST(MAX(CASE WHEN rgd.Ano = 2019 AND rgd.MesSem = 11 THEN rgd.Valor ELSE 0 END) as VARCHAR), 8) +
											RIGHT('00000000' + CAST(MAX(CASE WHEN rgd.Ano = 2019 AND rgd.MesSem = 12 THEN rgd.Valor ELSE 0 END) as VARCHAR), 8) +
											RIGHT('00000000' + CAST(MAX(CASE WHEN rgd.Ano = 2020 AND rgd.MesSem = 1 THEN rgd.Valor ELSE 0 END) as VARCHAR), 8) +
											RIGHT('00000000' + CAST(MAX(CASE WHEN rgd.Ano = 2020 AND rgd.MesSem = 2 THEN rgd.Valor ELSE 0 END) as VARCHAR), 8) +
											RIGHT('00000000' + CAST(MAX(CASE WHEN rgd.Ano = 2020 AND rgd.MesSem = 3 THEN rgd.Valor ELSE 0 END) as VARCHAR), 8) +
											RIGHT('00000000' + CAST(MAX(CASE WHEN rgd.Ano = 2020 AND rgd.MesSem = 4 THEN rgd.Valor ELSE 0 END) as VARCHAR), 8) +
											RIGHT('00000000' + CAST(MAX(CASE WHEN rgd.Ano = 2020 AND rgd.MesSem = 5 THEN rgd.Valor ELSE 0 END) as VARCHAR), 8) +
											RIGHT('00000000' + CAST(MAX(CASE WHEN rgd.Ano = 2020 AND rgd.MesSem = 6 THEN rgd.Valor ELSE 0 END) as VARCHAR), 8) +
											RIGHT('00000000' + CAST(MAX(CASE WHEN rgd.Ano = 2020 AND rgd.MesSem = 7 THEN rgd.Valor ELSE 0 END) as VARCHAR), 8) +
											RIGHT('00000000' + CAST(MAX(CASE WHEN rgd.Ano = 2020 AND rgd.MesSem = 8 THEN rgd.Valor ELSE 0 END) as VARCHAR), 8) +
											RIGHT('00000000' + CAST(MAX(CASE WHEN rgd.Ano = 2020 AND rgd.MesSem = 9 THEN rgd.Valor ELSE 0 END) as VARCHAR), 8) +
											RIGHT('00000000' + CAST(MAX(CASE WHEN rgd.Ano = 2020 AND rgd.MesSem = 10 THEN rgd.Valor ELSE 0 END) as VARCHAR), 8) +
											RIGHT('00000000' + CAST(MAX(CASE WHEN rgd.Ano = 2020 AND rgd.MesSem = 11 THEN rgd.Valor ELSE 0 END) as VARCHAR), 8) +
											RIGHT('00000000' + CAST(MAX(CASE WHEN rgd.Ano = 2020 AND rgd.MesSem = 12 THEN rgd.Valor ELSE 0 END) as VARCHAR), 8) +
											RIGHT('00000000' + CAST(MAX(CASE WHEN rgd.Ano = 2021 AND rgd.MesSem = 1 THEN rgd.Valor ELSE 0 END) as VARCHAR), 8) +
											RIGHT('00000000' + CAST(MAX(CASE WHEN rgd.Ano = 2021 AND rgd.MesSem = 2 THEN rgd.Valor ELSE 0 END) as VARCHAR), 8) +
											RIGHT('00000000' + CAST(MAX(CASE WHEN rgd.Ano = 2021 AND rgd.MesSem = 3 THEN rgd.Valor ELSE 0 END) as VARCHAR), 8) +
											RIGHT('00000000' + CAST(MAX(CASE WHEN rgd.Ano = 2021 AND rgd.MesSem = 4 THEN rgd.Valor ELSE 0 END) as VARCHAR), 8) +
											RIGHT('00000000' + CAST(MAX(CASE WHEN rgd.Ano = 2021 AND rgd.MesSem = 5 THEN rgd.Valor ELSE 0 END) as VARCHAR), 8) +
											RIGHT('00000000' + CAST(MAX(CASE WHEN rgd.Ano = 2021 AND rgd.MesSem = 6 THEN rgd.Valor ELSE 0 END) as VARCHAR), 8) +
											RIGHT('00000000' + CAST(MAX(CASE WHEN rgd.Ano = 2021 AND rgd.MesSem = 7 THEN rgd.Valor ELSE 0 END) as VARCHAR), 8) +
											RIGHT('00000000' + CAST(MAX(CASE WHEN rgd.Ano = 2021 AND rgd.MesSem = 8 THEN rgd.Valor ELSE 0 END) as VARCHAR), 8) +
											RIGHT('00000000' + CAST(MAX(CASE WHEN rgd.Ano = 2021 AND rgd.MesSem = 9 THEN rgd.Valor ELSE 0 END) as VARCHAR), 8) +
											RIGHT('00000000' + CAST(MAX(CASE WHEN rgd.Ano = 2021 AND rgd.MesSem = 10 THEN rgd.Valor ELSE 0 END) as VARCHAR), 8) +
											RIGHT('00000000' + CAST(MAX(CASE WHEN rgd.Ano = 2021 AND rgd.MesSem = 11 THEN rgd.Valor ELSE 0 END) as VARCHAR), 8) +
											RIGHT('00000000' + CAST(MAX(CASE WHEN rgd.Ano = 2021 AND rgd.MesSem = 12 THEN rgd.Valor ELSE 0 END) as VARCHAR), 8) +
											RIGHT('00000000' + CAST(MAX(CASE WHEN rgd.Ano = 2022 AND rgd.MesSem = 1 THEN rgd.Valor ELSE 0 END) as VARCHAR), 8) +
											RIGHT('00000000' + CAST(MAX(CASE WHEN rgd.Ano = 2022 AND rgd.MesSem = 2 THEN rgd.Valor ELSE 0 END) as VARCHAR), 8) +
											RIGHT('00000000' + CAST(MAX(CASE WHEN rgd.Ano = 2022 AND rgd.MesSem = 3 THEN rgd.Valor ELSE 0 END) as VARCHAR), 8) +
											RIGHT('00000000' + CAST(MAX(CASE WHEN rgd.Ano = 2022 AND rgd.MesSem = 4 THEN rgd.Valor ELSE 0 END) as VARCHAR), 8) +
											RIGHT('00000000' + CAST(MAX(CASE WHEN rgd.Ano = 2022 AND rgd.MesSem = 5 THEN rgd.Valor ELSE 0 END) as VARCHAR), 8) +
											RIGHT('00000000' + CAST(MAX(CASE WHEN rgd.Ano = 2022 AND rgd.MesSem = 6 THEN rgd.Valor ELSE 0 END) as VARCHAR), 8) +
											RIGHT('00000000' + CAST(MAX(CASE WHEN rgd.Ano = 2022 AND rgd.MesSem = 7 THEN rgd.Valor ELSE 0 END) as VARCHAR), 8) +
											RIGHT('00000000' + CAST(MAX(CASE WHEN rgd.Ano = 2022 AND rgd.MesSem = 8 THEN rgd.Valor ELSE 0 END) as VARCHAR), 8) +
											RIGHT('00000000' + CAST(MAX(CASE WHEN rgd.Ano = 2022 AND rgd.MesSem = 9 THEN rgd.Valor ELSE 0 END) as VARCHAR), 8) +
											RIGHT('00000000' + CAST(MAX(CASE WHEN rgd.Ano = 2022 AND rgd.MesSem = 10 THEN rgd.Valor ELSE 0 END) as VARCHAR), 8) +
											RIGHT('00000000' + CAST(MAX(CASE WHEN rgd.Ano = 2022 AND rgd.MesSem = 11 THEN rgd.Valor ELSE 0 END) as VARCHAR), 8) +
											RIGHT('00000000' + CAST(MAX(CASE WHEN rgd.Ano = 2022 AND rgd.MesSem = 12 THEN rgd.Valor ELSE 0 END) as VARCHAR), 8) +
											RIGHT('00000000' + CAST(MAX(CASE WHEN rgd.Ano = 2023 AND rgd.MesSem = 1 THEN rgd.Valor ELSE 0 END) as VARCHAR), 8) +
											RIGHT('00000000' + CAST(MAX(CASE WHEN rgd.Ano = 2023 AND rgd.MesSem = 2 THEN rgd.Valor ELSE 0 END) as VARCHAR), 8) +
											RIGHT('00000000' + CAST(MAX(CASE WHEN rgd.Ano = 2023 AND rgd.MesSem = 3 THEN rgd.Valor ELSE 0 END) as VARCHAR), 8) +
											RIGHT('00000000' + CAST(MAX(CASE WHEN rgd.Ano = 2023 AND rgd.MesSem = 4 THEN rgd.Valor ELSE 0 END) as VARCHAR), 8) +
											RIGHT('00000000' + CAST(MAX(CASE WHEN rgd.Ano = 2023 AND rgd.MesSem = 5 THEN rgd.Valor ELSE 0 END) as VARCHAR), 8)
											as [pesos]
									FROM 	dbo.RG_Individuos_Pesos_OOH rgd
									WHERE	rgd.Ano >= 2019
									GROUP	by RIGHT('000000000' + CAST(rgd.idIndividuo as VARCHAR), 9)
									ORDER	by 1 ASC")->fetchAll('assoc');
																						
		$connection = ConnectionManager::get('BR_SPRI_OOH');
		$years		= [2023, 2022, 2021, 2020, 2019];
		$layout 	= $connection->execute("SELECT Nombre, spVariable, Largo, PosInicial FROM dbo.RG_Panelis_Variables ORDER by PosInicial ASC")->fetchAll('assoc');
		
		$n = 0;
		$this->verbose("Cantidad de Individuos a procesar: " . count($weights));
		$connection = ConnectionManager::get('BR_KWP');
		foreach($weights as $ind) {
			
			$n++;
			$this->verbose("Procesando Individuo: " . $n . " Código: " . $ind['idIndividuo']);
			$main_row = [];
			
			$query	= $connection->execute("SELECT [Pais], [idIndividuo], [ciudad], [sexo], [edad], [nse], [trabaja], [Ano] FROM dbo.RG_Panelis_OOH WHERE RIGHT('000000000' + CAST(idIndividuo as VARCHAR), 9) = :idIndividuo AND Ano IN (2023, 2022, 2021, 2020, 2019) ORDER by ANO DESC", ['idIndividuo' => $ind['idIndividuo']]);
			$rows	= $query->fetchAll('assoc');
			
			foreach($years as $year) {
				
				$key 	= array_search($year, array_column($rows, 'Ano'));
				$line = '';				
				
				if($key !== false) {
			
					foreach($layout as $element) {
						//$this->abort(print_r($rows[$key], true));
						//$rows[$key][$element['Nombre']] = (!is_numeric($rows[$key][$element['Nombre']])) ? 0 : $rows[$key][$element['Nombre']];
						$line .= str_pad($rows[$key][$element['Nombre']], $element['Largo'], "0", STR_PAD_LEFT);
						
						if(strlen($rows[$key][$element['Nombre']]) > $element['Largo']) {
							$this->warn("idIndividuo " . $ind['idIndividuo'] . " en año " . $year . " posee la variable " . $element['Nombre'] . " con un largo " . strlen($rows[$key][$element['Nombre']]) . " y se esperaba un largo " . $element['Largo']);
						}
					}
					
				} else {
					
					foreach($layout as $element) {
						//$rows[0][$element['Nombre']] = (!is_numeric($rows[0][$element['Nombre']])) ? 0 : $rows[0][$element['Nombre']];
						$line .= str_pad($rows[0][$element['Nombre']], $element['Largo'], "0", STR_PAD_LEFT);	
					}
					
				}
				
				$main_row[] = str_pad($line, 100, "0", STR_PAD_RIGHT);
				
			}
			
			//$clave = array_search($ind['idIndividuo'], array_column($weights, 'idIndividuo'));
			$pesosPeriodos = $ind['pesos'];
			
			
			$text = implode("", $main_row).$pesosPeriodos;
			//fwrite($file, str_pad($text, 904, "0", STR_PAD_RIGHT) . PHP_EOL);
			fwrite($file, str_pad($text, ( (5 * 100) + strlen($ind['pesos']) ), "0", STR_PAD_RIGHT) . PHP_EOL);
			
		}
		
	}
    
}