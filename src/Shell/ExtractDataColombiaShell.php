<?php
namespace App\Shell;

use Cake\Console\Shell;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;
use PHPExcel;
use PHPExcel_IOFactory;
use Cake\Datasource\ConnectionManager;
use Cake\Mailer\Email;
use ZipArchive;


class ExtractDataColombiaShell extends Shell
{

    public function main()
    {
    	$connection = ConnectionManager::get('CO_KWP');	
    	$categories	= $connection->execute('SELECT DB_NAME() as Country, * FROM dbo.A_PainelProduto WHERE idPainel = 1')->fetchAll('assoc');
    	
		$this->info('- Comenzando Proceso de extracción para CO');
    	
    	foreach($categories as $category) {
	    	
	    	$results = fopen(TMP . "extract" . DS . $category['Country'] . "_idProduto_".$category['IdProduto'].".csv", 'w');
	    	
	    	$this->out('-- Procesando Categoria: idProduto: ' . $category['IdProduto']);
	    	
	    	$data = $connection->execute("	SELECT	j.idAto,
											h.Usuario,
											j.idDomicilio,
											CASE 
												WHEN r.NSE_LOC IN (1) THEN 'Nse 1'
												WHEN r.NSE_LOC IN (2) THEN 'Nse 2'
												WHEN r.NSE_LOC IN (3) THEN 'Nse 3'
												WHEN r.NSE_LOC IN (4) THEN 'Nse 4'
												WHEN r.NSE_LOC IN (5, 6) THEN 'Nse 5y6'
											END as NSE,
											CASE
												WHEN r.CIUDAD IN (1,5,6,32,53,54) THEN 'Total Bogota'
												WHEN r.CIUDAD IN (3,42,43,44) THEN 'Total Antioquia'
												WHEN r.CIUDAD IN (4,11,12,13,14,26,27,28,29,35,36,37,38) THEN 'Total Atlantico'
												WHEN r.CIUDAD IN (2,9,10,30,31,46,47,48) THEN 'Total Pacifico'
												WHEN r.CIUDAD IN (19,20,21,40,41) THEN 'Total eje Cafetero'
												WHEN r.CIUDAD IN (15,16,17,33,49,50,51,52) THEN 'Total Santanderes'
												WHEN r.CIUDAD IN (18,22,23,24,25,45) THEN 'Total Nororiente'
												WHEN r.CIUDAD IN (7,8,34,39) THEN 'Total Centro'			
											END as Zona,
											j.idPainel,
											CASE WHEN j.idPainel = 1 THEN 'PNC' ELSE 'OTRO' END as Painel,
											j.idCanal,
											c.Descricao as Canal,
											j.Ano, 
											j.Mes,
											j.IdArtigo,
											j.idProduto, 
											z.Produto,
											j.idSub,
											z.Sub,
											z.IdMarca,
											z.Marca,
											z.idFabricante,
											z.Fabricante,
											z.CdC01,
											z.Clas01,
											z.CdC02,
											z.Clas02,
											z.CdC03,
											z.Clas03,
											z.CdC04,
											z.Clas04,
											z.CdC05,
											z.Clas05,
											z.IdConteudo,
											z.Conteudo,
											z.Coef1 as Z_Coef1,
											z.Coef2 as Z_Coef2,
											j.COEF_01, j.COEF_02,
											dbo.Base64ToDec(j.FACTOR_RW) as FACTOR_RW,
											j.Quantidade,
											j.FlgRegalo,
											CAST(j.Preco as INTEGER) as J_Preco,
											CAST(h.Preco_Unitario as INTEGER) as H_Precio_Unitario,
											p.valor as Peso_PNC,
											CAST((p.Valor * (dbo.Base64ToDec(j.FACTOR_RW) / CAST(100 as FLOAT)) * j.COEF_01) / 1000 as INTEGER) as Vol_1,
											CAST((p.Valor * (dbo.Base64ToDec(j.FACTOR_RW) / CAST(100 as FLOAT)) * j.COEF_02) / 1000 as INTEGER) as Vol_2
								FROM		dbo.J_AtosCompra_New j (nolock)
								JOIN		dbo.RG_Domicilios_Pesos p (nolock) ON p.Ano = j.Ano AND p.messem = j.Mes AND p.idDomicilio = j.IdDomicilio AND p.idpeso = 1
								JOIN		dbo.VW_ArtigoZ z (nolock) ON z.idArtigo = j.idArtigo
								JOIN		dbo.RG_Panelis r (nolock) ON r.idDomicilio = j.idDomicilio AND r.Ano = j.Ano
								JOIN		dbo.A_Canal c (nolock) ON c.IdCanal = j.IdCanal
								JOIN		dbo.HAto_Cabecalho h (nolock) ON h.IdHato_Cabecalho = j.IdHato_Cabecalho
								WHERE		j.idProduto = :idProduto
								AND			j.Ano * 100 + j.Mes >= 201801", ['idProduto' => $category['IdProduto']]);
	    	
		    $flag = 1;
		    while($line = $data->fetch('assoc')) {
		    	if($flag == 1) {
			    	$tmpLine = implode(";", array_keys($line));
					fwrite($results, $tmpLine . PHP_EOL);
					$flag = 0;
		    	}
		    	$tmpLine = implode(";", $line);
				fwrite($results, $tmpLine . PHP_EOL);
			}
			
			$zip = new \ZipArchive();
			$filename = TMP . "extract" . DS . $category['Country'] . "_idProduto_".$category['IdProduto'].".zip";
			
			if ($zip->open($filename, ZipArchive::CREATE)!==TRUE) {
			    exit("cannot open <$filename>\n");
			}
			
			$zip->addFile(TMP . "extract" . DS . $category['Country'] . "_idProduto_".$category['IdProduto'].".csv", $category['Country'] . "_idProduto_".$category['IdProduto'].".csv");
			$zip->close();
			
			unlink(TMP . "extract" . DS . $category['Country'] . "_idProduto_".$category['IdProduto'].".csv");
	    	
    	}
		
    }
}
