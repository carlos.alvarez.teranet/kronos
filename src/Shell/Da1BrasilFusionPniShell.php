<?php
namespace App\Shell;

use Cake\Console\Shell;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;
use Cake\Datasource\ConnectionManager;


class Da1BrasilFusionPniShell extends Shell
{

    public function initialize()
    {
        parent::initialize();
    }

	public function main() {
		
		$file 		= fopen(TMP . "da1" . DS . "Pesos2303_Brasil_Fusion_PNI.da1", 'w');
		
		$connection = ConnectionManager::get('BR_KWP');
		//$inds		= $connection->execute("SELECT RIGHT('00000000' + CAST(idDomicilio as VARCHAR), 8) + RIGHT('00' + CAST(idIndividuo as VARCHAR), 2)  as idIndividuo FROM dbo.RG_Individuos_Pesos_PNI WHERE Ano >= 2018 GROUP by idDomicilio, idIndividuo ORDER by CAST(idDomicilio as INTEGER) ASC, CAST(idIndividuo as INTEGER) ASC")->fetchAll('assoc');
		
		$weights	= $connection->execute("SELECT 	RIGHT('0000000' + CAST(rgd.PNI_HouseHold_Id as VARCHAR), 7) + RIGHT('000' + CAST(rgd.PNI_Individual_Id as VARCHAR), 3) as idIndividuo,
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Year = 2018 AND rgd.Period = 1 THEN rgd.PNI_Weight ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Year = 2018 AND rgd.Period = 2 THEN rgd.PNI_Weight ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Year = 2018 AND rgd.Period = 3 THEN rgd.PNI_Weight ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Year = 2018 AND rgd.Period = 4 THEN rgd.PNI_Weight ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Year = 2018 AND rgd.Period = 5 THEN rgd.PNI_Weight ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Year = 2018 AND rgd.Period = 6 THEN rgd.PNI_Weight ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Year = 2018 AND rgd.Period = 7 THEN rgd.PNI_Weight ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Year = 2018 AND rgd.Period = 8 THEN rgd.PNI_Weight ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Year = 2018 AND rgd.Period = 9 THEN rgd.PNI_Weight ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Year = 2018 AND rgd.Period = 10 THEN rgd.PNI_Weight ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Year = 2018 AND rgd.Period = 11 THEN rgd.PNI_Weight ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Year = 2018 AND rgd.Period = 12 THEN rgd.PNI_Weight ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Year = 2019 AND rgd.Period = 1 THEN rgd.PNI_Weight ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Year = 2019 AND rgd.Period = 2 THEN rgd.PNI_Weight ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Year = 2019 AND rgd.Period = 3 THEN rgd.PNI_Weight ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Year = 2019 AND rgd.Period = 4 THEN rgd.PNI_Weight ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Year = 2019 AND rgd.Period = 5 THEN rgd.PNI_Weight ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Year = 2019 AND rgd.Period = 6 THEN rgd.PNI_Weight ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Year = 2019 AND rgd.Period = 7 THEN rgd.PNI_Weight ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Year = 2019 AND rgd.Period = 8 THEN rgd.PNI_Weight ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Year = 2019 AND rgd.Period = 9 THEN rgd.PNI_Weight ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Year = 2019 AND rgd.Period = 10 THEN rgd.PNI_Weight ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Year = 2019 AND rgd.Period = 11 THEN rgd.PNI_Weight ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Year = 2019 AND rgd.Period = 12 THEN rgd.PNI_Weight ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Year = 2020 AND rgd.Period = 1 THEN rgd.PNI_Weight ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Year = 2020 AND rgd.Period = 2 THEN rgd.PNI_Weight ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Year = 2020 AND rgd.Period = 3 THEN rgd.PNI_Weight ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Year = 2020 AND rgd.Period = 4 THEN rgd.PNI_Weight ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Year = 2020 AND rgd.Period = 5 THEN rgd.PNI_Weight ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Year = 2020 AND rgd.Period = 6 THEN rgd.PNI_Weight ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Year = 2020 AND rgd.Period = 7 THEN rgd.PNI_Weight ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Year = 2020 AND rgd.Period = 8 THEN rgd.PNI_Weight ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Year = 2020 AND rgd.Period = 9 THEN rgd.PNI_Weight ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Year = 2020 AND rgd.Period = 10 THEN rgd.PNI_Weight ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Year = 2020 AND rgd.Period = 11 THEN rgd.PNI_Weight ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Year = 2020 AND rgd.Period = 12 THEN rgd.PNI_Weight ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Year = 2021 AND rgd.Period = 1 THEN rgd.PNI_Weight ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Year = 2021 AND rgd.Period = 2 THEN rgd.PNI_Weight ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Year = 2021 AND rgd.Period = 3 THEN rgd.PNI_Weight ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Year = 2021 AND rgd.Period = 4 THEN rgd.PNI_Weight ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Year = 2021 AND rgd.Period = 5 THEN rgd.PNI_Weight ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Year = 2021 AND rgd.Period = 6 THEN rgd.PNI_Weight ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Year = 2021 AND rgd.Period = 7 THEN rgd.PNI_Weight ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Year = 2021 AND rgd.Period = 8 THEN rgd.PNI_Weight ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Year = 2021 AND rgd.Period = 9 THEN rgd.PNI_Weight ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Year = 2021 AND rgd.Period = 10 THEN rgd.PNI_Weight ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Year = 2021 AND rgd.Period = 11 THEN rgd.PNI_Weight ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Year = 2021 AND rgd.Period = 12 THEN rgd.PNI_Weight ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Year = 2022 AND rgd.Period = 1 THEN rgd.PNI_Weight ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Year = 2022 AND rgd.Period = 2 THEN rgd.PNI_Weight ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Year = 2022 AND rgd.Period = 3 THEN rgd.PNI_Weight ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Year = 2022 AND rgd.Period = 4 THEN rgd.PNI_Weight ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Year = 2022 AND rgd.Period = 5 THEN rgd.PNI_Weight ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Year = 2022 AND rgd.Period = 6 THEN rgd.PNI_Weight ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Year = 2022 AND rgd.Period = 7 THEN rgd.PNI_Weight ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Year = 2022 AND rgd.Period = 8 THEN rgd.PNI_Weight ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Year = 2022 AND rgd.Period = 9 THEN rgd.PNI_Weight ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Year = 2022 AND rgd.Period = 10 THEN rgd.PNI_Weight ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Year = 2022 AND rgd.Period = 11 THEN rgd.PNI_Weight ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Year = 2022 AND rgd.Period = 12 THEN rgd.PNI_Weight ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Year = 2023 AND rgd.Period = 1 THEN rgd.PNI_Weight ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Year = 2023 AND rgd.Period = 2 THEN rgd.PNI_Weight ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Year = 2023 AND rgd.Period = 3 THEN rgd.PNI_Weight ELSE 0 END) as VARCHAR), 6)
											as [pesos]
									FROM 	dbo.Fusion_Portal_Results_OOH_PNI rgd
									WHERE	rgd.Year >= 2018
									GROUP	by RIGHT('0000000' + CAST(rgd.PNI_HouseHold_Id as VARCHAR), 7) + RIGHT('000' + CAST(rgd.PNI_Individual_Id as VARCHAR), 3)
									ORDER	by RIGHT('0000000' + CAST(rgd.PNI_HouseHold_Id as VARCHAR), 7) + RIGHT('000' + CAST(rgd.PNI_Individual_Id as VARCHAR), 3) ASC")->fetchAll('assoc');
													
		$connection = ConnectionManager::get('BR_SPRI_PNI');
		$years		= [2023, 2022, 2021, 2020, 2019];
		$layout 	= $connection->execute("SELECT Nombre, spVariable, Largo, PosInicial FROM dbo.RG_Panelis_Variables ORDER by PosInicial ASC")->fetchAll('assoc');
		
		$n = 0;
		$this->verbose("Cantidad de Domicilios a procesar: " . count($weights));
		foreach($weights as $ind) {
			
			$n++;
			$this->verbose("Procesando individuo: " . $n);
			$main_row = [];
			
			/*
			$query	= $connection->execute("SELECT [PAIS],[idDomicilio],[SEQDOM],[CIUDAD],[EDINDIVIDUO],[FX_EDINDIVIDUO],[SEXOIND],[ACTIVIDADIND],[PNSE_LOC],[NSE_LOC],[IDCORPELE],[GRAUPARENTESCO],[EDDONACASA],[FLAG_DC],[PCRIANCAS],[BR_FXREGIAO],[BR_FXREGIAO_SEMMICRO],[SPLIT],[COBERTURA_OOH],[FM01],[FM02],[FM03],[FM04],[FM05],[FM06],[FM07],[FM08],[FM09],[FM10],[FM11],[FM12], [Ano]  FROM dbo.RG_Panelis WHERE RIGHT('00000000' + CAST(Dom as VARCHAR), 8) + RIGHT('00' + CAST(Ind as VARCHAR), 2) = :idIndividuo AND Ano IN (2022, 2021, 2020, 2019, 2018) ORDER by ANO DESC", ['idIndividuo' =>  $ind['idIndividuo']]);
			*/
			$query	= $connection->execute("SELECT [PAIS],[idDomicilio],[SEQDOM],[CIUDAD],[EDINDIVIDUO],[FX_EDINDIVIDUO],[SEXOIND],[ACTIVIDADIND],[PNSE_LOC],[NSE_LOC],[IDCORPELE],[GRAUPARENTESCO],[EDDONACASA],[FLAG_DC],[PCRIANCAS],[BR_FXREGIAO],[BR_FXREGIAO_SEMMICRO],[SPLIT],[COBERTURA_OOH],[FM01],[FM02],[FM03],[FM04],[FM05],[FM06],[FM07],[FM08],[FM09],[FM10],[FM11],[FM12], [Ano]  FROM dbo.RG_Panelis WHERE idDomicilio = :idIndividuo AND Ano IN (2023, 2022, 2021, 2020, 2019) ORDER by ANO DESC", ['idIndividuo' => $ind['idIndividuo']]);
			
			$rows	= $query->fetchAll('assoc');
			
			foreach($years as $year) {
				
				$key 	= array_search($year, array_column($rows, 'Ano'));
				$line = '';
				
				if($key !== false) {
			
					foreach($layout as $element) {
						//$this->abort(print_r($rows[$key], true));
						$add_line = str_pad($rows[$key][$element['Nombre']], $element['Largo'], "0", STR_PAD_LEFT);
						if(strlen($add_line) != $element['Largo']) {
							$this->warn("Arreglo: " . print_r($rows[$key], true));
							$this->warn("Variable: " . $element['Nombre']);
							$this->abort("Error de longitud de variable");
						}
						$line .= $add_line;
					}
					
				} else {
									
					foreach($layout as $element) {
						if(in_array($element['Nombre'], ['FM01', 'FM02', 'FM03', 'FM04', 'FM05', 'FM06', 'FM07', 'FM08', 'FM09', 'FM10', 'FM11', 'FM12'])) {
							$line .= '000000';
						} else {
							$line .= str_pad($rows[0][$element['Nombre']], $element['Largo'], "0", STR_PAD_LEFT);
						}
							
					}
					
				}
				
				$main_row[] = str_pad($line, 500, "0", STR_PAD_RIGHT);
				
			}
			
			//$clave = array_search($ind['idIndividuo'], array_column($weights, 'idIndividuo'));
			$pesosPeriodos = $ind['pesos'];
			
			
			$text = implode("", $main_row).$pesosPeriodos;
			//fwrite($file, str_pad($text, 3244, "0", STR_PAD_RIGHT) . PHP_EOL);
			fwrite($file, str_pad($text, ( (5 * 500) + strlen($ind['pesos']) ), "0", STR_PAD_RIGHT) . PHP_EOL);
			
		}
		
	}
    
}