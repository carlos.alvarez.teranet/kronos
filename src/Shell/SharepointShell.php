<?php
namespace App\Shell;

use Cake\Console\Shell;
use Office365\Runtime\Auth\ClientCredential;
use Office365\SharePoint\ClientContext;
use Office365\SharePoint\File;
use Office365\SharePoint\Folder;
use Office365\SharePoint\User;
use Cake\Datasource\ConnectionManager;
  
class SharepointShell extends Shell
{
	
	public function initialize()
    {
        $this->elements = [
	        'ARGENTINA',
	        'BOLIVIA',
	        'BRASIL',
	        'CAM',
	        'CHILE',
	        'COLOMBIA',
	        'ECUADOR',
	        'MEXICO',
	        'PERU'
        ];
        
        $this->periods = [
	        date("Y/Y_m", strtotime(date('Y-m-d') .' - 60 days')),
	        date("Y/Y_m", strtotime(date('Y-m-d') .' - 30 days')),
	    	date('Y/Y_m')  	
        ];
        
        $this->databases = [];
        
    }
	
	public function main() {
		
		$connection = ConnectionManager::get('Kronos');
		
		$this->out("-- Actualizando bases de datos Kronos (Sharepoint)");
		$credentials = new ClientCredential("eec397dd-ad63-416c-8817-c2ce92a45e8d", "DC+WEq2TTf7EwJpkQk8ajE8Qgn+wTDU/ciroMkCrQSI=");
		$client = (new ClientContext("https://ktglbuc.sharepoint.com/sites/ROCLatAm"))->withCredentials($credentials);
		

		function forEachFile(Folder $parentFolder, $recursive, $per, callable $action, $level=0)
		{
		    $files = $parentFolder->getFiles()->get()->executeQuery();
		    /** @var File $file */
		    foreach ($files as $file) {
		        $action($file, $level, $per);
		    }
		
		    if ($recursive) {
		        /** @var Folder $folder */
		        foreach ($parentFolder->getFolders()->get()->executeQuery() as $folder) {
		            forEachFile($folder, $recursive, $per, $action, $level++);
		        }
		    }
		}
		
		foreach($this->periods as $period) {
			
			$this->info('Periodo: '.$period);
			
			foreach($this->elements as $element) {
				
				$this->info('País: '.$element);
			
				$rootFolder = $client->getWeb()->getFolderByServerRelativeUrl("Reportes%20de%20Liberacin/Bases a Liberar/$element/$period");
				forEachFile($rootFolder, true, $period, function (File $file,$level,$per){
					if(strpos($file->getName(), '.zip') !== false OR strpos($file->getName(), '.rar') !== false) {
						//$author = $file->getAuthor()->getEmail();
						$this->out($file->getName() . " ; " . $file->getTimeCreated());
						array_push($this->databases, ['name' => $file->getName(), 'date' => $file->getTimeCreated(), 'year' => explode("/", $per)[0], 'month' => explode("_", $per)[1]]);
					} else {
						$this->warn($file->getName() . ";" . $file->getTimeCreated());
					}
				});
				
			}
		}
		
		foreach($this->databases as $database) {
			
			//$this->out("Actualizando: " . $database['name']);
			try {
				$query = $connection->execute("	UPDATE tasks JOIN periods ON periods.id = tasks.period_id 
									JOIN maestros ON maestros.id = tasks.master_id 
									SET	tasks.uploaded = 1, uploaded_created = DATE_FORMAT(STR_TO_DATE(:date,'%Y-%m-%dT%H:%i:%sZ'),'%Y-%m-%d %H:%i:%s')
									WHERE periods.year = :year AND periods.month_id = :month AND maestros.dbname = :name AND tasks.uploaded = 0", 
									['year' => $database['year'], 'month' => $database['month'], 'name' => substr($database['name'], 0, 8), 'date' => $database['date']],
									['year' => 'integer', 'month' => 'integer', 'name' => 'string', 'date' => 'string']
									);
									
				if($query->count() > 0) {
					$this->out("Actualizando: " . $database['name']);
				}
			} catch (\Exception $e) {
				$this->warn("Error al actualizar: " . $database['name']);
			}
			
		}		
		
	}
	
}