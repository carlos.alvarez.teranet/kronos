<?php
namespace App\Shell;

use Cake\Console\ConsoleOptionParser;
use Cake\Console\Shell;
use Cake\Log\Log;
use Cake\Datasource\ConnectionManager;


class DolarShell extends Shell
{
	
	public function initialize() 
	{
		parent::initialize();
	}
	
    public function main($test = 0)
    {
    	$connection = ConnectionManager::get('IT_WORKFLOW_33');
    	$output = [
			'db' => null,
			'idPais' => null,
			'Country' => null,
			'ISO' => null,
			'Week' => null,
			'Year' => null,
			'value' => null
		];
		
		$arrContextOptions=array(
			"ssl"=>array(
				"verify_peer" => false,
				"verify_peer_name" => false
		    )
		); 
    	
    	$values		= $connection->execute("SET DATEFIRST 1
											SELECT 'PE_KWP' as db, idPais, DATEPART(WEEK, CONVERT(DATE, DATEADD(WEEK,-1,GETDATE()))) as Week, DATEPART(year, CONVERT(DATE, DATEADD(WEEK,-1,GETDATE()))) as [Year], CONVERT(DATE, DATEADD(MONTH,-1,GETDATE())) as Param1, CONVERT(DATE, GETDATE()) as Param2, * FROM PE_KWP.dbo.Pais
											UNION ALL
											SELECT 'BO_KWP' as db, idPais, DATEPART(WEEK, CONVERT(DATE, DATEADD(WEEK,-1,GETDATE()))) as Week, DATEPART(year, CONVERT(DATE, DATEADD(WEEK,-1,GETDATE()))) as [Year],  CONVERT(DATE, DATEADD(MONTH,-1,GETDATE())) as Param1, CONVERT(DATE, GETDATE()) as Param2, * FROM BO_KWP.dbo.Pais
											UNION ALL
											SELECT 'AR_KWP' as db, idPais, DATEPART(WEEK, CONVERT(DATE, DATEADD(WEEK,-1,GETDATE()))) as Week, DATEPART(year, CONVERT(DATE, DATEADD(WEEK,-1,GETDATE()))) as [Year],  CONVERT(DATE, DATEADD(MONTH,-1,GETDATE())) as Param1, CONVERT(DATE, GETDATE()) as Param2, * FROM [172.28.232.36\KWSTASQL901].AR_KWP.dbo.Pais
											UNION ALL
											SELECT 'CL_KWP' as db, idPais, DATEPART(WEEK, CONVERT(DATE, DATEADD(WEEK,-1,GETDATE()))) as Week, DATEPART(year, CONVERT(DATE, DATEADD(WEEK,-1,GETDATE()))) as [Year],  CONVERT(DATE, DATEADD(MONTH,-1,GETDATE())) as Param1, CONVERT(DATE, GETDATE()) as Param2, * FROM [172.28.232.36\KWSTASQL901].CL_KWP.dbo.Pais
											UNION ALL
											SELECT 'CO_KWP' as db, idPais, DATEPART(WEEK, CONVERT(DATE, DATEADD(WEEK,-1,GETDATE()))) as Week, DATEPART(year, CONVERT(DATE, DATEADD(WEEK,-1,GETDATE()))) as [Year],  CONVERT(DATE, DATEADD(MONTH,-1,GETDATE())) as Param1, CONVERT(DATE, GETDATE()) as Param2, * FROM [172.28.232.36\KWSTASQL901].CO_KWP.dbo.Pais
											UNION ALL
											SELECT 'MX_KWP' as db, idPais, DATEPART(WEEK, CONVERT(DATE, DATEADD(WEEK,-1,GETDATE()))) as Week, DATEPART(year, CONVERT(DATE, DATEADD(WEEK,-1,GETDATE()))) as [Year],  CONVERT(DATE, DATEADD(MONTH,-1,GETDATE())) as Param1, CONVERT(DATE, GETDATE()) as Param2, * FROM [172.28.232.8].MX_KWP.dbo.Pais
											UNION ALL
											SELECT 'BR_KWP' as db, idPais, DATEPART(WEEK, CONVERT(DATE, DATEADD(WEEK,-1,GETDATE()))) as Week, DATEPART(year, CONVERT(DATE, DATEADD(WEEK,-1,GETDATE()))) as [Year],  CONVERT(DATE, DATEADD(MONTH,-1,GETDATE())) as Param1, CONVERT(DATE, GETDATE()) as Param2, * FROM [172.28.232.32].BR_KWP.dbo.Pais
											UNION ALL
											SELECT 'ECU_KWP' as db, idPais, DATEPART(WEEK, CONVERT(DATE, DATEADD(WEEK,-1,GETDATE()))) as Week, DATEPART(year, CONVERT(DATE, DATEADD(WEEK,-1,GETDATE()))) as [Year],  CONVERT(DATE, DATEADD(MONTH,-1,GETDATE())) as Param1, CONVERT(DATE, GETDATE()) as Param2, * FROM [172.28.232.8].ECU_KWP.dbo.Pais
											UNION ALL
											SELECT 'CAM_KWP' as db, idPais, DATEPART(WEEK, CONVERT(DATE, DATEADD(WEEK,-1,GETDATE()))) as Week, DATEPART(year, CONVERT(DATE, DATEADD(WEEK,-1,GETDATE()))) as [Year],  CONVERT(DATE, DATEADD(MONTH,-1,GETDATE())) as Param1, CONVERT(DATE, GETDATE()) as Param2, * FROM [172.28.232.32].CAM_KWP.dbo.Pais")->fetchAll('assoc');
		
		$this->info(implode(array_keys($output), "\t\t"));									
		foreach($values as $value)
		{
			
			$json 	= file_get_contents('https://www.oanda.com/fx-for-business/historical-rates/api/data/update/?&source=OANDA&adjustment=0&base_currency=USD&start_date='.$value['Param1'].'&end_date='.$value['Param2'].'&period=weekly&price=bid&view=table&quote_currency_0='.$value['CurrencyISO'], false, stream_context_create($arrContextOptions));
			$obj 	= json_decode($json);
			
			$output = [
				'db' => $value['db'],
				'idPais' => $value['idPais'],
				'Country' => $value['Abreviatura'],
				'ISO' => $value['CurrencyISO'],
				'Week' => $value['Week'],
				'Year' => $value['Year'],
				'value' => $obj->widget[0]->data[0][1]
			];
			
			$conn 	= ConnectionManager::get($value['db']);
			$check = $conn->execute("SELECT * FROM dbo.Dolar WHERE Periodo = :Periodo AND Ano = :Ano AND Tipo = 'S' AND idPais = :idPais", ['Periodo' => $value['Week'], 'Ano' => $value['Year'], 'idPais' => $value['idPais']])->count();
			if($check == 0 AND $test == 0) {
				$insert	= $conn->execute("INSERT INTO dbo.Dolar (Periodo, Ano, Tipo, idPais, Valor) VALUES (:Periodo, :Ano, 'S', :idPais, :Valor)", ['Periodo' => $value['Week'], 'Ano' => $value['Year'], 'idPais' => $value['idPais'], 'Valor' => $obj->widget[0]->data[0][1]]);
			}
			
			$this->out(implode($output, "\t\t"));
			sleep(5);
		}
    }
}
