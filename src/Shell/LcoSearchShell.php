<?php
namespace App\Shell;

use Cake\Console\Shell;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;

class LcoSearchShell extends Shell
{
	
	public function getOptionParser()
    {
        $parser = parent::getOptionParser();

        return $parser;
    }
    
    private function splitEqual($string) {
		$array = explode("=", $string);
		return $array[1];
	}
    
    private function getLineWithString($fileName, $str) {
	    $lines = file($fileName);
	    foreach ($lines as $lineNumber => $line) {
	        if (strpos($line, $str) !== false) {
	            return trim($this->splitEqual($line));
	        }
	    }
	    return null;
	}
	
	private function getLineWithStringAndHeader($fileName, $str, $header) {
	    $lines = file($fileName);
	    $flag = 0;
	    foreach ($lines as $lineNumber => $line) {
		    if (strpos($line, $header) !== false and !$flag) {
	            $flag = 1;
	        }
	        if($flag == 1) {
		        if (strpos($line, $str) !== false) {
		            return trim($this->splitEqual($line));
		        }
	        }
	    }
	    return null;
	}
	
	public function main()
    {
	    
        $this->info("Buscando recursivamente");
        
        $dir 	= new Folder("/mnt/WPO/TNSCub/Orders/KPI");
        $files 	= $dir->findRecursive('.*\.(lco)');	
		
		foreach($files as $key => $file) {
	        
	        $this->out(print_r($file, true));
	        
	    }
	    
	}    
}