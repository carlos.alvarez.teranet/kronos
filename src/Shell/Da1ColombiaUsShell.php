<?php
namespace App\Shell;

use Cake\Console\Shell;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;
use Cake\Datasource\ConnectionManager;


class Da1ColombiaUsShell extends Shell
{

    public function initialize()
    {
        parent::initialize();
    }

	public function main() {

		$file 		= fopen(TMP . "da1" . DS . "Pesos2303_Colombia_US.da1", 'w');

		$connection = ConnectionManager::get('CO_KWP');
		//$inds		= $connection->execute("SELECT RIGHT('00000000' + CAST(idDomicilio as VARCHAR), 8) as idDomicilio FROM dbo.RG_Domicilios_Pesos WHERE Ano >= 2020 AND idPeso = 1 GROUP by idDomicilio ORDER by CAST(idDomicilio as INTEGER) ASC")->fetchAll('assoc');

		$weights	= $connection->execute("SELECT 	RIGHT('0000000' + CAST(rgd.idDomicilio as VARCHAR), 7) + RIGHT('000' + CAST(rgd.idIndividuo as VARCHAR), 3) as idDomicilio,
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2021 AND rgd.MesSem = 4 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2021 AND rgd.MesSem = 5 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2021 AND rgd.MesSem = 6 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2021 AND rgd.MesSem = 7 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2021 AND rgd.MesSem = 8 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2021 AND rgd.MesSem = 9 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2021 AND rgd.MesSem = 10 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2021 AND rgd.MesSem = 11 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2021 AND rgd.MesSem = 12 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2022 AND rgd.MesSem = 1 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2022 AND rgd.MesSem = 2 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2022 AND rgd.MesSem = 3 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2022 AND rgd.MesSem = 4 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2022 AND rgd.MesSem = 5 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2022 AND rgd.MesSem = 6 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2022 AND rgd.MesSem = 7 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2022 AND rgd.MesSem = 8 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2022 AND rgd.MesSem = 9 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2022 AND rgd.MesSem = 10 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2022 AND rgd.MesSem = 11 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2022 AND rgd.MesSem = 12 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2023 AND rgd.MesSem = 1 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2023 AND rgd.MesSem = 2 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2023 AND rgd.MesSem = 3 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6)
											as [pesos]
									FROM 	dbo.RG_Individuos_Pesos_US rgd
									LEFT JOIN	dbo.RG_Individuos_Pesos_US_CSV rgc ON rgc.Iddomicilio = rgd.Iddomicilio AND rgc.idIndividuo = rgd.idIndividuo AND rgc.Ano = rgd.Ano AND rgc.MesSem = rgd.messem
									WHERE	rgd.idPeso = 1
									AND		rgd.Ano >= 2021
									GROUP	by RIGHT('0000000' + CAST(rgd.idDomicilio as VARCHAR), 7) + RIGHT('000' + CAST(rgd.idIndividuo as VARCHAR), 3)
									ORDER	by 1 ASC")->fetchAll('assoc');

		$connection = ConnectionManager::get('CO_KWP');
		$years		= [2023, 2022, 2021];
		$layout 	= $connection->execute("SELECT Nombre, spVariable, Largo, PosInicial FROM dbo.RG_Panelis_Variables_US ORDER by PosInicial ASC")->fetchAll('assoc');

		$n = 0;
		$this->verbose("Cantidad de Domicilios a procesar: " . count($weights));
		foreach($weights as $ind) {

			$n++;
			$this->verbose("Procesando domiciio: " . $n);
			$main_row = [];

			$query	= $connection->execute("SELECT [IDDOMICILIO], [DOM], [IND], [CIUDAD], [EDINDIVIDUO], [SEXOIND], [NSE_LOC], [PAIS], [ACTIVIDADIND], [EDOCIVIL], [INDPARENTESCO], [JEFEFAM], [DONACASA], [DIST], [REGIONEW], [Ano] FROM dbo.RG_Panelis_US WHERE idDomicilio = :idDomicilio AND Ano IN (2023, 2022, 2021) ORDER by ANO DESC", ['idDomicilio' => $ind['idDomicilio']]);
			$rows	= $query->fetchAll('assoc');

			foreach($years as $year) {

				$key 	= array_search($year, array_column($rows, 'Ano'));
				$line = '';

				if($key !== false) {

					foreach($layout as $element) {
						//$this->abort(print_r($rows[$key], true));
						$rows[$key][$element['Nombre']] = (!is_numeric($rows[$key][$element['Nombre']])) ? 0 : $rows[$key][$element['Nombre']];
						$line .= str_pad($rows[$key][$element['Nombre']] * 1, $element['Largo'], "0", STR_PAD_LEFT);
					}

				} else {

					foreach($layout as $element) {
						$rows[0][$element['Nombre']] = (!is_numeric($rows[0][$element['Nombre']])) ? 0 : $rows[0][$element['Nombre']];
						$line .= str_pad($rows[0][$element['Nombre']] * 1, $element['Largo'], "0", STR_PAD_LEFT);
					}

				}

				$main_row[] = str_pad($line, 500, "0", STR_PAD_RIGHT);

			}

			//$clave = array_search($ind['idDomicilio'], array_column($weights, 'idDomicilio'));
			$pesosPeriodos = $ind['pesos'];


			$text = implode("", $main_row).$pesosPeriodos;
			//fwrite($file, str_pad($text, 2926, "0", STR_PAD_RIGHT) . PHP_EOL);
			fwrite($file, str_pad($text, ( (1 * 500) + strlen($ind['pesos']) ), "0", STR_PAD_RIGHT) . PHP_EOL);


		}

	}

}