<?php
namespace App\Shell;

use Cake\Console\Shell;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;
use Cake\Datasource\ConnectionManager;


class Da1TelecomShell extends Shell
{

    public function initialize()
    {
        parent::initialize();
    }

	public function main() {
		
		$file 		= fopen(TMP . "da1" . DS . "AR_TELECOM.da1", 'w');
		
		$connection = ConnectionManager::get('AR_KWP');
		$inds		= $connection->execute("SELECT RIGHT('000000' + CAST(idDomicilio as VARCHAR), 6) as idDomicilio FROM dbo.RG_Domicilios_Pesos_Telecom GROUP by idDomicilio ORDER by idDomicilio ASC")->fetchAll('assoc');
		
		$weights	= $connection->execute("SELECT 	RIGHT('000000' + CAST(idDomicilio as VARCHAR), 6) as idDomicilio,
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2017 AND MesSem = 6 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2017 AND MesSem = 9 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2017 AND MesSem = 12 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2018 AND MesSem = 3 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2018 AND MesSem = 6 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2018 AND MesSem = 9 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2018 AND MesSem = 12 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2019 AND MesSem = 3 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2019 AND MesSem = 6 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2019 AND MesSem = 9 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2019 AND MesSem = 12 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2020 AND MesSem = 3 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2020 AND MesSem = 6 THEN Valor ELSE 0 END) as VARCHAR), 6) as [pesos]
											FROM 	dbo.RG_Domicilios_Pesos_Telecom
											WHERE	idPeso = 1
											AND		Ano * 100 + MesSem >= 201706
											GROUP	by RIGHT('000000' + CAST(idDomicilio as VARCHAR), 6)")->fetchAll('assoc');
		
		$connection = ConnectionManager::get('AR_SPRI');
		$years		= [2020, 2019, 2018, 2017, 2016, 2015];
		$layout 	= $connection->execute("SELECT Nombre, spVariable, Largo, PosInicial FROM dbo.RG_Panelis_Telecom_Variables ORDER by PosInicial ASC")->fetchAll('assoc');
		
		foreach($inds as $ind) {
			
			$main_row = [];
			
			$query	= $connection->execute("SELECT * FROM dbo.RG_Panelis_Telecom WHERE NPAN = :idDomicilio AND Ano IN (2020, 2019, 2018, 2017, 2016, 2015) ORDER by ANO DESC", ['idDomicilio' => $ind['idDomicilio']]);
			$rows	= $query->fetchAll('assoc');
			
			foreach($years as $year) {
				
				$key 	= array_search($year, array_column($rows, 'Ano'));
				$line = '';
				
				if($key !== false) {
			
					foreach($layout as $element) {
						$line .= str_pad(trim($rows[$key][$element['Nombre']]), $element['Largo'], '0', STR_PAD_LEFT);
					}
					
				} else {
									
					foreach($layout as $element) {
						$line .= str_pad(trim($rows[0][$element['Nombre']]), $element['Largo'], '0', STR_PAD_LEFT);	
					}
					
				}
				
				$main_row[] = str_pad($line, 100, '0', STR_PAD_RIGHT);
				
			}
			
			$clave = array_search($ind['idDomicilio'], array_column($weights, 'idDomicilio'));
			$pesosPeriodos = $weights[$clave]['pesos'];
			
			
			$text = implode('', $main_row).$pesosPeriodos;
			fwrite($file, str_pad($text, 678, '0', STR_PAD_RIGHT) . PHP_EOL);
			
		}
		
	}
    
}