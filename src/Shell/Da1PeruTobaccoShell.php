<?php
namespace App\Shell;

use Cake\Console\Shell;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;
use Cake\Datasource\ConnectionManager;


class Da1PeruTobaccoShell extends Shell
{

    public function initialize()
    {
        parent::initialize();
    }

	public function main() {
		
		$file 		= fopen(TMP . "da1" . DS . "Pesos2304_Peru_TBC.da1", 'w');
		
		$connection = ConnectionManager::get('PE_KWP');
		//$inds		= $connection->execute("SELECT RIGHT('0000000' + CAST(idDomicilio as VARCHAR), 7) + RIGHT('000' + CAST(idIndividuo as VARCHAR), 3) as idIndividuo FROM dbo.RG_Individuos_Pesos_BAT WHERE Ano >= 2021 GROUP by idDomicilio, idIndividuo ORDER by CAST(idDomicilio as INTEGER) ASC, CAST(idIndividuo as INTEGER) ASC")->fetchAll('assoc');
		
		$weights	= $connection->execute("SELECT 	RIGHT('0000000' + CAST(idDomicilio as VARCHAR), 7) + RIGHT('000' + CAST(idIndividuo as VARCHAR), 3) as idIndividuo,
													RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2021 AND rgd.MesSem = 2 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2021 AND rgd.MesSem = 3 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2021 AND rgd.MesSem = 4 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2021 AND rgd.MesSem = 5 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2021 AND rgd.MesSem = 6 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2021 AND rgd.MesSem = 7 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2021 AND rgd.MesSem = 8 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2021 AND rgd.MesSem = 9 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6)  +
													RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2021 AND rgd.MesSem = 10 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2021 AND rgd.MesSem = 11 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6)  +
													RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2021 AND rgd.MesSem = 12 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6)  +
													RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2022 AND rgd.MesSem = 1 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6)  +
													RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2022 AND rgd.MesSem = 2 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2022 AND rgd.MesSem = 3 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2022 AND rgd.MesSem = 4 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2022 AND rgd.MesSem = 5 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2022 AND rgd.MesSem = 6 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2022 AND rgd.MesSem = 7 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2022 AND rgd.MesSem = 8 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2022 AND rgd.MesSem = 9 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2022 AND rgd.MesSem = 10 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2022 AND rgd.MesSem = 11 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2022 AND rgd.MesSem = 12 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2023 AND rgd.MesSem = 1 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2023 AND rgd.MesSem = 2 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2023 AND rgd.MesSem = 3 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2023 AND rgd.MesSem = 4 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6)
													as pesos
											FROM 	dbo.RG_Individuos_Pesos_Tobacco rgd
											WHERE	rgd.idPeso = 1
											AND		rgd.Ano >= 2017
											GROUP	by RIGHT('0000000' + CAST(idDomicilio as VARCHAR), 7) + RIGHT('000' + CAST(idIndividuo as VARCHAR), 3)
											ORDER	by RIGHT('0000000' + CAST(idDomicilio as VARCHAR), 7) + RIGHT('000' + CAST(idIndividuo as VARCHAR), 3) ASC")->fetchAll('assoc');
															
		$connection = ConnectionManager::get('PE_KWP');
		$years		= [2023, 2022, 2021];
		$layout 	= $connection->execute("SELECT Nombre, spVariable, Largo, PosInicial FROM dbo.RG_Panelis_Variables_Tobacco ORDER by PosInicial ASC")->fetchAll('assoc');
		
		$n = 0;
		$this->verbose("Cantidad de Individuos a procesar: " . count($weights));
		foreach($weights as $ind) {
			
			$n++;
			$this->verbose("Procesando Individuo: " . $n);
			$main_row = [];
			
			$query	= $connection->execute("SELECT [PAIS], [NPAN], [CIUDAD], [NSE_LOC], [EDINDIVIDUO], [SEXOIND], [Ano] FROM dbo.RG_Panelis_Tobacco WHERE NPAN = :idIndividuo AND Ano IN (2023, 2022, 2021) ORDER by ANO DESC", ['idIndividuo' => $ind['idIndividuo']]);
			$rows	= $query->fetchAll('assoc');
			
			foreach($years as $year) {
				
				$key 	= array_search($year, array_column($rows, 'Ano'));
				$line = '';				
				
				if($key !== false) {
			
					foreach($layout as $element) {
						//$this->abort(print_r($rows[$key], true));
						$add_line = str_pad(trim($rows[$key][$element['Nombre']]), $element['Largo'], "0", STR_PAD_LEFT);
						if(strlen($add_line) != $element['Largo']) {
							$this->warn("Arreglo: " . print_r($rows[$key], true));
							$this->warn("Variable: " . $element['Nombre']);
							$this->warn("Dato: " . $add_line);
							$this->info("strlen: " . strlen($add_line));
							$this->info("largo: " . $element['Largo']);
							$this->abort("Error de longitud de variable");
						}
						$line .= $add_line;
					}
					
				} else {
									
					foreach($layout as $element) {
						$line .= str_pad($rows[0][$element['Nombre']], $element['Largo'], "0", STR_PAD_LEFT);	
					}
					
				}
								
				$main_row[] = str_pad($line, 100, "0", STR_PAD_RIGHT);
				
			}
			
			//$clave = array_search($ind['idIndividuo'], array_column($weights, 'idIndividuo'));
			$pesosPeriodos = $ind['pesos'];
			
			
			$text = implode("", $main_row).$pesosPeriodos;
			fwrite($file, str_pad($text, ( (1 * 100) + strlen($ind['pesos']) ), "0", STR_PAD_RIGHT) . PHP_EOL);
			
		}
		
	}
    
}