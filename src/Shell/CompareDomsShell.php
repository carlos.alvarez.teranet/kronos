<?php
namespace App\Shell;

use Cake\Console\Shell;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;
use Cake\Datasource\ConnectionManager;


class CompareDomsShell extends Shell
{

    public function initialize()
    {
        parent::initialize();
        $this->files = [
	        'Pesos2303_vs_Pesos2304_Argentina_PNC' => ['Pesos2303_Argentina_PNC', 'Pesos2304_Argentina_PNC']
        ];
    }

	public function main() {
	
		$layout = file_get_contents(TMP . "doms" . DS . "Layout_PNC_54.json");
		$layout = json_decode($layout);
		
		foreach($this->files as $title => $files) {
			$this->info("Procesando: ".$title);
			
			$results = fopen(TMP . "doms" . DS . $title . ".csv", 'w');
			
			$mainFolder = "/mnt/procesos/ZIbanz/Domicilios";
			//$mainFolder = TMP . "doms";
			$fileA = fopen($mainFolder . DS . $files[0].".da1", 'r');
			$fileB = fopen($mainFolder . DS . $files[1].".da1", 'r');
			
			$arrayA = [];
			$arrayB = [];
			
			$this->info("-- Preparando primer archivo para comparación: ".$files[0]);
			$bigArrayA = [];
			while(!feof($fileA)) {
				$lineA = fgets($fileA);
				$subArrayA = [];
				foreach($layout as $element) {
					$subArrayA[$element->nombre] = substr($lineA, $element->inicio, $element->largo);
					if($element->nombre == "NPAN") {
						$keyA = substr($lineA, $element->inicio, $element->largo);
					}
				}
				$bigArrayA[$keyA] = $subArrayA;
			}
			
			$this->info("-- Preparando segundo archivo para comparación: ".$files[1]);
			$bigArrayB = [];
			while(!feof($fileB)) {
				$lineB = fgets($fileB);
				$subArrayB = [];
				foreach($layout as $element) {
					$subArrayB[$element->nombre] = substr($lineB, $element->inicio, $element->largo);
					if($element->nombre == "NPAN") {
						$keyB = substr($lineB, $element->inicio, $element->largo);
					}
				}
				$bigArrayB[$keyB] = $subArrayB;
			}
			
			// Comparativo de A v/s B
			$lineResults = [];
			$lineResults[] = ['NPAN', 'database', 'year', 'campo', 'valor_anterior', 'valor_nuevo'];
			foreach($bigArrayA as $littleKeyA => $littleArrayA) {
				if(array_key_exists($littleKeyA, $bigArrayB)) {
					$diffA = array_diff_assoc($littleArrayA, $bigArrayB[$littleKeyA]);
					if($diffA) {
						$diffAOriginal = [];
						foreach($diffA as $k => $v) {
							$diffB[$k] = $bigArrayB[$littleKeyA][$k];
							$lineResults[] = [
								'NPAN' => $littleKeyA,
								'database' => 'DA1',
								'year' => explode("-", $k)[0],
								'campo' => explode("-", $k)[1],
								'valor_anterior' => $diffA[$k],
								'valor_nuevo' => $diffB[$k]
							];
						}						
					} else {
						$this->verbose("No se encontraron diferencias en: ". $littleKeyA);
					}
				} else {
					$this->warn(implode(";", $littleArrayA));
				}
			}
			
			$this->info("Escribiendo resultados en CSV para: ".$title);
			foreach($lineResults as $line) {
				$tmpLine = implode(";", $line);
				fwrite($results, $tmpLine . PHP_EOL);
			}
			
			
		}
		
		
	}	   
}