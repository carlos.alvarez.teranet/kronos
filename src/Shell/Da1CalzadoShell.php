<?php
namespace App\Shell;

use Cake\Console\Shell;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;
use Cake\Datasource\ConnectionManager;


class Da1CalzadoShell extends Shell
{

    public function initialize()
    {
        parent::initialize();
    }

	public function main() {
		
		$file 		= fopen(TMP . "da1" . DS . "AR_CALZADO.da1", 'w');
		
		$connection = ConnectionManager::get('AR_KWP');
		$inds		= $connection->execute("SELECT RIGHT('000000' + CAST(idDomicilio as VARCHAR), 6) + RIGHT('00' + CAST(idIndividuo as VARCHAR), 2) as idIndividuo FROM dbo.RG_Individuos_Pesos_Calzado GROUP by idDomicilio, idIndividuo ORDER by CAST(idDomicilio as INTEGER) ASC, CAST(idIndividuo as INTEGER) ASC")->fetchAll('assoc');
		
		$weights	= $connection->execute("SELECT 	RIGHT('000000' + CAST(idDomicilio as VARCHAR), 6) + RIGHT('00' + CAST(idIndividuo as VARCHAR), 2) as idIndividuo,
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2014 AND MesSem = 3 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2014 AND MesSem = 6 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2014 AND MesSem = 9 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2014 AND MesSem = 12 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2015 AND MesSem = 3 THEN Valor ELSE 0 END) as VARCHAR), 6)+
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2015 AND MesSem = 6 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2015 AND MesSem = 9 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2015 AND MesSem = 12 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2016 AND MesSem = 3 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2016 AND MesSem = 6 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2016 AND MesSem = 9 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2016 AND MesSem = 12 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2017 AND MesSem = 3 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2017 AND MesSem = 6 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2017 AND MesSem = 9 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2017 AND MesSem = 12 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2018 AND MesSem = 3 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2018 AND MesSem = 6 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2018 AND MesSem = 9 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2018 AND MesSem = 12 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2019 AND MesSem = 3 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2019 AND MesSem = 6 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2019 AND MesSem = 9 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2019 AND MesSem = 12 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2020 AND MesSem = 3 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2020 AND MesSem = 6 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2020 AND MesSem = 9 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2020 AND MesSem = 12 THEN Valor ELSE 0 END) as VARCHAR), 6) as [pesos]
											FROM 	dbo.RG_Individuos_Pesos_Calzado
											WHERE	idPeso = 1
											GROUP	by RIGHT('000000' + CAST(idDomicilio as VARCHAR), 6) + RIGHT('00' + CAST(idIndividuo as VARCHAR), 2)")->fetchAll('assoc');
													
		$connection = ConnectionManager::get('AR_SPRI');
		$years		= [2020, 2019, 2018, 2017, 2016, 2015, 2014];
		$layout 	= $connection->execute("SELECT Nombre, spVariable, Largo, PosInicial FROM dbo.RG_Panelis_Calzado_Variables ORDER by PosInicial ASC")->fetchAll('assoc');
		
		foreach($inds as $ind) {
			
			$main_row = [];
			
			$query	= $connection->execute("SELECT * FROM dbo.RG_Panelis_Calzado WHERE NPAN = :idIndividuo AND Ano IN (2020, 2019, 2018, 2017, 2016, 2015, 2014) ORDER by ANO DESC", ['idIndividuo' => $ind['idIndividuo']]);
			$rows	= $query->fetchAll('assoc');
			
			foreach($years as $year) {
				
				$key 	= array_search($year, array_column($rows, 'Ano'));
				$line = '';
				
				if($key !== false) {
			
					foreach($layout as $element) {
						$line .= str_pad($rows[$key][$element['Nombre']], $element['Largo'], "0", STR_PAD_LEFT);
					}
					
				} else {
									
					foreach($layout as $element) {
						$line .= str_pad($rows[0][$element['Nombre']], $element['Largo'], "0", STR_PAD_LEFT);	
					}
					
				}
				
				$main_row[] = str_pad($line, 100, "0", STR_PAD_RIGHT);
				
			}
			
			$clave = array_search($ind['idIndividuo'], array_column($weights, 'idIndividuo'));
			$pesosPeriodos = $weights[$clave]['pesos'];
			
			
			$text = implode("", $main_row).$pesosPeriodos;
			fwrite($file, str_pad($text, 868, "0", STR_PAD_RIGHT) . PHP_EOL);
			
		}
		
	}
    
}