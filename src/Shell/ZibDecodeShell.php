<?php
namespace App\Shell;

use Cake\Console\Shell;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;
use Cake\Datasource\ConnectionManager;

class ZibDecodeShell extends Shell
{
	
	public function initialize()
    {
        parent::initialize();
        $this->file = 'Demo.zib';
        $this->name = 'L_REGION';
    }
    
    public function main() {
	    
	    $mainFolder = TMP . "zib";
		$file = fopen($mainFolder . DS . $this->file, 'r');
	
		
		$row = 1;
		$start_row = 0;
		$nombre = '';
		
		while(!feof($file)) {
			$line = fgets($file);
						
			if(trim($line) == $this->name) {
				$start_row = $row;
				$this->out('Encontrado en linea: ' . $start_row);
			} 
			
			if($row >= $start_row + 8 and $start_row > 0) {
				
				if(substr($line, 0, 1) == "(" or strpos(trim($line), "=")) {
					
					$numero = [];
					$estructura = [];
					$operator = '';
					$conteo = 0;
					$desde = null;
					$hasta = null;
					$flag = false;
					
					foreach(str_split(trim($line)) as $pos => $letter) {
						
						// Si es numero
						if(is_numeric($letter)) {
							$numero[] = $letter;
						// Si no es numero
						} else {
							
							if($flag == true) {
								
								$hasta = implode("", $numero);
								$numero = [];
								for($n = $desde; $n <= $hasta; $n++) {
									$estructura[$conteo]['values'][] = (int)$n;
								}
								$flag = false;
								
							} else {
								
								if($letter == '/') {
									$desde = implode("", $numero);
									$flag = true;
									$numero = [];
								}
								if($letter == '=') {
									$estructura[$conteo]['index'] = (int)implode("", $numero);
									$operator = " IN ";
									$numero = [];
								}  	
								if($letter == '<' or $letter == '>') {
									$estructura[$conteo]['index'] = (int)implode("", $numero);
									$operator = " NOT IN ";
									$numero = [];
								}							
								if($letter == ',') {
									$estructura[$conteo]['values'][] = (int)implode("", $numero);
									$numero = [];
								}
								if($letter == '+') {
									$estructura[$conteo]['values'][] = (int)implode("", $numero);
									$numero = [];
									$conteo++;
								}
								if($letter == ')') {
									$estructura[$conteo]['values'][] = (int)implode("", $numero);
									$numero = [];
								}
								
							} 
							
						} 
					}
					if(count($estructura) > 1 and $flag_or == true) { 
						$this->warn("OR");
					}
					foreach($estructura as $k => $v) {
						$flag_or = true;
						if($k > 0) {
							$this->warn("AND");
						}
						$this->warn($v['index'] . " IN " . "(" . implode(",", $v['values']) . ")");
					}
					
					
				} else {
					$nombre = trim($line);
					$this->out($nombre);
					$flag_or = false;
				} 
				
				if(trim($line) == 'N' or trim($line) == "Y") {
					break;
				}
			
			}
			
			$row++;
		}
		
	}
	
}