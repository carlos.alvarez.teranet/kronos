<?php
namespace App\Shell;

use Cake\Console\Shell;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;
use Cake\Datasource\ConnectionManager;
use ZipArchive;

class Da1BrasilShell extends Shell
{

    public function initialize()
    {
        parent::initialize();
    }

	public function main() {

		$file 		= fopen(TMP . "da1" . DS . "Pesos2305_Brasil_P.da1", 'w');

		$connection = ConnectionManager::get('BR_KWP');
		//$inds		= $connection->execute("SELECT RIGHT('00000000' + CAST(idDomicilio as VARCHAR), 8) + RIGHT('00' + CAST(SEQDOM as VARCHAR), 2) as idDomicilio FROM dbo.RG_Domicilios_Pesos WHERE Ano >= 2019 GROUP by idDomicilio, SEQDOM ORDER by CAST(idDomicilio as INTEGER) ASC, CAST(SEQDOM as INTEGER) ASC")->fetchAll('assoc');

		$weights	= $connection->execute("SELECT 	RIGHT('00000000' + CAST(rgd.idDomicilio as VARCHAR), 8) + RIGHT('00' + CAST(rgd.SEQDOM as VARCHAR), 2) as idDomicilio,
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2019 AND rgd.MesSem = 1 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2019 AND rgd.MesSem = 2 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2019 AND rgd.MesSem = 3 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2019 AND rgd.MesSem = 4 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2019 AND rgd.MesSem = 5 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2019 AND rgd.MesSem = 6 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2019 AND rgd.MesSem = 7 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2019 AND rgd.MesSem = 8 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2019 AND rgd.MesSem = 9 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2019 AND rgd.MesSem = 10 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2019 AND rgd.MesSem = 11 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2019 AND rgd.MesSem = 12 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2020 AND rgd.MesSem = 1 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2020 AND rgd.MesSem = 2 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2020 AND rgd.MesSem = 3 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2020 AND rgd.MesSem = 4 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2020 AND rgd.MesSem = 5 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2020 AND rgd.MesSem = 6 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2020 AND rgd.MesSem = 7 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2020 AND rgd.MesSem = 8 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2020 AND rgd.MesSem = 9 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2020 AND rgd.MesSem = 10 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2020 AND rgd.MesSem = 11 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2020 AND rgd.MesSem = 12 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2021 AND rgd.MesSem = 1 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2021 AND rgc.MesSem = 1 AND rgc.idPeso = 2 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2021 AND rgc.MesSem = 1 AND rgc.idPeso = 3 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2021 AND rgc.MesSem = 1 AND rgc.idPeso = 4 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2021 AND rgc.MesSem = 1 AND rgc.idPeso = 5 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2021 AND rgc.MesSem = 1 AND rgc.idPeso = 6 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2021 AND rgc.MesSem = 1 AND rgc.idPeso = 7 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2021 AND rgc.MesSem = 1 AND rgc.idPeso = 8 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2021 AND rgc.MesSem = 1 AND rgc.idPeso = 9 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2021 AND rgd.MesSem = 2 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2021 AND rgc.MesSem = 2 AND rgc.idPeso = 2 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2021 AND rgc.MesSem = 2 AND rgc.idPeso = 3 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2021 AND rgc.MesSem = 2 AND rgc.idPeso = 4 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2021 AND rgc.MesSem = 2 AND rgc.idPeso = 9 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2021 AND rgd.MesSem = 3 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2021 AND rgc.MesSem = 3 AND rgc.idPeso = 4 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2021 AND rgc.MesSem = 3 AND rgc.idPeso = 5 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2021 AND rgc.MesSem = 3 AND rgc.idPeso = 6 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2021 AND rgc.MesSem = 3 AND rgc.idPeso = 9 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2021 AND rgd.MesSem = 4 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2021 AND rgc.MesSem = 4 AND rgc.idPeso = 2 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2021 AND rgd.MesSem = 5 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2021 AND rgc.MesSem = 5 AND rgc.idPeso = 2 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2021 AND rgc.MesSem = 5 AND rgc.idPeso = 3 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2021 AND rgc.MesSem = 5 AND rgc.idPeso = 4 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2021 AND rgc.MesSem = 5 AND rgc.idPeso = 5 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2021 AND rgc.MesSem = 5 AND rgc.idPeso = 6 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2021 AND rgc.MesSem = 5 AND rgc.idPeso = 7 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2021 AND rgc.MesSem = 5 AND rgc.idPeso = 8 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2021 AND rgc.MesSem = 5 AND rgc.idPeso = 20 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6)	+
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2021 AND rgd.MesSem = 6 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2021 AND rgc.MesSem = 6 AND rgc.idPeso = 2 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2021 AND rgc.MesSem = 6 AND rgc.idPeso = 3 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2021 AND rgc.MesSem = 6 AND rgc.idPeso = 4 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2021 AND rgc.MesSem = 6 AND rgc.idPeso = 5 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2021 AND rgc.MesSem = 6 AND rgc.idPeso = 6 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2021 AND rgc.MesSem = 6 AND rgc.idPeso = 7 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2021 AND rgc.MesSem = 6 AND rgc.idPeso = 8 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2021 AND rgc.MesSem = 6 AND rgc.idPeso = 20 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2021 AND rgd.MesSem = 7 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2021 AND rgc.MesSem = 7 AND rgc.idPeso = 2 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2021 AND rgc.MesSem = 7 AND rgc.idPeso = 3 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2021 AND rgc.MesSem = 7 AND rgc.idPeso = 4 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2021 AND rgc.MesSem = 7 AND rgc.idPeso = 5 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2021 AND rgc.MesSem = 7 AND rgc.idPeso = 6 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2021 AND rgc.MesSem = 7 AND rgc.idPeso = 7 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2021 AND rgc.MesSem = 7 AND rgc.idPeso = 8 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2021 AND rgc.MesSem = 7 AND rgc.idPeso = 20 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6)	+
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2021 AND rgd.MesSem = 8 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2021 AND rgc.MesSem = 8 AND rgc.idPeso = 2 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2021 AND rgc.MesSem = 8 AND rgc.idPeso = 3 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2021 AND rgc.MesSem = 8 AND rgc.idPeso = 4 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2021 AND rgc.MesSem = 8 AND rgc.idPeso = 5 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2021 AND rgc.MesSem = 8 AND rgc.idPeso = 6 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2021 AND rgc.MesSem = 8 AND rgc.idPeso = 7 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2021 AND rgc.MesSem = 8 AND rgc.idPeso = 8 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2021 AND rgc.MesSem = 8 AND rgc.idPeso = 20 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6)	+
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2021 AND rgd.MesSem = 9 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2021 AND rgc.MesSem = 9 AND rgc.idPeso = 2 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2021 AND rgc.MesSem = 9 AND rgc.idPeso = 3 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2021 AND rgc.MesSem = 9 AND rgc.idPeso = 4 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2021 AND rgc.MesSem = 9 AND rgc.idPeso = 5 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2021 AND rgc.MesSem = 9 AND rgc.idPeso = 6 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2021 AND rgc.MesSem = 9 AND rgc.idPeso = 7 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2021 AND rgc.MesSem = 9 AND rgc.idPeso = 8 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2021 AND rgc.MesSem = 9 AND rgc.idPeso = 20 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6)	+
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2021 AND rgd.MesSem = 10 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2021 AND rgc.MesSem = 10 AND rgc.idPeso = 2 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2021 AND rgc.MesSem = 10 AND rgc.idPeso = 3 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2021 AND rgc.MesSem = 10 AND rgc.idPeso = 4 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2021 AND rgc.MesSem = 10 AND rgc.idPeso = 5 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2021 AND rgc.MesSem = 10 AND rgc.idPeso = 6 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2021 AND rgc.MesSem = 10 AND rgc.idPeso = 7 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2021 AND rgc.MesSem = 10 AND rgc.idPeso = 8 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2021 AND rgc.MesSem = 10 AND rgc.idPeso = 20 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2021 AND rgd.MesSem = 11 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2021 AND rgc.MesSem = 11 AND rgc.idPeso = 2 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2021 AND rgc.MesSem = 11 AND rgc.idPeso = 3 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2021 AND rgc.MesSem = 11 AND rgc.idPeso = 4 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2021 AND rgc.MesSem = 11 AND rgc.idPeso = 5 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2021 AND rgc.MesSem = 11 AND rgc.idPeso = 6 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2021 AND rgc.MesSem = 11 AND rgc.idPeso = 7 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2021 AND rgc.MesSem = 11 AND rgc.idPeso = 8 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2021 AND rgc.MesSem = 11 AND rgc.idPeso = 20 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2021 AND rgd.MesSem = 12 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2021 AND rgc.MesSem = 12 AND rgc.idPeso = 2 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2021 AND rgc.MesSem = 12 AND rgc.idPeso = 3 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2021 AND rgc.MesSem = 12 AND rgc.idPeso = 4 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2021 AND rgc.MesSem = 12 AND rgc.idPeso = 5 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2021 AND rgc.MesSem = 12 AND rgc.idPeso = 6 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2021 AND rgc.MesSem = 12 AND rgc.idPeso = 7 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2021 AND rgc.MesSem = 12 AND rgc.idPeso = 8 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2021 AND rgc.MesSem = 12 AND rgc.idPeso = 20 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2022 AND rgd.MesSem = 1 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 1 AND rgc.idPeso = 2 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 1 AND rgc.idPeso = 3 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 1 AND rgc.idPeso = 4 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 1 AND rgc.idPeso = 5 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 1 AND rgc.idPeso = 6 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 1 AND rgc.idPeso = 7 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 1 AND rgc.idPeso = 8 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 1 AND rgc.idPeso = 20 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2022 AND rgd.MesSem = 2 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 2 AND rgc.idPeso = 2 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 2 AND rgc.idPeso = 3 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 2 AND rgc.idPeso = 4 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 2 AND rgc.idPeso = 5 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 2 AND rgc.idPeso = 6 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 2 AND rgc.idPeso = 7 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 2 AND rgc.idPeso = 8 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 2 AND rgc.idPeso = 20 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2022 AND rgd.MesSem = 3 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 3 AND rgc.idPeso = 2 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 3 AND rgc.idPeso = 3 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 3 AND rgc.idPeso = 4 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 3 AND rgc.idPeso = 5 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 3 AND rgc.idPeso = 6 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 3 AND rgc.idPeso = 7 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 3 AND rgc.idPeso = 8 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 3 AND rgc.idPeso = 20 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2022 AND rgd.MesSem = 4 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 4 AND rgc.idPeso = 2 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 4 AND rgc.idPeso = 3 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 4 AND rgc.idPeso = 4 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 4 AND rgc.idPeso = 5 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 4 AND rgc.idPeso = 6 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 4 AND rgc.idPeso = 7 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 4 AND rgc.idPeso = 8 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 4 AND rgc.idPeso = 20 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2022 AND rgd.MesSem = 5 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 5 AND rgc.idPeso = 2 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 5 AND rgc.idPeso = 3 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 5 AND rgc.idPeso = 4 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 5 AND rgc.idPeso = 5 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 5 AND rgc.idPeso = 6 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 5 AND rgc.idPeso = 7 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 5 AND rgc.idPeso = 8 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 5 AND rgc.idPeso = 20 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2022 AND rgd.MesSem = 6 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 6 AND rgc.idPeso = 2 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 6 AND rgc.idPeso = 3 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 6 AND rgc.idPeso = 4 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 6 AND rgc.idPeso = 5 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 6 AND rgc.idPeso = 6 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 6 AND rgc.idPeso = 7 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 6 AND rgc.idPeso = 8 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 6 AND rgc.idPeso = 20 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2022 AND rgd.MesSem = 7 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 7 AND rgc.idPeso = 2 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 7 AND rgc.idPeso = 3 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 7 AND rgc.idPeso = 4 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 7 AND rgc.idPeso = 5 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 7 AND rgc.idPeso = 6 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 7 AND rgc.idPeso = 7 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 7 AND rgc.idPeso = 8 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 7 AND rgc.idPeso = 20 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2022 AND rgd.MesSem = 8 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 8 AND rgc.idPeso = 2 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 8 AND rgc.idPeso = 3 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 8 AND rgc.idPeso = 4 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 8 AND rgc.idPeso = 5 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 8 AND rgc.idPeso = 6 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 8 AND rgc.idPeso = 7 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 8 AND rgc.idPeso = 8 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 8 AND rgc.idPeso = 20 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2022 AND rgd.MesSem = 9 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 9 AND rgc.idPeso = 2 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 9 AND rgc.idPeso = 3 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 9 AND rgc.idPeso = 4 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 9 AND rgc.idPeso = 5 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 9 AND rgc.idPeso = 6 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 9 AND rgc.idPeso = 7 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 9 AND rgc.idPeso = 8 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 9 AND rgc.idPeso = 20 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2022 AND rgd.MesSem = 10 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 10 AND rgc.idPeso = 2 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 10 AND rgc.idPeso = 3 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 10 AND rgc.idPeso = 4 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 10 AND rgc.idPeso = 5 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 10 AND rgc.idPeso = 6 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 10 AND rgc.idPeso = 7 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 10 AND rgc.idPeso = 8 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 10 AND rgc.idPeso = 20 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2022 AND rgd.MesSem = 11 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 11 AND rgc.idPeso = 2 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 11 AND rgc.idPeso = 3 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 11 AND rgc.idPeso = 4 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 11 AND rgc.idPeso = 5 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 11 AND rgc.idPeso = 6 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 11 AND rgc.idPeso = 7 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 11 AND rgc.idPeso = 8 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 11 AND rgc.idPeso = 20 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2022 AND rgd.MesSem = 12 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 12 AND rgc.idPeso = 2 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 12 AND rgc.idPeso = 3 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 12 AND rgc.idPeso = 4 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 12 AND rgc.idPeso = 5 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 12 AND rgc.idPeso = 6 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 12 AND rgc.idPeso = 7 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 12 AND rgc.idPeso = 8 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 12 AND rgc.idPeso = 20 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2023 AND rgd.MesSem = 1 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 1 AND rgc.idPeso = 2 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 1 AND rgc.idPeso = 3 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 1 AND rgc.idPeso = 4 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 1 AND rgc.idPeso = 5 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 1 AND rgc.idPeso = 6 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 1 AND rgc.idPeso = 7 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 1 AND rgc.idPeso = 8 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 1 AND rgc.idPeso = 20 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2023 AND rgd.MesSem = 2 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 2 AND rgc.idPeso = 2 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 2 AND rgc.idPeso = 3 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 2 AND rgc.idPeso = 4 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 2 AND rgc.idPeso = 5 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 2 AND rgc.idPeso = 6 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 2 AND rgc.idPeso = 7 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 2 AND rgc.idPeso = 8 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 2 AND rgc.idPeso = 20 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2023 AND rgd.MesSem = 3 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 3 AND rgc.idPeso = 2 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 3 AND rgc.idPeso = 3 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 3 AND rgc.idPeso = 4 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 3 AND rgc.idPeso = 5 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 3 AND rgc.idPeso = 6 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 3 AND rgc.idPeso = 7 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 3 AND rgc.idPeso = 8 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 3 AND rgc.idPeso = 20 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2023 AND rgd.MesSem = 4 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 4 AND rgc.idPeso = 2 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 4 AND rgc.idPeso = 3 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 4 AND rgc.idPeso = 4 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 4 AND rgc.idPeso = 5 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 4 AND rgc.idPeso = 6 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 4 AND rgc.idPeso = 7 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 4 AND rgc.idPeso = 8 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 4 AND rgc.idPeso = 20 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2023 AND rgd.MesSem = 5 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 5 AND rgc.idPeso = 2 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 5 AND rgc.idPeso = 3 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 5 AND rgc.idPeso = 4 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 5 AND rgc.idPeso = 5 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 5 AND rgc.idPeso = 6 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 5 AND rgc.idPeso = 7 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 5 AND rgc.idPeso = 8 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 5 AND rgc.idPeso = 20 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6)
											as [pesos]
									FROM 	dbo.RG_Domicilios_Pesos rgd
									LEFT JOIN	dbo.RG_Domicilios_Pesos rgc ON rgc.Iddomicilio = rgd.idDomicilio AND rgc.SEQDOM = rgd.SEQDOM AND rgc.Ano = rgd.Ano AND rgc.MesSem = rgd.messem
									WHERE	rgd.idPeso = 1
									AND		rgd.Ano >= 2019
									GROUP	by RIGHT('00000000' + CAST(rgd.idDomicilio as VARCHAR), 8) + RIGHT('00' + CAST(rgd.SEQDOM as VARCHAR), 2)
									ORDER	by 1 ASC")->fetchAll('assoc');

		$connection = ConnectionManager::get('BR_SPRI');
		$years		= [2023, 2022, 2021, 2020, 2019];
		$layout 	= $connection->execute("SELECT Nombre, spVariable, Largo, PosInicial FROM dbo.RG_Panelis_Variables ORDER by PosInicial ASC")->fetchAll('assoc');

		$n = 0;
		$this->verbose("Cantidad de Domicilios a procesar: " . count($weights));
		foreach($weights as $ind) {

			$n++;
			$this->verbose("Procesando Domicilio: " . $n);
			$main_row = [];

			/*
			$query	= $connection->execute("SELECT [PAIS],[idDomicilio],[SEQDOM],[CIUDAD],[EDINDIVIDUO],[FX_EDINDIVIDUO],[SEXOIND],[ACTIVIDADIND],[PNSE_LOC],[NSE_LOC],[IDCORPELE],[GRAUPARENTESCO],[EDDONACASA],[FLAG_DC],[PCRIANCAS],[BR_FXREGIAO],[BR_FXREGIAO_SEMMICRO],[SPLIT],[COBERTURA_OOH],[FM01],[FM02],[FM03],[FM04],[FM05],[FM06],[FM07],[FM08],[FM09],[FM10],[FM11],[FM12], [Ano]  FROM dbo.RG_Panelis WHERE RIGHT('00000000' + CAST(Dom as VARCHAR), 8) + RIGHT('00' + CAST(Ind as VARCHAR), 2) = :idIndividuo AND Ano IN (2023, 2022, 2021, 2020, 2019) ORDER by ANO DESC", ['idIndividuo' =>  $ind['idIndividuo']]);
			*/
			$query	= $connection->execute("SELECT [PAIS],[NPAN],[SEQDOM],[EDAC],[ANAC],[SEXAC],[SLAC],[NIAC],[ACPSH],[EDPSH],[SEXPSH],[SLPSH],[NIPSH],[PNSE_ABEP_2014],[PNSE_LOC],[FILLER1],[CLSM],[PNSELP],[CNSELP],[NI],[NIMA18],[NI15],[NI12],[NI5],[NI3],[N_4_5],[N_6_12],[N_10_15],[N_13_17],[N_16_19],[ADOL],[PHIJO],[HIJOS],[IME],[IMA],[FEM],[EDFEMMEN],[EDFEMMAY],[MASC],[EDMASCMEN],[EDMASCMAY],[ZONA],[CIUDAD],[DIST],[FILLER2],[FRAC],[RAD],[TZON],[AUTOS],[LLV],[LLF],[LN],[PC],[NET],[ANOS_BA],[ANOS_TE],[EMP_NET],[NTEL],[TVCOLOR],[VD],[DVD],[REFRIG],[LVJ],[VCAB],[TVSA],[TVBN],[CELU],[CG],[MICR],[ASP],[ENC],[EQM],[PAVA],[OLLA],[PLV],[RLJ],[FILM],[FOTO_ROL],[FOTO_DIG],[FAX],[HTHE],[SV],[GOS],[GAT],[ET],[THOG],[EDMA],[TIPH],[TOIL],[AMB],[DORM],[RADI],[TRAN],[LR],[LD],[CANA],[SIND1],[AIND1],[PIND1],[CEL1],[SIND2],[AIND2],[PIND2],[CEL2],[SIND3],[AIND3],[PIND3],[CEL3],[SIND4],[AIND4],[PIND4],[CEL4],[SIND5],[AIND5],[PIND5],[CEL5],[SIND6],[AIND6],[PIND6],[CEL6],[SIND7],[AIND7],[PIND7],[CEL7],[SIND8],[AIND8],[PIND8],[CEL8],[SIND9],[AIND9],[PIND9],[CEL9],[SIND10],[AIND10],[PIND10],[CEL10],[SIND11],[AIND11],[PIND11],[CEL11],[SIND12],[AIND12],[PIND12],[CEL12],[SIND13],[AIND13],[PIND13],[CEL13],[SIND14],[AIND14],[PIND14],[CEL14],[SIND15],[AIND15],[PIND15],[CEL15],[SIND16],[AIND16],[PIND16],[CEL16],[PER_RAZ],[PER_SRA],[PER_GIG],[PER_GRA],[PER_MED],[PER_PEQ],[PER_MIN],[GAT_RAZ],[GAT_SRA],[CMAS],[COMEDOR],[LIVING],[DORMPPAL],[DORMIT],[BAÑOPPAL],[COCINA],[TERRAZA],[BR_12],[BR_BEBE],[BR_VGAME],[BR_PFD],[BR_EV],[BR_PELE],[BR_SEGPROC],[BR_12_19],[BR_MAQLAVSEC],[BR_IMC],[BR_CCRED],[BR_SITDOM],[BR_MAQTAN],[BR_CELULAR],[BR_FAIXREND],[BR_UFMUN],[BR_FXREGIAO],[EDBEQ1],[EDBEQ2],[EDBEQ3],[EDBEQ4],[BR_SPLITCO],[BR_NRFILHOS],[BR_0_6],[BR_7_12],[BR_13_17],[BR_MAQ09],[BR_MAQ10],[BR_MAQ45],[PLSM],[R5],[BR_BANDALARGA],[PNSE2011],[NSE2011],[CV],[CSHOPTRIB],[Empregada],[CM1],[CM2],[NSEREG],[PNSE_ORIGINAL],[NSE_LOC],[GRAU_INSTRUCAO_CF],[LSM20],[LSG20],[NIME18],[EDJF],[TIPOCOLETA],[Split],[BEBE_MES01],[BEBE_MES02],[BEBE_MES03],[BEBE_MES04],[BEBE_MES05],[BEBE_MES06],[BEBE_MES07],[BEBE_MES08],[BEBE_MES09],[BEBE_MES10],[BEBE_MES11],[BEBE_MES12],[BEBE_MES],[FEM10_13],[FEM14_17],[FEM18_24],[FEM25_49],[TEEN_FEEM_10_17],[TEEN_FEEM_18_24],[N_0_3],[N_4_6],[N_7_12],[FILLER1ESP],[N_7_10],[N_11_13],[N_14_17],[CMEDIA],[IMEQ1],[IMEQ2],[IMEQ3],[IMEQ4],[Origen],[MIRROR01],[MIRROR02],[MIRROR03],[MIRROR04],[MIRROR05],[MIRROR06],[MIRROR07],[MIRROR08],[MIRROR09],[MIRROR10],[MIRROR11],[MIRROR12],[Ano] FROM dbo.RG_Panelis WHERE RIGHT('00000000' + CAST(NPAN as VARCHAR), 8) + RIGHT('00' + CAST(SEQDOM as VARCHAR), 2) = :idDomicilio AND Ano IN (2023, 2022, 2021, 2020, 2019) ORDER by ANO DESC", ['idDomicilio' => $ind['idDomicilio']]);

			$rows	= $query->fetchAll('assoc');

			foreach($years as $year) {

				$key 	= array_search($year, array_column($rows, 'Ano'));
				$line = '';

				if($key !== false) {

					foreach($layout as $element) {
						//$this->abort(print_r($rows[$key], true));
						$add_line = str_pad($rows[$key][$element['Nombre']], $element['Largo'], "0", STR_PAD_LEFT);
						if(strlen($add_line) != $element['Largo']) {
							$this->warn("Arreglo: " . print_r($rows[$key], true));
							$this->warn("Variable: " . $element['Nombre']);
							$this->abort("Error de longitud de variable");
						}
						$line .= $add_line;
					}

				} else {

					foreach($layout as $element) {
						$line .= str_pad($rows[0][$element['Nombre']], $element['Largo'], "0", STR_PAD_LEFT);
					}

				}

				$main_row[] = str_pad($line, 500, "0", STR_PAD_RIGHT);

			}

			$clave = array_search($ind['idDomicilio'], array_column($weights, 'idDomicilio'));
			$pesosPeriodos = $weights[$clave]['pesos'];


			$text = implode("", $main_row).$pesosPeriodos;
			//fwrite($file, str_pad($text, 4048, "0", STR_PAD_RIGHT) . PHP_EOL);
			fwrite($file, str_pad($text, ( (5 * 500) + strlen($ind['pesos']) ), "0", STR_PAD_RIGHT) . PHP_EOL);


		}

	}

}