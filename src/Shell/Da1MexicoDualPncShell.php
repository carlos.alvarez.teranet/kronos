<?php
namespace App\Shell;

use Cake\Console\Shell;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;
use Cake\Datasource\ConnectionManager;


class Da1MexicoDualPncShell extends Shell
{

    public function initialize()
    {
        parent::initialize();
    }

	public function main() {
		
		$file 		= fopen(TMP . "da1" . DS . "Pesos2303_Mexico_P.da1", 'w');
		
		$connection = ConnectionManager::get('MX_KWP');
		
		$weights	= $connection->execute("SELECT 	RIGHT('00000000' + CAST(rgd.idDomicilio as VARCHAR), 8) as idDomicilio,
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2019 AND rgd.MesSem = 1 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2019 AND rgd.MesSem = 2 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2019 AND rgd.MesSem = 3 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2019 AND rgd.MesSem = 4 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2019 AND rgd.MesSem = 5 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2019 AND rgd.MesSem = 6 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2019 AND rgd.MesSem = 7 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2019 AND rgd.MesSem = 8 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2019 AND rgd.MesSem = 9 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2019 AND rgd.MesSem = 10 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2019 AND rgd.MesSem = 11 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2019 AND rgd.MesSem = 12 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2020 AND rgd.MesSem = 1 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2020 AND rgd.MesSem = 2 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2020 AND rgd.MesSem = 3 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2020 AND rgd.MesSem = 4 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2020 AND rgd.MesSem = 5 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2020 AND rgd.MesSem = 6 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2020 AND rgd.MesSem = 7 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2020 AND rgd.MesSem = 8 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2020 AND rgd.MesSem = 9 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2020 AND rgd.MesSem = 10 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2020 AND rgd.MesSem = 11 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2020 AND rgd.MesSem = 12 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2021 AND rgd.MesSem = 1 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2021 AND rgd.MesSem = 2 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2021 AND rgd.MesSem = 3 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2021 AND rgd.MesSem = 4 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2021 AND rgd.MesSem = 5 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2021 AND rgd.MesSem = 6 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2021 AND rgd.MesSem = 7 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2021 AND rgd.MesSem = 8 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2021 AND rgd.MesSem = 9 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2021 AND rgd.MesSem = 10 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2021 AND rgd.MesSem = 11 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2021 AND rgd.MesSem = 12 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2022 AND rgd.MesSem = 1 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2022 AND rgd.MesSem = 2 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2022 AND rgd.MesSem = 3 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2022 AND rgd.MesSem = 4 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2022 AND rgd.MesSem = 5 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2022 AND rgd.MesSem = 6 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2022 AND rgd.MesSem = 7 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2022 AND rgd.MesSem = 8 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2022 AND rgd.MesSem = 9 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2022 AND rgd.MesSem = 10 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2022 AND rgd.MesSem = 11 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2022 AND rgd.MesSem = 12 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2023 AND rgd.MesSem = 1 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2023 AND rgd.MesSem = 2 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2023 AND rgd.MesSem = 3 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6)
											as [pesos]
									FROM 	dbo.RG_Domicilios_Pesos_SE rgd
									LEFT JOIN	dbo.RG_Domicilios_Pesos_SE rgc ON rgc.Iddomicilio = rgd.Iddomicilio AND rgc.Ano = rgd.Ano AND rgc.MesSem = rgd.messem
									WHERE	rgd.idPeso = 1
									AND		rgd.Ano >= 2019
									GROUP	by RIGHT('00000000' + CAST(rgd.idDomicilio as VARCHAR), 8)
									ORDER	by 1 ASC")->fetchAll('assoc');
									
		$length = strlen($weights[0]['pesos']);
									
									
									
		$weights_QTZ	= $connection->execute("SELECT 	RIGHT('00000000' + CAST(rgd.idDomicilio as VARCHAR), 8) as idDomicilio,
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2019 AND rgd.MesSem = 1 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2019 AND rgd.MesSem = 2 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2019 AND rgd.MesSem = 3 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2019 AND rgd.MesSem = 4 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2019 AND rgd.MesSem = 5 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2019 AND rgd.MesSem = 6 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2019 AND rgd.MesSem = 7 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2019 AND rgd.MesSem = 8 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2019 AND rgd.MesSem = 9 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2019 AND rgd.MesSem = 10 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2019 AND rgd.MesSem = 11 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2019 AND rgd.MesSem = 12 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2020 AND rgd.MesSem = 1 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2020 AND rgd.MesSem = 2 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2020 AND rgd.MesSem = 3 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2020 AND rgd.MesSem = 4 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2020 AND rgd.MesSem = 5 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2020 AND rgd.MesSem = 6 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2020 AND rgd.MesSem = 7 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2020 AND rgd.MesSem = 8 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2020 AND rgd.MesSem = 9 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2020 AND rgd.MesSem = 10 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2020 AND rgd.MesSem = 11 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2020 AND rgd.MesSem = 12 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2021 AND rgd.MesSem = 1 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2021 AND rgd.MesSem = 2 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2021 AND rgd.MesSem = 3 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2021 AND rgd.MesSem = 4 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2021 AND rgd.MesSem = 5 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2021 AND rgd.MesSem = 6 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2021 AND rgd.MesSem = 7 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2021 AND rgd.MesSem = 8 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2021 AND rgd.MesSem = 9 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2021 AND rgd.MesSem = 10 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2021 AND rgd.MesSem = 11 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2021 AND rgd.MesSem = 12 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2022 AND rgd.MesSem = 1 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2022 AND rgd.MesSem = 2 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2022 AND rgd.MesSem = 3 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2022 AND rgd.MesSem = 4 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2022 AND rgd.MesSem = 5 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2022 AND rgd.MesSem = 6 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2022 AND rgd.MesSem = 7 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2022 AND rgd.MesSem = 8 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2022 AND rgd.MesSem = 9 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2022 AND rgd.MesSem = 10 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2022 AND rgd.MesSem = 11 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2022 AND rgd.MesSem = 12 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2023 AND rgd.MesSem = 1 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 1 AND rgc.idPeso = 2 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 1 AND rgc.idPeso = 3 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 1 AND rgc.idPeso = 4 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 1 AND rgc.idPeso = 5 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2023 AND rgd.MesSem = 2 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 2 AND rgc.idPeso = 2 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 2 AND rgc.idPeso = 3 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 2 AND rgc.idPeso = 4 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 2 AND rgc.idPeso = 5 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2023 AND rgd.MesSem = 3 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 3 AND rgc.idPeso = 2 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 3 AND rgc.idPeso = 3 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 3 AND rgc.idPeso = 4 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 3 AND rgc.idPeso = 5 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 3 AND rgc.idPeso = 6 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 3 AND rgc.idPeso = 7 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 3 AND rgc.idPeso = 8 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 3 AND rgc.idPeso = 20 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6)
											as [pesos]
									FROM 	dbo.RG_Domicilios_Pesos rgd
									LEFT JOIN	dbo.RG_Domicilios_Pesos rgc ON rgc.Iddomicilio = rgd.Iddomicilio AND rgc.Ano = rgd.Ano AND rgc.MesSem = rgd.messem
									WHERE	rgd.idPeso = 1
									AND		rgd.Ano >= 2019
									GROUP	by RIGHT('00000000' + CAST(rgd.idDomicilio as VARCHAR), 8)
									ORDER	by 1 ASC")->fetchAll('assoc');
									
		$length_QTZ = strlen($weights_QTZ[0]['pesos']);	
									
													
		$connection = ConnectionManager::get('MX_SPRI');
		$years		= [2023, 2022, 2021, 2020, 2019];
		$layout 	= $connection->execute("SELECT Nombre, spVariable, Largo, PosInicial FROM dbo.RG_Panelis_Variables ORDER by PosInicial ASC")->fetchAll('assoc');
		
		$n = 0;
		$this->verbose("Cantidad de Domicilios a procesar: " . count($weights));
		foreach($weights_QTZ as $ind) {
			
			$n++;
			$this->verbose("Procesando domicilio: " . $n);
			$main_row = [];
			
			$query	= $connection->execute("SELECT [PAIS],[NPAN],[EDAC],[NIAC],[NSE_LOC],[NI],[NI15],[NI12],[N_16_19],[IME],[ZONA],[CIUDAD],[GOS],[GAT],[EDMAQ1],[EDMAQ2],[EDMAQ3],[EDMAQ4],[FILTRO_A],[REGION_BEER_NEW],[SLIME],[SLIMA],[PER_GIG],[PER_GRA],[PER_MED],[PER_PEQ],[PER_MIN],[CV],[3a5A],[6a10A],[11a15A],[NI19],[DIABET],[NIMEQ1],[NIMEQ2],[NIMEQ3],[NIMEQ4],[IMCAC_2],[IMCJF_2],[IMCPR_2],[VCAC],[NSEREG],[Region],[Estrato],[NIME18],[GAT_JOV],[GAT_ADU],[GAT_SEN],[PER_ALIM_CAS],[PER_ALIM_COMP],[PER_ALIM_COMB],[GAT_ALIM_CAS],[GAT_ALIM_COMP],[GAT_ALIM_COMB],[MIRROR01],[MIRROR02],[MIRROR03],[MIRROR04],[MIRROR05],[MIRROR06],[MIRROR07],[MIRROR08],[MIRROR09],[MIRROR10],[MIRROR11],[MIRROR12],[QUETZAL01],[QUETZAL02],[QUETZAL03],[QUETZAL04],[QUETZAL05],[QUETZAL06],[QUETZAL07],[QUETZAL08],[QUETZAL09],[QUETZAL10],[QUETZAL11],[QUETZAL12],[NSE_MIXTO],[NIME_MES01],[NIME_MES02],[NIME_MES03],[NIME_MES04],[NIME_MES05],[NIME_MES06],[NIME_MES07],[NIME_MES08],[NIME_MES09],[NIME_MES10],[NIME_MES11],[NIME_MES12],[Ano] FROM dbo.RG_Panelis WHERE idDomicilio = :idDomicilio AND Ano IN (2023, 2022, 2021, 2020, 2019) ORDER by ANO DESC", ['idDomicilio' => $ind['idDomicilio']]);
			$rows	= $query->fetchAll('assoc');
			
			foreach($years as $year) {
				
				$key 	= array_search($year, array_column($rows, 'Ano'));
				$line = '';				
				
				if($key !== false) {
			
					foreach($layout as $element) {
						$rows[$key][$element['Nombre']] = (!is_numeric($rows[$key][$element['Nombre']])) ? 0 : $rows[$key][$element['Nombre']];
						$line .= str_pad($rows[$key][$element['Nombre']] * 1, $element['Largo'], "0", STR_PAD_LEFT);
						
						if(strlen($rows[$key][$element['Nombre']]) > $element['Largo']) {
							$this->warn("idDomicilio " . $ind['idDomicilio'] . " en año " . $year . " posee la variable " . $element['Nombre'] . " con un largo " . strlen($rows[$key][$element['Nombre']]) . " y se esperaba un largo " . $element['Largo']);
						}
					}
					
				} else {
					
					foreach($layout as $element) {
						$rows[0][$element['Nombre']] = (!is_numeric($rows[0][$element['Nombre']])) ? 0 : $rows[0][$element['Nombre']];
						$line .= str_pad($rows[0][$element['Nombre']] * 1, $element['Largo'], "0", STR_PAD_LEFT);	
					}
					
				}
				
				$main_row[] = str_pad($line, 500, "0", STR_PAD_RIGHT);
				
			}
			
			$clave = array_search($ind['idDomicilio'], array_column($weights, 'idDomicilio'));
			if($clave === false) { $pesosPeriodos = str_pad("0", $length, "0", STR_PAD_RIGHT); } else { $pesosPeriodos = $weights[$clave]['pesos']; }

			$clave = array_search($ind['idDomicilio'], array_column($weights_QTZ, 'idDomicilio'));
			if($clave === false) { $pesosPeriodos_QTZ = str_pad("0", $length_QTZ, "0", STR_PAD_RIGHT); } else { $pesosPeriodos_QTZ = $weights_QTZ[$clave]['pesos']; }
			
			
			$text = implode("", $main_row).$pesosPeriodos_QTZ.$pesosPeriodos;
			fwrite($file, str_pad($text, ( (5 * 500) + $length + $length_QTZ ), "0", STR_PAD_RIGHT) . PHP_EOL);
			
		}
		
	}
    
}