<?php
namespace App\Shell;

use Cake\Console\Shell;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;
use Cake\Datasource\ConnectionManager;


class MarcoShell extends Shell
{

    public function initialize()
    {
        parent::initialize();
        $this->loadModel('Pruebas');
        $this->loadModel('Countries');
        
    }

	public function main() 
	{
	
		$data = $this->Countries->find('all', [
			'contain' => 'Clusters.Oohs'
		]);
		
		$this->out($data->toArray());
		
	}

}
?>