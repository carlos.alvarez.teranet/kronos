<?php
namespace App\Shell;

use Cake\Console\Shell;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;
use Cake\Datasource\ConnectionManager;


class Da2MergeShell extends Shell
{

    public function initialize()
    {
        parent::initialize();
    }
    
    public function main($idProduto = null) {
	    
	    $connection = ConnectionManager::get('MX_KWP');
	    
	    $data	= $connection->execute("SELECT	DA2_217
	    								FROM	dbo.J_Layout_PNC_Quetzal2
	    								WHERE	idDomicilio IN (1037469,1037470,1037471,1037472,1037473,1037474,1037475,1037476,1037477,1037478,1037479,1037480,1037481,1037482,1037483,1037485,1037487,1037489,1037490,1037491,1037492,1037493,1037494,1037496,1037499,1037500,1037502,1037503,1037504,1037505,1037508,1037509,1037510,1037512,1037513,1037514,1037515,1037516,1037517,1037519,1037520,1037525,1037527,1037528,1037529,1037530,1037531,1037532,1037533,1037534,1037536,1037537,1037538,1037539,1037540,1037542,1037543,1037547,1037548,1037549,1037552,1037553,1037554,1037555,1037558,1037560,1037561,1037562,1037565,1037567,1037568,1037569,1037570,1037571,1037572,1037574,1037575,1037576,1037577,1037578,1037579,1037580,1037581,1037582,1037583,1037584,1037586,1037587,1037589,1037590,1037592,1037593,1037594,1037595,1037596,1037597,1037598)
										AND		Ano = 2022 AND Mes = 2
										AND		idProduto = $idProduto")->fetchAll('assoc');
										
		$folder 	= "/mnt/procesos/ACTOS/MEXICO/2022_07/";
		$file		= $folder . "ATO" . str_pad($idProduto, 3) . ".072";
		$newfile	= $folder . "ATO" . str_pad($idProduto, 3) . ".072.updated";
		
		if (!copy($file, $newfile)) {
		    $this->abort("failed to copy $file...\n");
		}
		$this->out("Archivo copiado con éxito");
		
		$results	= 	fopen($newfile, "a");
		foreach($data as $row => $line) {
			$this->info("Escribiendo linea adicional: " . $line['DA2_217']);
			fwrite($results, $line['DA2_217']."\n");
		}
		fclose($results);
	    
    }
    
    public function mult55kw() {
	    
	    $connection 	= ConnectionManager::get('BR_KWP');
	    $folder			= '/mnt/procesos/ACTOS/BRASIL/2023_02/';
	    $destination	= '/mnt/procesos/ACTOS/BRASIL/atoMULT55KWcs.0223';
	    
	    $acts			= $connection->execute("SELECT	*, 'ATO'+Item+'.023' as Archivo
												FROM	dbo.DBM_DA2_Items
												WHERE	idDa2 = 126")->fetchAll('assoc');    
		
		$final = fopen($destination, "w");				
		foreach($acts as $act) {
			
			$this->info('Procesando acto: ' . $act['Archivo']);
			
			$origen	=	fopen($folder . $act['Archivo'], 'r');
			while(!feof($origen)) {
				$line = fgets($origen);
				if(substr($line, 0, 6) * 1 >= 202003) {
					fwrite($final, substr($line, 0, 236) . "\n");
				}
			}
			
		}
	    
	    
    }

	/*
	public function main() {
		
		$result		= "/mnt/P/ZIbanz/Atos/atoMULT55PGcs.0721";
		$results 	= fopen($result, 'r');
		
		$final		= fopen("/mnt/P/ZIbanz/Atos/atoMULT55PGcs.0721.New", "w");
		
		while(!feof($results)) {
				
			$line = fgets($results);
			//if(substr($line, 0, 6) * 1 >= 201808) {
				fwrite($final, substr($line, 0, 236) . PHP_EOL);
				//fwrite($final, $line . PHP_EOL);
			//}
			
		}
		
		
	}
	*/
	
	/*
	public function main() {
		
		$folder		= "/mnt/P/ACTOS/MEXICO/oficial_2021_07";
		$result		= "/mnt/P/ZIbanz/Atos/atoMULT52KWcs.0721.New";
		$results 	= fopen($result, 'w');
		
		$connection = ConnectionManager::get('BR_KWP');
		$acts		= $connection->execute("SELECT * FROM dbo.DBM_DA2_Items WHERE idDa2 = 114")->fetchAll('assoc');
		
		foreach($acts as $act) {
			
			$this->info("[" . date('Y-m-d h:i:s') . "] " . "idProduto: " . $act['Item']);			
			$file = fopen($folder . DS . "ATO".$act['Item'].".071", 'r');
			
			while(!feof($file)) {
				
				$line = fgets($file);
				if(substr($line, 0, 6) * 1 >= 201808) {
					fwrite($results, substr($line, 0, 236) . PHP_EOL);
				}
				
			}
			
		}
				
	}
	*/
    
}