<?php
namespace App\Shell;

use Cake\Console\Shell;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;
use PHPExcel;
use PHPExcel_IOFactory;
use Cake\Datasource\ConnectionManager;
use Cake\Mailer\Email;


class UploadExcelWeightShell extends Shell
{

    public function main($c_sel = null, $p_sel = null, $key = null)
    {
    	// Archivo configuración
		$string 	= file_get_contents( CONFIG . DS . "platform" . DS . "weighings.json" );
		$config 	= json_decode($string, true);
		$log		= LOGS . "weights" . DS . $config['countries'][$c_sel]['code'] . "_" . $config['countries'][$c_sel]['panels'][$p_sel]['code'] . ".json";
		$file 		= new File($log, true, 0644);
		$jsonData 	= json_decode($file->read(), true);
		
		$output			= LOGS . "weights" . DS . $config['countries'][$c_sel]['code'] . "_" . $config['countries'][$c_sel]['panels'][$p_sel]['code'] . "_file_" . $key . ".json";
		$outputFile 	= new File($output, true, 0644);		
		$inputFileName	= $jsonData['items'][$key]['file'];
		
		$year 	=  $jsonData['items'][$key]['year'];
		$month	=  $jsonData['items'][$key]['month'];
		
		$init 	= date('d-m-Y H:i:s');
		
		
		//  Leer archivo guardado previamente en las carpetas de webroot
		try {
		    $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
		    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
		    $objPHPExcel = $objReader->load($inputFileName);
		} catch(Exception $e) {
		    die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
		}
		
		$sheet = $objPHPExcel->getSheet(0);
		
		$initialRow = 2;
		$highestRow = $sheet->getHighestRow(); 
		$initialColumn = 1;
		$highestColumn = $sheet->getHighestColumn(); 
		
		$rowData = $sheet->rangeToArray('A' . $initialRow . ':' . $highestColumn . $highestRow,
		                                    NULL,
		                                    TRUE,
		                                    FALSE);
		                                    
		$headerData = $sheet->rangeToArray('A' . ($initialRow - 1) . ':' . $highestColumn . ($initialRow - 1),
		                                    NULL,
		                                    TRUE,
		                                    FALSE)[0];
		                                    
		$headerData = array_map('trim', $headerData);                                   
		foreach($rowData as $items) {
			$aux = [];
			foreach($items as $key2 => $value) {
				//$head = ($headerData[$key2]) ? $headerData[$key] : 'Original';
				$head = $headerData[$key2];
				$aux[$head] = $value;
			}
			$data[] = $aux;
		}
		
		//$outputFile->write(json_encode($data));
		// Subir datos a SQL
		$dbname	= $config['countries'][$c_sel]['panels'][$p_sel]['csv']['db'];
		$table	= $config['countries'][$c_sel]['panels'][$p_sel]['csv']['table'];
		$columns = $config['countries'][$c_sel]['panels'][$p_sel]['csv']['columns'];
		$columns_insert = [];
		
		foreach($columns as $key3 => $column) {
			$columns_insert[strtolower($column['from'])] = strtolower($column['to']);
		}
		
		$connection = ConnectionManager::get($dbname);
		
		$total = count($data);
		foreach($data as $key4 => $element) {
			$registry = [];
			foreach($element as $header => $value) {
				if(array_search(strtolower($header), array_keys($columns_insert)) !== false) {
					$registry[$columns_insert[strtolower($header)]] = strval($value);
				}
			}
			if( (($registry['ano'] * 100) + ($registry['messem'] * 1)) == (($year * 100) + ($month * 1)) ) {
				$this->verbose("INSERT INTO $table (" . implode(",", array_keys($registry)) . ") VALUES (" . implode(",", array_values($registry)) . ")");
				$connection->execute("INSERT INTO $table (" . implode(",", array_keys($registry)) . ") VALUES (" .  sprintf("'%s'", implode("','", array_values($registry) ) ) . ")");
			}
			
			$outputFile->append(json_encode(['progress' => number_format((($key4 + 1) / $total * 100), 1, ",", "."), 'init' => $init, 'end' => date('d-m-Y H:i:s')]).PHP_EOL);
			
		}
		
		$jsonData['items'][$key]['last_execution'] = date('d-m-Y H:i:s');
		$jsonData = json_encode($jsonData);
		$file->write($jsonData);
		
		sleep(3);
		$outputFile->delete();
		
		// Summary
		$connection = ConnectionManager::get($config['countries'][$c_sel]['panels'][$p_sel]['weights']['db']);
		$table 		= $config['countries'][$c_sel]['panels'][$p_sel]['weights']['table'];
		$weights	= $connection->execute("SELECT	Ano,
													SUM(CASE WHEN MesSem = 1 THEN Valor ELSE 0 END) as Mes1,
													SUM(CASE WHEN MesSem = 2 THEN Valor ELSE 0 END) as Mes2,
													SUM(CASE WHEN MesSem = 3 THEN Valor ELSE 0 END) as Mes3,
													SUM(CASE WHEN MesSem = 4 THEN Valor ELSE 0 END) as Mes4,
													SUM(CASE WHEN MesSem = 5 THEN Valor ELSE 0 END) as Mes5,
													SUM(CASE WHEN MesSem = 6 THEN Valor ELSE 0 END) as Mes6,
													SUM(CASE WHEN MesSem = 7 THEN Valor ELSE 0 END) as Mes7,
													SUM(CASE WHEN MesSem = 8 THEN Valor ELSE 0 END) as Mes8,
													SUM(CASE WHEN MesSem = 9 THEN Valor ELSE 0 END) as Mes9,
													SUM(CASE WHEN MesSem = 10 THEN Valor ELSE 0 END) as Mes10,
													SUM(CASE WHEN MesSem = 11 THEN Valor ELSE 0 END) as Mes11,
													SUM(CASE WHEN MesSem = 12 THEN Valor ELSE 0 END) as Mes12
											FROM	$table
											WHERE	Ano >= 2018
											AND		idPeso = 1
											GROUP	by Ano
											ORDER	by Ano")->fetchAll('assoc');
											
		$samples	= $connection->execute("SELECT	Ano,
													SUM(CASE WHEN MesSem = 1 THEN 1 ELSE 0 END) as Mes1,
													SUM(CASE WHEN MesSem = 2 THEN 1 ELSE 0 END) as Mes2,
													SUM(CASE WHEN MesSem = 3 THEN 1 ELSE 0 END) as Mes3,
													SUM(CASE WHEN MesSem = 4 THEN 1 ELSE 0 END) as Mes4,
													SUM(CASE WHEN MesSem = 5 THEN 1 ELSE 0 END) as Mes5,
													SUM(CASE WHEN MesSem = 6 THEN 1 ELSE 0 END) as Mes6,
													SUM(CASE WHEN MesSem = 7 THEN 1 ELSE 0 END) as Mes7,
													SUM(CASE WHEN MesSem = 8 THEN 1 ELSE 0 END) as Mes8,
													SUM(CASE WHEN MesSem = 9 THEN 1 ELSE 0 END) as Mes9,
													SUM(CASE WHEN MesSem = 10 THEN 1 ELSE 0 END) as Mes10,
													SUM(CASE WHEN MesSem = 11 THEN 1 ELSE 0 END) as Mes11,
													SUM(CASE WHEN MesSem = 12 THEN 1 ELSE 0 END) as Mes12
											FROM	$table
											WHERE	Ano >= 2018
											AND		idPeso = 1
											GROUP	by Ano
											ORDER	by Ano")->fetchAll('assoc');
											
		//if($config['countries'][$c_sel]['panels'][$p_sel]['mirror'] == 1) {
		//	$connection->execute("EXEC dbo.VSP_UPDATE_MIRROR '2021'");
		//}											
		
		$email = new Email('default');
		$email->from(['roclatam@kantar-services.com' => 'ROCLatAm Production'])
		    ->to('aldo.rubio@kantar.com')
		    ->addTo('jose.matos@kantar.com')
		    ->addTo('matias.calma@kantar.com')
		    ->addTo('anapaula.lujan@kantar.com')
		    ->addTo('rut.castillo@kantar.com')
		    ->addTo('ligia.duran@kantar.com')
		    ->addTo('luis.vasquez@kantar.com')
		    ->addTo('jorge.urdaneta@kantar.com')
		    ->addTo('juan.vizcaya@kantar.com')
		    ->addTo('esteban.castillo@kantar.com')
		    ->addTo('sergio.picon@kantar.com')
		    ->addTo('verihuska.bogadi@kantar.com')
		    ->addTo('alejandro.vielma@kantar.com')
		    ->addTo('catalina.hernandez@kantar.com')
		    ->addTo('juan.vizcaya@kantar.com')
		    ->addTo('jorge.urdaneta@kantar.com')
		    ->addTo('alejandro.teran@kantar.com')
		    ->addTo('juancarlos.olivares@kantar.com')
		    ->addTo('luan.correa@kantar.com')
		    ->addTo('roclatam.dq@kantar.com')
		    ->addTo('roclatam.dp@kantar.com')
		    ->subject(sprintf('[%s - %s - %s - Web] - Término de carga de pesos', $config['countries'][$c_sel]['code'], $config['countries'][$c_sel]['panels'][$p_sel]['name'], str_pad($month, 2, 0, STR_PAD_LEFT)."/".$year))
		    ->template('load_weighing', 'fancy')
		    ->setViewVars(['country' => $config['countries'][$c_sel]['name'], 'panel' => $config['countries'][$c_sel]['panels'][$p_sel]['name'], 'period' => str_pad($month, 2, 0, STR_PAD_LEFT)."/".$year, 'table' => $config['countries'][$c_sel]['panels'][$p_sel]['weights']['table'], 'db' => $config['countries'][$c_sel]['panels'][$p_sel]['weights']['db'], 'weights' => $weights, 'samples' => $samples])
		    ->emailFormat('both')
		    ->send();
		
		return true;	
		
    }
}
