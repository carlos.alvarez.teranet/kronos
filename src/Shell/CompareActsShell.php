<?php
namespace App\Shell;

use Cake\Console\Shell;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;
use Cake\Datasource\ConnectionManager;


class CompareActsShell extends Shell
{

    public function initialize()
    {
        parent::initialize();
        $this->databases = [
	        'MAKE55MC' => ['1022', '1122']
        ];
        $this->periods = [2023, 2022, 2021, 2020, 2019];
    }

	public function main($param_db = null, $param_period_a = null, $param_period_b = null, $country = null) {
		
		$flag_shell = 0;
		if(!is_null($param_db) and !is_null($param_period_a) and !is_null($param_period_b)) {
			$this->databases = [
				$param_db => [$param_period_a, $param_period_b]
			];
			$flag_shell = 1;
		}
	
		/*
		if($param_period_a <= 0323) {
			$layout_A = file_get_contents(TMP . "acts" . DS . "Layout_PNC_".$country."_old.json");
		} else {
			$layout_A = file_get_contents(TMP . "acts" . DS . "Layout_PNC_".$country.".json");
		}
		*/
		$layout_A = file_get_contents(TMP . "acts" . DS . "Layout_PNC_".$country.".json");
		$layout_A = json_decode($layout_A);
		
		$layout_B = file_get_contents(TMP . "acts" . DS . "Layout_PNC_".$country.".json");
		$layout_B = json_decode($layout_B);
		
		foreach($this->databases as $database => $periods) {
			$this->info("Procesando: ".$database);
			
			$results = fopen(TMP . "acts" . DS . $database."_".$periods[0]."_vs_".$periods[1]."_".uniqid().".csv", 'w');
			
			$mainFolder = "/mnt/procesos/ZIbanz/Atos";
			//$mainFolder = TMP . "acts";
			
			foreach($this->periods as $year) {
				
				$fileA = fopen($mainFolder . DS . "ato".$database."cs.".$periods[0], 'r');
				$fileB = fopen($mainFolder . DS . "ato".$database."cs.".$periods[1], 'r');
			
				$arrayA = [];
				$arrayB = [];
				
				$this->info($year." -- Preparando primer archivo para comparación: "."ato".$database."cs.".$periods[0]);
				$bigArrayA = [];
				while(!feof($fileA)) {
					$lineA = trim(fgets($fileA));
					
					if(!isset($flagA)) {
						$this->info("--- Largo de DA2: " . strlen($lineA));
						$flagA = true;
					}
					
					$subArrayA = [];
					if(substr($lineA, 0, 4) * 1 != $year) {
						continue;
					}
					foreach($layout_A as $element) {
						$subArrayA[$element->nombre] = substr($lineA, $element->inicio, $element->largo);								
					}
					$keyA = $subArrayA['IDATO'];
					if(strpos($database, 'US') > 0) {
						$keyA .= $subArrayA['idIndividuo'];
					}
					$bigArrayA[$keyA] = $subArrayA;
				}
				fclose($fileA);
				
				$this->info($year." -- Preparando segundo archivo para comparación: "."ato".$database."cs.".$periods[1]);
				$bigArrayB = [];
				while(!feof($fileB)) {
					$lineB = trim(fgets($fileB));
					
					if(!isset($flagB)) {
						$this->info("--- Largo de DA2: " . strlen($lineB));
						$flagB = true;
					}
					
					$subArrayB = [];
					if(substr($lineB, 0, 4) * 1 != $year) {
						continue;
					}
					foreach($layout_B as $element) {
						$subArrayB[$element->nombre] = substr($lineB, $element->inicio, $element->largo);
					}
					$keyB = $subArrayB['IDATO'];
					if(strpos($database, 'US') > 0) {
						$keyB .= $subArrayB['idIndividuo'];
					}
					$bigArrayB[$keyB] = $subArrayB;
				}
				fclose($fileB);
				
				// Comparativo de A v/s B
				$lineResults = [];
				if($year == $this->periods[0]) {
					$lineResults[] = ['IDATO', 'year', 'month', 'database', 'tipo', 'campo', 'valor_anterior', 'valor_nuevo'];
				}
				foreach($bigArrayA as $littleKeyA => $littleArrayA) {
					if(array_key_exists($littleKeyA, $bigArrayB)) {
						$diffA = array_diff_assoc($littleArrayA, $bigArrayB[$littleKeyA]);
						if($diffA) {
							$diffAOriginal = [];
							foreach($diffA as $k => $v) {
								$diffB[$k] = $bigArrayB[$littleKeyA][$k];
								$lineResults[] = [
									'IDATO' => $littleKeyA,
									'year' => $littleArrayA['ANO_PROCESSAMENTO'],
									'month' => $littleArrayA['MES_PROCESSAMENTO'],
									'database' => $database,
									'tipo' => $periods[0],
									'campo' => $k,
									'valor_anterior' => $diffA[$k],
									'valor_nuevo' => $diffB[$k]
								];
							}						
						} 
					} else {
						$lineResults[] = [
							'IDATO' => $littleKeyA,
							'year' => $littleArrayA['ANO_PROCESSAMENTO'],
							'month' => $littleArrayA['MES_PROCESSAMENTO'],
							'database' => $database,
							'tipo' => 'Acto eliminado',
							'campo' => null,
							'valor_anterior' => null,
							'valor_nuevo' => null
						];
					}
					
				}
				
				foreach($bigArrayB as $littleKeyB => $littleArrayB) {
					if(!array_key_exists($littleKeyB, $bigArrayA)) {
						$lineResults[] = [
							'IDATO' => $littleKeyB,
							'year' => $littleArrayB['ANO_PROCESSAMENTO'],
							'month' => $littleArrayB['MES_PROCESSAMENTO'],
							'database' => $database,
							'tipo' => 'Acto nuevo',
							'campo' => null,
							'valor_anterior' => null,
							'valor_nuevo' => null
						];
					}
				}
				
				$this->info("Escribiendo resultados en CSV para: ".$database);
				foreach($lineResults as $line) {
					//if($line['campo'] <> 'CDC08') {
					//	continue;
					//}
					$tmpLine = implode(";", $line);
					fwrite($results, $tmpLine . PHP_EOL);
				}
				
			}
			
			/*
			$this->info("Traspasando archivo a la red.");
			if($flag_shell == 1) {
				try {
					copy(TMP . "acts" . DS . $database."_".$periods[0]."_vs_".$periods[1].".csv", $mainFolder . DS . 'Comparatives' . DS . $database."_".$periods[0]."_vs_".$periods[1].".csv");
				} catch(\Exception $e) {
					$this->info($e->getMessage());
				}
				if(copy(TMP . "acts" . DS . $database."_".$periods[0]."_vs_".$periods[1].".csv", $mainFolder . DS . 'Comparatives' . DS . $database."_".$periods[0]."_vs_".$periods[1].".csv")) {
					$this->info("Copiado exitósamente");
					unlink(TMP . "acts" . DS . $database."_".$periods[0]."_vs_".$periods[1].".csv");
					$this->info("Borrado exitósamente");
				}	
			}
			*/
			
			
		}
		
		
	}	   
}