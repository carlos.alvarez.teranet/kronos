<?php
namespace App\Shell;

use Cake\Console\Shell;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;
use Cake\Datasource\ConnectionManager;


class RecoverShell extends Shell
{

    public function initialize()
    {
        parent::initialize();
        $this->loadModel('Recoveries');
        
        $connection = ConnectionManager::get('CAM_KWP');
		$this->Recoveries->setConnection($connection);
    }

    public function main()
    {
        $items = [
	        ['folder' => '127', 'structure' => [
		        'anio_proc' => ['init' => 1, 'length' => 4],
				'mes_proc' => ['init' => 5, 'length' => 2],
				'CD' => ['init' => 7, 'length' => 3],
				'Domicilio' => ['init' => 10, 'length' => 10],
				'SEQ' => ['init' => 20, 'length' => 2],
				'INDIVIDUO' => ['init' => 22, 'length' => 2],
				'ACCESOCANAL' => ['init' => 24, 'length' => 1],
				'FLGESTIMA' => ['init' => 25, 'length' => 1],
				'SEXO' => ['init' => 26, 'length' => 1],
				'IDADE' => ['init' => 27, 'length' => 3],
				'Pack_Comprado' => ['init' => 30, 'length' => 3],
				'Unidades_pack' => ['init' => 33, 'length' => 3],
				'tiempo_bebida' => ['init' => 36, 'length' => 2],
				'PROD' => ['init' => 38, 'length' => 4],
				'SPROD' => ['init' => 42, 'length' => 3],
				'FABR' => ['init' => 45, 'length' => 4],
				'MARCA' => ['init' => 49, 'length' => 6],
				'CONT' => ['init' => 55, 'length' => 4],
				'PROM' => ['init' => 59, 'length' => 4],
				'QTDADE' => ['init' => 63, 'length' => 5],
				'PUNIT_LC' => ['init' => 68, 'length' => 9],
				'PUNIT_DOLAR' => ['init' => 77, 'length' => 9],
				'S_GRANEL' => ['init' => 86, 'length' => 1],
				'CANAL' => ['init' => 87, 'length' => 4],
				'COEF1' => ['init' => 91, 'length' => 9],
				'COEF1_PM' => ['init' => 100, 'length' => 9],
				'COEF2' => ['init' => 109, 'length' => 9],
				'COEF2_PM' => ['init' => 118, 'length' => 9],
				'COEF3' => ['init' => 127, 'length' => 9],
				'COEF3_PM' => ['init' => 136, 'length' => 9],
				'DIA_DA_SEMANA' => ['init' => 145, 'length' => 1],
				'CLAS1' => ['init' => 146, 'length' => 4],
				'CLAS2' => ['init' => 150, 'length' => 4],
				'CLAS3' => ['init' => 154, 'length' => 4],
				'CLAS4' => ['init' => 158, 'length' => 4],
				'CLAS5' => ['init' => 162, 'length' => 4],
				'CLAS6' => ['init' => 166, 'length' => 4],
				'CLAS7' => ['init' => 170, 'length' => 4],
				'CLAS8' => ['init' => 174, 'length' => 4],
				'CLAS9' => ['init' => 178, 'length' => 4],
				'IDATO' => ['init' => 182, 'length' => 12],
				'IDARTIGO' => ['init' => 194, 'length' => 8],
				'EMBALAGEM' => ['init' => 202, 'length' => 2],
				'USO' => ['init' => 204, 'length' => 2],
				'Tipo_compra' => ['init' => 206, 'length' => 2],
				'forma_pago' => ['init' => 208, 'length' => 2],
				'anio_compra' => ['init' => 210, 'length' => 4],
				'mes_compra' => ['init' => 214, 'length' => 2],
				'dia_compra' => ['init' => 216, 'length' => 2],
				'VALUE_PM' => ['init' => 218, 'length' => 9],
				'SHOPPING' => ['init' => 227, 'length' => 1],
				'FACTOR_RW' => ['init' => 228, 'length' => 6]
	        ]]
        ];
        
        $base_dir = '/mnt/da2/XSM/OUTPUT/CAM';
        
        foreach($items as $item) {
	        
	        $this->out('Leyendo directorio: '.$item['folder']);
	        
	        $dir 	= new Folder($base_dir . DS . $item['folder']);
	        $files 	= $dir->find('.*\.1217');
	        
	        foreach($files as $file) {
		        
		        $this->out('Leyendo archivo: '.$file);
		        $element = fopen($base_dir . DS . $item['folder'] . DS . $file, "r");
		        
		        while(!feof($element)) {
			        $line = fgets($element);
			        
			        $entity = [];
			        foreach($item['structure'] as $column => $structure) {
				        //$this->out($column . ":" . substr($line, $structure['init'] - 1, $structure['length']));
				        $entity[$column] = substr($line, $structure['init'] - 1, $structure['length']) * 1;
			        }
			        
			        $recover = $this->Recoveries->newEntity($entity);
			        $save = $this->Recoveries->save($recover);
			        
			        $this->out(implode(" - ", $entity));
			    }
			    fclose($element);
		        
	        }
	        
        }
    }
}
?>