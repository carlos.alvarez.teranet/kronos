<?php
namespace App\Shell;

use Cake\Console\Shell;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;
use Cake\Datasource\ConnectionManager;


class FusionShell extends Shell
{

    public function initialize()
    {
        parent::initialize();
        
        $this->connection = ConnectionManager::get('IT_WORKFLOW');
        
        $this->loadModel('Clusters');
        $this->loadModel('Executions');
        $this->loadModel('Countries');
        $this->loadModel('FusionLogs');
    }
    
    public function Da1ToCsv() {
	    
	    error_reporting(0);
	    
	    $base = '/home/production/public_html/tmp/da1';
	    
	    $dir = new Folder($base);
	    $elements = $dir->read();
	    
	    foreach($elements[0] as $folder) {
		    
		    $folder = new Folder($base . DS . $folder);
		    $files	= $folder->read(true);
		    
		    if(count($files[1]) > 0) {
			    
			    $csv = $folder->find('.*\.csv');
			    $da1 = $folder->find('.*\.da1');
			    
			    if(count($csv) == 1 and count($da1) == 1) {
				    
				    $header		= 0;
				    $first		= [];
				    $file_csv	= file($folder->pwd() . DS . $csv[0]);
				    $file_da1	= file($folder->pwd() . DS . $da1[0]);
				    $file_end	= fopen($folder->pwd() . DS . 'Result.res', "w");
				    
				    foreach($file_da1 as $rownum => $data) {
					
						$write = [];
						foreach($file_csv as $layout) {
							
							if($layout != '') {
								$split = explode(";", $layout);
								if($header == 0) {
									$first[] = (string)$split[2];
								}
								$write[] = (string)substr($data, ($split[3] - 1), $split[4]);
							}
							
						}
						
						if($header == 0) {
							fwrite($file_end, implode(",", $first) . PHP_EOL);
							$header = 1;
						}
						fwrite($file_end, implode(",", $write) . PHP_EOL);
						$this->info("Procesando linea: " . $rownum . " de " . count($file_da1));
					
					} 
					
					$this->info('Archivo procesado: ' . $folder->pwd() . DS . 'Result.res' . ' exitósamente.');
				    
			    } else {
				    
				    $this->warn('Directorio: ' . $folder->pwd() . ' no posee registros CSV y DA1 o posee mas de un archivo con dichas extensiones.');
				    
			    }
			    
			    fclose($file_csv);
				fclose($file_da1);
				fclose($file_end);
			    
		    } else {
			    
			    $this->warn('Directorio: ' . $folder->pwd() . ' no posee archivos.');
			    
		    }
		    
	    }
	    
    }
    
    public function rebuildDa1ooh($country_id = null, $month = null, $year = null) {
	    
	    $file 	= fopen("/home/production/public_html/tmp/da1/DA1_MX_OOH_1803/Pesos1803_Mexico_OOH.da1", "r");
	    $layout = fopen("/home/production/public_html/tmp/da1/DA1_MX_OOH_1803/Pesos1803_Mexico_OOH.layout", "r");
	    $fp 	= fopen('/home/production/public_html/tmp/da1/DA1_MX_OOH_1803/Pesos1803_Mexico_OOH.da1_new', 'w');
	    
	    // Lectura de Layout
	    /*
	    $positions = [];
	    while (!feof($layout)) {
		   $data = fgets($file);
		   $positions[$data[0]] = $data[4];
		}
		*/
		
		// Lectura de Archivo DA2
		$line = 0;
		while (!feof($file)) {
			
			$data = fgets($file);
			$ind = substr($data, 2, 9);
			
			//$this->info('Leyendo linea: '. $line+1 .' Domicilio : '. $dom .' Individuo: '. $ind);
		   
			$connection = ConnectionManager::get('MX_KWP');
		   
			$input = '';
				
		   	$search = $connection->execute("SELECT Peso FROM dbo.Fusion_Web_Pesos_Imputado_OOH_US WHERE RIGHT('000000000' + Ind_OOH, 9) = :ind AND Ano = :ano AND Mes = :mes", [$ind, $period['Ano'], $period['Mes']])->fetch('assoc');
		    $this->info("Individuo encontrado: " . $ind);
		   
		   	if($search) {
				$peso = $search['Peso'];
				$this->out('Nuevo peso encontrado ('.$peso.') imputado para '.$period['Mes'].' '.$period['Ano']);
			} else {
				$peso = 0;
				$this->warn('No se ha encontrado peso para '.$period['Mes'].' '.$period['Ano']);
			}
			
			$input += str_pad($peso, 6, 0, STR_PAD_LEFT);
				
			fwrite($fp, $data . $input . PHP_EOL);
		   
		}
		
		fclose($file);
		fclose($fp);
	    
    }
    
    public function rebuildDa1($country_id = null, $month = null, $year = null) {
	    
	    $file 	= fopen("/home/production/public_html/tmp/da1/br/Pesos1806_Brasil_U&S.da1", "r");
	    $layout = fopen("/home/production/public_html/tmp/da1/br/Pesos1806_Brasil_U&S.layout", "r");
	    $fp 	= fopen('/home/production/public_html/tmp/da1/br/Pesos1806_Brasil_U&S.da1_new', 'w');
	    
	    $periods = [
		    ['Ano' => 2017, 'Mes' => 6],
		    ['Ano' => 2017, 'Mes' => 7],
		    ['Ano' => 2017, 'Mes' => 8],
		    ['Ano' => 2017, 'Mes' => 9],
		    ['Ano' => 2017, 'Mes' => 10],
		    ['Ano' => 2017, 'Mes' => 11],
		    ['Ano' => 2017, 'Mes' => 12],
		    ['Ano' => 2018, 'Mes' => 1],
		    ['Ano' => 2018, 'Mes' => 2],
		    ['Ano' => 2018, 'Mes' => 3],
		    ['Ano' => 2018, 'Mes' => 4],
		    ['Ano' => 2018, 'Mes' => 5],
		    ['Ano' => 2018, 'Mes' => 6]		    	
	    ];
	    
	    // Lectura de Layout
	    /*
	    $positions = [];
	    while (!feof($layout)) {
		   $data = fgets($file);
		   $positions[$data[0]] = $data[4];
		}
		*/
		
		// Lectura de Archivo DA1
		$line = 0;
		while (!feof($file)) {
			
			//$connection = ConnectionManager::get('MX_KWP');
			$connection = ConnectionManager::get('BR_KWP');
			
			$data = fgets($file);
			// Mexico ///////////////////
			//$dom = substr($data, 2, 6);
			//$ind = substr($data, 8, 2);
			
			// Brasil ///////////////////
			$dom = substr($data, 2, 7);
			$ind = substr($data, 9, 3);
			
			
			$this->info('Linea: ' . ($line+1) .' Domicilio : '. $dom .' Individuo: '. $ind);
		   
			$input = '';
			foreach($periods as $period) {
				
			   	$search = $connection->execute("SELECT New_Peso FROM dbo.Fusion_Web_Pesos_Imputado_OOH_US WHERE Dom_US = :dom AND Ind_US = :ind AND Ano = :ano AND Mes = :mes", [$dom*1, $ind*1, $period['Ano'], $period['Mes']])->fetch('assoc');
			   
			   	if($search) {
					$peso = $search['New_Peso'];
					$this->out('Nuevo peso encontrado ('.$peso.') imputado para '.$period['Mes'].' '.$period['Ano']);
				} else {
					$peso = 0;
					$this->warn('No se ha encontrado peso para '.$period['Mes'].' '.$period['Ano']);
				}
				
				$input += str_pad(trim($peso), 6, 0, STR_PAD_LEFT);
			
			}
		   
			$line++;
			fwrite($fp, $data . $input);
		   
		}
		
		fclose($file);
		fclose($fp);
	    
    }

    public function main($dbcode = null, $data = null, $country_id = null, $section = null, $type = null)
    {
	    
	    $connection = ConnectionManager::get($dbcode);
	    
	    $this->inactivateLog(['execution_id' => $data, 'country_id' => $country_id, 'section' => $section, 'type' => $type]);
	    $this->insertLog(['execution_id' => $data, 'country_id' => $country_id, 'section' => $section, 'type' => $type], ['Descricao' => 'Inicio de ejecución']);
	    
	    $clusters = $this->createTable($connection, $data);
	    $this->insertLog(['execution_id' => $data, 'country_id' => $country_id, 'section' => $section, 'type' => $type], ['Descricao' => 'Clusters a procesar: '.json_encode($clusters, true)]);
	    
	    $ciclos = $this->getCiclos($connection, $data, $clusters);
	    
	    $this->insertLog(['execution_id' => $data, 'country_id' => $country_id, 'section' => $section, 'type' => $type], ['Descricao' => 'Ciclos a recorrer: '.count($ciclos)]);
	    if(empty($ciclos)) {
	    $this->insertLog(['execution_id' => $data, 'country_id' => $country_id, 'section' => $section, 'type' => $type], ['Descricao' => '[ERROR] No existen ciclos a recorrer']);
		    $this->abort('[ERROR] No existen ciclos a recorrer');
	    }
	    
	    foreach($ciclos as $ciclo) {
		    
		    $relations 	= $this->getRelationsAvailable($ciclo, $connection, $data, $clusters);
		    $oohs		= $this->getOOHs($ciclo, $connection, $data, $clusters);
		    
		    // Insertar registros utilizando relaciones anteriores
		    foreach($relations as $relation) {
			    
			    $oohs 	= $this->insertPreviousRelation($ciclo, $relation, $oohs, $connection, $data, $clusters);
			    
		    }
		    
		    // Insertar relaciones nuevas
		    foreach($oohs as $key => $ooh) {
			    
			    $result = $this->insertNewRelations($ciclo, $ooh, $connection, $data, $clusters);
			    
		    }
		    
	    }
	    
	    $this->insertLog(['execution_id' => $data, 'country_id' => $country_id, 'section' => $section, 'type' => $type], ['Descricao' => 'Calculo de pesos']);
	    
	    $calculo = $this->newPeso($connection, $data);
	    
	    $this->insertLog(['execution_id' => $data, 'country_id' => $country_id, 'section' => $section, 'type' => $type], ['Descricao' => 'Fin de ejecución']);
	    
	    	    
	}
	
	private function insertNewRelations($ciclo = null, $ooh = null, $connection = null, $data = null, $clusters = null) {
		
		$this->out(sprintf('Procesando individuo: %s (veces: %s, conteo: %s)', $ooh['Domicilio']."-".$ooh['Individuo'], $ooh['veces'], $ooh['conteo']));
		
		if($ooh['conteo'] == $ooh['veces']) {
			return true;
		}
		
		$where = [
			'hin.Ano = '.$ciclo['Ano'],
			'hin.Mes = '.$ciclo['Mes']
		];
		
		foreach($clusters as $cluster) {
			$where[] = "hin.".$cluster." = ".$ciclo[$cluster];
		}
		
		$values = [];
		foreach($clusters as $cluster) {
			$values[] = $ciclo[$cluster];
		}
		
		do {
			
			$candidate = $connection->execute('SELECT	hin.*
												FROM	dbo.Fusion_Web_Homologacion_US_'.$data.' hin
												LEFT	JOIN dbo.Fusion_Web_Pesos_Imputado_OOH_US ios
														ON ios.Dom_US = hin.Domicilio
														AND	ios.Ind_US = hin.Individuo
														AND ios.Ano = :Ano
														AND ios.Mes = :Mes
												WHERE	'.implode(" AND ", $where).'
												AND		ios.Dom_US IS NULL
												AND		ios.Ind_US IS NULL',
												[
													'Ano' => $ciclo['Ano'],
													'Mes' => $ciclo['Mes']
												])->fetch('assoc');
							
			if(!$candidate) {					
				debug('SELECT	hin.*
												FROM	dbo.Fusion_Web_Homologacion_US_'.$data.' hin
												LEFT	JOIN dbo.Fusion_Web_Pesos_Imputado_OOH_US ios
														ON ios.Dom_US = hin.Domicilio
														AND	ios.Ind_US = hin.Individuo
														AND ios.Ano = :Ano
														AND ios.Mes = :Mes
												WHERE	'.implode(" AND ", $where).'
												AND		ios.Dom_US IS NULL
												AND		ios.Ind_US IS NULL');
				debug($ciclo);
				debug($where);
				$this->abort('SELECT	hin.*
												FROM	dbo.Fusion_Web_Homologacion_US_'.$data.' hin
												LEFT	JOIN dbo.Fusion_Web_Pesos_Imputado_OOH_US ios
														ON ios.Dom_US = hin.Domicilio
														AND	ios.Ind_US = hin.Individuo
														AND ios.Ano = :Ano
														AND ios.Mes = :Mes
												WHERE	'.implode(" AND ", $where).'
												AND		ios.Dom_US IS NULL
												AND		ios.Ind_US IS NULL');
			}
							
			$this->info(sprintf('Asociando individuo %s [%s de %s] (Dom: %s - Ind: %s)', $ooh['Domicilio']."-".$ooh['Individuo'], $ooh['conteo'], $ooh['veces'], $candidate['Domicilio'], $candidate['Individuo']));					
			$insert = $connection->execute('INSERT	INTO dbo.Fusion_Web_Pesos_Imputado_OOH_US
													(Ano, Mes, Dom_OOH, Ind_OOH, '.implode(", ", $clusters).', Peso, Times, New_Peso, Dom_US, Ind_US)
											VALUES	(:Ano, :Mes, :Dom_OOH, :Ind_OOH, '.implode(", ", $values).', :Peso, :Times, :New_Peso, :Dom_US, :Ind_US)',
											[
												'Ano' => $ciclo['Ano'],
												'Mes' => $ciclo['Mes'],
												'Dom_OOH' => $ooh['Domicilio'],
												'Ind_OOH' => $ooh['Individuo'],
												'Peso' => $ooh['Peso'],
												'Times' => $ooh['veces'],
												'New_Peso' => 0,
												'Dom_US' => $candidate['Domicilio'],
												'Ind_US' => $candidate['Individuo']
											]);
			
			
											
			$ooh['conteo'] = $ooh['conteo'] + 1;
			
		} while ($ooh['conteo'] < $ooh['veces']);
		
		return true;
		
	}
	
	private function insertPreviousRelation($ciclo = null, $relation = null, $oohs = null, $connection = null, $data = null, $clusters = null) {
				
		// Si ya se completo la cuota
		$key = array_search($relation['Ind_OOH'], array_column($oohs, 'Ind_OOH'));
		if($oohs[$key]['conteo'] == $oohs[$key]['veces']) {
			return $oohs;
		}
		
		$where = [
			'Ano = '.$ciclo['Ano'],
			'Mes = '.$ciclo['Mes']
		];
		
		foreach($clusters as $cluster) {
			$where[] = $cluster." = ".$ciclo[$cluster];
		}
		
		$values = [];
		foreach($clusters as $cluster) {
			$values[] = $ciclo[$cluster];
		}
		
		// Si ya ha sido asignado previamente
		$exists	= $connection->execute('SELECT	*
										FROM	dbo.Fusion_Web_Pesos_Imputado_OOH_US
										WHERE	'.implode(" AND ", $where).'
										AND		Dom_US = :dom
										AND		Ind_US = :ind',
										[
											'dom' => $relation['Dom_US'],
											'ind' => $relation['Ind_US']
										])->fetchAll('assoc');
										
		if(count($exists) > 0) {
			return $oohs;
		}
		
		$insert = $connection->execute('INSERT	INTO dbo.Fusion_Web_Pesos_Imputado_OOH_US
												(Ano, Mes, Dom_OOH, Ind_OOH, '.implode(", ", $clusters).', Peso, Times, New_Peso, Dom_US, Ind_US)
										VALUES	(:Ano, :Mes, :Dom_OOH, :Ind_OOH, '.implode(", ", $values).', :Peso, :Times, :New_Peso, :Dom_US, :Ind_US)',
										[
											'Ano' => $ciclo['Ano'],
											'Mes' => $ciclo['Mes'],
											'Dom_OOH' => $relation['Dom_OOH'],
											'Ind_OOH' => $relation['Ind_OOH'],
											'Peso' => $oohs[$key]['Peso'],
											'Times' => $oohs[$key]['veces'],
											'New_Peso' => 0,
											'Dom_US' => $relation['Dom_US'],
											'Ind_US' => $relation['Ind_US']
										]);
										
		if($insert) {
			$oohs[$key]['conteo']++;
		}
		
		return $oohs;
		
	}
	
	private function getRelationsAvailable($ciclo = null, $connection = null, $data = null, $clusters = null) {
		
		$ooh = [];
		$us  = [];
		$where = [
			'mix.Ano = '.$ciclo['Ano'],
			'mix.Mes = '.$ciclo['Mes']
		];
		
		foreach($clusters as $cluster) {
			$ooh[] = "hoo.".$cluster." = mix.".$cluster;
			$us[] = "hus.".$cluster." = mix.".$cluster;
			$where[] = "mix.".$cluster." = ".$ciclo[$cluster];
		}
		
		$relations = $connection->execute('	SELECT
												mix.Dom_OOH,
												mix.Ind_OOH,
												mix.Dom_US,
												mix.Ind_US,
												MAX(mix.Mes) as Mes,
												MAX(mix.Ano) as Ano
											FROM
												dbo.Fusion_Web_Pesos_Imputado_OOH_US mix 
											JOIN
												dbo.Fusion_Web_Homologacion_US_'.$data.' hus
												ON hus.Domicilio = mix.Dom_US
												AND hus.Individuo = mix.Ind_US
												AND hus.Ano = mix.Ano
												AND hus.Mes = mix.Mes
												AND '.implode(" AND ", $us).'
											JOIN
												dbo.Fusion_Web_Homologacion_OOH_'.$data.' hoo
												ON hoo.Domicilio = mix.Dom_OOH
												AND	hoo.Individuo = mix.Ind_OOH
												AND hoo.Ano = mix.Ano
												AND hoo.Mes = mix.Mes
												AND '.implode(" AND ", $ooh).'
											WHERE
												'.implode(" AND ", $where).'
											GROUP
												by mix.Dom_US, mix.Ind_US, mix.Dom_OOH, mix.Ind_OOH
											ORDER
												by MAX(mix.Ano) ASC, MAX(mix.Mes) ASC');
											
		return $relations;
		
	}
	
	private function getCiclos($connection = null, $data = null, $clusters = null) {
		
		$ciclos = $connection->execute('SELECT	Ano, Mes, '.implode(", ", $clusters).', COUNT(*) as Cantidad
										FROM	dbo.Fusion_Web_Homologacion_US_'.$data.'
										GROUP	by Ano, Mes, '.implode(", ", $clusters).'
										ORDER	by Ano, Mes, '.implode(", ", $clusters))->fetchAll('assoc');
										
		return $ciclos;
		
	}
	
	private function getOOHs($ciclo = null, $connection = null, $data = null, $clusters = null) {
		
		$where = [
			'Ano = '.$ciclo['Ano'],
			'Mes = '.$ciclo['Mes']
		];
		
		foreach($clusters as $cluster) {
			$where[] = $cluster." = ".$ciclo[$cluster];
		}
		
		$oohs = $connection->execute('SELECT 	*, 0 as veces, 0 as conteo
									  FROM 		dbo.Fusion_Web_Homologacion_OOH_'.$data.'
									  WHERE 	'.implode(" AND ", $where).'
									  ORDER 	by Domicilio, Individuo')->fetchAll('assoc');
									  
		
									  
		$total_oohs	= count($oohs);
		$division	= ($total_oohs) ? intval($ciclo['Cantidad'] / $total_oohs) : 0;
		$especials	= $ciclo['Cantidad'] - ($division * $total_oohs); 
		
		foreach($oohs as $key => $ooh) {
			
			if($key < $especials) {
				$veces = $division + 1;
			} else {
				$veces = $division;
			}
			
			$oohs[$key]['veces'] = $veces;
			
		}
									  
		return $oohs;
		
	}
	
	private function newPeso($connection = null, $data = null) {
		
		$this->Executions->setConnection(ConnectionManager::get('IT_WORKFLOW'));
		
		// Obtener informacion de la ejecucion
		$execution = $this->Executions->get($data)->toArray();
		
		$oohs = $connection->execute("SELECT DISTINCT Ano, Mes, Dom_OOH, Ind_OOH, Peso, Times FROM dbo.Fusion_Web_Pesos_Imputado_OOH_US WHERE Ano = :Ano AND Mes = :Mes", ['Ano' => $execution['Ano'], 'Mes' => $execution['Mes']])->fetchAll('assoc');
		
		foreach($oohs as $ooh) {
			$peso = $ooh['Peso'];
			$times = $ooh['Times'];
			
			$new = intval($peso / $times);
			$rest = $peso - ($new * $times);
			
			$exe1 = $connection->execute("UPDATE dbo.Fusion_Web_Pesos_Imputado_OOH_US SET New_Peso = :new WHERE Ano = :Ano AND Mes = :Mes AND Dom_OOH = :Dom_OOH AND Ind_OOH = :Ind_OOH", ['new' => $new, 'Ano' => $ooh['Ano'], 'Mes' => $ooh['Mes'], 'Dom_OOH' => $ooh['Dom_OOH'], 'Ind_OOH' => $ooh['Ind_OOH']]);
			
			$exe2 = $connection->execute("UPDATE TOP (".$rest.") dbo.Fusion_Web_Pesos_Imputado_OOH_US SET New_Peso = (New_Peso + 1) WHERE Ano = :Ano AND Mes = :Mes AND Dom_OOH = :Dom_OOH AND Ind_OOH = :Ind_OOH", ['Ano' => $ooh['Ano'], 'Mes' => $ooh['Mes'], 'Dom_OOH' => $ooh['Dom_OOH'], 'Ind_OOH' => $ooh['Ind_OOH']]);
			
		}
		
	}
	
	private function createTable($connection = null, $data = null) {
		
		$this->Clusters->setConnection(ConnectionManager::get('IT_WORKFLOW'));
		$this->Executions->setConnection(ConnectionManager::get('IT_WORKFLOW'));
		$this->Countries->setConnection(ConnectionManager::get('IT_WORKFLOW'));
		
		
		// Obtener informacion de la ejecucion
		$execution = $this->Executions->get($data, [
			'contain' => ['Countries', 'Countries.Clusters']
		])->toArray();
		
		// Incorporacion de Clusters
		$clusters = [];
		foreach($execution['country']['clusters'] as $cluster) {
			$clusters[] = $cluster['Descricao']." INT NOT NULL,";
		}
		
		$connection->execute("	IF OBJECT_ID('dbo.Fusion_Web_Pesos_Imputado_OOH_US', 'U') IS NULL
								BEGIN
									CREATE TABLE dbo.Fusion_Web_Pesos_Imputado_OOH_US (
										Id BIGINT NOT NULL IDENTITY(1,1),
										Ano INT NOT NULL,
										Mes INT NOT NULL,
										".implode(" ", $clusters)."
										Dom_OOH VARCHAR(60) NOT NULL,
										Ind_OOH VARCHAR(60) NOT NULL,
										Peso INT NOT NULL,
										Times INT NULL,
										Dom_US VARCHAR(60) NOT NULL,
										Ind_US VARCHAR(60) NOT NULL,
										New_Peso INT NULL
									);
									ALTER TABLE dbo.Fusion_Web_Pesos_Imputado_OOH_US ADD CONSTRAINT PK_Fusion_Web_Pesos_Imputado_OOH_US PRIMARY KEY (Id);
									CREATE NONCLUSTERED INDEX INDEX_Fusion_Web_Pesos_Imputado_OOH_US_1 ON Fusion_Web_Pesos_Imputado_OOH_US (Ano);
									CREATE NONCLUSTERED INDEX INDEX_Fusion_Web_Pesos_Imputado_OOH_US_2 ON Fusion_Web_Pesos_Imputado_OOH_US (Mes);
									CREATE NONCLUSTERED INDEX INDEX_Fusion_Web_Pesos_Imputado_OOH_US_3 ON Fusion_Web_Pesos_Imputado_OOH_US (Region_Fusion);
									CREATE NONCLUSTERED INDEX INDEX_Fusion_Web_Pesos_Imputado_OOH_US_4 ON Fusion_Web_Pesos_Imputado_OOH_US (Tramo_Fusion);
									CREATE NONCLUSTERED INDEX INDEX_Fusion_Web_Pesos_Imputado_OOH_US_5 ON Fusion_Web_Pesos_Imputado_OOH_US (NSE_Fusion);
									CREATE NONCLUSTERED INDEX INDEX_Fusion_Web_Pesos_Imputado_OOH_US_6 ON Fusion_Web_Pesos_Imputado_OOH_US (Sexo_Fusion);
									CREATE NONCLUSTERED INDEX INDEX_Fusion_Web_Pesos_Imputado_OOH_US_7 ON Fusion_Web_Pesos_Imputado_OOH_US (Dom_OOH);
									CREATE NONCLUSTERED INDEX INDEX_Fusion_Web_Pesos_Imputado_OOH_US_8 ON Fusion_Web_Pesos_Imputado_OOH_US (Ind_OOH);
								END");
		
		$clusters = [];		  
		foreach($execution['country']['clusters'] as $cluster) {
			$clusters[] = $cluster['Descricao'];
		}
		
		return $clusters;
		
	}
	
	private function insertLog($data = null, $fields = null) {
		
		$this->FusionLogs->setConnection($this->connection);
		
		$log = [
			'Country_id' => $data['country_id'],
			'Group_id' => $data['section'],
			'Type_id' => $data['type'],
			'Execution_id' => $data['execution_id'],
			'Descricao' => null
		];
		
		foreach($fields as $key => $field) {
			if(array_key_exists($key, $log)) {
				$log[$key] = $field;
			}
		}
		
		$init = $this->FusionLogs->newEntity($log);
		$save = $this->FusionLogs->save($init);
		
		return $save;
		
	}
	
	private function inactivateLog($data = null) {
		
		$this->FusionLogs->setConnection($this->connection);
		
		$update = $this->FusionLogs->updateAll(
	        ['FlgDesativacao' => 'AR'], // fields
	        ['Execution_id' => $data['execution_id'], 'Group_id' => $data['section'], 'Type_id' => $data['type']] // conditions
	    );
	    
	    return $update;
		
	}
}
?>