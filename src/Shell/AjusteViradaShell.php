<?php
namespace App\Shell;

use Cake\Console\Shell;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;
use Cake\Datasource\ConnectionManager;
use ZipArchive;

class AjusteViradaShell extends Shell
{
	public function initialize()
    {
        parent::initialize();
    }
    
    public function main()
    {
        $dir 			= new Folder('/mnt/procesos/ACTOS/MEXICO/2022_02');
        $files 			= $dir->find('.*\.022');
        
        $layout 		= file_get_contents(TMP . "acts" . DS . "Layout_PNC_52.json");
		$layout 		= json_decode($layout);
        
        $connection 	= ConnectionManager::get('MX_KWP');
        
        /*
        $changes		= $connection->execute("SELECT '55' + RIGHT('00000000' + CAST(idDomicilio as VARCHAR), 8) as idDomicilio, 
        											   '55' + RIGHT('00000000' + CAST(Donante as VARCHAR), 8) as Donante 
												FROM 	dbo.Ajustes_BR_20220323")->fetchAll('assoc');
		*/
		
		$changes		= $connection->execute("SELECT idDomicilio, 
        											   Donante 
												FROM   MX_SPRI.dbo.Ajustes_MX_20220323")->fetchAll('assoc');
        
        $donantes = [];
        foreach($changes as $change) {
	        $donantes[$change['idDomicilio']] = $change['Donante'];
        }
        
        
        foreach ($files as $file) {
	        
	        if(strlen($file) <> 10 or $file <> "ATO256.022") {
		        continue;
	        }
	        
	        $this->info("Procesando Acto: " . $file);
	        
	        $archivo	= fopen($dir->pwd() . DS . $file, 'r');
	        $results	= fopen($dir->pwd() . DS . $file . '_new', 'w');
	        			
			while(!feof($archivo)) {
				$linea = fgets($archivo);
				
				if(substr($linea, 0, 6) <> "202202") {
					fwrite($results, trim($linea) . PHP_EOL);
				} else {
				
					$subArray = [];
					foreach($layout as $element) {
						$subArray[$element->nombre] = substr($linea, $element->inicio, $element->largo);
					}
					if(in_array($subArray['idDomicilio'], array_keys($donantes))) {
						$linea = substr($linea, 0, 9) . str_pad($donantes[$subArray['idDomicilio']*1], 10, "0", STR_PAD_LEFT) . substr($linea, 19);
						fwrite($results, trim($linea) . PHP_EOL);
					} else {
						fwrite($results, trim($linea) . PHP_EOL);
					}
				}
				
			}
			
			fclose($archivo);
			unlink($dir->pwd() . DS . $file);
			fclose($results);
			rename($dir->pwd() . DS . $file . '_new', $dir->pwd() . DS . $file);
						
		}
        
    }
}
