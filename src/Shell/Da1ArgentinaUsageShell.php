<?php
namespace App\Shell;

use Cake\Console\Shell;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;
use Cake\Datasource\ConnectionManager;


class Da1ArgentinaUsageShell extends Shell
{

    public function initialize()
    {
        parent::initialize();
    }

	public function main() {
		
		$file 		= fopen(TMP . "da1" . DS . "Pesos2303_Argentina_Usage.da1", 'w');
		
		$connection = ConnectionManager::get('AR_KWP');
		//$inds		= $connection->execute("SELECT RIGHT('00000000' + CAST(idDomicilio as VARCHAR), 8) as idDomicilio FROM dbo.RG_Domicilios_Pesos_Orion WHERE Ano >= 2017 AND idPeso = 1 GROUP by idDomicilio ORDER by CAST(idDomicilio as INTEGER) ASC")->fetchAll('assoc');
		
		$weights	= $connection->execute("SELECT 	RIGHT('000000' + CAST(rgd.idDomicilio as VARCHAR), 6) + RIGHT('0000' + CAST(rgd.idIndividuo as VARCHAR), 4) as idIndividuo,
													RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2023 AND rgd.MesSem = 3 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6)
													as [pesos]
											FROM 	dbo.RG_Individuos_Pesos_Usage rgd
											LEFT JOIN	dbo.RG_Individuos_Pesos_CSV_Usage rgc ON rgc.Iddomicilio = rgd.Iddomicilio AND rgc.idIndividuo = rgd.idIndividuo AND rgc.Ano = rgd.Ano AND rgc.MesSem = rgd.messem
											WHERE	rgd.idPeso = 1
											AND		rgd.Ano >= 2023
											GROUP	by RIGHT('000000' + CAST(rgd.idDomicilio as VARCHAR), 6) + RIGHT('0000' + CAST(rgd.idIndividuo as VARCHAR), 4)
											ORDER	by RIGHT('000000' + CAST(rgd.idDomicilio as VARCHAR), 6) + RIGHT('0000' + CAST(rgd.idIndividuo as VARCHAR), 4) ASC")->fetchAll('assoc');
													
		$connection = ConnectionManager::get('AR_KWP');
		$years		= [2023];
		$layout 	= $connection->execute("SELECT Nombre, Largo, PosInicial FROM dbo.UPX_Individuos_Variables ORDER by PosInicial ASC")->fetchAll('assoc');
		
		$n = 0;
		$this->verbose("Cantidad de Domicilios a procesar: " . count($weights));
		foreach($weights as $ind) {
			
			$n++;
			$this->verbose("Procesando individuo: " . $n);
			$main_row = [];
			
			$query	= $connection->execute("SELECT [NPAN],[Ano_Nac],[Sexo],[Idade],[EDAC],[NSE],[EP5],[H5],[rp6],[DonadeCasa],[ChefedeFamilia],[idAtividade],[idInstrucao],[idParentesco],[idEstadoCivil], [CV],[Ano] FROM dbo.UPX_Individuos WHERE NPAN = :NPAN AND Ano IN (2023) ORDER by ANO DESC", ['NPAN' => $ind['idIndividuo']]);
			$rows	= $query->fetchAll('assoc');
			
			foreach($years as $year) {
				
				$key 	= array_search($year, array_column($rows, 'Ano'));
				$line = '';				
				
				if($key !== false) {
			
					foreach($layout as $element) {
						//$this->abort(print_r($rows[$key], true));
						$line .= str_pad(trim($rows[$key][$element['Nombre']]), $element['Largo'], "0", STR_PAD_LEFT);
					}
					
				} else {
					
					foreach($layout as $element) {
						$line .= str_pad(trim($rows[0][$element['Nombre']]), $element['Largo'], "0", STR_PAD_LEFT);	
					}
					
				}
				
				$main_row[] = str_pad($line, 100, "0", STR_PAD_RIGHT);
				
			}
			
			//$clave = array_search($ind['idDomicilio'], array_column($weights, 'idDomicilio'));
			$pesosPeriodos = $ind['pesos'];
			
			
			$text = implode("", $main_row).$pesosPeriodos;
			//fwrite($file, str_pad($text, 788, "0", STR_PAD_RIGHT) . PHP_EOL);
			fwrite($file, str_pad($text, ( (1 * 100) + strlen($ind['pesos']) ), "0", STR_PAD_RIGHT) . PHP_EOL);
			
		}
		
	}
    
}