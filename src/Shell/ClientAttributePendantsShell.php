<?php
namespace App\Shell;

use Cake\Console\Shell;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\TableRegistry;


class ClientAttributePendantsShell extends Shell
{
	
	private $Attributes;
	private $AttributesCountries;
	private $Markets;
	private $Subs;
	private $Products;
	private $Dimensions;
	private $AttributesPendantsDetail;
	private $fields;
	
	public function initialize() 
	{
		$this->Attributes = TableRegistry::get('Attributes');
		$this->AttributesCountries = TableRegistry::get('AttributesCountries');
		$this->Markets = TableRegistry::get('Markets');
		$this->Subs = TableRegistry::get('Subs');
		$this->Products = TableRegistry::get('Products');
		$this->Dimensions = TableRegistry::get('Dimensions');
		$this->AttributesPendantsDetails = TableRegistry::get('AttributesPendantsDetails');
		$this->Clients = TableRegistry::get('Clients');
		
		$this->fields = [
			'IdProduto' => 'Produto',
			'IdSub' => 'Sub',
			'IdFabricante' => 'Fabricante',
			'IdMarca' => 'Marca',
			'IdConteudo' => 'Conteudo',
			'CdC01' => 'Clas01',
			'CdC02' => 'Clas02',
			'CdC03' => 'Clas03',
			'CdC04' => 'Clas04',
			'CdC05' => 'Clas05',
			'CdC06' => 'Clas06',
			'CdC07' => 'Clas07',
			'CdC08' => 'Clas08',
			'CdC09' => 'Clas09',
			'CdC10' => 'Clas10',
			'IdArtigo' => 'CodBar'
		];
	}
    
    public function main($country_id = null, $idClientAttribute = null)
	{
		
        // Listado de Paises
        $countries = $this->AttributesCountries->find('all');
        if($country_id) {
	        $countries->where(['Id' => $country_id]);
	        $this->info('Ejecutando Pais: '.$country_id);
        }
        
        foreach($countries as $country) {
	        
	        if(file_exists(LOGS . DS . "refresh" . DS . $country['Descricao'] . DS . $country_id . ".lock")) {
				continue;
			}	        
	        $log = fopen(LOGS . DS . "refresh" . DS . $country['Descricao'] . DS . date("Ymd") . ".log", "w");
	        $lock = fopen(LOGS . DS . "refresh" . DS . $country['Descricao'] . DS . $country_id . ".lock", "w");
	        
	        $this->info('Analizando Pais: '.$country['Descricao']);
	        fwrite($log, date("d-m-Y H:i:s") . ' - Analizando Pais: '.$country['Descricao'] . PHP_EOL);
	        
	        $connection = ConnectionManager::get($country['DbConfig']);
	        $this->Attributes->setConnection($connection);
	        $this->Markets->setConnection($connection);
	        $this->Subs->setConnection($connection);
	        $this->Products->setConnection($connection);
	        $this->Dimensions->setConnection($connection);
	        $this->Clients->setConnection($connection);
	        
	        $fields = $this->fields;
	        
	        $attributes = $this->Attributes->find('all', [
				'contain' => [
					'Markets.Subs.Products', 
					'Dimensions' => [
						'conditions' => [
							'Dimensions.Descricao IN ' => array_keys($fields)
						]
					],
					'Clients'
				],
				'conditions' => [
					'Attributes.UpdateCount' => 1
				]
			]);
			
			if($idClientAttribute) {
		        $attributes->where(['Attributes.IdClientAttribute' => $idClientAttribute]);
		        $this->info('Ejecutando idClientAttribute: '.$idClientAttribute);
		        fwrite($log, date("d-m-Y H:i:s") . ' - Ejecutando idClientAttribute: ('.$idClientAttribute.')' . PHP_EOL);
	        }
	        
	        // Eliminar registros para ingresar nuevos
			$this->AttributesPendantsDetails->deleteAll([
				'Country_id' => $country['Id'],
				'FlgDesativacao' => 'AT'
			]);
			
			foreach($attributes as $attribute) {
				
				$this->info('- Analizando Client Attribute: ('.$attribute->IdClientAttribute.') '.$attribute->Descricao);
				fwrite($log, date("d-m-Y H:i:s") . ' - Analizando Client Attribute: ('.$attribute->IdClientAttribute.') '.$attribute->Descricao . PHP_EOL);
				
				// Build Query
				$select = [];
				$function = [];
				foreach($attribute->dimensions as $dimension) {
					
					if($dimension['Descricao'] == 'IdMarca') {
						$select[] 	= 'IdFabricante';
						$select[] 	= 'Fabricante';
					}
					
					if($dimension['Descricao'] == 'IdArtigo') {
						$select[] 	= 'IdProduto';
						$select[] 	= 'Produto';
						$select[] 	= 'IdSub';
						$select[] 	= 'Sub';
						$select[] 	= 'IdFabricante';
						$select[] 	= 'Fabricante';
						$select[] 	= 'IdMarca';
						$select[] 	= 'Marca';
						$select[] 	= 'IdConteudo';
						$select[] 	= 'Conteudo';
						$select[] 	= 'CdC01';
						$select[] 	= 'Clas01';
						$select[] 	= 'CdC02';
						$select[] 	= 'Clas02';
						$select[] 	= 'CdC03';
						$select[] 	= 'Clas03';
						$select[] 	= 'CdC04';
						$select[] 	= 'Clas04';
						$select[] 	= 'CdC05';
						$select[] 	= 'Clas05';
						$select[] 	= 'CdC06';
						$select[] 	= 'Clas06';
						$select[] 	= 'CdC07';
						$select[] 	= 'Clas07';
						$select[] 	= 'CdC08';
						$select[] 	= 'Clas08';
						$select[] 	= 'CdC09';
						$select[] 	= 'Clas09';
					}
					
					$select[] 	= $dimension['Descricao'];
					if(!is_null($fields[$dimension['Descricao']])) {
						$select[] 	= $fields[$dimension['Descricao']];
					}					
					$function[]	= $dimension['Descricao'];	
				}
				
				$query = "SELECT ".implode(", ", $select).", dbo.fc_ClientAttribute(".$attribute->IdClientAttribute.", ".implode(", ", array_pad($function, 10, 0)).") as Value";
				
				$query .= " FROM dbo.VW_ArtigoCA (nolock) WHERE DtCriacao <= (SELECT CONVERT(VARCHAR(8),MAX(DATEVALUE),112) FROM dbo.SPREnd) ";
				
				$where = [];
				foreach($attribute->market->subs as $sub) {
					$where[] = "(IdProduto = ".$sub['IdProduto']." AND IdSub = ".$sub['IdSub'].")";	
				}
				$query .= "AND (" . implode(" OR ", $where) . ")";
				
				// Filtro adicional de Mercado (Market)
				if($attribute->market->Filtro) {
					$query .= " AND ( " . $attribute->market->Filtro . ") ";
				}
				
				$group = [];
				foreach($attribute->dimensions as $dimension) {
					
					if($dimension['Descricao'] == 'IdMarca') {
						$group[] 	= 'IdFabricante';
						$group[] 	= 'Fabricante';
					}
					
					if($dimension['Descricao'] == 'IdArtigo') {
						$group[] 	= 'IdProduto';
						$group[] 	= 'Produto';
						$group[] 	= 'IdSub';
						$group[] 	= 'Sub';
						$group[] 	= 'IdFabricante';
						$group[] 	= 'Fabricante';
						$group[] 	= 'IdMarca';
						$group[] 	= 'Marca';
						$group[] 	= 'IdConteudo';
						$group[] 	= 'Conteudo';
						$group[] 	= 'CdC01';
						$group[] 	= 'Clas01';
						$group[] 	= 'CdC02';
						$group[] 	= 'Clas02';
						$group[] 	= 'CdC03';
						$group[] 	= 'Clas03';
						$group[] 	= 'CdC04';
						$group[] 	= 'Clas04';
						$group[] 	= 'CdC05';
						$group[] 	= 'Clas05';
						$group[] 	= 'CdC06';
						$group[] 	= 'Clas06';
						$group[] 	= 'CdC07';
						$group[] 	= 'Clas07';
						$group[] 	= 'CdC08';
						$group[] 	= 'Clas08';
						$group[] 	= 'CdC09';
						$group[] 	= 'Clas09';
					}
					
					$group[] = $dimension['Descricao'];
					if(!is_null($fields[$dimension['Descricao']])) {
						$group[] = $fields[$dimension['Descricao']];	
					}
				}
				$query .= " GROUP by ".implode(", ", $group);
				
				$this->out('- Ejecutando consulta de pendientes');
				//$this->info($query);
				fwrite($log, date("d-m-Y H:i:s") . ' - Ejecutando consulta de pendientes' . PHP_EOL);
				
				try {
					$results = $connection->execute($query)->fetchAll('assoc');
				} catch(\Exception $e) {
					$this->warn('-- Este Client Attribute no puede ser procesado. Continuando con el siguiente...');
					fwrite($log, date("d-m-Y H:i:s") . ' -- Este Client Attribute no puede ser procesado. Continuando con el siguiente...' . PHP_EOL);
					$this->warn("-- " . $query);
					fwrite($log, date("d-m-Y H:i:s") . " -- " . $query . PHP_EOL);
				}

				
				$this->out('- Buscando pendientes');
				fwrite($log, date("d-m-Y H:i:s") . ' - Buscando pendientes' . PHP_EOL);
				// Registro de Pendientes
				foreach($results as $result) {
					if($result['Value'] < 0) {
						$insert = $result;
						
						$insert['Country_id'] = $country['Id'];
						$insert['ClientAttribute_id'] = $attribute->IdClientAttribute;
						$insert['CreatedBy'] = 'System';
						$insert['IdClient'] = $attribute->client->IdClient;
						$insert['Client'] = $attribute->client->Descricao;						
						
						$entity = $this->AttributesPendantsDetails->newEntity($insert);
						
						$save = $this->AttributesPendantsDetails->save($entity);
						
						if($save) {
							$this->verbose('-- Pendiente guardado exitosamente: ('.implode(", ", $select).')');
							fwrite($log, date("d-m-Y H:i:s") . ' -- Pendiente guardado exitosamente: ('.implode(", ", $select).')' . PHP_EOL);
						} else {
							$this->warn('-- Pendiente presenta problemas de guardado: ('.implode(", ", $select).')');
							fwrite($log, date("d-m-Y H:i:s") . ' -- Pendiente presenta problemas de guardado: ('.implode(", ", $select).')' . PHP_EOL);
						}
						
					}
				}
				
			}
	        
        }
        fwrite($log, date("d-m-Y H:i:s") . ' -- Proceso finalizado'. PHP_EOL);
        fclose($log);
        fclose($lock);
        unlink(LOGS . DS . "refresh" . DS . $country['Descricao'] . DS . $country_id . ".lock");
        
        // Registro ultima fecha
        $lu = date("Y-m-d H:i:s");
        if(file_exists(LOGS . DS . "refresh" . DS . $country['Descricao'] . DS . $lu . ".update")) {
			unlink(LOGS . DS . "refresh" . DS . $country['Descricao'] . DS . $lu . ".update");
		}
		$up = fopen(LOGS . DS . "refresh" . DS . $country['Descricao'] . DS . $lu . ".update", "w");
		fclose($up);
        
    }
}
