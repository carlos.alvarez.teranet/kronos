<?php
namespace App\Shell;

use Cake\Console\Shell;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;
use Cake\Datasource\ConnectionManager;


class DeletePicturesShell extends Shell
{

    public function initialize()
    {
        parent::initialize();
    }

    public function main()
    {
	    $this->info("Iniciando proceso");
	    
		$connection = ConnectionManager::get('IT_WORKFLOW');
		$files		= $connection->execute("SELECT * FROM dbo.ELIMINAR_FOTOS WHERE Status IS NULL");
		
		$folder		= "/mnt/psFotos";
		
		while($file = $files->fetch('assoc')) {
			if(unlink($folder . DS . $file['NAMEFOTO'])) {
				$this->info("Archivo eliminado: " . $file['NAMEFOTO']);
				$connection->execute("UPDATE dbo.ELIMINAR_FOTOS SET Status = 'Eliminado' WHERE NAMEFOTO = '" . $file['NAMEFOTO'] . "'");
			} else {
				$this->warn("Archivo no se pudo eliminar: " . $file['NAMEFOTO']);
				$connection->execute("UPDATE dbo.ELIMINAR_FOTOS SET Status = 'Sin eliminar' WHERE NAMEFOTO = '" . $file['NAMEFOTO'] . "'");
			}
		}
    }
}
?>