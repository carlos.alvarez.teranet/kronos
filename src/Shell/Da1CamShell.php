<?php
namespace App\Shell;

use Cake\Console\Shell;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;
use Cake\Datasource\ConnectionManager;


class Da1CamShell extends Shell
{

    public function initialize()
    {
        parent::initialize();
    }

	public function main() {

		$file 		= fopen(TMP . "da1" . DS . "Pesos2305_CAM_P.da1", 'w');

		$connection = ConnectionManager::get('CAM_KWP');
		//$inds		= $connection->execute("SELECT RIGHT('00000000' + CAST(idDomicilio as VARCHAR), 8) as idDomicilio FROM dbo.RG_Domicilios_Pesos WHERE Ano >= 2019 GROUP by idDomicilio ORDER by CAST(idDomicilio as INTEGER) ASC")->fetchAll('assoc');

		$weights	= $connection->execute("SELECT 	RIGHT('00000000' + CAST(rgd.idDomicilio as VARCHAR), 8) as idDomicilio,
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2019 AND rgd.MesSem = 1 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2019 AND rgd.MesSem = 2 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2019 AND rgd.MesSem = 3 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2019 AND rgd.MesSem = 4 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2019 AND rgd.MesSem = 5 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2019 AND rgd.MesSem = 6 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2019 AND rgd.MesSem = 7 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2019 AND rgd.MesSem = 8 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2019 AND rgd.MesSem = 9 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2019 AND rgd.MesSem = 10 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2019 AND rgd.MesSem = 11 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2019 AND rgd.MesSem = 12 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2020 AND rgd.MesSem = 1 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2020 AND rgd.MesSem = 2 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2020 AND rgd.MesSem = 3 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2020 AND rgd.MesSem = 4 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2020 AND rgd.MesSem = 5 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2020 AND rgd.MesSem = 6 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2020 AND rgd.MesSem = 7 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2020 AND rgd.MesSem = 8 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2020 AND rgd.MesSem = 9 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2020 AND rgd.MesSem = 10 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2020 AND rgd.MesSem = 11 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2020 AND rgd.MesSem = 12 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2021 AND rgd.MesSem = 1 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2021 AND rgd.MesSem = 2 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2021 AND rgd.MesSem = 3 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2021 AND rgd.MesSem = 4 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2021 AND rgd.MesSem = 5 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2021 AND rgd.MesSem = 6 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2021 AND rgd.MesSem = 7 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2021 AND rgd.MesSem = 8 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2021 AND rgd.MesSem = 9 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2021 AND rgd.MesSem = 10 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2021 AND rgd.MesSem = 11 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2021 AND rgd.MesSem = 12 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2022 AND rgd.MesSem = 1 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 1 AND rgc.idPeso = 2 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 1 AND rgc.idPeso = 3 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 1 AND rgc.idPeso = 4 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 1 AND rgc.idPeso = 5 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 1 AND rgc.idPeso = 6 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 1 AND rgc.idPeso = 7 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 1 AND rgc.idPeso = 8 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 1 AND rgc.idPeso = 20 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2022 AND rgd.MesSem = 2 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 2 AND rgc.idPeso = 2 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 2 AND rgc.idPeso = 3 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 2 AND rgc.idPeso = 4 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 2 AND rgc.idPeso = 5 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 2 AND rgc.idPeso = 6 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 2 AND rgc.idPeso = 7 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 2 AND rgc.idPeso = 8 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 2 AND rgc.idPeso = 20 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2022 AND rgd.MesSem = 3 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 3 AND rgc.idPeso = 2 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 3 AND rgc.idPeso = 3 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 3 AND rgc.idPeso = 4 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 3 AND rgc.idPeso = 5 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 3 AND rgc.idPeso = 6 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 3 AND rgc.idPeso = 7 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 3 AND rgc.idPeso = 8 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 3 AND rgc.idPeso = 20 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2022 AND rgd.MesSem = 4 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 4 AND rgc.idPeso = 2 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 4 AND rgc.idPeso = 3 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 4 AND rgc.idPeso = 4 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 4 AND rgc.idPeso = 5 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 4 AND rgc.idPeso = 6 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 4 AND rgc.idPeso = 7 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 4 AND rgc.idPeso = 8 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 4 AND rgc.idPeso = 20 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2022 AND rgd.MesSem = 5 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 5 AND rgc.idPeso = 2 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 5 AND rgc.idPeso = 3 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 5 AND rgc.idPeso = 4 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 5 AND rgc.idPeso = 5 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 5 AND rgc.idPeso = 6 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 5 AND rgc.idPeso = 7 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 5 AND rgc.idPeso = 8 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 5 AND rgc.idPeso = 20 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2022 AND rgd.MesSem = 6 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 6 AND rgc.idPeso = 2 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 6 AND rgc.idPeso = 3 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 6 AND rgc.idPeso = 4 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 6 AND rgc.idPeso = 5 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 6 AND rgc.idPeso = 6 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 6 AND rgc.idPeso = 7 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 6 AND rgc.idPeso = 8 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 6 AND rgc.idPeso = 20 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2022 AND rgd.MesSem = 7 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 7 AND rgc.idPeso = 2 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 7 AND rgc.idPeso = 3 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 7 AND rgc.idPeso = 4 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 7 AND rgc.idPeso = 5 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 7 AND rgc.idPeso = 6 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 7 AND rgc.idPeso = 7 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 7 AND rgc.idPeso = 8 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 7 AND rgc.idPeso = 20 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2022 AND rgd.MesSem = 8 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 8 AND rgc.idPeso = 2 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 8 AND rgc.idPeso = 3 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 8 AND rgc.idPeso = 4 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 8 AND rgc.idPeso = 5 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 8 AND rgc.idPeso = 6 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 8 AND rgc.idPeso = 7 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 8 AND rgc.idPeso = 8 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 8 AND rgc.idPeso = 20 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2022 AND rgd.MesSem = 9 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 9 AND rgc.idPeso = 2 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 9 AND rgc.idPeso = 3 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 9 AND rgc.idPeso = 4 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 9 AND rgc.idPeso = 5 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 9 AND rgc.idPeso = 6 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 9 AND rgc.idPeso = 7 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 9 AND rgc.idPeso = 8 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 9 AND rgc.idPeso = 20 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2022 AND rgd.MesSem = 10 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 10 AND rgc.idPeso = 2 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 10 AND rgc.idPeso = 3 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 10 AND rgc.idPeso = 4 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 10 AND rgc.idPeso = 5 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 10 AND rgc.idPeso = 6 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 10 AND rgc.idPeso = 7 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 10 AND rgc.idPeso = 8 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 10 AND rgc.idPeso = 20 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2022 AND rgd.MesSem = 11 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 11 AND rgc.idPeso = 2 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 11 AND rgc.idPeso = 3 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 11 AND rgc.idPeso = 4 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 11 AND rgc.idPeso = 5 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 11 AND rgc.idPeso = 6 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 11 AND rgc.idPeso = 7 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 11 AND rgc.idPeso = 8 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 11 AND rgc.idPeso = 20 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2022 AND rgd.MesSem = 12 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 12 AND rgc.idPeso = 2 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 12 AND rgc.idPeso = 3 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 12 AND rgc.idPeso = 4 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 12 AND rgc.idPeso = 5 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 12 AND rgc.idPeso = 6 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 12 AND rgc.idPeso = 7 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 12 AND rgc.idPeso = 8 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2022 AND rgc.MesSem = 12 AND rgc.idPeso = 20 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2023 AND rgd.MesSem = 1 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 1 AND rgc.idPeso = 2 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 1 AND rgc.idPeso = 3 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 1 AND rgc.idPeso = 4 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 1 AND rgc.idPeso = 5 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 1 AND rgc.idPeso = 6 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 1 AND rgc.idPeso = 7 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 1 AND rgc.idPeso = 8 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 1 AND rgc.idPeso = 20 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2023 AND rgd.MesSem = 2 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 2 AND rgc.idPeso = 2 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 2 AND rgc.idPeso = 3 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 2 AND rgc.idPeso = 4 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 2 AND rgc.idPeso = 5 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 2 AND rgc.idPeso = 6 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 2 AND rgc.idPeso = 7 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 2 AND rgc.idPeso = 8 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 2 AND rgc.idPeso = 20 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2023 AND rgd.MesSem = 3 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 3 AND rgc.idPeso = 2 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 3 AND rgc.idPeso = 3 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 3 AND rgc.idPeso = 4 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 3 AND rgc.idPeso = 5 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 3 AND rgc.idPeso = 6 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 3 AND rgc.idPeso = 7 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 3 AND rgc.idPeso = 8 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 3 AND rgc.idPeso = 20 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2023 AND rgd.MesSem = 4 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 4 AND rgc.idPeso = 2 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 4 AND rgc.idPeso = 3 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 4 AND rgc.idPeso = 4 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 4 AND rgc.idPeso = 5 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 4 AND rgc.idPeso = 6 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 4 AND rgc.idPeso = 7 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 4 AND rgc.idPeso = 8 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 4 AND rgc.idPeso = 20 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgd.Ano = 2023 AND rgd.MesSem = 5 THEN rgd.Valor ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 5 AND rgc.idPeso = 2 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 5 AND rgc.idPeso = 3 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 5 AND rgc.idPeso = 4 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 5 AND rgc.idPeso = 5 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 5 AND rgc.idPeso = 6 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 5 AND rgc.idPeso = 7 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 5 AND rgc.idPeso = 8 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6) +
											RIGHT('000000' + CAST(MAX(CASE WHEN rgc.Ano = 2023 AND rgc.MesSem = 5 AND rgc.idPeso = 20 THEN ISNULL(rgc.Valor, 0) ELSE 0 END) as VARCHAR), 6)
											as [pesos]
									FROM 	dbo.RG_Domicilios_Pesos rgd
									LEFT JOIN	dbo.RG_Domicilios_Pesos_CSV rgc ON rgc.Iddomicilio = rgd.Iddomicilio AND rgc.Ano = rgd.Ano AND rgc.MesSem = rgd.messem
									WHERE	rgd.idPeso = 1
									AND		rgd.Ano >= 2019
									GROUP	by RIGHT('00000000' + CAST(rgd.idDomicilio as VARCHAR), 8)
									ORDER	by 1 ASC")->fetchAll('assoc');

		$connection = ConnectionManager::get('CAM_KWP');
		$years		= [2023, 2022, 2021, 2020, 2019];
		$layout 	= $connection->execute("SELECT Nombre, spVariable, Largo, PosInicial FROM dbo.RG_Panelis_Variables ORDER by PosInicial ASC")->fetchAll('assoc');

		$n = 0;
		$this->verbose("Cantidad de Domicilios a procesar: " . count($weights));
		foreach($weights as $ind) {

			$n++;
			$this->verbose("Procesando domiciio: " . $n);
			$main_row = [];

			$query	= $connection->execute("SELECT * FROM dbo.RG_Panelis WHERE idDomicilio = :idDomicilio AND Ano IN (2023, 2022, 2021, 2020, 2019) ORDER by ANO DESC", ['idDomicilio' => $ind['idDomicilio']]);
			$rows	= $query->fetchAll('assoc');

			foreach($years as $year) {

				$key 	= array_search($year, array_column($rows, 'Ano'));
				$line = '';

				if($key !== false) {

					foreach($layout as $element) {
						//$this->abort(print_r($rows[$key], true));
						$line .= str_pad($rows[$key][$element['Nombre']], $element['Largo'], "0", STR_PAD_LEFT);
					}

				} else {

					foreach($layout as $element) {
						$line .= str_pad($rows[0][$element['Nombre']], $element['Largo'], "0", STR_PAD_LEFT);
					}

				}

				$main_row[] = str_pad($line, 500, "0", STR_PAD_RIGHT);

			}

			//$clave = array_search($ind['idDomicilio'], array_column($weights, 'idDomicilio'));
			$pesosPeriodos = $ind['pesos'];


			$text = implode("", $main_row).$pesosPeriodos;
			//fwrite($file, str_pad($text, 2986, "0", STR_PAD_RIGHT) . PHP_EOL);
			fwrite($file, str_pad($text, ( (5 * 500) + strlen($ind['pesos']) ), "0", STR_PAD_RIGHT) . PHP_EOL);

		}

	}

}