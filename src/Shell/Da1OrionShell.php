<?php
namespace App\Shell;

use Cake\Console\Shell;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;
use Cake\Datasource\ConnectionManager;


class Da1OrionShell extends Shell
{

    public function initialize()
    {
        parent::initialize();
    }

	public function main() {
		
		$file 		= fopen(TMP . "da1" . DS . "CL_DA1_202101.da1", 'w');
		
		$connection = ConnectionManager::get('CL_KWP');
		$inds		= $connection->execute("SELECT RIGHT('00000000' + CAST(idDomicilio as VARCHAR), 8) as idDomicilio FROM dbo.RG_Domicilios_Pesos_Orion WHERE idPeso = 1 GROUP by idDomicilio ORDER by CAST(idDomicilio as INTEGER) ASC")->fetchAll('assoc');
		
		$weights	= $connection->execute("SELECT 	RIGHT('00000000' + CAST(idDomicilio as VARCHAR), 8) as idDomicilio,
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2017 AND MesSem = 1 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2017 AND MesSem = 2 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2017 AND MesSem = 3 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2017 AND MesSem = 4 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2017 AND MesSem = 5 THEN Valor ELSE 0 END) as VARCHAR), 6)+
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2017 AND MesSem = 6 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2017 AND MesSem = 7 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2017 AND MesSem = 8 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2017 AND MesSem = 9 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2017 AND MesSem = 10 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2017 AND MesSem = 11 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2017 AND MesSem = 12 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2018 AND MesSem = 1 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2018 AND MesSem = 2 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2018 AND MesSem = 3 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2018 AND MesSem = 4 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2018 AND MesSem = 5 THEN Valor ELSE 0 END) as VARCHAR), 6)+
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2018 AND MesSem = 6 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2018 AND MesSem = 7 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2018 AND MesSem = 8 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2018 AND MesSem = 9 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2018 AND MesSem = 10 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2018 AND MesSem = 11 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2018 AND MesSem = 12 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2019 AND MesSem = 1 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2019 AND MesSem = 2 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2019 AND MesSem = 3 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2019 AND MesSem = 4 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2019 AND MesSem = 5 THEN Valor ELSE 0 END) as VARCHAR), 6)+
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2019 AND MesSem = 6 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2019 AND MesSem = 7 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2019 AND MesSem = 8 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2019 AND MesSem = 9 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2019 AND MesSem = 10 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2019 AND MesSem = 11 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2019 AND MesSem = 12 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2020 AND MesSem = 1 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2020 AND MesSem = 2 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2020 AND MesSem = 3 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2020 AND MesSem = 4 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2020 AND MesSem = 5 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2020 AND MesSem = 6 THEN Valor ELSE 0 END) as VARCHAR), 6) +
													RIGHT('000000' + CAST(MAX(CASE WHEN Ano = 2021 AND MesSem = 1 THEN Valor ELSE 0 END) as VARCHAR), 6) as [pesos]
											FROM 	dbo.RG_Domicilios_Pesos_Orion
											WHERE	idPeso = 1
											AND		Ano * 100 + MesSem >= 201701
											GROUP	by RIGHT('00000000' + CAST(idDomicilio as VARCHAR), 8)")->fetchAll('assoc');
													
		$connection = ConnectionManager::get('CL_SPRI');
		$years		= [2020, 2019, 2018, 2017, 2016];
		$layout 	= $connection->execute("SELECT Nombre, spVariable, Largo, PosInicial FROM dbo.RG_Panelis_Variables ORDER by PosInicial ASC")->fetchAll('assoc');
		
		foreach($inds as $ind) {
			
			$main_row = [];
			
			$query	= $connection->execute("SELECT * FROM dbo.RG_Panelis WHERE NPAN = :idDomicilio AND Ano IN (2020, 2019, 2018, 2017, 2016) ORDER by ANO DESC", ['idDomicilio' => $ind['idDomicilio']]);
			$rows	= $query->fetchAll('assoc');
			
			foreach($years as $year) {
				
				$key 	= array_search($year, array_column($rows, 'Ano'));
				$line = '';
								
				if($key !== false) {
			
					foreach($layout as $element) {
						$line .= str_pad($rows[$key][$element['Nombre']], $element['Largo'], "0", STR_PAD_LEFT);
					}
					
				} else {
									
					foreach($layout as $element) {
						$line .= str_pad($rows[0][$element['Nombre']], $element['Largo'], "0", STR_PAD_LEFT);	
					}
					
				}
				
				$main_row[] = str_pad($line, 500, "0", STR_PAD_RIGHT);
				
			}
			
			$clave = array_search($ind['idDomicilio'], array_column($weights, 'idDomicilio'));
			if($clave === false) {
				$pesosPeriodos = str_pad('', strlen($weights[0]['pesos']), "0", STR_PAD_RIGHT);
			} else {
				$pesosPeriodos = $weights[$clave]['pesos'];
			}
			
			
			$this->verbose($ind['idDomicilio']);
			$this->verbose($clave);
			$this->verbose($pesosPeriodos);
			
			
			$text = implode("", $main_row).$pesosPeriodos;
			fwrite($file, str_pad($text, 2824, "0", STR_PAD_RIGHT) . PHP_EOL);
			
		}
		
	}
    
}