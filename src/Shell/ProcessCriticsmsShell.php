<?php
namespace App\Shell;
use Cake\Console\Shell;
use Cake\Datasource\ConnectionManager;

class ProcessCriticsmsShell extends Shell {
	private $countries = ['AR'=>54, 'BO'=>591, 'BR'=>55, 'CAM'=>102, 'CL'=>56, 'CO'=>57, 'EC'=>593, 'MX'=>52, 'PE'=>51];
	public function initialize()
	{
		parent::initialize();
		$this->loadModel('CriticismsCountries');
		$this->loadModel('CriticismsTables');
		$this->loadModel('CriticismsReviews');
		$this->loadModel('CriticismsRules');
		$this->loadModel('CriticismsActions');
		$this->loadModel('HAtoCabecalhoTest');
	}
	public function startup()
	{
		$this->clear();
		if (empty($this->args[0])) {
			return $this->abort('Please enter a CountryISO2A.');
		} else if (!(array_key_exists($this->args[0], $this->countries) )) {
			return $this->abort(sprintf('Argument: %s for CountryISO2A is not allowed.', $this->args[0]));
		}
	}
	public function main($country)
	{
		$this->clear();
		$trpe=0;
		$stats=['Inicio'=>['Fecha inicio'=>date('d-m-Y H:i:s'),'Inicio en segundos'=>microtime(true)],'Fin'=>['Fecha Fin'=>null,'Fin en segundos'=>null]];
		$conection=ConnectionManager::get('IT_WORKFLOW_36');
		$sql_str='SELECT NEXT VALUE FOR criticas.EID AS [EID]';
		$execute_eid=$conection->execute($sql_str)->fetchAll('assoc');
		$eid=$execute_eid[0]['EID'];
		unset($execute_eid);
		$cc=0;
		$r=0;
		$i=0;
		$this->out(sprintf("[%s] Iniciando proceso de críticas para %s - EID: %s", date('d-m-Y H:i:s'), $country,$eid));
		$this->out(sprintf("[%s] Cargando datos de críticas", date('d-m-Y H:i:s'), $country));
		$CriticismsCountriesA=$this->CriticismsCountries->find('all',['conditions' => ['CriticismsCountries.short_desc' => $country]])->toArray();
		foreach($CriticismsCountriesA as $CriticismsCountryR) {
			$CriticismsTablesA=$this->CriticismsTables->find('all',['conditions' => ['CriticismsTables.country_id' => $CriticismsCountryR->country_id]])->toArray();
			$this->out(sprintf("[%s] Cantidad de tablas configuradas para procesar: %s en %s", date('d-m-Y H:i:s'), count($CriticismsTablesA),$country));
			$stats['Tablas']=['Configuradas'=>count($CriticismsTablesA)];
			$t=0;
			$tp=0;
			$ttt=microtime(true);
			foreach($CriticismsTablesA as $CriticismsTableR) {
				//print_r($CriticismsTableR);
				//return;
				$CriticismsReviewsA=$this->CriticismsReviews->find('all',['conditions'=>['CriticismsReviews.table_id'=>$CriticismsTableR->table_id]])->toArray();
				//$CriticismsReviewsA=$this->CriticismsReviews->find('all',['conditions'=>['CriticismsReviews.table_id'=>$CriticismsTableR->table_id,'CriticismsReviews.review_id'=>1739]])->toArray();
				//print_r($CriticismsReviewsA);
				//return;
				$stats['Tablas']['Detalles'][$t]=['Nombre'=>$CriticismsTableR->description, 'Criticas'=>['Configuradas'=>count($CriticismsReviewsA)]];
				$c=0;
				$cp=0;
				$ttc=microtime(true);
				foreach($CriticismsReviewsA as $CriticismsReviewR) {
					//$this->out(sprintf("[%s] %s - %s ========================================================================<", date('d-m-Y H:i:s'),$CriticismsReviewR->review_id , $CriticismsReviewR->description));
					$CriticismsRulesA=$this->CriticismsRules->find('all',['conditions'=>['CriticismsRules.status'=>1,'CriticismsRules.review_id'=>$CriticismsReviewR->review_id]])->toArray();
					$stats['Tablas']['Detalles'][$t]['Criticas']['Detalles'][$c]=['Nombre'=>$CriticismsReviewR->description,'Reglas'=>['Configuradas'=>count($CriticismsRulesA)]];
					$rp=0;
					$ttr=microtime(true);
					foreach($CriticismsRulesA as $CriticismsRuleR) {
						//$this->out(sprintf("[%s] Procesando '%s' - '%s'",date('d-m-Y H:i:s'),$CriticismsReviewR->description,$CriticismsRuleR->description));
						$this->out(sprintf("[%s] Procesando '%s'",date('d-m-Y H:i:s'),$CriticismsReviewR->description));
						$CriticismsActionsA=$this->CriticismsActions->find('all',['conditions'=>['CriticismsActions.rule_id'=>$CriticismsRuleR->rule_id]])->toArray();
						if (count($CriticismsActionsA)>0){
							$stats['Tablas']['Detalles'][$t]['Criticas']['Detalles'][$c]['Reglas']['Detalles'][$r]=['Nombre'=>$CriticismsRuleR->description,'Acciones'=>['Configuradas'=>count($CriticismsActionsA)]];
							$a=0;
							$ap=0;
							$tta=microtime(true);
							$str_set="";
							$str_fields="";
							$str_new_values="";
							$str_old_values="";
							foreach($CriticismsActionsA as $CriticismsActionR) {
								$sql_str="SELECT DATA_TYPE FROM ".$CriticismsTableR->database_n.".INFORMATION_SCHEMA.COLUMNS WHERE (TABLE_NAME LIKE '".$CriticismsTableR->description."' OR TABLE_NAME LIKE 'hato_cabecalho_ext') AND COLUMN_NAME like '".$CriticismsActionR->field."'";
								//$this->out(sprintf($sql_str));
								$execute=$conection->execute($sql_str)->fetchAll('assoc');
								$field_type=$execute[0]['DATA_TYPE'];
								if ($field_type=='varchar' || $field_type=='nvarchar') {
									$str_set.=$CriticismsActionR->field."='".$CriticismsActionR->value."',";
									$str_old_values.="isnull(".$CriticismsActionR->field.",'null')+','+";
								} else {
									$str_set.=$CriticismsActionR->field."=".$CriticismsActionR->value.",";
									$str_old_values.="convert(varchar,isnull(".$CriticismsActionR->field.",0))+','+";
								}
								$str_fields.=$CriticismsActionR->field."+";
								$str_new_values.=sprintf("%s",$CriticismsActionR->value).",";
								++$a;
							}
							$str_set=rtrim($str_set,",");
							$str_fields=rtrim($str_fields,"+");
							$str_new_values=rtrim($str_new_values,",");
							$str_old_values=rtrim($str_old_values,"+','+");
							//$stats['Tablas']['Detalles'][$t]['Criticas']['Detalles'][$c]['Reglas']['Detalles'][$r]['Acciones']['Detalles'][$a]=['Nombre'=>$CriticismsActionR->action_dml." ".$CriticismsActionR->field."=".$CriticismsActionR->value];
							//$this->out(sprintf("[%s] Inicio Acción %s - %s",date('d-m-Y H:i:s'),$CriticismsActionR->action_id,$CriticismsActionR->action_dml));
							//$condiciones=trim(str_ireplace("if ","",substr($CriticismsRuleR->sentence,0,strpos($CriticismsRuleR->sentence,"then"))));
							$condiciones=$CriticismsRuleR->sentence;
							//$this->out(sprintf("condiciones:  %s",$condiciones));
							$flg=$CriticismsTableR->description=="hato_cabecalho_ext_dev"?"flg_critics_ext":"flg_critics";
							$complement=sprintf("from IT_WORKFLOW.criticas.HAto_Cabecalho_CL_test t inner join CL_KWP.dbo.hato_cabecalho_ext_dev e on t.idhato_cabecalho=e.idhato_cabecalho where %s<>%s and isnull(%s,0)=0 and ",$CriticismsActionR->field,$CriticismsActionR->value,$flg);
							//$this->out(sprintf("condiciones2: %s",$condiciones2));
							/*$sql = sprintf("%s %s.%s.%s set ".$str_set." where %s and isnull(flg_critics,0)=0",
								$CriticismsActionR->action_dml,
								$CriticismsTableR->database_n,
								$CriticismsTableR->schema_n,
								$CriticismsTableR->description,
								$condiciones
							);*/
							$sql = sprintf("%s %s.%s.%s set %s=%s, flg_critics_ext=1 %s %s",
								$CriticismsActionR->action_dml,
                                                                $CriticismsTableR->database_n,
                                                                $CriticismsTableR->schema_n,
                                                                $CriticismsTableR->description,
								$CriticismsActionR->field,
								$CriticismsActionR->value,
								$complement,
								$condiciones
							);
							//$this->out(sprintf("SQL Criticas: %s",$sql));
							$sql_insert=sprintf("INSERT INTO [it_workflow].[criticas].[Logs] ");
							//$sql_select=sprintf("SELECT ".$CriticismsTableR->table_id." as [table_id], IdHato_Cabecalho as [affected_id], '".str_replace('+',',',$str_fields)."' as [fields], ".$CriticismsRuleR->rule_id." as [rule_id], ".$str_old_values." as [old_values], '".$str_new_values."' as [new_values], GETDATE() as [date], ".$a." as [action_count], ".$eid." as [EID] FROM [criticas].[".$CriticismsTableR->description."] WHERE ".$condiciones." and isnull(flg_critics,0)=0");
							$sql_select=sprintf("SELECT %s as [table_id], t.IdHato_Cabecalho as [affected_id], '%s' as [fields], %s as [rule_id], %s as [old_values], '%s' as [new_values], GETDATE() as [date], %s as [action_count], %s as [EID] %s %s",
								$CriticismsTableR->table_id,
								str_replace('+',',',$str_fields),
								$CriticismsRuleR->rule_id,
								$str_old_values,
								$str_new_values,
								$a,
								$eid,
								$complement,
								$condiciones
							);
							//$this->out(sprintf("SQL Logs: %s %s",$sql_insert,$sql_select));
							$tei=microtime(true);
							$this->out(sprintf("[%s] Inicio de la ejecucion de la consulta INSERT en logs",date('d-m-Y H:i:s')));
							$this->out(sprintf("%s",$sql_insert.$sql_select));
							$exec_logs=$conection->execute($sql_insert.$sql_select);
							//$exec_logs=$conection->execute("select null");
							$this->out(sprintf("[%s] Fin de la ejecucion del INSERT en logs",date('d-m-Y H:i:s')));
							$tei=microtime(true)-$tei;
							$teu=microtime(true);
							$this->out(sprintf("[%s] Inicio de la ejecucion de la consulta UPDATE en tabla objetivo",date('d-m-Y H:i:s')));
							$this->out(sprintf("%s",$sql));
							$exec_sql=$conection->execute($sql);
							//$exec_sql=$conection->execute("select null");
							$this->out(sprintf("[%s] Fin de la ejecucion del UPDATE en tabla objetivo",date('d-m-Y H:i:s')));
							$teu=microtime(true)-$teu;
							if ($exec_sql->rowCount()==0){
								$this->out(sprintf("[%s] No se encontraron registros para procesar",date('d-m-Y H:i:s')));
							} else {
								$trpe+=$exec_sql->rowCount();
								$teia[$i]=$tei;
								$teua[$i]=$teu;
								$ter[$i]=$tei+$teu;
								++$r;
								++$i;
								$this->out(sprintf("Se procesaron %s registro(s) en %f segundos",$exec_sql->rowCount(),$teu));
                                                                $this->out(sprintf("Se insertaron %s registro(s) en %f segundos",$exec_logs->rowCount(),$tei));
							}
							/*$stats['Tablas']['Detalles'][$t]['Criticas']['Detalles'][$c]['Reglas']['Detalles'][$r]['Acciones']['Detalles'][$a]['Registros procesados']=$exec_sql->rowCount();
							$stats['Tablas']['Detalles'][$t]['Criticas']['Detalles'][$c]['Reglas']['Detalles'][$r]['Acciones']['Detalles'][$a]['Logs Insertados']=$exec_logs->rowCount();
							$stats['Tablas']['Detalles'][$t]['Criticas']['Detalles'][$c]['Reglas']['Detalles'][$r]['Acciones']['Detalles'][$a]['Tiempo de ejecución']=$tea;
							$stats['Tablas']['Detalles'][$t]['Criticas']['Detalles'][$c]['Reglas']['Detalles'][$r]['Acciones']['Detalles'][$a]['SQL de Critica']=$sql;
							$stats['Tablas']['Detalles'][$t]['Criticas']['Detalles'][$c]['Reglas']['Detalles'][$r]['Acciones']['Detalles'][$a]['SQL de Logs']=$sql_insert.$sql_select;*/
							++$cp;
							++$rp;
							++$ap;
							//$this->out(sprintf("[%s] Fin Acción %s - %s",date('d-m-Y H:i:s'),$CriticismsActionR->action_id,$CriticismsActionR->action_dml));
							$stats['Tablas']['Detalles'][$t]['Criticas']['Detalles'][$c]['Reglas']['Detalles'][$r]['Acciones']['Procesadas']=$ap;
							$stats['Tablas']['Detalles'][$t]['Criticas']['Detalles'][$c]['Reglas']['Detalles'][$r]['Acciones']['Tiempo Total']=microtime(true)-$tta;
						} else {
							$this->out(sprintf("[%s] No se encontraron acciones asociadas a la regla %s",date('d-m-Y H:i:s'),$CriticismsRuleR->description));
						}
					}
					if($trpe!=0) {++$cc;}
					$stats['Tablas']['Detalles'][$t]['Criticas']['Detalles'][$c]['Reglas']['Procesadas']=$rp;
					$stats['Tablas']['Detalles'][$t]['Criticas']['Detalles'][$c]['Reglas']['Tiempo Total Reglas']=microtime(true)-$ttr;
					++$c;
				}
				$stats['Tablas']['Detalles'][$t]['Criticas']['Procesadas']=$cp;
				$stats['Tablas']['Detalles'][$t]['Criticas']['Tiempo Total Criticas']=microtime(true)-$ttc;
				if($cp>0) {++$tp;}
				++$t;
			}
			//Cantidad de tablas procesadas
			$stats['Tablas']['Procesadas']=$tp;
			$stats['Tablas']['Tiempo Total Tablas']=microtime(true)-$ttt;
		}
		$stats['Fin']['Fecha Fin']=date('d-m-Y H:i:s');
		$stats['Fin']['Fin en segundos']=microtime(true);
		$stats['Tiempo transcurrido']=$stats['Fin']['Fin en segundos']-$stats['Inicio']['Inicio en segundos'];
		$this->out(sprintf("[%s] Estadisticas de la ejecución del programa ", date('d-m-Y H:i:s')));
		$this->out(sprintf("[%s] Tiempo de ejecución del programa (segundos): %s", date('d-m-Y H:i:s'),$stats['Tiempo transcurrido']));
		$this->out(sprintf("[%s] Total Registros Procesados: %s", date('d-m-Y H:i:s'),$trpe));
		$this->out(sprintf("[%s] Total Criticas Ejecutadas: %s", date('d-m-Y H:i:s'),$cc));
		if(isset($ter)) {
			$suma=array_sum($ter);
			$cantidad=count($ter);
			$promedio=$suma/$cantidad;
			$maximo=max($ter);
			$minimo=min($ter);
			$this->out(sprintf("[%s] Tiempo total de las ejecuciones de update e insert: %f",date('d-m-Y H:i:s'),$suma));
			$this->out(sprintf("[%s] Cantidad total de ejecuciones de update e insert: %f",date('d-m-Y H:i:s'),$cantidad));
			$this->out(sprintf("[%s] Tiempo promedio de ejecuciones de update e insert: %f",date('d-m-Y H:i:s'),$promedio));
			$this->out(sprintf("[%s] Tiempo máximo de ejecuciones de update e insert: %f",date('d-m-Y H:i:s'),$maximo));
			$this->out(sprintf("[%s] Tiempo minimo de ejecuciones de update e insert: %f",date('d-m-Y H:i:s'),$minimo));
			$suma=array_sum($teua);
                        $cantidad=count($teua);
                        $promedio=$suma/$cantidad;
                        $maximo=max($teua);
                        $minimo=min($teua);
                        $this->out(sprintf("[%s] Tiempo total de las ejecuciones de update: %f",date('d-m-Y H:i:s'),$suma));
                        $this->out(sprintf("[%s] Cantidad total de ejecuciones de update: %f",date('d-m-Y H:i:s'),$cantidad));
                        $this->out(sprintf("[%s] Tiempo promedio de ejecuciones de update: %f",date('d-m-Y H:i:s'),$promedio));
                        $this->out(sprintf("[%s] Tiempo máximo de ejecuciones de update: %f",date('d-m-Y H:i:s'),$maximo));
                        $this->out(sprintf("[%s] Tiempo minimo de ejecuciones de update: %f",date('d-m-Y H:i:s'),$minimo));
			$suma=array_sum($teia);
                        $cantidad=count($teia);
                        $promedio=$suma/$cantidad;
                        $maximo=max($teia);
                        $minimo=min($teia);
                        $this->out(sprintf("[%s] Tiempo total de las ejecuciones de insert: %f",date('d-m-Y H:i:s'),$suma));
                        $this->out(sprintf("[%s] Cantidad total de ejecuciones de insert: %f",date('d-m-Y H:i:s'),$cantidad));
                        $this->out(sprintf("[%s] Tiempo promedio de ejecuciones de insert: %f",date('d-m-Y H:i:s'),$promedio));
                        $this->out(sprintf("[%s] Tiempo máximo de ejecuciones de insert: %f",date('d-m-Y H:i:s'),$maximo));
                        $this->out(sprintf("[%s] Tiempo minimo de ejecuciones de insert: %f",date('d-m-Y H:i:s'),$minimo));
			//$this->out(sprintf("[%s] Tiempos de ejecución de las reglas: %s", date('d-m-Y H:i:s'),print_r($ter,true)));
			//$this->out(sprintf("[%s] Tiempos de ejecución de los update: %s", date('d-m-Y H:i:s'),print_r($teua,true)));
			//$this->out(sprintf("[%s] Tiempos de ejecución de los insert: %s", date('d-m-Y H:i:s'),print_r($teia,true)));
		}
		$this->out(sprintf("[%s] Total Cantidad de reglas ejecutadas: %s", date('d-m-Y H:i:s'),$r));
		//$this->out(sprintf("[%s]",print_r($stats,true)));
	}
}

