<?php
namespace App\Shell;

use Cake\Console\Shell;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;
use PHPExcel;
use PHPExcel_IOFactory;
use Cake\Datasource\ConnectionManager;
use Cake\Mailer\Email;
use ZipArchive;


class ExtractDataShell extends Shell
{

    public function main()
    {
    	$connection = ConnectionManager::get('MX_KWP');	
    	$categories	= $connection->execute('SELECT DB_NAME() as Country, * FROM dbo.A_PainelProduto WHERE idPainel = 1')->fetchAll('assoc');
    	
		$this->info('- Comenzando Proceso de extracción para MX');
    	
    	foreach($categories as $category) {
	    	
	    	$results = fopen(TMP . "extract" . DS . $category['Country'] . "_idProduto_".$category['IdProduto'].".csv", 'w');
	    	
	    	$this->out('-- Procesando Categoria: idProduto: ' . $category['IdProduto']);
	    	
	    	$data = $connection->execute("	SELECT	j.idAto,
											h.Usuario,
											j.idDomicilio,
											CASE
												WHEN r.NSE_LOC IN (1,2) THEN 'ABC+'
												WHEN r.NSE_LOC IN (3) THEN 'C'
												WHEN r.NSE_LOC IN (4) THEN 'D'
												WHEN r.NSE_LOC IN (5,6) THEN 'D-E'
											END as NSE,
											CASE
												WHEN r.CIUDAD IN (5,12,28,33,55,61,62,63,64,65,80,101,104,105) THEN 'Noroeste'
												WHEN r.CIUDAD IN (6,7,13,14,18,19,23,36,37,38,39,56,67,68,4,81,82,83,84,95,97,98,103) THEN 'Noreste'
												WHEN r.CIUDAD IN (11,15,24,25,26,31,32,40,41,42,48,49,50,51,52,53,3,85,106,107) THEN 'Occidente'
												WHEN r.CIUDAD IN (8,10,16,17,20,27,30,43,44,45,46,47,54,58,59,60,69,70,71,72,73,74,75,77,86,87,88,89,90,91,92,93,94,96,99,100,102) THEN 'Centro'
												WHEN r.CIUDAD IN (1,2) THEN 'Amcm'
												WHEN r.CIUDAD IN (9,21,22,29,34,35,57,66,76,78,79,108) THEN 'Sureste'
											END as Zona,
											j.idPainel,
											CASE WHEN j.idPainel = 1 THEN 'PNC' ELSE 'OTRO' END as Painel,
											j.idCanal,
											c.Descricao as Canal,
											j.Ano, 
											j.Mes,
											j.IdArtigo,
											j.idProduto, 
											z.Produto,
											j.idSub,
											z.Sub,
											z.IdMarca,
											z.Marca,
											z.idFabricante,
											z.Fabricante,
											z.CdC01,
											z.Clas01,
											z.CdC02,
											z.Clas02,
											z.CdC03,
											z.Clas03,
											z.CdC04,
											z.Clas04,
											z.CdC05,
											z.Clas05,
											z.IdConteudo,
											z.Conteudo,
											z.Coef1 as Z_Coef1,
											z.Coef2 as Z_Coef2,
											j.COEF_01, j.COEF_02,
											dbo.Base64ToDec(j.FACTOR_RW) as FACTOR_RW,
											j.Quantidade,
											j.FlgRegalo,
											CAST(j.Preco as INTEGER) as J_Preco,
											CAST(h.Preco_Unitario as INTEGER) as H_Precio_Unitario,
											p.valor as Peso_PNC,
											CAST((p.Valor * (dbo.Base64ToDec(j.FACTOR_RW) / CAST(100 as FLOAT)) * j.COEF_01) / 1000 as INTEGER) as Vol_1,
											CAST((p.Valor * (dbo.Base64ToDec(j.FACTOR_RW) / CAST(100 as FLOAT)) * j.COEF_02) / 1000 as INTEGER) as Vol_2
								FROM		dbo.J_AtosCompra_New j (nolock)
								JOIN		dbo.RG_Domicilios_Pesos p (nolock) ON p.Ano = j.Ano AND p.messem = j.Mes AND p.idDomicilio = j.IdDomicilio AND p.idpeso = 1
								JOIN		dbo.VW_ArtigoZ z (nolock) ON z.idArtigo = j.idArtigo
								JOIN		MX_SPRI.dbo.RG_Panelis r (nolock) ON r.idDomicilio = j.idDomicilio AND r.Ano = j.Ano
								JOIN		dbo.A_Canal c (nolock) ON c.IdCanal = j.IdCanal
								JOIN		dbo.HAto_Cabecalho h (nolock) ON h.IdHato_Cabecalho = j.IdHato_Cabecalho
								WHERE		j.idProduto = :idProduto
								AND			j.Ano * 100 + j.Mes >= 201801", ['idProduto' => $category['IdProduto']]);
	    	
		    $flag = 1;
		    while($line = $data->fetch('assoc')) {
		    	if($flag == 1) {
			    	$tmpLine = implode(";", array_keys($line));
					fwrite($results, $tmpLine . PHP_EOL);
					$flag = 0;
		    	}
		    	$tmpLine = implode(";", $line);
				fwrite($results, $tmpLine . PHP_EOL);
			}
			
			$zip = new \ZipArchive();
			$filename = TMP . "extract" . DS . $category['Country'] . "_idProduto_".$category['IdProduto'].".zip";
			
			if ($zip->open($filename, ZipArchive::CREATE)!==TRUE) {
			    exit("cannot open <$filename>\n");
			}
			
			$zip->addFile(TMP . "extract" . DS . $category['Country'] . "_idProduto_".$category['IdProduto'].".csv", $category['Country'] . "_idProduto_".$category['IdProduto'].".csv");
			$zip->close();
			
			unlink(TMP . "extract" . DS . $category['Country'] . "_idProduto_".$category['IdProduto'].".csv");
	    	
    	}
		
    }
}
