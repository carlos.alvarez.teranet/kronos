<?php
namespace App\Shell;

use Cake\Console\Shell;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;
use Cake\Datasource\ConnectionManager;
use ZipArchive;

class RevisionDa2Shell extends Shell
{
	public function initialize()
    {
        parent::initialize();
    }
    
    public function main()
    {
		$folder		= '/mnt/procesos/ZIbanz/Atos';
		$file		= 'atoMULT52KWcs.0922';
		
		$archivo	= fopen($folder . DS . $file, 'r');
		$nuevo		= fopen($folder . DS . $file . '.new' , 'w');
		
		$resultados = [];
		while(!feof($archivo)) {
				$linea = fgets($archivo);
				$linea = str_replace(array('.', ' ', "\n", "\t", "\r"), '', $linea);
				
				fwrite($nuevo, trim($linea) . PHP_EOL);
		}
		fclose($archivo);
		fclose($nuevo);
	}
	
	public function cumpen() {
		
		$layout 	= file_get_contents(TMP . "acts" . DS . "Layout_PNC_52_old.json");
		$layout 	= json_decode($layout);
		
		$folder		= '/mnt/procesos/ZIbanz/Atos';
		$file		= 'atoMULT52KWcs.0922.cumpen11';
		
		$archivo	= fopen($folder . DS . $file, 'r');
		$idAtos		= [];
		
		$this->info("Procesando archivo: " . $file);		
		while(!feof($archivo)) {
			$linea = fgets($archivo);
 			$idAto = substr($linea, 181, 12) * 1;
			$idAtos[$idAto] = $linea;
			$this->out("idAto: " . $idAto);
		}
		
		$file		= 'atoMULT52KWcs.0922';
		
		$archivo	= fopen($folder . DS . $file, 'r');
		$new		= fopen($folder . DS . $file . '.updated11', 'wb');
		
		$count = 0;
		$this->info("Procesando archivo secundario: " . $file);		
		while(!feof($archivo)) {
			$linea = fgets($archivo);
 			$idAto = substr($linea, 181, 12) * 1;
			if(array_key_exists($idAto, $idAtos)) {
				fwrite($new, trim($idAtos[$idAto]) . "\r\n");
				$this->out($count . ' - Agregando Linea nueva');
			} else {
				fwrite($new, trim($linea) . "\r\n");
				$this->out($count . ' - Agregando Linea original');
			}
			$count++;
		}	
		
	}
	
	public function modificar() {
		
		$layout = file_get_contents(TMP . "acts" . DS . "Layout_PNC_52.json");
		$layout = json_decode($layout);
		
		$connection = ConnectionManager::get('MX_KWP');
		$data	= $connection->execute("SELECT	DISTINCT idAto as key, DA2_217 as value
	    								FROM	dbo.MX_Adjustments_ARUBIO_20221108_Detail")->toList();
	    								
	    $this->abort(print_r($data, true));
		
		$folder		= '/mnt/procesos/ZIbanz/Atos';
		$file		= 'atoMULT52KWcs.0922';
		
		$archivo	= fopen($folder . DS . $file, 'r');
		$nuevo		= fopen($folder . DS . $file . '_new' , 'w');
		
		while(!feof($archivo)) {
			$linea = fgets($archivo);
 			$idAto = substr($linea, 181, 12) * 1;
			if(array_key_exists($idAto, $changes)) {
				$this->info("idAto encontrado: " . $idAto);
				$linea = substr($linea, 0, 217) . str_pad($changes[$idAto], 9, "0", STR_PAD_LEFT) . substr($linea, 226);
			}
			
			fwrite($nuevo, trim($linea) . PHP_EOL);
			
		}
		
		fclose($archivo);
		fclose($nuevo);
		
	}

	public function restaurar() {
		
		$layout 	= file_get_contents(TMP . "acts" . DS . "Layout_PNC_".$this->args[0]."_OLD.json");
		$layout 	= json_decode($layout);
		
		$folder		= '/mnt/procesos/ZIbanz/Atos';
		$file		= $folder . DS . $this->args[1];
		
		$pos		= ['inicio' => 181, 'largo' => 12];
		
		$idAtos 	= explode(",", $this->args[2]);
				
		$archivo	= fopen($file, 'r');
		
		$resultado = [];
		while(!feof($archivo)) {
			$linea = fgets($archivo);
 			$idAto = substr($linea, $pos['inicio'], $pos['largo']) * 1;	
 			if(in_array($idAto, $idAtos)) {
	 			$this->info("idAto encontrado | " . $idAto . " | ");
	 			
	 			foreach($layout as $element) {
		 			$resultado[$idAto][$element->nombre] = substr($linea, $element->inicio, $element->largo);
	 			}
	 			
 			}
		}
		
		$flag = 0;
		foreach($resultado as $ato => $element) {
			if($flag == 0) {
				$this->info(implode(";", array_keys($element)));
				$flag = 1;
			}
			$this->out(implode(";", $element));
		}
		
	}
}