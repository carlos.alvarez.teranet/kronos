<?php
namespace App\Shell;

use Cake\Console\Shell;
use Cake\Mailer\Email;


class TestEmailShell extends Shell
{

    public function initialize()
    {
        parent::initialize();
    }

	public function main() {
		
		$email = new Email('default');
		$email->from(['roclatam@kantar-services.com' => 'ROCLatAm Production'])
		    ->to('aldo.rubio@kantar.com')
		    ->subject('Prueba')
		    ->emailFormat('both')
		    ->send();
		
	}
}