<?php
namespace App\Shell;

use Cake\Console\Shell;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;
use Cake\Datasource\ConnectionManager;
use ZipArchive;

class Da1PetsShell extends Shell
{
	public function initialize()
    {
        parent::initialize();
    }
    
    public function main()
    {
	    
	    $folder		= TMP . "da1";
	    $file1		= fopen($folder . DS . 'Pesos2210_Brasil_P.da1', 'r');
	    $nuevo		= fopen($folder . DS . 'Pesos2210_Brasil_P.da1' . '.new' , 'wb');
	    
	    $layout 	= file_get_contents(TMP . "doms" . DS . "Layout_PNC_52.json");
		$layout 	= json_decode($layout);
		
		$array_pets = [];
		
		while(!feof($file2)) {
			
			$linea2	= fgets($file2);
			$array2	= explode(';', $linea2);
			
			$array_pets[$array2[0] * 1] = $array2[2];
			
		}
		
		while(!feof($file1)) {
				$linea1 = fgets($file1);
				$linea1 = str_replace(array("\n", "\t", "\r"), '', $linea1);
				$npan	= substr($linea1, 2, 8) * 1;
				$this->out("Procesando idDomicilio: " . $npan);
				
				$pets	= str_pad('0', 725, '0', STR_PAD_LEFT);
				if( array_key_exists($npan, $array_pets) ) {
					$pets = $array_pets[$npan];
				}
				
				fwrite($nuevo, $linea1 . $pets  . "\r\n");
				
		}
		fclose($file1);
	    
	    
    }
    
}