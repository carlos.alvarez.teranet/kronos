<?php
namespace App\Shell;

use Cake\Console\Shell;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;
use Cake\Datasource\ConnectionManager;
use ZipArchive;

class MirrorEspejoShell extends Shell
{
	public function initialize()
    {
        parent::initialize();
    }
    
    public function main()
    {
        $origen 		= fopen("/mnt/procesos/ZIbanz/Atos/atoMULT52KWcs.1121", 'r');
        $destino		= fopen("/mnt/procesos/ZIbanz/Atos/atoMULT52KWcs_Mirror.1121", 'w');
        
        $connection = ConnectionManager::get('MX_KWP');
        
        $bloque_202106 = $connection->execute("SELECT idDomicilio, New_Donante FROM dbo.Domicilios_Mirror_Status WHERE Ano = 2021 AND Mes = 6 AND Tipo = 'ESPEJO'")->fetchAll('assoc');
        $bloque_202107 = $connection->execute("SELECT idDomicilio, New_Donante FROM dbo.Domicilios_Mirror_Status WHERE Ano = 2021 AND Mes = 7 AND Tipo = 'ESPEJO'")->fetchAll('assoc');
        $bloque_202108 = $connection->execute("SELECT idDomicilio, New_Donante FROM dbo.Domicilios_Mirror_Status WHERE Ano = 2021 AND Mes = 8 AND Tipo = 'ESPEJO'")->fetchAll('assoc');
        $bloque_202109 = $connection->execute("SELECT idDomicilio, New_Donante FROM dbo.Domicilios_Mirror_Status WHERE Ano = 2021 AND Mes = 9 AND Tipo = 'ESPEJO'")->fetchAll('assoc');
        $bloque_202110 = $connection->execute("SELECT idDomicilio, New_Donante FROM dbo.Domicilios_Mirror_Status WHERE Ano = 2021 AND Mes = 10 AND Tipo = 'ESPEJO'")->fetchAll('assoc');
        $bloque_202111 = $connection->execute("SELECT idDomicilio, New_Donante FROM dbo.Domicilios_Mirror_Status WHERE Ano = 2021 AND Mes = 11 AND Tipo = 'ESPEJO'")->fetchAll('assoc');
        
        
        foreach($bloque_202106 as $element) {
	        $list_202106[$element['idDomicilio']] = $element['New_Donante'];
        }
        
        foreach($bloque_202107 as $element) {
	        $list_202107[$element['idDomicilio']] = $element['New_Donante'];
        }
        
        foreach($bloque_202108 as $element) {
	        $list_202108[$element['idDomicilio']] = $element['New_Donante'];
        }
        
        foreach($bloque_202109 as $element) {
	        $list_202109[$element['idDomicilio']] = $element['New_Donante'];
        }
        
        foreach($bloque_202110 as $element) {
	        $list_202110[$element['idDomicilio']] = $element['New_Donante'];
        }
        
        foreach($bloque_202111 as $element) {
	        $list_202111[$element['idDomicilio']] = $element['New_Donante'];
        }
        
        
        $periodo = '000000';        
        while(!feof($origen)) {
			$linea_origen = fgets($origen);
			
			$periodo 	= substr($linea_origen, 0, 6);
			$domicilio 	= substr($linea_origen, 11, 8) * 1;
			
			$this->info("Procesando domicilio: " . $domicilio . " en periodo: " . $periodo);
			//$this->out($periodo);
			
			$newLine = $linea_origen;
			
			if($periodo == '202106') {
				if(in_array($domicilio, array_keys($list_202106))) {
					
					$inicio 	= substr($linea_origen, 0, 11);
					$fin		= substr($linea_origen, (strlen($linea_origen) - 21) * -1);
					$newDom		= substr('00000000' . $list_202106[$domicilio], -8);
					
					$newLine	= $inicio . $newDom . '01' . $fin;
				}
			}
			
			if($periodo == '202107') {
				if(in_array($domicilio, array_keys($list_202107))) {
					
					$inicio 	= substr($linea_origen, 0, 11);
					$fin		= substr($linea_origen, (strlen($linea_origen) - 21) * -1);
					$newDom		= substr('00000000' . $list_202107[$domicilio], -8);
					
					$newLine	= $inicio . $newDom . '01' . $fin;
				}
			}
			
			if($periodo == '202108') {
				if(in_array($domicilio, array_keys($list_202108))) {
					
					$inicio 	= substr($linea_origen, 0, 11);
					$fin		= substr($linea_origen, (strlen($linea_origen) - 21) * -1);
					$newDom		= substr('00000000' . $list_202108[$domicilio], -8);
					
					$newLine	= $inicio . $newDom . '01' . $fin;
				}
			}
			
			if($periodo == '202109') {
				if(in_array($domicilio, array_keys($list_202109))) {
					
					$inicio 	= substr($linea_origen, 0, 11);
					$fin		= substr($linea_origen, (strlen($linea_origen) - 21) * -1);
					$newDom		= substr('00000000' . $list_202109[$domicilio], -8);
					
					$newLine	= $inicio . $newDom . '01' . $fin;
				}
			}
			
			if($periodo == '202110') {
				if(in_array($domicilio, array_keys($list_202110))) {
					
					$inicio 	= substr($linea_origen, 0, 11);
					$fin		= substr($linea_origen, (strlen($linea_origen) - 21) * -1);
					$newDom		= substr('00000000' . $list_202110[$domicilio], -8);
					
					$newLine	= $inicio . $newDom . '01' . $fin;
				}
			}
			
			if($periodo == '202111') {
				if(in_array($domicilio, array_keys($list_202111))) {
					
					$inicio 	= substr($linea_origen, 0, 11);
					$fin		= substr($linea_origen, (strlen($linea_origen) - 21) * -1);
					$newDom		= substr('00000000' . $list_202111[$domicilio], -8);
					
					$newLine	= $inicio . $newDom . '01' . $fin;
				}
			}
			
			fwrite($destino, $newLine);
			
		}
        
    }
}
