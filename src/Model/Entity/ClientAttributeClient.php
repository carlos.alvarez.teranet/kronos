<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ClientAttributeClient Entity
 *
 * @property int $IdClient
 * @property string $Descricao
 * @property \Cake\I18n\FrozenTime $DtCriacao
 * @property \Cake\I18n\FrozenTime $DtAtualizacao
 * @property string $FlgDesativacao
 */
class ClientAttributeClient extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'Descricao' => true,
        'DtCriacao' => true,
        'DtAtualizacao' => true,
        'FlgDesativacao' => true
    ];
}
