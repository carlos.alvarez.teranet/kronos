<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * UroborosProcess Entity
 *
 * @property int $pid
 * @property int $tid
 * @property int $cid
 * @property string $description
 */
class UroborosProcess extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'tid' => true,
        'cid' => true,
        'description' => true
    ];
}
