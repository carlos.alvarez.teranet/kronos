<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * UroborosLog Entity
 *
 * @property int $lid
 * @property int $eid
 * @property string $log_type
 * @property int $log_level
 * @property string $message
 * @property \Cake\I18n\FrozenTime $created_on
 * @property string $detail
 * @property string $subject
 * @property int $aid
 */
class UroborosLog extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'eid' => true,
        'log_type' => true,
        'log_level' => true,
        'message' => true,
        'created_on' => true,
        'detail' => true,
        'subject' => true,
        'aid' => true
    ];
}
