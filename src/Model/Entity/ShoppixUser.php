<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ShoppixUser Entity
 *
 * @property string $Id
 * @property string $Email
 * @property string $PasswordHash
 * @property string $TemporaryPasswordHash
 * @property string $TemporaryEmail
 * @property bool $IsUserDisabled
 * @property \Cake\I18n\FrozenTime $UserDisabledDate
 * @property bool $IsAdminDisabled
 * @property \Cake\I18n\FrozenTime $AdminDisabledDate
 * @property string $ReferralCode
 * @property string $ReferrerCode
 * @property \Cake\I18n\FrozenTime $RegisteredDate
 * @property string $RefreshToken
 * @property \Cake\I18n\FrozenTime $LastGeneralReferralsReminderDate
 * @property \Cake\I18n\FrozenTime $LastCompleteProfileReminderDate
 * @property bool $IsFirstEmailConfirmed
 * @property int $LoginAttemptsCount
 * @property \Cake\I18n\FrozenTime $BlockDateTime
 * @property \Cake\I18n\FrozenTime $CreatedDateTime
 * @property string $CreatedBy
 * @property \Cake\I18n\FrozenTime $UpdatedDateTime
 * @property string $UpdatedBy
 * @property int $ShortId
 * @property \Cake\I18n\FrozenTime $ResetPasswordDate
 * @property \Cake\I18n\FrozenTime $LastReceiptSubmissionDateTime
 * @property string $EReceiptAccountId
 * @property int $EReceiptRegistrationStatus
 * @property \Cake\I18n\FrozenTime $EReceiptRegistrationStatusChangedDateTime
 * @property bool $EReceiptRegistrationRenewalSent
 * @property string $WebSiteRefreshToken
 * @property \Cake\I18n\FrozenTime $WebSiteBlockDateTime
 * @property int $WebSiteLoginAttemptsCount
 * @property \Cake\I18n\FrozenTime $EReceiptRegistrationExpirationDate
 * @property \Cake\I18n\FrozenTime $EReceiptRegistrationDateTime
 * @property string $EReceiptRegistrationStatusUpdatedBy
 * @property bool $EReceiptConsent
 * @property \Cake\I18n\FrozenTime $EReceiptConsentDateTime
 * @property \Cake\I18n\FrozenTime $ConfirmationMailDateTime
 * @property string $FaceBookId
 * @property string $ActiveSubmissionEmail
 * @property bool $ActiveSubmissionAccess
 */
class ShoppixUser extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'Email' => true,
        'PasswordHash' => true,
        'TemporaryPasswordHash' => true,
        'TemporaryEmail' => true,
        'IsUserDisabled' => true,
        'UserDisabledDate' => true,
        'IsAdminDisabled' => true,
        'AdminDisabledDate' => true,
        'ReferralCode' => true,
        'ReferrerCode' => true,
        'RegisteredDate' => true,
        'RefreshToken' => true,
        'LastGeneralReferralsReminderDate' => true,
        'LastCompleteProfileReminderDate' => true,
        'IsFirstEmailConfirmed' => true,
        'LoginAttemptsCount' => true,
        'BlockDateTime' => true,
        'CreatedDateTime' => true,
        'CreatedBy' => true,
        'UpdatedDateTime' => true,
        'UpdatedBy' => true,
        'ShortId' => true,
        'ResetPasswordDate' => true,
        'LastReceiptSubmissionDateTime' => true,
        'EReceiptAccountId' => true,
        'EReceiptRegistrationStatus' => true,
        'EReceiptRegistrationStatusChangedDateTime' => true,
        'EReceiptRegistrationRenewalSent' => true,
        'WebSiteRefreshToken' => true,
        'WebSiteBlockDateTime' => true,
        'WebSiteLoginAttemptsCount' => true,
        'EReceiptRegistrationExpirationDate' => true,
        'EReceiptRegistrationDateTime' => true,
        'EReceiptRegistrationStatusUpdatedBy' => true,
        'EReceiptConsent' => true,
        'EReceiptConsentDateTime' => true,
        'ConfirmationMailDateTime' => true,
        'FaceBookId' => true,
        'ActiveSubmissionEmail' => true,
        'ActiveSubmissionAccess' => true
    ];
}
