<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * HAtoCabecalho Entity
 *
 * @property int $IdHato_Cabecalho
 * @property int $Semana
 * @property int $idDomicilio
 * @property int $idIndividuo
 * @property \Cake\I18n\FrozenTime $Data_Compra
 * @property int $idCanal
 * @property int $idArtigo
 * @property \Cake\I18n\FrozenTime $Data_Processamento
 * @property \Cake\I18n\FrozenTime $Data_Criacao
 * @property \Cake\I18n\FrozenTime $Data_Compra_Utilizada
 * @property int $idProduto
 * @property int $idSub
 * @property int $idMarca
 * @property int $Quantidade
 * @property int $idConteudo
 * @property int $idPromocao
 * @property float $Preco_Unitario
 * @property string $Usuario
 * @property int $Tipo_Ato
 * @property string $Usuario_Autorizacao
 * @property string $CodBarr
 * @property string $Desc_Prod
 * @property string $Desc_Canal
 * @property string $Peso
 * @property string $Data_inv
 * @property int $Id_Cabec
 * @property int $Id_Shop
 * @property int $Id_Item
 * @property float $Preco_Total
 * @property int $Forma_pagto
 * @property bool $flgShopMis
 * @property int $idPais
 * @property int $id_viaje
 * @property int $id_equipo
 * @property \Cake\I18n\FrozenTime $Data_Correcao
 * @property int $Formato_Canal
 * @property int $idApp
 */
class HAtoCabecalho extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'Semana' => true,
        'idDomicilio' => true,
        'idIndividuo' => true,
        'Data_Compra' => true,
        'idCanal' => true,
        'idArtigo' => true,
        'Data_Processamento' => true,
        'Data_Criacao' => true,
        'Data_Compra_Utilizada' => true,
        'idProduto' => true,
        'idSub' => true,
        'idMarca' => true,
        'Quantidade' => true,
        'idConteudo' => true,
        'idPromocao' => true,
        'Preco_Unitario' => true,
        'Usuario' => true,
        'Tipo_Ato' => true,
        'Usuario_Autorizacao' => true,
        'CodBarr' => true,
        'Desc_Prod' => true,
        'Desc_Canal' => true,
        'Peso' => true,
        'Data_inv' => true,
        'Id_Cabec' => true,
        'Id_Shop' => true,
        'Id_Item' => true,
        'Preco_Total' => true,
        'Forma_pagto' => true,
        'flgShopMis' => true,
        'idPais' => true,
        'id_viaje' => true,
        'id_equipo' => true,
        'Data_Correcao' => true,
        'Formato_Canal' => true,
        'idApp' => true
    ];
}
