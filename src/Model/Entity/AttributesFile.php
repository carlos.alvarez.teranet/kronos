<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * AttributesFile Entity
 *
 * @property int $Id
 * @property string $Descricao
 * @property int $Country_id
 * @property string $ClientAttribute_id
 * @property int $Changes
 * @property \Cake\I18n\FrozenTime $DtCriacao
 * @property string $CreatedBy
 */
class AttributesFile extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'Descricao' => true,
        'Country_id' => true,
        'ClientAttribute_id' => true,
        'Changes' => true,
        'DtCriacao' => true,
        'CreatedBy' => true
    ];
}
