<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * CriticismsLog Entity
 *
 * @property int $log_id
 * @property int $tabla_id
 * @property int $affected_id
 * @property string $field
 * @property int $action_id
 * @property string $old_value
 * @property string $new_value
 * @property \Cake\I18n\FrozenTime $date
 *
 * @property \App\Model\Entity\Table $table
 * @property \App\Model\Entity\Affected $affected
 * @property \App\Model\Entity\Action $action
 */
class CriticismsLog extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'tabla_id' => true,
        'affected_id' => true,
        'field' => true,
        'action_id' => true,
        'old_value' => true,
        'new_value' => true,
        'date' => true
    ];
}
