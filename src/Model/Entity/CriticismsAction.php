<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * CriticismsAction Entity
 *
 * @property int $action_id
 * @property string $field
 * @property int $rule_id
 * @property string $action_dml
 * @property string $value
 *
 * @property \App\Model\Entity\Rule $rule
 */
class CriticismsAction extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'field' => true,
        'rule_id' => true,
        'action_dml' => true,
        'value' => true,
        'rule' => true
    ];
}
