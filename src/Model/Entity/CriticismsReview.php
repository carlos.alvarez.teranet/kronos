<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * CriticismsReview Entity
 *
 * @property int $review_id
 * @property string $description
 * @property string $created_for
 * @property string $updated_for
 * @property \Cake\I18n\FrozenTime $creation_date
 * @property \Cake\I18n\FrozenTime $updated_date
 * @property int $table_id
 *
 * @property \App\Model\Entity\Table $table
 */
class CriticismsReview extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'description' => true,
        'created_for' => true,
        'updated_for' => true,
        'creation_date' => true,
        'updated_date' => true,
        'table_id' => true,
        'table' => true
    ];
}
