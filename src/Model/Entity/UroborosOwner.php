<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * UroborosOwner Entity
 *
 * @property int $rid
 * @property int $mid
 * @property int $pid
 * @property string $params
 * @property string $nombre
 * @property string $apellido
 * @property string $mail
 * @property string $usuario
 * @property bool $owner
 */
class UroborosOwner extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'rid' => true,
        'mid' => true,
        'pid' => true,
        'params' => true,
        'nombre' => true,
        'apellido' => true,
        'mail' => true,
        'usuario' => true,
        'owner' => true
    ];
    
    protected function _getDisplay() {
	    return ucwords($this->_properties['nombre'] . '  ' .
            $this->_properties['apellido']);
    }
}
