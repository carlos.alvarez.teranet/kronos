<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * CriticismsRule Entity
 *
 * @property int $rule_id
 * @property int $review_id
 * @property int $idProduto
 * @property int $idSub
 * @property string $description
 * @property string $sentence
 * @property int $status
 *
 * @property \App\Model\Entity\Review $review
 */
class CriticismsRule extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'review_id' => true,
        'idProduto' => true,
        'idSub' => true,
        'description' => true,
        'sentence' => true,
        'status' => true,
        'review' => true
    ];
}
