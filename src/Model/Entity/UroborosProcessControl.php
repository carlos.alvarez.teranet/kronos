<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * UroborosProcessControl Entity
 *
 * @property int $eid
 * @property int $pid
 * @property string $status
 * @property string $params
 * @property \Cake\I18n\FrozenTime $start_date
 * @property \Cake\I18n\FrozenTime $end_date
 * @property string $unix
 * @property int $smid
 * @property bool $checking
 * @property bool $mail
 * @property \Cake\I18n\FrozenTime $checkingdate
 * @property bool $done
 * @property \Cake\I18n\FrozenDate $done_date
 * @property string $done_user
 */
class UroborosProcessControl extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'pid' => true,
        'status' => true,
        'params' => true,
        'start_date' => true,
        'end_date' => true,
        'unix' => true,
        'smid' => true,
        'checking' => true,
        'mail' => true,
        'checkingdate' => true,
        'done' => true,
        'done_date' => true,
        'done_user' => true
    ];
}
