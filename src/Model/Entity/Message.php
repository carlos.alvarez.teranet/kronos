<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Message Entity
 *
 * @property int $idMessage
 * @property string $pw
 * @property int $period
 * @property string $message
 * @property string $owner
 * @property string $email
 * @property \Cake\I18n\FrozenTime $date
 */
class Message extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
	    'type' => true,
        'reference' => true,
        'period' => true,
        'message' => true,
        'owner' => true,
        'email' => true,
        'date' => true,
        'userId' => true
    ];
}
