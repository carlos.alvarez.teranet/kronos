<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * AttributesAdmin Entity
 *
 * @property string $Admin
 * @property bool $AllowUpdate
 */
class AttributesAdmin extends Entity
{

	protected function _getAdmin($data)
    {
        return strtolower($data);
    }

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'AllowUpdate' => true
    ];
}
