<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * CriticismsTable Entity
 *
 * @property int $table_id
 * @property int $country_id
 * @property string $description
 * @property string $schema_n
 * @property string $database_n
 *
 * @property \App\Model\Entity\Country $country
 */
class CriticismsTable extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'country_id' => true,
        'description' => true,
	'schema_n' => true,
        'database_n' => true,
        'country' => true
    ];
}
