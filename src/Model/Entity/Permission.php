<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Permission Entity
 *
 * @property int $Id
 * @property string $mail
 * @property string $controller
 * @property string $prefix
 * @property \Cake\I18n\FrozenTime $created
 */
class Permission extends Entity
{

	protected function _getDescription()
    {
        return $this->_properties['prefix'] . '/' .
            $this->_properties['controller']. '/' .
            $this->_properties['action'];
    }

}
