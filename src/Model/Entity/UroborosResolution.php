<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * UroborosResolution Entity
 *
 * @property int $rid
 * @property int $eid
 * @property string $gid
 * @property string $comments
 * @property \Cake\I18n\FrozenTime $created
 */
class UroborosResolution extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'eid' => true,
        'gid' => true,
        'comments' => true,
        'created' => true
    ];
}
