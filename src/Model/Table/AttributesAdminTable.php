<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * AttributesAdmin Model
 *
 * @method \App\Model\Entity\AttributesAdmin get($primaryKey, $options = [])
 * @method \App\Model\Entity\AttributesAdmin newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\AttributesAdmin[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\AttributesAdmin|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\AttributesAdmin patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\AttributesAdmin[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\AttributesAdmin findOrCreate($search, callable $callback = null, $options = [])
 */
class AttributesAdminTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('A_ClientAttribute_Admin');
        $this->setDisplayField('AllowUpdate');
        $this->setPrimaryKey('Admin');
    }

}
