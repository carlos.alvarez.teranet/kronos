<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ClientAttributeClients Model
 *
 * @method \App\Model\Entity\ClientAttributeClient get($primaryKey, $options = [])
 * @method \App\Model\Entity\ClientAttributeClient newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ClientAttributeClient[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ClientAttributeClient|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ClientAttributeClient patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ClientAttributeClient[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ClientAttributeClient findOrCreate($search, callable $callback = null, $options = [])
 */
class ClientAttributeClientsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('ClientAttribute_Client');
        $this->setDisplayField('Descricao');
        $this->setPrimaryKey('IdClient');
        
        $this->HasMany('AttributesPendantsDetails', [
	       'foreignKey' => 'IdClient',
	       'bindingKey' => 'IdClient'
        ]);
    }

    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName()
    {
        return 'IT_WORKFLOW';
    }
}
