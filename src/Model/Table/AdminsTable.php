<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class AdminsTable extends Table
{

    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('PORTAL_Admin');
        $this->setDisplayField('Description');
        $this->setPrimaryKey('Id');

        $this->addBehavior('Timestamp');
    }

    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('Id', 'create');

        $validator
            ->scalar('mail')
            ->requirePresence('mail', 'create')
            ->notEmpty('mail');

        $validator
            ->scalar('controller')
            ->requirePresence('controller', 'create')
            ->notEmpty('controller');

        $validator
            ->scalar('prefix')
            ->requirePresence('prefix', 'create')
            ->notEmpty('prefix');

        return $validator;
    }
}
