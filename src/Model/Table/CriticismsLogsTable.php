<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * CriticismsLogs Model
 *
 * @property \App\Model\Table\TablesTable|\Cake\ORM\Association\BelongsTo $Tables
 * @property \App\Model\Table\AffectedsTable|\Cake\ORM\Association\BelongsTo $Affecteds
 * @property \App\Model\Table\ActionsTable|\Cake\ORM\Association\BelongsTo $Actions
 *
 * @method \App\Model\Entity\CriticismsLog get($primaryKey, $options = [])
 * @method \App\Model\Entity\CriticismsLog newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\CriticismsLog[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\CriticismsLog|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CriticismsLog patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\CriticismsLog[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\CriticismsLog findOrCreate($search, callable $callback = null, $options = [])
 */
class CriticismsLogsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('criticas.Logs');
        $this->setDisplayField('log_id');
        $this->setPrimaryKey('log_id');

        $this->belongsTo('Tables', [
            'foreignKey' => 'tabla_id'
        ]);
        $this->belongsTo('Affecteds', [
            'foreignKey' => 'affected_id'
        ]);
        $this->belongsTo('Actions', [
            'foreignKey' => 'action_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('log_id')
            ->allowEmpty('log_id', 'create');

        $validator
            ->scalar('field')
            ->maxLength('field', 18)
            ->allowEmpty('field');

        $validator
            ->scalar('old_value')
            ->maxLength('old_value', 20)
            ->requirePresence('old_value', 'create')
            ->notEmpty('old_value');

        $validator
            ->scalar('new_value')
            ->maxLength('new_value', 20)
            ->requirePresence('new_value', 'create')
            ->notEmpty('new_value');

        $validator
            ->dateTime('date')
            ->requirePresence('date', 'create')
            ->notEmpty('date');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['tabla_id'], 'Tables'));
        $rules->add($rules->existsIn(['affected_id'], 'Affecteds'));
        $rules->add($rules->existsIn(['action_id'], 'Actions'));

        return $rules;
    }

    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName()
    {
        return 'IT_WORKFLOW_36';
    }
}
