<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * CriticismsCountries Model
 *
 * @method \App\Model\Entity\CriticismsCountry get($primaryKey, $options = [])
 * @method \App\Model\Entity\CriticismsCountry newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\CriticismsCountry[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\CriticismsCountry|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CriticismsCountry patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\CriticismsCountry[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\CriticismsCountry findOrCreate($search, callable $callback = null, $options = [])
 */
class CriticismsCountriesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('criticas.Countries');
        $this->setDisplayField('short_desc');
        $this->setPrimaryKey('country_id');

	$this->hasMany('CriticismsTables', [
            'foreignKey' => 'country_id',
            'bindingKey' => 'country_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('country_id')
            ->allowEmpty('country_id', 'create');

        $validator
            ->scalar('description')
            ->maxLength('description', 50)
            ->requirePresence('description', 'create')
            ->notEmpty('description');

        $validator
            ->scalar('short_desc')
            ->maxLength('short_desc', 10)
            ->requirePresence('short_desc', 'create')
            ->notEmpty('short_desc');

        $validator
            ->integer('idCountry')
            ->allowEmpty('idCountry', 'create');

        return $validator;
    }

    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName()
    {
        return 'IT_WORKFLOW_36';
    }
}
