<?php
	
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\I18n\Time;
use Cake\Validation\Validator;

class CargaproductivaTable extends Table
{
	
	public function initialize(array $config)
    {
        $this->table('WPO_CargaProductiva');
        $this->primaryKey('IdCargaProductiva');
        $this->belongsTo('Periodo')
             ->setForeignKey('IdPeriodo');
    }
    
    public static function defaultConnectionName()
    {
        return 'IT_WORKFLOW_33';
    }
	
}	
?>