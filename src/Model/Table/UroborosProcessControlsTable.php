<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * UroborosProcessControl Model
 *
 * @method \App\Model\Entity\UroborosProcessControls get($primaryKey, $options = [])
 * @method \App\Model\Entity\UroborosProcessControls newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\UroborosProcessControls[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\UroborosProcessControls|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\UroborosProcessControls patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\UroborosProcessControls[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\UroborosProcessControls findOrCreate($search, callable $callback = null, $options = [])
 */
class UroborosProcessControlsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('WF_PROCESS_CONTROL');
        $this->setDisplayField('eid');
        $this->setPrimaryKey('eid');

        $this->belongsTo('UroborosProcesses', [
	        'bindingKey' => 'pid',
	        'foreignKey' => 'pid'
        ]);

        $this->belongsTo('UroborosStatuses', [
	        'foreignKey' => 'status',
	        'bindingKey' => 'code'
        ]);

        $this->hasMany('UroborosLogs', [
	        'foreignKey' => 'eid',
	        'bindingKey' => 'eid'
        ]);

        $this->hasOne('UroborosResolutions', [
	        'foreignKey' => 'eid',
	        'bindingKey' => 'eid',
	        'strategy' => 'select'
        ]);

        $this->hasOne('UroborosOwners', [
	        'foreignKey' => ['pid', 'params'],
	        'bindingKey' => ['pid', 'params'],
	        'conditions' => [
		        'UroborosOwners.owner' => true
	        ]
        ]);

    }

    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName()
    {
        return 'IT_WORKFLOW';
    }

    public function loadPendants($mail = null) {

	    $data = $this->find('all', [
		    'conditions' => [
			    'UroborosProcessControls.checking' => true,
			    'UroborosProcessControls.done IS NULL',
			    'YEAR(UroborosProcessControls.end_date) >= 2019'
		    ],
        'limit' => 100,
		    'contain' => [
			    'UroborosProcesses',
			    'UroborosStatuses',
			    'UroborosLogs' => [
				    'conditions' => [
					    "log_type IN ('W', 'E', 'C')"
				    ]
			    ],
			    'UroborosOwners'
		    ],
        'order' => [
			    'eid' => 'DESC'
		    ]
	    ]);

	    if($mail) {
		    $data->where([
			    'UroborosOwners.mail' => $mail
		    ]);
	    }

	    return $data;

    }

    public function loadFinishes($mail = null) {

	    $data = $this->find('all', [
		    'conditions' => [
			    'UroborosProcessControls.checking' => true,
			    'UroborosProcessControls.done IS NOT NULL',
			    'UroborosProcessControls.done_date >= DATEADD(week, -2, GETDATE())',
			    'YEAR(UroborosProcessControls.end_date) >= 2019'
		    ],
		    'contain' => [
			    'UroborosProcesses',
			    'UroborosStatuses',
			    'UroborosLogs' => [
				    'conditions' => [
					    "log_type IN ('W', 'E', 'C')"
				    ]
			    ],
			    'UroborosOwners'
		    ],
		    'order' => [
			    'eid' => 'DESC'
		    ]
	    ]);

	    if($mail) {
		    $data->where([
			    'UroborosOwners.mail' => $mail
		    ]);
	    }

	    return $data;
    }

    public function loadDetail($id = null) {

	    $data = $this->get($id, [
		    'contain' => [
			    'UroborosProcesses',
			    'UroborosStatuses',
			    'UroborosLogs' => [
				    'conditions' => [
					    "log_type IN ('W', 'E', 'C')"
				    ]
			    ],
			    'UroborosResolutions',
			    'UroborosOwners'
		    ],
		    'order' => [
			    'eid' => 'DESC'
		    ]
	    ]);

	    return $data;

    }
}
