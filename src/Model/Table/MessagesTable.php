<?php
	
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\I18n\Time;
use Cake\Validation\Validator;

class MessagesTable extends Table
{

	public function initialize(array $config)
    {
        $this->table('DBM_Messages');
        $this->primaryKey(['idMessage']);
    }
    
    public static function defaultConnectionName()
    {
        return 'IT_WORKFLOW_33';
    }
	
}	
?>