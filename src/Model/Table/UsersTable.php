<?php
	
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\I18n\Time;
use Cake\Validation\Validator;
use Cake\Event\Event;
use League\OAuth2\Client\Provider\AbstractProvider;


class UsersTable extends Table
{
	
	public function initialize(array $config)
    {
        $this->table('Portal_Users');
        $this->primaryKey('Id');
    }
	
}	
?>