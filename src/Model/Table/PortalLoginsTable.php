<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * PortalLogins Model
 *
 * @method \App\Model\Entity\PortalLogin get($primaryKey, $options = [])
 * @method \App\Model\Entity\PortalLogin newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\PortalLogin[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\PortalLogin|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PortalLogin patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\PortalLogin[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\PortalLogin findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class PortalLoginsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('PORTAL_Logins');
        $this->setDisplayField('Id');
        $this->setPrimaryKey('Id');
        
    }
}
