<?php
	
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\I18n\Time;
use Cake\Validation\Validator;

class CountriesTable extends Table
{
	
	public function initialize(array $config)
    {
        $this->table('Fusion_Countries');
        $this->primaryKey('Id');
        $this->belongsTo('Flags')
        		->setForeignKey('FlgDesativacao')
        		->setStrategy('select');
        $this->hasMany('Parameters', [
	        'foreignKey' => 'Country_id'
        ]);
        $this->hasMany('Joins', [
	        'foreignKey' => 'Country_id'
        ]);
        $this->hasMany('FieldsGroups', [
	        'foreignKey' => 'Country_id'
        ]);
        $this->hasMany('Executions', [
	        'foreignKey' => 'Country_id'
        ]);
        $this->hasMany('Clusters', [
	        'foreignKey' => 'Country_id'
        ]);
        $this->hasMany('FusionLogs', [
	        'foreignKey' => 'Country_id'
        ]);
    }
    
    public static function defaultConnectionName()
    {
        return 'IT_WORKFLOW';
    }
    
    public function beforeSave($event, $entity, $options)
	{
		// Default para DtCriacao
	    if($entity->isNew() && !isset($entity->DtCriacao))
	    {
		    $now = Time::now();
		    $now = $now->format('Y-m-d\TH:i:s');
	        $entity->DtCriacao = $now;
	    }
	    
	    // Default para DtAtualizacao
	    if($entity->isNew() && !isset($entity->DtAtualizacao))
	    {
		    $now = Time::now();
		    $now = $now->format('Y-m-d\TH:i:s');
	        $entity->DtAtualizacao = $now;
	    }
	    
	    // Default para CreatedBy
	    if($entity->isNew() && !isset($entity->CreatedBy))
	    {
	        $entity->CreatedBy = $_SESSION['Auth']['User']['displayName'];
	    }
	    
	    return true;
	}
	
	public function validationDefault(Validator $validator)
    {
	    $validator
        	->requirePresence('Descricao')
			->notEmpty('Descricao', 'Favor complete nombre del pais')
			->requirePresence('Short')
			->notEmpty('Short', 'Favor complete nombre corto del pais');

        return $validator;
    }
	
}	
?>