<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * CriticismsActions Model
 *
 * @property \App\Model\Table\RulesTable|\Cake\ORM\Association\BelongsTo $Rules
 *
 * @method \App\Model\Entity\CriticismsAction get($primaryKey, $options = [])
 * @method \App\Model\Entity\CriticismsAction newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\CriticismsAction[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\CriticismsAction|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CriticismsAction patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\CriticismsAction[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\CriticismsAction findOrCreate($search, callable $callback = null, $options = [])
 */
class CriticismsActionsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('criticas.Actions');
        $this->setDisplayField('field');
        $this->setPrimaryKey('action_id');

        $this->belongsTo('Rules', [
            'foreignKey' => 'rule_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('action_id')
            ->allowEmpty('action_id', 'create');

        $validator
            ->scalar('field')
            ->maxLength('field', 20)
            ->allowEmpty('field');

        $validator
            ->scalar('action_dml')
            ->maxLength('action_dml', 20)
            ->requirePresence('action_dml', 'create')
            ->notEmpty('action_dml');

        $validator
            ->scalar('value')
            ->maxLength('value', 20)
            ->requirePresence('value', 'create')
            ->notEmpty('value');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['rule_id'], 'Rules'));

        return $rules;
    }

    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName()
    {
        return 'IT_WORKFLOW_36';
    }
}
