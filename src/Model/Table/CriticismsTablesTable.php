<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * CriticismsTables Model
 *
 * @property \App\Model\Table\CountriesTable|\Cake\ORM\Association\BelongsTo $Countries
 *
 * @method \App\Model\Entity\CriticismsTable get($primaryKey, $options = [])
 * @method \App\Model\Entity\CriticismsTable newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\CriticismsTable[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\CriticismsTable|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CriticismsTable patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\CriticismsTable[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\CriticismsTable findOrCreate($search, callable $callback = null, $options = [])
 */
class CriticismsTablesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('criticas.Tables');
        $this->setDisplayField('description');
        $this->setPrimaryKey('table_id');

        $this->belongsTo('CriticismsCountries', [
            'foreignKey' => 'country_id',
            'joinType' => 'INNER'
        ]);

        $this->hasMany('CriticismsReviews', [
            'foreignKey' => 'table_id',
            'bindingKey' => 'table_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('table_id')
            ->allowEmpty('table_id', 'create');

        $validator
            ->scalar('description')
            ->maxLength('description', 200)
            ->requirePresence('description', 'create')
            ->notEmpty('description');

        $validator
            ->scalar('schema_n')
            ->maxLength('schema_n', 20)
            ->requirePresence('schema_n', 'create')
            ->notEmpty('schema_n');

	$validator
            ->scalar('database_n')
            ->maxLength('database_n', 20)
            ->requirePresence('database_n', 'create')
            ->notEmpty('database_n');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['country_id'], 'Countries'));

        return $rules;
    }

    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName()
    {
        return 'IT_WORKFLOW_36';
    }
}
