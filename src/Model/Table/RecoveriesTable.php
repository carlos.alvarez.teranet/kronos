<?php
	
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\I18n\Time;
use Cake\Validation\Validator;

class RecoveriesTable extends Table
{
	
	public function initialize(array $config)
    {
        $this->table('Atos_Recover');
        $this->primaryKey('idAtosRecover');
    }
	
}	
?>