<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * UroborosLogs Model
 *
 * @method \App\Model\Entity\UroborosLog get($primaryKey, $options = [])
 * @method \App\Model\Entity\UroborosLog newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\UroborosLog[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\UroborosLog|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\UroborosLog patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\UroborosLog[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\UroborosLog findOrCreate($search, callable $callback = null, $options = [])
 */
class UroborosLogsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('WF_PROCESS_LOGS');
        $this->setDisplayField('message');
        $this->setPrimaryKey('lide');
    }

    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName()
    {
        return 'IT_WORKFLOW';
    }
}
