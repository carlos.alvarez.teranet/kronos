<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * UroborosOwners Model
 *
 * @method \App\Model\Entity\UroborosOwner get($primaryKey, $options = [])
 * @method \App\Model\Entity\UroborosOwner newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\UroborosOwner[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\UroborosOwner|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\UroborosOwner patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\UroborosOwner[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\UroborosOwner findOrCreate($search, callable $callback = null, $options = [])
 */
class UroborosOwnersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('WF_RESPONSABLES');
        $this->setPrimaryKey('rid');
    }

    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName()
    {
        return 'IT_WORKFLOW';
    }
    
}
