<?php
	
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\I18n\Time;
use Cake\Validation\Validator;

class AttributesPendantsTable extends Table
{
	
	public function initialize(array $config)
    {
        $this->table('ClientAttribute_Pendants');
        $this->primaryKey('Id');
    }
    
    public function beforeSave($event, $entity, $options)
	{
		// Default para DtCriacao
	    if($entity->isNew() && !isset($entity->DtCriacao))
	    {
		    $now = Time::now();
		    $now = $now->format('Y-m-d\TH:i:s');
	        $entity->DtCriacao = $now;
	    }
	    
	    // Default para DtAtualizacao
	    if($entity->isNew() && !isset($entity->DtAtualizacao))
	    {
		    $now = Time::now();
		    $now = $now->format('Y-m-d\TH:i:s');
	        $entity->DtAtualizacao = $now;
	    }
	    
	    // Default para CreatedBy
	    if($entity->isNew() && !isset($entity->CreatedBy))
	    {
	        $entity->CreatedBy = $_SESSION['Auth']['User']['cn'][0];
	    }
	    
	    return true;
	}
	
	public static function defaultConnectionName() {
        return 'IT_WORKFLOW';
    }
	
}	
?>