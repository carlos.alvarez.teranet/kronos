<?php
	
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\I18n\Time;
use Cake\Validation\Validator;

class AttributesConfigTable extends Table
{
	
	public function initialize(array $config)
    {
        $this->table('A_ClientAttribute_Config');
        $this->primaryKey(['IdClientAttribute', 'Dim01', 'Dim02', 'Dim03', 'Dim04', 'Dim05', 'Dim06', 'Dim07', 'Dim08', 'Dim09', 'Dim10']);
    }
    
    public function beforeSave($event, $entity, $options)
	{
	    if($entity->isNew() && !isset($entity->Dim01))
	    {
	        $entity->Dim01 = 0;
	    }
	    
	    if($entity->isNew() && !isset($entity->Dim02))
	    {
	        $entity->Dim02 = 0;
	    }
	    
	    if($entity->isNew() && !isset($entity->Dim03))
	    {
	        $entity->Dim03 = 0;
	    }
	    
	    if($entity->isNew() && !isset($entity->Dim04))
	    {
	        $entity->Dim04 = 0;
	    }
	    
	    if($entity->isNew() && !isset($entity->Dim05))
	    {
	        $entity->Dim05 = 0;
	    }
	    
	    if($entity->isNew() && !isset($entity->Dim06))
	    {
	        $entity->Dim06 = 0;
	    }
	    
	    if($entity->isNew() && !isset($entity->Dim07))
	    {
	        $entity->Dim07 = 0;
	    }
	    
	    if($entity->isNew() && !isset($entity->Dim08))
	    {
	        $entity->Dim08 = 0;
	    }
	    
	    if($entity->isNew() && !isset($entity->Dim09))
	    {
	        $entity->Dim09 = 0;
	    }
	    
	    if($entity->isNew() && !isset($entity->Dim10))
	    {
	        $entity->Dim10 = 0;
	    }
	    	    
	    return true;
	}
	
}	
?>