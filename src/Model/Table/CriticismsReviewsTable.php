<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * CriticismsReviews Model
 *
 * @property \App\Model\Table\TablesTable|\Cake\ORM\Association\BelongsTo $Tables
 *
 * @method \App\Model\Entity\CriticismsReview get($primaryKey, $options = [])
 * @method \App\Model\Entity\CriticismsReview newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\CriticismsReview[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\CriticismsReview|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CriticismsReview patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\CriticismsReview[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\CriticismsReview findOrCreate($search, callable $callback = null, $options = [])
 */
class CriticismsReviewsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('criticas.Reviews');
        $this->setDisplayField('description');
        $this->setPrimaryKey('review_id');

        $this->belongsTo('Tables', [
            'foreignKey' => 'table_id',
            'joinType' => 'INNER'
        ]);
        
        $this->hasMany('CriticismsRules', [
            'foreignKey' => 'review_id',
            'bindingKey' => 'review_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('review_id')
            ->allowEmpty('review_id', 'create');

        $validator
            ->scalar('description')
            ->maxLength('description', 200)
            ->requirePresence('description', 'create')
            ->notEmpty('description');

        $validator
            ->scalar('created_for')
            ->maxLength('created_for', 100)
            ->requirePresence('created_for', 'create')
            ->notEmpty('created_for');

        $validator
            ->scalar('updated_for')
            ->maxLength('updated_for', 100)
            ->requirePresence('updated_for', 'create')
            ->notEmpty('updated_for');

        $validator
            ->dateTime('creation_date')
            ->requirePresence('creation_date', 'create')
            ->notEmpty('creation_date');

        $validator
            ->dateTime('updated_date')
            ->requirePresence('updated_date', 'create')
            ->notEmpty('updated_date');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['table_id'], 'Tables'));

        return $rules;
    }

    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName()
    {
        return 'IT_WORKFLOW_36';
    }
}
