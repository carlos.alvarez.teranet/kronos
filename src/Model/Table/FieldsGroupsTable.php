<?php
	
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\I18n\Time;
use Cake\Validation\Validator;

class FieldsGroupsTable extends Table
{
	
	public function initialize(array $config)
    {
        $this->table('Fusion_Fields_Groups');
        $this->primaryKey('Id');
        $this->hasMany('Fields', [
	        'foreignKey' => 'FieldsGroup_Id'
        ]);
    }
    
    public function beforeSave($event, $entity, $options)
	{
		// Default para DtCriacao
	    if($entity->isNew() && !isset($entity->DtCriacao))
	    {
		    $now = Time::now();
		    $now = $now->format('Y-m-d\TH:i:s');
	        $entity->DtCriacao = $now;
	    }
	    
	    // Default para DtAtualizacao
	    if($entity->isNew() && !isset($entity->DtAtualizacao))
	    {
		    $now = Time::now();
		    $now = $now->format('Y-m-d\TH:i:s');
	        $entity->DtAtualizacao = $now;
	    }
	    
	    // Default para CreatedBy
	    if($entity->isNew() && !isset($entity->CreatedBy))
	    {
	        $entity->CreatedBy = $_SESSION['Auth']['User']['cn'][0];
	    }
	    
	    return true;
	}
	
}	
?>