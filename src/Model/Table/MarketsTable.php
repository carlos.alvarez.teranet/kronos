<?php
	
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\I18n\Time;
use Cake\Validation\Validator;

class MarketsTable extends Table
{
	
	public function initialize(array $config)
    {
        $this->table('A_Markets');
        $this->primaryKey('IdMarket');
        $this->belongsToMany('Subs', [
	        		'joinTable' => 'A_Market_Products',
	        		'targetForeignKey' => ['IdProduto', 'IdSub'],
	        		'foreignKey' => 'IdMarket',
	        		'saveStrategy' => 'append'
        		])
        		->setStrategy('select');
        $this->belongsTo('Flags')
        		->setForeignKey('FlgDesativacao')
        		->setBindingKey('code')
        		->setStrategy('select');
    }
    
    public function beforeSave($event, $entity, $options)
	{
		// Default para DtCriacao
	    if($entity->isNew() && !isset($entity->DtCriacao))
	    {
		    $now = Time::now();
		    $now = $now->format('Y-m-d\TH:i:s');
	        $entity->DtCriacao = $now;
	    }
	    
	    // Default para DtAtualizacao
	    if($entity->isNew() && !isset($entity->DtAtualizacao))
	    {
		    $now = Time::now();
		    $now = $now->format('Y-m-d\TH:i:s');
	        $entity->DtAtualizacao = $now;
	    }
	    return true;
	}
	
	public function validationDefault(Validator $validator)
    {
	    $validator
        	->requirePresence('Descricao')
			->notEmpty('Descricao', 'Favor complete nombre de cliente');

        return $validator;
    }
	
}	
?>