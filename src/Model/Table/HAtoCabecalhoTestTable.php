<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * HAtoCabecalhoTest Model
 *
 * @method \App\Model\Entity\HAtoCabecalhoTest get($primaryKey, $options = [])
 * @method \App\Model\Entity\HAtoCabecalhoTest newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\HAtoCabecalhoTest[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\HAtoCabecalhoTest|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\HAtoCabecalhoTest patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\HAtoCabecalhoTest[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\HAtoCabecalhoTest findOrCreate($search, callable $callback = null, $options = [])
 */
class HAtoCabecalhoTestTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('criticas.HAto_Cabecalho_test');
        $this->setDisplayField('IdHato_Cabecalho');
        $this->setPrimaryKey('IdHato_Cabecalho');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->requirePresence('IdHato_Cabecalho', 'create')
            ->notEmpty('IdHato_Cabecalho');

        $validator
            ->requirePresence('Semana', 'create')
            ->notEmpty('Semana');

        $validator
            ->integer('idDomicilio')
            ->requirePresence('idDomicilio', 'create')
            ->notEmpty('idDomicilio');

        $validator
            ->requirePresence('idIndividuo', 'create')
            ->notEmpty('idIndividuo');

        $validator
            ->dateTime('Data_Compra')
            ->requirePresence('Data_Compra', 'create')
            ->notEmpty('Data_Compra');

        $validator
            ->integer('idCanal')
            ->requirePresence('idCanal', 'create')
            ->notEmpty('idCanal');

        $validator
            ->integer('idArtigo')
            ->requirePresence('idArtigo', 'create')
            ->notEmpty('idArtigo');

        $validator
            ->dateTime('Data_Processamento')
            ->allowEmpty('Data_Processamento');

        $validator
            ->dateTime('Data_Criacao')
            ->requirePresence('Data_Criacao', 'create')
            ->notEmpty('Data_Criacao');

        $validator
            ->dateTime('Data_Compra_Utilizada')
            ->allowEmpty('Data_Compra_Utilizada');

        $validator
            ->integer('idProduto')
            ->requirePresence('idProduto', 'create')
            ->notEmpty('idProduto');

        $validator
            ->integer('idSub')
            ->requirePresence('idSub', 'create')
            ->notEmpty('idSub');

        $validator
            ->integer('idMarca')
            ->requirePresence('idMarca', 'create')
            ->notEmpty('idMarca');

        $validator
            ->integer('Quantidade')
            ->requirePresence('Quantidade', 'create')
            ->notEmpty('Quantidade');

        $validator
            ->integer('idConteudo')
            ->requirePresence('idConteudo', 'create')
            ->notEmpty('idConteudo');

        $validator
            ->integer('idPromocao')
            ->requirePresence('idPromocao', 'create')
            ->notEmpty('idPromocao');

        $validator
            ->decimal('Preco_Unitario')
            ->requirePresence('Preco_Unitario', 'create')
            ->notEmpty('Preco_Unitario');

        $validator
            ->scalar('Usuario')
            ->maxLength('Usuario', 10)
            ->requirePresence('Usuario', 'create')
            ->notEmpty('Usuario');

        $validator
            ->requirePresence('Tipo_Ato', 'create')
            ->notEmpty('Tipo_Ato');

        $validator
            ->scalar('Usuario_Autorizacao')
            ->maxLength('Usuario_Autorizacao', 10)
            ->allowEmpty('Usuario_Autorizacao');

        $validator
            ->scalar('CodBarr')
            ->maxLength('CodBarr', 13)
            ->allowEmpty('CodBarr');

        $validator
            ->scalar('Desc_Prod')
            ->maxLength('Desc_Prod', 50)
            ->allowEmpty('Desc_Prod');

        $validator
            ->scalar('Desc_Canal')
            ->maxLength('Desc_Canal', 20)
            ->allowEmpty('Desc_Canal');

        $validator
            ->scalar('Peso')
            ->maxLength('Peso', 10)
            ->allowEmpty('Peso');

        $validator
            ->scalar('Data_inv')
            ->maxLength('Data_inv', 10)
            ->allowEmpty('Data_inv');

        $validator
            ->integer('Id_Cabec')
            ->allowEmpty('Id_Cabec');

        $validator
            ->integer('Id_Shop')
            ->allowEmpty('Id_Shop');

        $validator
            ->integer('Id_Item')
            ->allowEmpty('Id_Item');

        $validator
            ->decimal('Preco_Total')
            ->allowEmpty('Preco_Total');

        $validator
            ->integer('Forma_pagto')
            ->allowEmpty('Forma_pagto');

        $validator
            ->boolean('flgShopMis')
            ->allowEmpty('flgShopMis');

        $validator
            ->integer('idPais')
            ->requirePresence('idPais', 'create')
            ->notEmpty('idPais');

        $validator
            ->allowEmpty('id_viaje');

        $validator
            ->integer('id_equipo')
            ->allowEmpty('id_equipo');

        $validator
            ->dateTime('Data_Correcao')
            ->allowEmpty('Data_Correcao');

        $validator
            ->integer('idApp')
            ->allowEmpty('idApp');

        $validator
            ->integer('flg_critics')
            ->allowEmpty('flg_critics');

        return $validator;
    }

    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName()
    {
        return 'IT_WORKFLOW_36';
    }
}
