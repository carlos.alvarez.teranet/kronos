<?php
	
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\I18n\Time;
use Cake\Validation\Validator;

class JoinsTable extends Table
{
	
	public function initialize(array $config)
    {
        $this->table('Fusion_Joins');
        $this->primaryKey('Id');
        $this->belongsTo('Countries');
    }
    
    public function beforeSave($event, $entity, $options)
	{
		// Default para DtCriacao
	    if($entity->isNew() && !isset($entity->DtCriacao))
	    {
		    $now = Time::now();
		    $now = $now->format('Y-m-d\TH:i:s');
	        $entity->DtCriacao = $now;
	    }
	    
	    // Default para DtAtualizacao
	    if($entity->isNew() && !isset($entity->DtAtualizacao))
	    {
		    $now = Time::now();
		    $now = $now->format('Y-m-d\TH:i:s');
	        $entity->DtAtualizacao = $now;
	    }
	    
	    // Default para CreatedBy
	    if($entity->isNew() && !isset($entity->CreatedBy))
	    {
	        $entity->CreatedBy = $_SESSION['Auth']['User']['displayName'];
	    }
	    
	    return true;
	}
	
	public function validationDefault(Validator $validator)
    {
	    $validator
        	->requirePresence('Country_id', 'create')
			->notEmpty('Country_id', 'Favor complete información de país')
			->requirePresence('Field_Ind', 'create')
			->notEmpty('Field_Ind', 'Favor complete información de tabla individuos')
			->requirePresence('Operator', 'create')
			->notEmpty('Operator', 'Favor complete el operador')
			->requirePresence('Field_Ind', 'create')
			->notEmpty('Field_Pan', 'Favor complete información de tabla panel');

        return $validator;
    }
	
}	
?>