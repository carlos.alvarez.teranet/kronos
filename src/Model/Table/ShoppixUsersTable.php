<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ShoppixUsers Model
 *
 * @method \App\Model\Entity\ShoppixUser get($primaryKey, $options = [])
 * @method \App\Model\Entity\ShoppixUser newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ShoppixUser[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ShoppixUser|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ShoppixUser patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ShoppixUser[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ShoppixUser findOrCreate($search, callable $callback = null, $options = [])
 */
class ShoppixUsersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('AppUser');
        $this->setDisplayField('Email');
        $this->setPrimaryKey('Id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->uuid('Id')
            ->allowEmpty('Id', 'create');

        $validator
            ->scalar('Email')
            ->maxLength('Email', 320)
            ->allowEmpty('Email');

        $validator
            ->scalar('PasswordHash')
            ->maxLength('PasswordHash', 1024)
            ->requirePresence('PasswordHash', 'create')
            ->notEmpty('PasswordHash');

        $validator
            ->scalar('TemporaryPasswordHash')
            ->maxLength('TemporaryPasswordHash', 1024)
            ->allowEmpty('TemporaryPasswordHash');

        $validator
            ->scalar('TemporaryEmail')
            ->maxLength('TemporaryEmail', 320)
            ->allowEmpty('TemporaryEmail');

        $validator
            ->boolean('IsUserDisabled')
            ->requirePresence('IsUserDisabled', 'create')
            ->notEmpty('IsUserDisabled');

        $validator
            ->dateTime('UserDisabledDate')
            ->allowEmpty('UserDisabledDate');

        $validator
            ->boolean('IsAdminDisabled')
            ->requirePresence('IsAdminDisabled', 'create')
            ->notEmpty('IsAdminDisabled');

        $validator
            ->dateTime('AdminDisabledDate')
            ->allowEmpty('AdminDisabledDate');

        $validator
            ->scalar('ReferralCode')
            ->maxLength('ReferralCode', 1024)
            ->allowEmpty('ReferralCode');

        $validator
            ->scalar('ReferrerCode')
            ->maxLength('ReferrerCode', 1024)
            ->allowEmpty('ReferrerCode');

        $validator
            ->dateTime('RegisteredDate')
            ->requirePresence('RegisteredDate', 'create')
            ->notEmpty('RegisteredDate');

        $validator
            ->scalar('RefreshToken')
            ->maxLength('RefreshToken', 1024)
            ->allowEmpty('RefreshToken');

        $validator
            ->dateTime('LastGeneralReferralsReminderDate')
            ->allowEmpty('LastGeneralReferralsReminderDate');

        $validator
            ->dateTime('LastCompleteProfileReminderDate')
            ->allowEmpty('LastCompleteProfileReminderDate');

        $validator
            ->boolean('IsFirstEmailConfirmed')
            ->requirePresence('IsFirstEmailConfirmed', 'create')
            ->notEmpty('IsFirstEmailConfirmed');

        $validator
            ->integer('LoginAttemptsCount')
            ->requirePresence('LoginAttemptsCount', 'create')
            ->notEmpty('LoginAttemptsCount');

        $validator
            ->dateTime('BlockDateTime')
            ->allowEmpty('BlockDateTime');

        $validator
            ->dateTime('CreatedDateTime')
            ->requirePresence('CreatedDateTime', 'create')
            ->notEmpty('CreatedDateTime');

        $validator
            ->scalar('CreatedBy')
            ->maxLength('CreatedBy', 300)
            ->allowEmpty('CreatedBy');

        $validator
            ->dateTime('UpdatedDateTime')
            ->requirePresence('UpdatedDateTime', 'create')
            ->notEmpty('UpdatedDateTime');

        $validator
            ->scalar('UpdatedBy')
            ->maxLength('UpdatedBy', 300)
            ->allowEmpty('UpdatedBy');

        $validator
            ->integer('ShortId')
            ->requirePresence('ShortId', 'create')
            ->notEmpty('ShortId')
            ->add('ShortId', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->dateTime('ResetPasswordDate')
            ->allowEmpty('ResetPasswordDate');

        $validator
            ->dateTime('LastReceiptSubmissionDateTime')
            ->allowEmpty('LastReceiptSubmissionDateTime');

        $validator
            ->scalar('EReceiptAccountId')
            ->maxLength('EReceiptAccountId', 100)
            ->allowEmpty('EReceiptAccountId');

        $validator
            ->integer('EReceiptRegistrationStatus')
            ->allowEmpty('EReceiptRegistrationStatus');

        $validator
            ->dateTime('EReceiptRegistrationStatusChangedDateTime')
            ->allowEmpty('EReceiptRegistrationStatusChangedDateTime');

        $validator
            ->boolean('EReceiptRegistrationRenewalSent')
            ->allowEmpty('EReceiptRegistrationRenewalSent');

        $validator
            ->scalar('WebSiteRefreshToken')
            ->maxLength('WebSiteRefreshToken', 1024)
            ->allowEmpty('WebSiteRefreshToken');

        $validator
            ->dateTime('WebSiteBlockDateTime')
            ->allowEmpty('WebSiteBlockDateTime');

        $validator
            ->integer('WebSiteLoginAttemptsCount')
            ->requirePresence('WebSiteLoginAttemptsCount', 'create')
            ->notEmpty('WebSiteLoginAttemptsCount');

        $validator
            ->dateTime('EReceiptRegistrationExpirationDate')
            ->allowEmpty('EReceiptRegistrationExpirationDate');

        $validator
            ->dateTime('EReceiptRegistrationDateTime')
            ->allowEmpty('EReceiptRegistrationDateTime');

        $validator
            ->scalar('EReceiptRegistrationStatusUpdatedBy')
            ->maxLength('EReceiptRegistrationStatusUpdatedBy', 300)
            ->allowEmpty('EReceiptRegistrationStatusUpdatedBy');

        $validator
            ->boolean('EReceiptConsent')
            ->allowEmpty('EReceiptConsent');

        $validator
            ->dateTime('EReceiptConsentDateTime')
            ->allowEmpty('EReceiptConsentDateTime');

        $validator
            ->dateTime('ConfirmationMailDateTime')
            ->allowEmpty('ConfirmationMailDateTime');

        $validator
            ->scalar('FaceBookId')
            ->maxLength('FaceBookId', 200)
            ->allowEmpty('FaceBookId');

        $validator
            ->scalar('ActiveSubmissionEmail')
            ->maxLength('ActiveSubmissionEmail', 150)
            ->allowEmpty('ActiveSubmissionEmail');

        $validator
            ->boolean('ActiveSubmissionAccess')
            ->allowEmpty('ActiveSubmissionAccess');

        return $validator;
    }
    
    public function summary()
    {
	    return $this->find('all', [
			'fields' => [
				'total' => 'COUNT(*)',
				'referred' => 'SUM(CASE WHEN ReferralCode IS NOT NULL THEN 1 ELSE 0 END)',
				'confirmed' => 'SUM(CASE WHEN IsFirstEmailConfirmed = 1 THEN 1 ELSE 0 END)'
			]
		])->first();
    }
    
    public function total_chart() {
	    
	    return $this->find('all', [
		    'fields' => [
		    	'label' => "CAST(DATEPART(week, RegisteredDate) as VARCHAR) + '-' + CAST(YEAR(RegisteredDate) as VARCHAR)",
		    	'value' => 'COUNT(*)'
		    ],
		    'group' => ['DATEPART(week, RegisteredDate)', 'YEAR(RegisteredDate)'],
		    'sort' => ['YEAR(RegisteredDate)', 'DATEPART(week, RegisteredDate)']
		]);
	    
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['ShortId']));

        return $rules;
    }
}
