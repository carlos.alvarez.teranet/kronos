<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * UroborosResolutions Model
 *
 * @method \App\Model\Entity\UroborosResolution get($primaryKey, $options = [])
 * @method \App\Model\Entity\UroborosResolution newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\UroborosResolution[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\UroborosResolution|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\UroborosResolution patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\UroborosResolution[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\UroborosResolution findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class UroborosResolutionsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('WF_PROCESS_RESOLUTION');
        $this->setDisplayField('rid');
        $this->setPrimaryKey('rid');

        $this->addBehavior('Timestamp');
        
        $this->belongsTo('UroborosProcessControls', [
	        'foreignKey' => 'eid',
	        'bindingKey' => 'eid'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('rid')
            ->allowEmpty('rid', 'create');

        $validator
            ->integer('eid')
            ->requirePresence('eid', 'create')
            ->notEmpty('eid');

        $validator
            ->scalar('gid')
            ->maxLength('gid', 32)
            ->requirePresence('gid', 'create')
            ->notEmpty('gid');

        $validator
            ->scalar('comments')
            ->maxLength('comments', 100)
            ->requirePresence('comments', 'create')
            ->notEmpty('comments');

        return $validator;
    }

    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName()
    {
        return 'IT_WORKFLOW';
    }
}
