<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * UroborosProcesses Model
 *
 * @method \App\Model\Entity\UroborosProcess get($primaryKey, $options = [])
 * @method \App\Model\Entity\UroborosProcess newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\UroborosProcess[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\UroborosProcess|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\UroborosProcess patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\UroborosProcess[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\UroborosProcess findOrCreate($search, callable $callback = null, $options = [])
 */
class UroborosProcessesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('WF_PROCESS');
        $this->setDisplayField('description');
        $this->setPrimaryKey('pid');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('pid')
            ->allowEmpty('pid', 'create');

        $validator
            ->integer('tid')
            ->requirePresence('tid', 'create')
            ->notEmpty('tid');

        $validator
            ->integer('cid')
            ->requirePresence('cid', 'create')
            ->notEmpty('cid');

        $validator
            ->scalar('description')
            ->allowEmpty('description');

        return $validator;
    }

    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName()
    {
        return 'IT_WORKFLOW';
    }
}
