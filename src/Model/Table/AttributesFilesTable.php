<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use PHPExcel;
use PHPExcel_IOFactory;

/**
 * AttributesFiles Model
 *
 * @method \App\Model\Entity\AttributesFile get($primaryKey, $options = [])
 * @method \App\Model\Entity\AttributesFile newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\AttributesFile[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\AttributesFile|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\AttributesFile patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\AttributesFile[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\AttributesFile findOrCreate($search, callable $callback = null, $options = [])
 */
class AttributesFilesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('ClientAttribute_Files');
        $this->setDisplayField('Id');
        $this->setPrimaryKey('Id');
        
        $this->belongsTo('Attributes', [
	        'foreignKey' => 'IdClientAttribute',
	        'bindingKey' => 'ClientAttribute_id'
        ]);
    }

    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName()
    {
        return 'IT_WORKFLOW';
    }
    
    public function uploadRead($file = null, $flag = false) {
	    
	    if($flag) {
		    $destFile = $file;
	    } else {
		    if(!is_uploaded_file($file['tmp_name'])){
		    	return false;
		    }
		    
		    $destFile = WWW_ROOT . 'excel' . DS . uniqid() . '_' . $file['name'];
		    if (!move_uploaded_file($file['tmp_name'], $destFile)){
		    	return false;
		    }
	    }
	    
	    $inputFileName = $destFile;

		//  Leer archivo guardado previamente en las carpetas de webroot
		try {
		    $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
		    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
		    $objPHPExcel = $objReader->load($inputFileName);
		} catch(Exception $e) {
		    die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
		}
		
		$sheet = $objPHPExcel->getSheet(0);
		
		$initialRow = 7;
		$highestRow = $sheet->getHighestRow(); 
		$initialColumn = 1;
		$highestColumn = $sheet->getHighestColumn(); 
		
		$rowData = $sheet->rangeToArray('A' . $initialRow . ':' . $highestColumn . $highestRow,
		                                    NULL,
		                                    TRUE,
		                                    FALSE);
		                                    
		$headerData = $sheet->rangeToArray('A' . ($initialRow - 1) . ':' . $highestColumn . ($initialRow - 1),
		                                    NULL,
		                                    TRUE,
		                                    FALSE)[0];
		                                    
		$data = [];
		foreach($rowData as $items) {
			$aux = [];
			foreach($items as $key => $value) {
				$head = ($headerData[$key]) ? $headerData[$key] : 'Original';
				$aux[$head] = $value;
			}
			$data[] = $aux;
		}
		
		return ['data' => $data, 'destFile' => $destFile];
	    
    }
}
