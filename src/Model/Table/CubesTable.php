<?php
	
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\I18n\Time;
use Cake\Validation\Validator;

class CubesTable extends Table
{
	
	public function initialize(array $config)
    {
        $this->table('WPO_Cubes');
        $this->primaryKey('id');
    }
    
    public static function defaultConnectionName()
    {
        return 'IT_WORKFLOW_33';
    }
	
}	
?>