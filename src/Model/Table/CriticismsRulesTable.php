<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * CriticismsRules Model
 *
 * @property \App\Model\Table\ReviewsTable|\Cake\ORM\Association\BelongsTo $Reviews
 *
 * @method \App\Model\Entity\CriticismsRule get($primaryKey, $options = [])
 * @method \App\Model\Entity\CriticismsRule newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\CriticismsRule[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\CriticismsRule|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CriticismsRule patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\CriticismsRule[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\CriticismsRule findOrCreate($search, callable $callback = null, $options = [])
 */
class CriticismsRulesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('criticas.Rules');
        $this->setDisplayField('description');
        $this->setPrimaryKey('rule_id');

        $this->belongsTo('Reviews', [
            'foreignKey' => 'review_id',
            'joinType' => 'INNER'
        ]);
        
        $this->hasMany('CriticismsActions', [
            'foreignKey' => 'rule_id',
            'bindingKey' => 'rule_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('rule_id')
            ->allowEmpty('rule_id', 'create');

        $validator
            ->integer('idProduto')
            ->allowEmpty('idProduto');

        $validator
            ->integer('idSub')
            ->allowEmpty('idSub');

        $validator
            ->scalar('description')
            ->maxLength('description', 200)
            ->requirePresence('description', 'create')
            ->notEmpty('description');

        $validator
            ->scalar('sentence')
            ->requirePresence('sentence', 'create')
            ->notEmpty('sentence');

        $validator
            ->allowEmpty('status');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['review_id'], 'Reviews'));

        return $rules;
    }

    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName()
    {
        return 'IT_WORKFLOW_36';
    }
}
