<?php
	
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\I18n\Time;
use Cake\Validation\Validator;

class CargaproductivacuboTable extends Table
{
	
	public function initialize(array $config)
    {
        $this->table('WPO_CargaProductivaCubo');
        $this->primaryKey('IdCargaProductivaCubo');
    }
    
    public static function defaultConnectionName()
    {
        return 'IT_WORKFLOW_33';
    }
	
}	
?>