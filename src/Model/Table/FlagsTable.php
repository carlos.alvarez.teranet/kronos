<?php
	
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\I18n\Time;
use Cake\Validation\Validator;

class FlagsTable extends Table
{
	
	public function initialize(array $config)
    {
        $this->table('SYS_FlgDesativacao');
        $this->primaryKey('id');
    }
	
	public function validationDefault(Validator $validator)
    {
	    $validator
        	->requirePresence('code')
			->notEmpty('code', 'Favor complete código')
			->requirePresence('description')
			->notEmpty('description', 'Favor comente respecto el flag (status)')
			->requirePresence('active');

        return $validator;
    }
	
}	
?>