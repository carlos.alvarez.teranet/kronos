<?php
	
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\I18n\Time;
use Cake\Validation\Validator;

class AttributesTable extends Table
{
	
	public function initialize(array $config)
    {
        $this->table('A_ClientAttribute');
        $this->primaryKey('IdClientAttribute');
        $this->belongsTo('Flags')
        		->setForeignKey('FlgDesativacao')
        		->setBindingKey('code')
        		->setStrategy('select');
        $this->belongsTo('Clients')
        		->setForeignKey('IdClient');
		$this->belongsTo('Markets')
        		->setForeignKey('IdMarket');
		$this->belongsToMany('Dimensions', [
					'joinTable' => 'A_ClientAttribute_Dimensions',
		    		'targetForeignKey' => ['IdDimension'],
		    		'foreignKey' => 'IdClientAttribute',
		    		'saveStrategy' => 'append',
		    		'sort' => ['Orden' => 'ASC']
				]);
		$this->hasMany('Owners')
        		->setForeignKey('IdClientAttribute')
        		->setStrategy('select');
        $this->hasOne('AttributesPendants', [
			        'foreignKey' => 'ClientAttribute_id',
		        ])->setStrategy('select');
    }
    
    public function beforeSave($event, $entity, $options)
	{
		// Default para DtCriacao
	    if($entity->isNew() && !isset($entity->DtCriacao))
	    {
		    $now = Time::now();
		    $now = $now->format('Y-m-d\TH:i:s');
	        $entity->DtCriacao = $now;
	    }
	    
	    // Default para DtAtualizacao
	    if($entity->isNew() && !isset($entity->DtAtualizacao))
	    {
		    $now = Time::now();
		    $now = $now->format('Y-m-d\TH:i:s');
	        $entity->DtAtualizacao = $now;
	    }
	    return true;
	}
	
	public function validationDefault(Validator $validator)
    {
	    $validator
        	->requirePresence('Descricao')
			->notEmpty('Descricao', 'Favor complete nombre de atributo')
			->requirePresence('IdClient')
			->notEmpty('IdClient', 'Favor seleccione cliente')
			->requirePresence('IdMarket')
			->notEmpty('IdMarket', 'Favor seleccione mercado');

        return $validator;
    }
	
}	
?>