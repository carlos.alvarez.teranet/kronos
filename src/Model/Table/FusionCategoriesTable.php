<?php
	
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\I18n\Time;
use Cake\Validation\Validator;

class FusionCategoriesTable extends Table
{
	
	public function initialize(array $config)
    {
        $this->table('Fusion_Categories');
        $this->primaryKey('Id');
        $this->setDisplayField('Description');
        $this->belongsTo('Countries');
    }
    
    public function beforeSave($event, $entity, $options)
	{
	    
	    // Default para CreatedBy
	    if($entity->isNew() && !isset($entity->CreatedBy))
	    {
	        $entity->CreatedBy = $_SESSION['Auth']['User']['displayName'];
	    }
	    
	    return true;
	}
	
	public static function defaultConnectionName()
    {
        return 'IT_WORKFLOW';
    }
	
}	
?>