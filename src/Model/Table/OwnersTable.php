<?php
	
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\I18n\Time;
use Cake\Validation\Validator;

class OwnersTable extends Table
{
	
	public function initialize(array $config)
    {
        $this->table('A_ClientAttribute_Owner');
    
    }
	
}	
?>