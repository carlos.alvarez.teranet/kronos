<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * UroborosStatuses Model
 *
 * @method \App\Model\Entity\UroborosStatus get($primaryKey, $options = [])
 * @method \App\Model\Entity\UroborosStatus newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\UroborosStatus[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\UroborosStatus|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\UroborosStatus patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\UroborosStatus[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\UroborosStatus findOrCreate($search, callable $callback = null, $options = [])
 */
class UroborosStatusesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('WF_STATUS');
        $this->setDisplayField('name');
        $this->setPrimaryKey('code');
    }

    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName()
    {
        return 'IT_WORKFLOW';
    }
}
