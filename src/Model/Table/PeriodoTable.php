<?php
	
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\I18n\Time;
use Cake\Validation\Validator;

class PeriodoTable extends Table
{
	
	public function initialize(array $config)
    {
        $this->table('Periodo');
        $this->primaryKey('IdPeriodo');
        $this->hasMany('WPO_CargaProductiva', [
            'foreignKey' => 'IdPeriodo',
        ]);
    }
    
    public static function defaultConnectionName()
    {
        return 'IT_WORKFLOW_33';
    }
	
}	
?>