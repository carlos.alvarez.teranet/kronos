<?php
	
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\I18n\Time;
use Cake\Validation\Validator;

class ExecutionsTable extends Table
{
	
	public function initialize(array $config)
    {
        $this->table('Fusion_Executions');
        $this->primaryKey('Id');
        $this->belongsTo('Countries');
    }
    
    public function beforeSave($event, $entity, $options)
	{
		// Default para DtCriacao
	    if($entity->isNew() && !isset($entity->DtCriacao))
	    {
		    $now = Time::now();
		    $now = $now->format('Y-m-d\TH:i:s');
	        $entity->DtCriacao = $now;
	    }
	    
	    // Default para DtAtualizacao
	    if(!isset($entity->DtAtualizacao))
	    {
		    $now = Time::now();
		    $now = $now->format('Y-m-d\TH:i:s');
	        $entity->DtAtualizacao = $now;
	    }
	    
	    // Default para CreatedBy
	    if($entity->isNew() && !isset($entity->CreatedBy))
	    {
	        $entity->CreatedBy = $_SESSION['Auth']['User']['displayName'];
	    }
	    
	    return true;
	}
	
	public function validationDefault(Validator $validator)
    {
	    $validator
        	->requirePresence('Descricao', 'create')
			->notEmpty('Descricao', 'Favor complete nombre de cliente')
			->requirePresence('Ano', 'create')
			->notEmpty('Ano', 'Favor complete año')
			->requirePresence('Mes', 'create')
			->notEmpty('Mes', 'Favor complete mes')
			->requirePresence('Country_id', 'create')
			->notEmpty('Country_id', 'Favor indique país');

        return $validator;
    }
	
}	
?>