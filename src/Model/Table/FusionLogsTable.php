<?php
	
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\I18n\Time;
use Cake\Validation\Validator;

class FusionLogsTable extends Table
{
	
	public function initialize(array $config)
    {
        $this->table('Fusion_Logs');
        $this->primaryKey('Id');
        $this->belongsTo('Executions');
    }
    
    public function beforeSave($event, $entity, $options)
	{
	    
	    // Default para CreatedBy
	    if($entity->isNew() && !isset($entity->CreatedBy))
	    {
	        $entity->CreatedBy = $_SESSION['Auth']['User']['displayName'];
	    }
	    
	    return true;
	}
	
	public function validationDefault(Validator $validator)
    {
	    $validator
        	->requirePresence('Country_id', 'create')
			->notEmpty('Country_id', 'Favor complete país')
			->requirePresence('Group_id', 'create')
			->notEmpty('Group_id', 'Favor complete grupo')
			->requirePresence('Type_id', 'create')
			->notEmpty('Type_id', 'Favor complete tipo')
			->requirePresence('Execution_id', 'create')
			->notEmpty('Execution_id', 'Favor complete código de ejecución')
			->requirePresence('Descricao', 'create')
			->notEmpty('Descricao', 'Favor complete descripción');

        return $validator;
    }
	
}	
?>