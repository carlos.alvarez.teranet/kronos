<?php
	
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\I18n\Time;
use Cake\Validation\Validator;

class SubsTable extends Table
{

	public function initialize(array $config)
    {
        $this->table('A_SubProduto');
        $this->primaryKey(['IdProduto', 'IdSub']);
        $this->belongsTo('Products')
        		->setForeignKey('IdProduto');
    }
	
}	
?>