<?php
namespace App\View\Helper;

use Cake\View\Helper;
use Cake\View\View;

/**
 * Crypt helper
 */
class CryptHelper extends Helper
{

    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [];
    
    public function toLetters($number = null) {
	    
	    if($number) {
		    
		    $number 	= ($number * 189) + 2354;
		    $letters 	= array_combine(range(0,25), range('a', 'z'));
		    
		    $array 		= str_split($number);
		    
		    $result = [];
		    foreach($array as $digit) {
			    $result[] = $letters[$digit];
		    }
		    
		    return implode('', $result); 
		    		    
	    } else {
		    return false;
	    }
	    
    }
    
    public function toNumbers($letters = null) {
	    
	    if($letters) {
		    
		    $numbers 	= array_combine(range('a', 'z'), range(1,26));
		    $array		= str_split($letters);
		    
		    $result = [];
		    foreach($array as $letter) {
			    $result[] = $numbers[$letter];
		    }
		    
		    return (((int)implode('', $result)) - 2354) / 189;
		    		    
	    } else {
		    return false;
	    }
	    
    }

}
