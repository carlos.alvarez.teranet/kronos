<?php
namespace App\View\Helper;

use Cake\View\Helper;
use Cake\View\View;

/**
 * Valid helper
 */
class ValidHelper extends Helper
{

    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [];
    
    public function checkPermission($prefix = null, $controller = null, $action = null) {
	    
	    $permissions = $this->request->session()->read('Auth.User.permissions');
        
        $valid = [
	        'prefix' => 0,
	        'controller' => 0,
	        'action' => 0
        ];
        
        foreach($permissions as $permission) {
	        
	        if(is_null($action)) {
		    	$valid['action'] = 1;   
	        } else if( strtolower($action) == strtolower($permission['action']) ) {
		        $valid['action'] = 1;
	        }
	        
	        if(is_null($controller)) {
		    	$valid['controller'] = 1;   
	        } else if( strtolower($controller) == strtolower($permission['controller']) ) {
		        $valid['controller'] = 1;
	        }
	        
	        if(is_null($prefix)) {
		    	$valid['prefix'] = 1;   
	        } else if( strtolower($prefix) == strtolower($permission['prefix']) ) {
		        $valid['prefix'] = 1;
	        }
	        
	        if(array_sum($valid) == 3) {
		        return true;
	        } else {
		        $valid['action'] = 0;
		        $valid['controller'] = 0;
		        $valid['prefix'] = 0;
	        }
        }
        
        return false;
	    
    }

}
