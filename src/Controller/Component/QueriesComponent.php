<?
namespace App\Controller\Component;

use Cake\Controller\Component;

class QueriesComponent extends Component
{
    public function homologation($execution, $group)
    {
	    $execution = $execution->toArray();
	    
	    $query = "IF OBJECT_ID('dbo.Fusion_Web_Homologacion_".$group."_".$execution['Id']."', 'U') IS NOT NULL DROP TABLE dbo.Fusion_Web_Homologacion_".$group."_".$execution['Id'].";";
	    
        $query .= "SELECT ";
        
        
        foreach($execution['country']['homologation']['fields'] as $field) {
	        $query .= ((empty($field[$group])) ? '1' : 'Ind.'.$field[$group])." AS ".$field['Description'].", ";
	    }
	    
	    
	    $array = [];
	    foreach($execution['country']['clusters'] as $key => $cluster) {
		    $array[] .= " CASE ";
		    foreach($cluster[strtolower($group)."s"] as $item) {
			    $array[$key] .= " WHEN Pan.".$item['Field']." = ".$item['Valor']." THEN ".$item['NGroup'];
			}
			$array[$key] .= " END AS ".$cluster['Descricao'];
		}
		$query .= implode(", ", $array);
		
		$query .= " INTO Fusion_Web_Homologacion_".$group."_".$execution['Id'];
		
		
		$query .= " FROM ".$execution['country']['parameters']['DbInd'.$group]['Value'].".dbo.".$execution['country']['parameters']['TbInd'.$group]['Value']." Ind";
		$query .= " JOIN ".$execution['country']['parameters']['DbPan'.$group]['Value'].".dbo.".$execution['country']['parameters']['TbPan'.$group]['Value']." Pan ON";
		
		
		// Join Section
		$array = [];
		foreach($execution['country']['joins'] as $join) {
			if ($join['IdType'] == $group) {
				$array[] = ( ($join['Function_Ind']) ? $join['Function_Ind'].'(' : '')." Ind.".$join['Field_Ind'].( ($join['Function_Ind']) ? ')' : '')." = ".( ($join['Function_Pan']) ? $join['Function_Pan'].'(' : '')."Pan.".$join['Field_Pan'].( ($join['Function_Pan']) ? ')' : '');
			}
		}
		$query .= implode(" AND ", $array);
		
		
		$query .= " WHERE ";
		$array = [];
		
		$key = array_search('Ano', array_column($execution['country']['homologation']['fields'], 'Description')); 
	    $array[] = " Ind.".$execution['country']['homologation']['fields'][$key][$group]." = ".$execution['Ano'];
	    
	    $key = array_search('Mes', array_column($execution['country']['homologation']['fields'], 'Description')); 
	    $array[] = " Ind.".$execution['country']['homologation']['fields'][$key][$group]." = ".$execution['Mes'];
	    
	    $array[] = " Pan.".$execution['country']['parameters']['CiDat'.$group]['Value']." IN (".$execution['country']['parameters']['ClDat'.$group]['Value'].")";
	    $array[] = " Pan.".$execution['country']['parameters']['EdDat'.$group]['Value']." BETWEEN ".$execution['country']['parameters']['E1Dat'.$group]['Value']." AND ".$execution['country']['parameters']['E2Dat'.$group]['Value'];
		$query .= implode(" AND ", $array);
        
        return $query;
    }
    
    public function imputar($execution, $group) {
	    
	    $execution = $execution->toArray();
	    
	    $query = " DELETE ".$execution['country']['parameters']['DbBfu'.$group]['Value'].".dbo.".$execution['country']['parameters']['TbBfu'.$group]['Value']." WHERE Id IS NOT NULL AND Ano = ".$execution['Ano']." AND mes = ".$execution['Mes'].";";
	    $query .= " INSERT INTO ".$execution['country']['parameters']['DbBfu'.$group]['Value'].".dbo.".$execution['country']['parameters']['TbBfu'.$group]['Value'];
	    $query .= " (IdArtigo, IdProduto, Coef1, Produto, Sub, idato, idhato_Cabecalho, individuo, Ano, mes, Quantidade, Preco_Unitario, Coef_Oficial, Data_Compra, Id, veces_final, New_Peso, Dom, Ind, Peso)";
	    
	    $query .= " SELECT ";
	    
	    foreach($execution['country']['articles']['fields'] as $field) {
	        $query .= ((empty($field[$group])) ? '1' : 'Art.'.$field[$group])." AS ".$field['Description'].", ";
	    }

	    foreach($execution['country']['buy']['fields'] as $field) {
		    if(empty($field[$group]) or $field['Description'] == 'IdProduto' or $field['Description'] == 'IdArtigo') {
			    continue;
		    }
	        $query .= ((empty($field[$group])) ? '1' : 'Buy.'.$field[$group])." AS ".$field['Description'].", ";
	    }
	    
	    $query .= "Fus.Id as Id, Fus.Times as Times, Fus.New_Peso as New_Peso, Fus.Dom_US as Dom_US, Fus.Ind_US as Ind_US, Fus.Peso as Peso";
	    
	    $query .= " FROM ".$execution['country']['parameters']['DbBuy'.$group]['Value'].".dbo.".$execution['country']['parameters']['TbBuy'.$group]['Value']." Buy";
		$query .= " INNER JOIN ".$execution['country']['parameters']['DbPan'.$group]['Value'].".dbo.Fusion_Web_Homologacion_OOH_".$execution['Id']." Pan ON";
		
		$array = [];
		$key = array_search('Ano', array_column($execution['country']['buy']['fields'], 'Description')); 
		$array[] = " Pan.Ano = Buy.".$execution['country']['buy']['fields'][$key][$group];
		
		$key = array_search('Mes', array_column($execution['country']['buy']['fields'], 'Description')); 
		$array[] = " Pan.Mes = Buy.".$execution['country']['buy']['fields'][$key][$group];
		
		$key = array_search('Individuo', array_column($execution['country']['buy']['fields'], 'Description')); 
		$array[] = " LTRIM(RTRIM(Pan.Individuo)) = LTRIM(RTRIM(Buy.".$execution['country']['buy']['fields'][$key][$group]."))";
		
		$query .= implode(" AND ", $array);
		
		// Seccion Articulos
		$query .= " INNER JOIN ".$execution['country']['parameters']['DbPro'.$group]['Value'].".dbo.".$execution['country']['parameters']['TbPro'.$group]['Value']." Art ON";
		
		$array = [];
		$key = array_search('IdArtigo', array_column($execution['country']['articles']['fields'], 'Description')); 
		$key2 = array_search('IdArtigo', array_column($execution['country']['buy']['fields'], 'Description'));
	    $array[] = " Art.".$execution['country']['articles']['fields'][$key][$group]." = Buy.".$execution['country']['buy']['fields'][$key2][$group];
	    
	    $query .= implode(" AND ", $array);
		
		// Seccion Fusion Pesos Imputado
		$query .= " INNER JOIN ".$execution['country']['parameters']['DbPan'.$group]['Value'].".dbo.Fusion_Web_Pesos_Imputado_OOH_US Fus ON";
		
		$array = [];
		$key = array_search('Ano', array_column($execution['country']['buy']['fields'], 'Description')); 
	    $array[] = " Fus.Ano = Buy.".$execution['country']['buy']['fields'][$key][$group];
	    
	    $key = array_search('Mes', array_column($execution['country']['buy']['fields'], 'Description')); 
	    $array[] = " Fus.Mes = Buy.".$execution['country']['buy']['fields'][$key][$group];
	    
	    $key = array_search('Individuo', array_column($execution['country']['buy']['fields'], 'Description')); 
	    $array[] = " LTRIM(RTRIM(Fus.Ind_OOH)) = LTRIM(RTRIM(Buy.".$execution['country']['buy']['fields'][$key][$group]."))";
	    
		$query .= implode(" AND ", $array);
		
		//Seccion WHERE
		$query .= " WHERE ";
		$array = [];
		
		$key = array_search('Ano', array_column($execution['country']['buy']['fields'], 'Description')); 
	    $array[] = " Buy.".$execution['country']['buy']['fields'][$key][$group]." = ".$execution['Ano'];
	    
	    $key = array_search('Mes', array_column($execution['country']['buy']['fields'], 'Description')); 
	    $array[] = " Buy.".$execution['country']['buy']['fields'][$key][$group]." = ".$execution['Mes'];

		$query .= implode(" AND ", $array);
		
	    return $query;
    }
    
    public function consume($execution, $group) {
	    
	    $query[0] 	= " IF OBJECT_ID('tempdb..##Consume_TMP1') IS NOT NULL DROP TABLE tempdb..##Consume_TMP1; ";
	    $query[1] 	= " IF OBJECT_ID('tempdb..##Consume_TMP2') IS NOT NULL DROP TABLE tempdb..##Consume_TMP2; ";
	    
	    $query[2]	= " SELECT";
	    
	    $array = [];
	    $key = array_search('IdAto', array_column($execution['country']['user']['fields'], 'Description')); 
	    $array[] = " Usr.".$execution['country']['user']['fields'][$key][$group]." AS ".$execution['country']['user']['fields'][$key]['Description'];
	    $array[] = sprintf(" %s as Ano ", $execution['Ano']);
	    $array[] = sprintf(" %s as Mes ", $execution['Mes']);
		$key = array_search('Domicilio', array_column($execution['country']['buy']['fields'], 'Description')); 
	    $array[] = " Buy.".$execution['country']['buy']['fields'][$key][$group]." AS ".$execution['country']['buy']['fields'][$key]['Description'];
	    $key = array_search('Individuo', array_column($execution['country']['user']['fields'], 'Description')); 
	    $array[] = " Usr.".$execution['country']['user']['fields'][$key][$group]." AS ".$execution['country']['user']['fields'][$key]['Description'];
	    $key = array_search('Consumo', array_column($execution['country']['user']['fields'], 'Description')); 
	    $array[] = " Usr.".$execution['country']['user']['fields'][$key][$group]." AS ".$execution['country']['user']['fields'][$key]['Description'];
	    $key = array_search('Morador', array_column($execution['country']['user']['fields'], 'Description')); 
	    $array[] = " Usr.".$execution['country']['user']['fields'][$key][$group]." AS ".$execution['country']['user']['fields'][$key]['Description'];
	    $key = array_search('Uso_Individual', array_column($execution['country']['user']['fields'], 'Description')); 
	    $array[] = " Usr.".$execution['country']['user']['fields'][$key][$group]." AS ".$execution['country']['user']['fields'][$key]['Description'];
	    $key = array_search('Factor', array_column($execution['country']['user']['fields'], 'Description')); 
	    $array[] = " Usr.".$execution['country']['user']['fields'][$key][$group]." AS ".$execution['country']['user']['fields'][$key]['Description'];
	    $key = array_search('PesoPNC', array_column($execution['country']['household']['fields'], 'Description')); 
	    $array[] = " Dom.".$execution['country']['household']['fields'][$key][$group]." AS ".$execution['country']['household']['fields'][$key]['Description'];
	    $key = array_search('PesoUS', array_column($execution['country']['individual']['fields'], 'Description')); 
	    $array[] = " Ind.".$execution['country']['individual']['fields'][$key][$group]." AS ".$execution['country']['individual']['fields'][$key]['Description'];
	    $array[] = " Pni.valor as PesoPNI";
	    $key = array_search('Consumo', array_column($execution['country']['user']['fields'], 'Description')); 
	    $array[] = " CASE WHEN Usr.".$execution['country']['user']['fields'][$key][$group]." = 1 THEN 2 WHEN Usr.".$execution['country']['user']['fields'][$key][$group]." = 2 THEN 4 WHEN Usr.".$execution['country']['user']['fields'][$key][$group]." = 3 THEN 8 END as ".$execution['country']['user']['fields'][$key]['Description']."_Formula";
	    $array[] = " Fus.Peso as Peso";
	    $array[] = " Fus.New_Peso as New_Peso";
	    
	    foreach($execution['country']['clusters'] as $cluster) {
		    $array[] = "Fus.".$cluster['Descricao']." AS ".$cluster['Descricao'];
	    }
	    
	    $query[2] .= implode(" , ", $array);
	    
	    // Sentencia INTO
	    
	    $query[2] .= " INTO ##Consume_TMP1";
	    
	    // Sentencia FROM
	    
	    $query[2] .= " FROM ".$execution['country']['parameters']['DbUsr'.$group]['Value'].".dbo.".$execution['country']['parameters']['TbUsr'.$group]['Value']." Usr";
	    
	    $query[2] .= " LEFT JOIN ".$execution['country']['parameters']['DbBuy'.$group]['Value'].".dbo.".$execution['country']['parameters']['TbBuy'.$group]['Value']." Buy ON ";
	    
	    $key = array_search('IdAto', array_column($execution['country']['buy']['fields'], 'Description')); 
	    $query[2] .= " Buy.".$execution['country']['buy']['fields'][$key][$group]." = ";
	    
	    $key = array_search('IdAto', array_column($execution['country']['user']['fields'], 'Description')); 
	    $query[2] .= " Usr.".$execution['country']['user']['fields'][$key][$group];
	    
	    $query[2] .= " LEFT JOIN ".$execution['country']['parameters']['DbDom'.$group]['Value'].".dbo.".$execution['country']['parameters']['TbDom'.$group]['Value']." Dom ON ";
	    
	    $key = array_search('Ano', array_column($execution['country']['household']['fields'], 'Description'));
	    $query[2] .= " Dom.".$execution['country']['household']['fields'][$key][$group]." = ";
	    
	    $key = array_search('Ano', array_column($execution['country']['buy']['fields'], 'Description'));
	    $query[2] .= " Buy.".$execution['country']['buy']['fields'][$key][$group]." AND ";
	    
	    $key = array_search('Mes', array_column($execution['country']['household']['fields'], 'Description'));
	    $query[2] .= " Dom.".$execution['country']['household']['fields'][$key][$group]." = ";
	    
	    $key = array_search('Mes', array_column($execution['country']['buy']['fields'], 'Description'));
	    $query[2] .= " Buy.".$execution['country']['buy']['fields'][$key][$group]." AND ";
	    
	    $key = array_search('IdDomicilio', array_column($execution['country']['household']['fields'], 'Description'));
	    $query[2] .= " Dom.".$execution['country']['household']['fields'][$key][$group]." = ";
	    
	    $key = array_search('Domicilio', array_column($execution['country']['buy']['fields'], 'Description'));
	    $query[2] .= " Buy.".$execution['country']['buy']['fields'][$key][$group]." AND ";
	    
	    $key = array_search('IdPeso', array_column($execution['country']['household']['fields'], 'Description'));
	    $query[2] .= " Dom.".$execution['country']['household']['fields'][$key][$group]." = 1";
	    
	    $query[2] .= " LEFT JOIN ".$execution['country']['parameters']['DbResFUS']['Value'].".dbo.".$execution['country']['parameters']['TbResFUS']['Value']." Fus ON ";
	    
	    $query[2] .= " Fus.Ano = ";
	    
	    $key = array_search('Ano', array_column($execution['country']['buy']['fields'], 'Description'));
	    $query[2] .= " Buy.".$execution['country']['buy']['fields'][$key][$group]." AND ";
	    
	    $query[2] .= " Fus.Mes = ";
	    
	    $key = array_search('Mes', array_column($execution['country']['buy']['fields'], 'Description'));
	    $query[2] .= " Buy.".$execution['country']['buy']['fields'][$key][$group]." AND ";
	    
	    $query[2] .= " Fus.Dom_US = ";
	    
	    $key = array_search('Domicilio', array_column($execution['country']['buy']['fields'], 'Description'));
	    $query[2] .= " Buy.".$execution['country']['buy']['fields'][$key][$group]." AND ";
	    
	    $query[2] .= " Fus.Ind_US = ";
	    
	    $key = array_search('Individuo', array_column($execution['country']['user']['fields'], 'Description'));
	    $query[2] .= " Usr.".$execution['country']['user']['fields'][$key][$group];
	    
	    $query[2] .= " LEFT JOIN ".$execution['country']['parameters']['DbIndUS']['Value'].".dbo.".$execution['country']['parameters']['TbIndUS']['Value']." Ind ON ";
	    
	    $key = array_search('Ano', array_column($execution['country']['individual']['fields'], 'Description'));
	    $query[2] .= " Ind.".$execution['country']['individual']['fields'][$key][$group]." = ";
	    
	    $key = array_search('Ano', array_column($execution['country']['buy']['fields'], 'Description'));
	    $query[2] .= " Buy.".$execution['country']['buy']['fields'][$key][$group]." AND ";
	    
	    $key = array_search('Mes', array_column($execution['country']['individual']['fields'], 'Description'));
	    $query[2] .= " Ind.".$execution['country']['individual']['fields'][$key][$group]." = ";
	    
	    $key = array_search('Mes', array_column($execution['country']['buy']['fields'], 'Description'));
	    $query[2] .= " Buy.".$execution['country']['buy']['fields'][$key][$group]." AND ";
	    
	    $key = array_search('Domicilio', array_column($execution['country']['individual']['fields'], 'Description'));
	    $query[2] .= " Ind.".$execution['country']['individual']['fields'][$key][$group]." = ";
	    
	    $key = array_search('Domicilio', array_column($execution['country']['buy']['fields'], 'Description'));
	    $query[2] .= " Buy.".$execution['country']['buy']['fields'][$key][$group]." AND ";
	    
	    $key = array_search('Individuo', array_column($execution['country']['individual']['fields'], 'Description'));
	    $query[2] .= " Ind.".$execution['country']['individual']['fields'][$key][$group]." = ";
	    
	    $key = array_search('Individuo', array_column($execution['country']['user']['fields'], 'Description'));
	    $query[2] .= " Usr.".$execution['country']['user']['fields'][$key][$group];
	    
	    $query[2] .= " LEFT JOIN dbo.RG_Individuos_Pesos_PNI Pni ON  Pni.ano =  Buy.Ano AND  Pni.messem =  Buy.Mes AND  Pni.iddomicilio =  Buy.IdDomicilio AND  Pni.idindividuo =  Usr.IdIndividuo";
	    
	    //Seccion WHERE
		$query[2] .= " WHERE ";
		$array = [];
		
		$key = array_search('Ano', array_column($execution['country']['buy']['fields'], 'Description')); 
	    $array[] = " Buy.".$execution['country']['buy']['fields'][$key][$group]." = ".$execution['Ano'];
	    
	    $key = array_search('Mes', array_column($execution['country']['buy']['fields'], 'Description')); 
	    $array[] = " Buy.".$execution['country']['buy']['fields'][$key][$group]." = ".$execution['Mes'];

		$query[2] .= implode(" AND ", $array);
		
		// Tercera Query
		
		$array = [];
		$query[3] = "SELECT ";
		
		$key = array_search('IdAto', array_column($execution['country']['user']['fields'], 'Description'));
		$array[] = $execution['country']['user']['fields'][$key]['Description'];
		$array[] = sprintf(" %s as Ano ", $execution['Ano']);
	    $array[] = sprintf(" %s as Mes ", $execution['Mes']);
		
		$key = array_search('Consumo', array_column($execution['country']['user']['fields'], 'Description'));
		$array[] = "SUM(".$execution['country']['user']['fields'][$key]['Description']."_Formula) AS ConsumoTotal";
		
		$query[3] .= implode(" , ", $array);
		
		$key = array_search('IdAto', array_column($execution['country']['user']['fields'], 'Description')); 
		$query[3] .= " INTO ##Consume_TMP2 FROM ##Consume_TMP1 GROUP by ".$execution['country']['user']['fields'][$key]['Description']." ORDER by 1";
		
		// Cuarta Query
		
		$array = [];
		
		$query[4] = "DELETE	J_User_PNC_Fusion_Web
					FROM	J_User_PNC_Fusion_Web Fwa 
					JOIN	dbo. Fusion_Web_ActosConsumo Jan ON jan.idAto = fwa.idAto
					WHERE	Jan.Ano = " . $execution['Ano'] . "
					AND		Jan.Mes = " . $execution['Mes'] . ";" . PHP_EOL;
					
		$query[4] .= "DELETE FROM dbo.Fusion_Web_ActosConsumo WHERE Ano = ". $execution['Ano'] . " AND Mes = " . $execution['Mes'] . ";" . PHP_EOL;
		
		$query[4] .= "INSERT INTO dbo.Fusion_Web_ActosConsumo SELECT ";
		
		$array[] = " tmp1.* ";
		
		$array[] = " tmp2.ConsumoTotal ";
		
		$key = array_search('Consumo', array_column($execution['country']['user']['fields'], 'Description'));
		$key1 = array_search('PesoPNC', array_column($execution['country']['household']['fields'], 'Description'));
		
		$array[] = sprintf(" CAST(tmp1.%s_Formula as FLOAT) / CAST(tmp2.ConsumoTotal as FLOAT) as Ratio_Consumo", $execution['country']['user']['fields'][$key]['Description']);
		
		$array[] = sprintf(" tmp1.New_Peso * (CAST(tmp1.%s_Formula as FLOAT) / CAST(tmp2.ConsumoTotal as FLOAT)) as Peso_Total", $execution['country']['user']['fields'][$key]['Description']);
		
		$array[] = sprintf(" tmp1.%s / (tmp1.New_Peso * (CAST(tmp1.%s_Formula as FLOAT) / CAST(tmp2.ConsumoTotal as FLOAT))) as Factor_New", 
					$execution['country']['household']['fields'][$key1]['Description'], 
					$execution['country']['user']['fields'][$key]['Description']);
		
		$array[] = sprintf("(CAST(tmp1.%s as FLOAT) / (CAST(tmp1.New_Peso as FLOAT) * (CAST(tmp1.%s_Formula as FLOAT) / CAST(tmp2.ConsumoTotal as FLOAT)))) as Resultado_A",
					$execution['country']['household']['fields'][$key1]['Description'],
					$execution['country']['user']['fields'][$key]['Description']);
					
		$array[] = sprintf("(CAST(tmp1.%s as FLOAT) / CAST(tmp1.New_Peso as FLOAT)) * (CAST(tmp1.%s_Formula as FLOAT) / CAST(tmp2.ConsumoTotal as FLOAT)) as Resultado_B",
					$execution['country']['household']['fields'][$key1]['Description'],
					$execution['country']['user']['fields'][$key]['Description']);
					
		$array[] = sprintf("(CAST(tmp1.%s as FLOAT) / CAST(tmp1.Peso as FLOAT)) * (CAST(tmp1.%s_Formula as FLOAT) / CAST(tmp2.ConsumoTotal as FLOAT)) as Resultado_C",
					$execution['country']['household']['fields'][$key1]['Description'],
					$execution['country']['user']['fields'][$key]['Description']);
					
		$array[] = sprintf("(CAST(tmp1.PesoPNI as FLOAT) / CAST(tmp1.New_Peso as FLOAT)) * (CAST(tmp1.%s_Formula as FLOAT) / CAST(tmp2.ConsumoTotal as FLOAT)) as Resultado_D",
					$execution['country']['user']['fields'][$key]['Description']);
		
		$query[4] .= implode(" , ", $array);
		
		$key = array_search('IdAto', array_column($execution['country']['user']['fields'], 'Description'));
		$query[4] .= sprintf(" FROM ##Consume_TMP1 tmp1 INNER JOIN ##Consume_TMP2 tmp2 ON tmp1.%s = tmp2.%s WHERE tmp1.%s IS NOT NULL", $execution['country']['user']['fields'][$key]['Description'], $execution['country']['user']['fields'][$key]['Description'], $execution['country']['clusters'][0]['Descricao']);
		
		$key1 = array_search('Ano', array_column($execution['country']['buy']['fields'], 'Description'));
		$key2 = array_search('Mes', array_column($execution['country']['buy']['fields'], 'Description'));	
		$key3 = array_search('IdAto', array_column($execution['country']['buy']['fields'], 'Description'));
		
								
		$query[5] = sprintf(" 	INSERT 	INTO J_User_PNC_Fusion_Web 
								SELECT 	Fwa.Idato, Fwa.IdIndividuo, Fwa.Consumo, Fwa.Flag_Morador, Fwa.UsoIndividual, 
								round(coalesce(Fwa.Resultado_A,0),11) * 100000000000 as Factor_A ,
								round(coalesce(Fwa.Resultado_B,0),11) * 100000000000 as Factor_B,
								round(coalesce(Fwa.Resultado_C,0),11) * 100000000000 as Factor_C,
								round(coalesce(Fwa.Resultado_D,0),11) * 100000000000 as Factor_D
								FROM 	Fusion_Web_ActosConsumo Fwa
								WHERE	EXISTS (SELECT IdAto FROM %s Buy WHERE Buy.%s = %s AND Buy.%s = %s AND Fwa.IdAto = Buy.%s)",
								$execution['country']['parameters']['DbBuy'.$group]['Value'].".dbo.".$execution['country']['parameters']['TbBuy'.$group]['Value'],
								$execution['country']['buy']['fields'][$key1][$group],
								$execution['Ano'],
								$execution['country']['buy']['fields'][$key2][$group],
								$execution['Mes'],
								$execution['country']['buy']['fields'][$key3][$group]);
		
	    
	    return $query;
	    
    }
}