<?
namespace App\Controller\Component;

use Cake\Controller\Component;

class ArraysComponent extends Component {
	
    function array_diff_assoc_recursive($array1, $array2, $dimensions, $values) {
		
		$dims = [];
		$deep = [];
		foreach($dimensions as $dimension) {
			$dims[] = $dimension['Descricao'];
		}
		
		// Ajuste de array 1
		$v1 = [];
		$p1 = [];
		foreach($array1 as $pos => $array) {

			$key1 = [];
			foreach($dimensions as $dimension) {
				$key1[$dimension['Descricao']] = $array[$dimension['Descricao']];
			}
			$v1[ implode("_", $key1) ] = $array['Value'];
			$p1[ implode("_", $key1) ] = $pos;
			
		}
		
		// Ajuste de array 2
		$v2 = [];
		$p2 = [];
		foreach($array2 as $pos => $array) {

			$key2 = [];
			foreach($dimensions as $dimension) {
				
				$key2[$dimension['Descricao']] = $array[$dimension['Descricao']];
			}
			$v2[ implode("_", $key2) ] = ($array['Value'] < 0) ? null : $values[$array['Value']];
			$p2[ implode("_", $key2) ] = $pos;
			
		}
		
		$diffs =  array_diff_assoc($v1, $v2);
		
		$return = [];
		foreach($diffs as $k => $diff) {
			$array1[$p1[$k]]['Original'] = ($array2[$p2[$k]]['Value'] < 0) ? '(Empty)' : $values[$array2[$p2[$k]]['Value']];
			$return[] = $array1[$p1[$k]];
		}
		return $return;
		
	}
}