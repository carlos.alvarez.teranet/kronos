<?php
namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;

/**
 * Excel component
 */
class ExcelComponent extends Component
{

    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [];
    
    public function weight_period($inputFileName = null)
    {
	    //  Leer archivo guardado previamente en las carpetas de webroot
		try {
		    $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
		    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
		    $objPHPExcel = $objReader->load($inputFileName);
		} catch(Exception $e) {
		    die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
		}
		
		$sheet = $objPHPExcel->getSheet(0);
		
		$initialRow = 2;
		$highestRow = $sheet->getHighestRow(); 
		$initialColumn = 1;
		$highestColumn = $sheet->getHighestColumn(); 
		
		$rowData = $sheet->rangeToArray('A' . $initialRow . ':' . $highestColumn . $highestRow,
		                                    NULL,
		                                    TRUE,
		                                    FALSE);
		                                    
		$headerData = $sheet->rangeToArray('A' . ($initialRow - 1) . ':' . $highestColumn . ($initialRow - 1),
		                                    NULL,
		                                    TRUE,
		                                    FALSE)[0];
		                                    
		$validate = [];                                    
		// Validación de columnas
		$headerData = array_map('strtolower', $headerData);
		$columns = $config['countries'][$c_sel]['panels'][$p_sel]['csv']['columns'];
		foreach($columns as $key => $column) {
			$validate['columns'][$column['from']] = array_search($column['from'], $headerData);
		}
		
		$data = [];
		if(!array_search(false, $validate['columns'], true)) {
			$jsonData['items'][$position['key']]['last_execution'] = date('d-m-Y H:i:s');
			// Ejecutar Shell de carga de datos
			exec(ROOT . "bin/cake upload_excel_weight " . $c_sel . " " . $p_sel . " " . $position['key'] . " > " . LOGS . "test");
		} else {
			$this->Flash->error('El archivo cargado no contiene todas las columnas necesarias para procesar.');
			$jsonData['items'][$position['key']]['error'] = [
				'header' => $headerData,
				'validate' => $validate
			];
		}
	}
    
}
