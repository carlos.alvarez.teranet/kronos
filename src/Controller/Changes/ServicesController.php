<?php
namespace App\Controller\Changes;

use Cake\Network\Session\DatabaseSession;
use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Datasource\ConnectionManager;


class ServicesController extends AppController
{
	
	public function initialize()
	{
		parent::initialize();
		$this->loadComponent('RequestHandler');
	}
	
	public function beforeFilter(Event $event) {
		
		parent::beforeFilter($event);
    }
    
    public function code($serv_id_by_project = null)
	{ 
		$this->viewBuilder()->setLayout('ajax');
		
		$connection = ConnectionManager::get('ARANDA');
		
		$detail = $connection->execute("SELECT 	c.serv_id_by_project as [CODE],
												c.serv_id as [INTERNAL_CODE],
												u.UNAME as CLIENT,
												u2.UNAME as RESPONSIBLE,
												imp.impa_description as IMPACT,
												urg.urge_description as URGENCY,
												pri.prio_description as PRIORITY,
												s.stat_name as STATUS,
												r.reas_description as REASON,
												t.ret_description as TYPE,
												p.CTG_CAPTION as CATEGORY,
												ser.NAME as SERVICE,
												g.GRP_NAME as [GROUP],
												co.description as COUNTRY,
												c.serv_description as DESCRIPTION,
												c.serv_opened_date as [DATE]
										FROM 	dbo.ASDK_SERVICE_CALL c
										JOIN	dbo.USUARIOS u ON u.CODUSUARIO = c.serv_customer_id
										JOIN	dbo.USUARIOS u2 ON u2.CODUSUARIO = c.serv_responsible_id
										JOIN 	dbo.AFW_STATUS s ON s.stat_id = c.serv_status_id
										JOIN	dbo.ASDK_REASON r ON r.reas_id = c.serv_reason_id
										JOIN	dbo.REGISTRY_TYPE t ON t.ret_id = c.serv_registry_type_id
										JOIN	dbo.V_CATEGORY_PROJECT p ON p.CTG_INDEX = c.serv_category_id
										JOIN	dbo.ASDK_SERVICE ser ON ser.FL_INT_SERVICE_ID = c.serv_service_id
										JOIN 	dbo.GROUPHD g ON g.GRP_id = c.serv_responsible_group_id
										JOIN 	dbo.COUNTRY co ON co.countryid = u.COUNTRYID
										JOIN	dbo.ASDK_IMPACT imp ON imp.impa_id = c.serv_impact_id
										JOIN	dbo.ASDK_URGENCY urg ON urg.urge_id = c.serv_urgency_id
										JOIN	dbo.ASDK_PRIORITY pri ON  pri.prio_id = c.serv_priority_id
										WHERE 	c.serv_id_by_project = :serv_id_by_project", ['serv_id_by_project' => $serv_id_by_project])->fetch('assoc');
										
		$this->set(compact('detail'));
        $this->set('_serialize', ['detail']);
		
	}

}