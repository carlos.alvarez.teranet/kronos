<?php
namespace App\Controller\Changes;

use Cake\Network\Session\DatabaseSession;
use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Datasource\ConnectionManager;


class CategoriesController extends AppController
{
	
	public function initialize()
	{
		parent::initialize();
	}
	
	public function beforeFilter(Event $event) {
		
		parent::beforeFilter($event);
    }
    
    public function index()
	{ 
		$pageTitle = 'Bitácora de cambios en categorías';
		
		// Archivo configuración
		$string = file_get_contents( CONFIG . DS . "platform" . DS . "changes.json" );
		$config = json_decode($string, true);
		
		foreach($config['countries'] as $country) {
			$connection = ConnectionManager::get($country['base']);
			$categories[$country['base']] = $connection->execute("SELECT	p.idProduto, p.Descricao, p.DtCriacao, p.FlgDesativacao, p.idCalendar,
																			STUFF((SELECT		CAST(LEFT(pa.Descricao, CHARINDEX(' - ', pa.Descricao) - 1) as VARCHAR) + ','
																					FROM	dbo.A_PainelProduto (nolock) pp
																					JOIN	dbo.Paineis pa (nolock) ON pa.idPainel = pp.idPainel
																					WHERE 	pp.IdProduto = p.IdProduto
																				FOR XML PATH ('')), 1, 0, '') as Painel
																	FROM	dbo.A_Produto p (nolock)
																	ORDER	by p.Descricao ASC")->fetchAll('assoc');
		}
		
		$this->set(compact('pageTitle', 'config', 'categories'));
        $this->set('_serialize', ['pageTitle', 'config', 'categories']);
	}
	
	public function registry($code = null, $idProduto = null) {
		
		$pageTitle = 'Bitácora de cambios en categorías';
		
		// Archivo configuración
		$string = file_get_contents( CONFIG . DS . "platform" . DS . "changes.json" );
		$config = json_decode($string, true);
		
		$country = array_values(array_filter($config['countries'], function($country) use ($code) {
			return $country['code'] == $code;
		}))[0];
		
		$connection = ConnectionManager::get($country['base']);
		
		$category = $connection->execute("SELECT	p.idProduto, p.Descricao, p.DtCriacao, p.FlgDesativacao, p.idCalendar,
															STUFF((SELECT		CAST(LEFT(pa.Descricao, CHARINDEX(' - ', pa.Descricao) - 1) as VARCHAR) + ','
																	FROM	dbo.A_PainelProduto (nolock) pp
																	JOIN	dbo.Paineis pa (nolock) ON pa.idPainel = pp.idPainel
																	WHERE 	pp.IdProduto = p.IdProduto
																FOR XML PATH ('')), 1, 0, '') as Painel
													FROM	dbo.A_Produto p (nolock)
													WHERE	p.idProduto = :idProduto
													ORDER	by p.Descricao ASC", ['idProduto' => $idProduto])->fetch('assoc');
													
		$databases = $connection->execute("SELECT	*
											FROM	dbo.DBM_PW_Detail
											WHERE	idProduto = :idProduto
											AND		FlgActivo <> 'AR'
											ORDER	by FlgActivo DESC, Grupo ASC", ['idProduto' => $idProduto])->fetchAll('assoc');
													
		$years = [date('Y') - 1, date('Y'), date('Y') + 1];
		$types = [
			1 => 'Cambio de clasificaciones',
			2 => 'Cambio de coeficientes',
			3 => 'Ajuste De - Para',
			4 => 'Reestructuración de categoría',
			5 => 'Otros'
		];
		
		
		$this->set(compact('pageTitle', 'country', 'category', 'years', 'types', 'databases'));
        $this->set('_serialize', ['pageTitle', 'country', 'category', 'years', 'types', 'databases']);
		
	}

}