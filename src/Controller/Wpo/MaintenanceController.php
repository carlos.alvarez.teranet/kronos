<?php
namespace App\Controller\Wpo;
use App\Controller\AppController;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\TableRegistry;
use Cake\Http\Response;


/**
 * Maintenance Controller
 *
 *
 * @method \App\Model\Entity\Maintenance[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class MaintenanceController extends AppController
{
	
	public function initialize()
	{
		parent::initialize();
		$this->loadModel('Cubes');
		$this->loadModel('Periodo');
		$this->loadModel('Cargaproductiva');
		$this->loadModel('Cargaproductivacubo');
		
		
	}

    public function index()
    {
	    $pageTitle = 'Mantenimiento de Cubos';
	    $cubes = $this->Cubes->find('all')->limit(5)->order(['id' => 'DESC']);
        $this->set(compact('cubes', 'pageTitle'));
		$this->set('_serialize', ['cubes', 'pageTitle']);
    } 

	public function listcargaproductiva()
    {
	    $pageTitle = 'Cargas productivas Cubos';

		$connection = ConnectionManager::get('IT_WORKFLOW_33');

		$query="
		select top 50 * from WPO_CargaProductiva Cargaproductiva
		join periodo p on Cargaproductiva.IdPeriodo=p.IdPeriodo
		ORDER BY p.Year Desc, p.Month DESC
		";
		$CargaProductiva=$connection->execute($query);
		
		$connection->disconnect();



        $this->set(compact('CargaProductiva', 'pageTitle'));
		$this->set('_serialize', ['CargaProductiva', 'pageTitle']);
    } 

	

	public function getcubosperiodo(){
		$idMes=$this->request->data("idMes");
		$idMes=(strlen($idMes)==1)?'0'.$idMes:$idMes;
		$cubes = $this->Cubes->find('all')->where(['M'.$idMes => 1])->limit(40)->order(['id' => 'DESC']);
		$obj=[];
		foreach ($cubes as $key => $value) {
			$obj[]=$value;
		}
		
		$arrResponse=array(
			'getCubos'=>($obj),
			'idMes'=>$idMes,
			'hashLog'=>MD5(date("u"))
		);
		$response = new Response();
		$response = $response->withType('application/json')->withStringBody(json_encode($arrResponse));
		return $response;
		
	}


	public function getcubosperiodonoselect(){
		$idMes=$this->request->data("idMes");
		$IdCargaProductiva=$this->request->data("IdCargaProductiva");

		$connection = ConnectionManager::get('IT_WORKFLOW_33');

		$query="
		select top 50 * from WPO_Cubes c 
		where id not in (
			select cpc.idCubo
			from WPO_CargaProductivaCubo cpc
			where cpc.IdCargaProductiva=".$IdCargaProductiva."
		)
		and M".$idMes."=1
		";
		$CargaProductivaCubos=$connection->execute($query);
		
		$connection->disconnect();


		$obj=[];
		foreach ($CargaProductivaCubos as $key => $value) {
			$obj[]=$value;
		}
		
		$arrResponse=array(
			'getCubos'=>($obj),
			'query'=>$query
		);
		

		$response = new Response();
		$response = $response->withType('application/json')->withStringBody(json_encode($arrResponse));
		return $response;
	}


	


	public function getdataperiodoparametro(){
		$IdPeriodo=$this->request->data("IdPeriodo");

		$CargaProductiva = $this->Cargaproductiva->find('all')
												//   ->select(['TotalSeleccionados'])
												 ->where(['IdPeriodo'=>$IdPeriodo])
												 ->order(['IdCargaProductiva' => 'DESC']);

		// $connection = ConnectionManager::get('IT_WORKFLOW_33');

		// $query="
		
		// ";
		// $CargaProductivaCubos=$connection->execute($query);
		
		// $connection->disconnect();
												
		$datos=array(
			'status'=>200,
			'CargaProductiva'=>$CargaProductiva
		);

		$response = new Response();
		$response = $response->withType('application/json')->withStringBody(json_encode($datos));
		return $response;
	}

	

	public function addcuboscargaproductiva(){
		
		
		$idCargaProductiva= $this->request->params['pass'][0];
		$pageTitle = 'Agregar Cubo a carga productiva';
		
		
	
	}

	

	public function createcargaproductiva(){
		
		$IdPeriodo=$this->request->data("IdPeriodo");
		$anio=$this->request->data("anio");
		$TotalCubos=$this->request->data("TotalCubos");
		$TotalSeleccionados=$this->request->data("TotalSeleccionados");
		$Cubos=$this->request->data("Cubos");//[50,51,52,53,54];

		
		$connection = ConnectionManager::get('IT_WORKFLOW_33');
		$connection->insert('WPO_CargaProductiva', [
			'IdPeriodo'=>$IdPeriodo,
			// 'FechaCreacion'=>date("Y-m-d H:i:s"),
			'UsuarioCreacion'=>12,
			'TotalCubos'=>$TotalCubos,
			'TotalSeleccionados'=>$TotalSeleccionados,
		], ['created' => 'datetime']);

		$connection->disconnect();
		
		$CargaProductiva = $this->Cargaproductiva->find()
												 ->select(['IdCargaProductiva'])
												 ->limit(1)
												 ->order(['IdCargaProductiva' => 'DESC'])
												 ->first();
		
		$obj=[];
		if($CargaProductiva){
			$iden=$CargaProductiva['IdCargaProductiva'];
			$arr=[];
			$query = $this->Cargaproductivacubo->query();
			foreach ($Cubos as $key => $c) {
				
				if($c!=''){
					
					$query->insert(['IdCargaProductiva', 'IdCubo','Tipo'])
						->values([
							'IdCargaProductiva' => $iden,
							'IdCubo' => $c,
							'Tipo'=>1
						]);
				}
			}
			$query->execute();
		}
		$arrResponse=array(
			'idMes'=>($idMes),
			'anio'=>$anio
		);
		echo json_encode($arrResponse);
		exit;
	
	}

	


	private function getArray($getData){
		$objArray=[];
		if(isset($getData)){
			$iden=0;
			foreach ($getData as $key => $p) {
				if($p[0]==''){
					$objArray[$iden]=mb_convert_case($p[1], MB_CASE_TITLE, "UTF-8");
					$iden++;
				}else{
					$objArray[$p[0]]=mb_convert_case($p[1], MB_CASE_TITLE, "UTF-8");	
				}
			}
		}
		return $objArray;
	}

	public function deletecubes(){
		$idCubo=$this->request->data("IdCubo");
		$connection = ConnectionManager::get('IT_WORKFLOW_33');
		$connection->delete('WPO_Cubes', ['id' => $idCubo]);
	}

	public function deletecubescargaproductiva(){
		$IdCargaProductivaCubo=$this->request->data("IdCargaProductivaCubo");
		$IdCargaProductiva=$this->request->data("IdCargaProductiva");
		
		$connection = ConnectionManager::get('IT_WORKFLOW_33');
		$connection->delete('WPO_CargaProductivaCubo', ['IdCargaProductivaCubo' => $IdCargaProductivaCubo]);
		$connection->disconnect();

		$this->actualizarTotales($IdCargaProductiva);

	}


	public function actualizarTotales($IdCargaProductiva){
		$connection = ConnectionManager::get('IT_WORKFLOW_33');
		$connection->execute("update WPO_CargaProductiva set TotalSeleccionados=(TotalSeleccionados-1) where IdCargaProductiva=".$IdCargaProductiva."");
		$connection->disconnect();

		

	}
	
	
	public function transaccubes(){
		$idCubo=$this->request->data("id");
		$connection = ConnectionManager::get('IT_WORKFLOW_33');
		if($idCubo!=''){
			$queryUpdate="update WPO_Cubes set Area='".$this->request->data("Area")."', idCountry='".$this->request->data("idCountry")."', Country='".$this->request->data("Country")."',Client='".$this->request->data("Client")."',idClient='".$this->request->data("idClient")."',Category='".$this->request->data("Category")."',LCO='".$this->request->data("LCO")."',idType='".$this->request->data("idType")."',Type='".$this->request->data("Type")."',PWDB='".$this->request->data("PWDB")."',M01=".$this->request->data("M01").",M02=".$this->request->data("M02").",M03=".$this->request->data("M03").",M04=".$this->request->data("M04").",M05=".$this->request->data("M05").",M06=".$this->request->data("M06").",M07=".$this->request->data("M07").",M08=".$this->request->data("M08").",M09=".$this->request->data("M09").",M10=".$this->request->data("M10").",M11=".$this->request->data("M11").",M12=".$this->request->data("M12").",Loader_Files=".$this->request->data("Loader_Files")."where id=".$this->request->data("id")."";
			$response=$connection->execute($queryUpdate);
		}else{		
			$connection->insert('WPO_Cubes', [
				'Area' => $this->request->data("Area"),
				'idCountry' =>$this->request->data("idCountry"),
				'Country' =>$this->request->data("Country"),
				'Client' =>$this->request->data("Client"),
				'idClient' =>$this->request->data("idClient"),
				'Category' =>$this->request->data("Category"),
				'LCO' =>$this->request->data("LCO"),
				'idType' =>$this->request->data("idType"),
				'Type' =>$this->request->data("Type"),
				'PWDB'=>$this->request->data("PWDB"),
				'M01'=>$this->request->data("M01"),
				'M02'=>$this->request->data("M02"),
				'M03'=>$this->request->data("M03"),
				'M04'=>$this->request->data("M04"),
				'M05'=>$this->request->data("M05"),
				'M06'=>$this->request->data("M06"),
				'M07'=>$this->request->data("M07"),
				'M08'=>$this->request->data("M08"),
				'M09'=>$this->request->data("M09"),
				'M10'=>$this->request->data("M10"),
				'M11'=>$this->request->data("M11"),
				'M12'=>$this->request->data("M12"),
				'Loader_Files'=>$this->request->data("Loader_Files")
			], ['created' => 'datetime']);
		
		
		}
	
		
		echo json_encode(array(
			'query'=>$queryUpdate,
			'response'=>$response
		));
		exit;
		
	}
	

	public function cargaproductiva()
    {
		$pageTitle = 'Carga Productiva Mensual';
	    $cubes = $this->Cubes->find('all')->limit(5);


		$Periodo = $this->Periodo->find('all')->order(['IdPeriodo' => 'DESC']);
        $this->set(compact('cubes', 'pageTitle','Periodo'));
		$this->set('_serialize', ['cubes', 'pageTitle','Periodo']);
	} 

	public function detallecargaproductiva()
    {
		
		$iden = $this->request->params['pass'][0];
		$CargaProductiva = $this->Cargaproductiva->find('all')
												 ->where(['IdCargaProductiva'=>$iden])
												 ->first();
		$nombreMes= nombreMes((int)$CargaProductiva['Month']).' '.$CargaProductiva['Year'];
		$connection = ConnectionManager::get('IT_WORKFLOW_33');

		$CargaProductivaCubos=$connection->execute("select * from WPO_Cubes c 
		join WPO_CargaProductivaCubo  cpc on cpc.idCubo=c.id
		where cpc.idcargaproductiva=".$iden."
		");
		
		$connection->disconnect();
			
		$pageTitle = 'Detalle Carga Productiva <strong>'.$nombreMes.'</strong>';								 
		$titleCargaProductiva= 'Carga Productiva <strong>'.$nombreMes.'</strong>';	
		$this->set(compact('CargaProductiva', 'pageTitle','CargaProductivaCubos','titleCargaProductiva'));
		$this->set('_serialize', ['CargaProductiva', 'pageTitle','CargaProductivaCubos','titleCargaProductiva']);
	} 

	
	

	public function getcubes()
    {
	    $pageTitle = 'Detalle Cubo';
		$iden = $this->request->params['pass'][0];

		$connection = ConnectionManager::get('IT_WORKFLOW_33');
		// $arrCliente=$this->getArray($connection->execute("select idClient,Client from WPO_Cubes group by idClient,Client"));
		
		$objPaises=$this->getArray($connection->execute("select idCountry,Country from WPO_Cubes group by idCountry,Country order by Country"));
		$arrFrecuency=$this->getArray($connection->execute("select Frecuency as IdFrecuency,Frecuency from WPO_Cubes group by Frecuency order by Frecuency"));
		$arrTipos=$this->getArray($connection->execute("select idType,Type from WPO_Cubes  group by idType,Type order by Type"));
		// $arrCategory=$this->getArray($connection->execute("select '',Category from WPO_Cubes group by Category"));
		// $arrLCO=$this->getArray("select '',LCO from WPO_Cubes group by LCO");
		//$arrPWDB=$this->getArray($connection->execute("select '',PWDB from WPO_Cubes group by PWDB"));
		// $arrArea=$this->getArray("select '',Area from WPO_Cubes group by Area");
		$cubes = $this->Cubes->find('all',['conditions' =>['id'=>$iden]])->first();
		
		$this->set(compact('cubes', 'pageTitle','objPaises','arrCliente','arrTipos','arrFrecuency'));
		$this->set('_serialize', ['cubes', 'pageTitle',' objPaises','arrCliente','arrTipos','arrFrecuency']);
    } 
}
