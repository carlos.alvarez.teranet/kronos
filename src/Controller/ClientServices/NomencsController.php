<?php
namespace App\Controller\ClientServices;

use App\Controller\AppController;
use Cake\Network\Session\DatabaseSession;
use Cake\Event\Event;
use Cake\Datasource\ConnectionManager;
use PHPExcel;
use PHPExcel_IOFactory;
use Cake\Console\ShellDispatcher;

/**
 * Nomencs Controller
 *
 */
class NomencsController extends AppController
{
	
	public function initialize()
	{
		parent::initialize();
		$this->loadComponent('Excel');
	}
	
	public function beforeFilter(Event $event) {
		
		parent::beforeFilter($event);
    }
    
    public function upload() {
	    
	    $this->viewBuilder()->layout('ajax');
	    
	    // Subir archivo
		if($this->request->is('post')) {
			$data = $this->request->data();
			/*
			if(!is_uploaded_file($data['pw']['tmp_name'])) {
		    	return false;
		    }
		    */
		}
		
		$this->set(compact('data'));
        $this->set('_serialize', ['data']);
	    
    }
    
    public function index($country = null)
	{
		$pageTitle = 'Nomenclaturas';
		
		// Archivo configuración
		$string = file_get_contents( CONFIG . DS . "platform" . DS . "countries.json" );
		$config = json_decode($string, true);
		
		$connection = ConnectionManager::get($config['countries'][$country]['dbname']);
		$name		= $config['countries'][$country]['name'];
		
		// Listado de bases de datos
		$databases	= $connection->execute("SELECT	Nombre, Grupo, Descripcion, Created
											FROM	dbo.ITF_Bases
											WHERE	FlgActivo = 'AT'
											AND		LEFT(Grupo, 1) <> '_'
											AND		Grupo NOT LIKE '%WPO%'
											ORDER	by Nombre")->fetchAll('assoc');
											
		// Conteo nomenclatura
		$quantity	= $connection->execute("SELECT	BasePV, COUNT(DISTINCT IdNomenclatura) as Quantity
											FROM	VW_ITF_BasesNomenclaturasDetalle
											GROUP	by BasePV")->fetchAll('assoc');
		$cantidad = [];									
		foreach($quantity as $key => $value) {
			$cantidad[$value['BasePV']] = $value['Quantity'];
		}
											
		$tabs	= [];
		foreach($databases as $database) {
			if(!in_array($database['Grupo'], $tabs)) {
				$tabs[] = $database['Grupo'];
			}
		}
		
		$this->set(compact('pageTitle', 'name', 'databases', 'tabs', 'country', 'cantidad'));
        $this->set('_serialize', ['pageTitle', 'name', 'databases', 'tabs', 'country', 'cantidad']);
		
	}
	
	public function elements($country = null, $database = null, $key = null) {
		
		$this->viewBuilder()->layout('ajax');
		
		// Archivo configuración
		$string = file_get_contents( CONFIG . DS . "platform" . DS . "countries.json" );
		$config = json_decode($string, true);
		
		$connection = ConnectionManager::get($config['countries'][$country]['dbname']);
		$name		= $config['countries'][$country]['name'];
		
		// listado de nomenclaturas
		$elements	= $connection->execute("SELECT	idNomenclatura, Nomenclatura, Nivel, idComposicion, Linea, Script, Articulos, NroActos1Y, ArtFaltantes, ArtDuplicados, FlgNomenclatura
											FROM	dbo.VW_ITF_BasesNomenclaturasDetalle (nolock)
											WHERE	BasePV = :Nombre
											AND		IdNomenclatura = :Nomenclatura
											ORDER	by 1 ASC", ['Nombre' => $database, 'Nomenclatura' => $key])->fetchAll('assoc');
											
		$this->set(compact('elements'));
        $this->set('_serialize', ['elements']);
		
	}
	
	public function detail($country = null, $database = null) {
		
		$pageTitle = 'Nomenclaturas';
		
		// Archivo configuración
		$string = file_get_contents( CONFIG . DS . "platform" . DS . "countries.json" );
		$config = json_decode($string, true);
		
		$connection = ConnectionManager::get($config['countries'][$country]['dbname']);
		$name		= $config['countries'][$country]['name'];
		
		// Detalle nomenclatura
		/*
		$details	= $connection->execute("SELECT	idNomenclatura, Nomenclatura, Nivel, idComposicion, Linea, Script, Articulos, NroActos1Y, ArtFaltantes, ArtDuplicados, FlgNomenclatura
											FROM	dbo.VW_ITF_BasesNomenclaturasDetalle (nolock)
											WHERE	BasePV = :Nombre
											ORDER	by NoLinea ASC", ['Nombre' => $database])->fetchAll('assoc');
		*/
		$details	= $connection->execute("SELECT	DISTINCT idNomenclatura, Nomenclatura, FlgNomenclatura
											FROM	dbo.VW_ITF_BasesNomenclaturasDetalle (nolock)
											WHERE	BasePV = :Nombre
											ORDER	by 1 ASC", ['Nombre' => $database])->fetchAll('assoc');
											
		$nomencs	= [];
		foreach($details as $detail) {
			if(!in_array($detail['Nomenclatura'], $nomencs)) {
				$nomencs[$detail['idNomenclatura']] = [$detail['Nomenclatura'], $detail['FlgNomenclatura']];
			}
		}
		
		// Detalle de base de datos
		$database	= $connection->execute("SELECT	Nombre, Grupo, Descripcion, Created
											FROM	dbo.ITF_Bases
											WHERE	FlgActivo = 'AT'
											AND		LEFT(Grupo, 1) <> '_'
											AND		Grupo NOT LIKE '%WPO%'
											AND		Nombre = :Nombre
											ORDER	by Nombre", ['Nombre' => $database])->fetch('assoc');
											

		
		$this->set(compact('pageTitle', 'name', 'database', 'country', 'config', 'details', 'nomencs'));
        $this->set('_serialize', ['pageTitle', 'name', 'database', 'country', 'config', 'details', 'nomencs']);
		
	}
	
	public function artigos($country = null, $nomenclatura = null, $composition = null)
	{
		
		// Archivo configuración
		$string = file_get_contents( CONFIG . DS . "platform" . DS . "countries.json" );
		$config = json_decode($string, true);
		
		$connection = ConnectionManager::get($config['countries'][$country]['dbname']);
		$name		= $config['countries'][$country]['name'];
		
		$data = $connection->execute("SELECT	z.idArtigo,
												z.idProduto,
												z.Produto,
												z.IdSub,
												z.Sub,
												z.IdFabricante,
												dbo.fc_ToProperCase(z.Fabricante) as Fabricante,
												z.idMarca,
												dbo.fc_ToProperCase(z.Marca) as Marca,
												z.Cdc01,
												z.Cdc02,
												z.Cdc03,
												z.Cdc04,
												z.Cdc05,
												dbo.fc_ToProperCase(z.Clas01) as Clas01,
												dbo.fc_ToProperCase(z.Clas02) as Clas02,
												dbo.fc_ToProperCase(z.Clas03) as Clas03,
												dbo.fc_ToProperCase(z.Clas04) as Clas04,
												dbo.fc_ToProperCase(z.Clas05) as Clas05,
												z.IdConteudo,
												z.Conteudo,
												z.FlgAtivo,
												z.FlgGranel,
												z.NroActos,
												z.CodBar,
												z.CodBar2,
												z.CodBar3,
												z.NroActosMAT,
												z.NroBuyersMAT
										FROM	VW_SPR_NomenclaturaArtigo a
										JOIN	dbo.VW_ArtigoZ z ON z.IdArtigo = a.idArtigo
										WHERE	a.idNomenclatura = :idNomenclatura
										AND		a.idComposicion = :idComposicion", ['idNomenclatura' => $nomenclatura, 'idComposicion' => $composition])->fetchAll('assoc');
										
		$this->set(compact('data'));
        $this->set('_serialize', ['data']);
		
	}
}
