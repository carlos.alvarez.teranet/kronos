<?php
namespace App\Controller\ClientAttribute;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Datasource\ConnectionManager;

class ClientsController extends AppController
{
	public function initialize()
	{
		parent::initialize();
		// Definir base de datos a utilizar
		$connection = ConnectionManager::get('BR_KWP');
		$this->Clients->setConnection($connection);
		
		// Tablas externas a utilizar
		$this->loadModel('Flags');
	}
	
	public function beforeFilter(Event $event) {
		
		parent::beforeFilter($event);
    }
    
	public function index()
	{
		
		// Listar clientes
		$clients = $this->Clients->find('all', [
			'contain' => ['Flags']
		]);
        $this->set('clients', $clients);	   
         
	}
	
	public function add()
    {
        $client = $this->Clients->newEntity();
        if ($this->request->is('post')) {
            $client = $this->Clients->patchEntity($client, $this->request->getData());
            if ($this->Clients->save($client)) {
                $this->Flash->success(__('El cliente ha sido creado.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('El cliente no pudo ser creado. Favor intente más tarde.'));
        }
        
        // Listado de FlgDesativacao
        $flags = $this->Flags->find('list', [
	        'keyField' => 'code',
	        'valueField' => 'description',
	        'conditions' => [
		        'active' => true
	        ]
        ]);
                
        $this->set(compact('client'));
        $this->set(compact('flags'));
        $this->set('_serialize', ['client', 'flags']);
    }
    
    public function edit($id = null)
    {
        $client = $this->Clients->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $client = $this->Clients->patchEntity($client, $this->request->getData());
            if ($this->Clients->save($client)) {
                $this->Flash->success(__('El cliente ha sido actualizado.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('El cliente no pudo ser actualizado. Favor intente más tarde.'));
        }
        
        // Listado de FlgDesativacao
        $flags = $this->Flags->find('list', [
	        'keyField' => 'code',
	        'valueField' => 'description',
	        'conditions' => [
		        'active' => true
	        ]
        ]);
        
        $this->set(compact('client'));
        $this->set(compact('flags'));
        $this->set('_serialize', ['client', 'flags']);
    }

}