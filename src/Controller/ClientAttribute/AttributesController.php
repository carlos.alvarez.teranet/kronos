<?php
namespace App\Controller\ClientAttribute;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\I18n\Time;
use Cake\Datasource\ConnectionManager;
use Cake\Network\Exception\NotFoundException;

class AttributesController extends AppController
{
	
	var $connection = null;
	
	public function initialize()
	{
		parent::initialize();
		// Definir base de datos a utilizar
		$this->connection = ConnectionManager::get('BR_KWP');
		$this->Attributes->setConnection($this->connection);
		
		// Tablas externas a utilizar
		$this->loadModel('Flags');
		$this->loadModel('Markets');
		$this->loadModel('Clients');
		$this->loadModel('Dimensions');
	}
	
	public function beforeFilter(Event $event) {
		
		parent::beforeFilter($event);
    }
    
    public function index()
	{
		
		$this->Clients->setConnection(ConnectionManager::get('BR_KWP'));
	    $this->Markets->setConnection(ConnectionManager::get('BR_KWP'));
		
		// Listar dimensiones
		$attributes = $this->Attributes->find('all', [
			'contain' => ['Flags', 'Clients', 'Markets']
		]);
        $this->set('attributes', $attributes);	   
         
	}
	
	public function add()
    {
	    
	    $this->Clients->setConnection(ConnectionManager::get('BR_KWP'));
	    $this->Markets->setConnection(ConnectionManager::get('BR_KWP'));
	    
	    // Obtener entidad
        $attribute = $this->Attributes->newEntity();
        if ($this->request->is('post')) {
            $attribute = $this->Attributes->patchEntity($attribute, $this->request->getData());
            if ($this->Attributes->save($attribute)) {
                $this->Flash->success(__('El atributo ha sido creado.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('El atributo no pudo ser creado. Favor intente más tarde.'));
        }
        
        // Listado de FlgDesativacao
        $flags = $this->Flags->find('list', [
	        'keyField' => 'code',
	        'valueField' => 'description',
	        'conditions' => [
		        'active' => true
	        ]
        ]);
        
        // Listado de Clientes
        $clients = $this->Clients->find('list', [
	        'keyField' => 'IdClient',
	        'valueField' => 'Descricao',
	        'conditions' => [
		        'FlgDesativacao' => 'AT'
	        ]
        ]);
        
        // Listado de Mercados
        $markets = $this->Markets->find('list', [
	        'keyField' => 'IdMarket',
	        'valueField' => 'Descricao',
	        'conditions' => [
		        'FlgDesativacao' => 'AT'
	        ]
        ]);
                
        $this->set(compact('attribute'));
        $this->set(compact('flags'));
        $this->set(compact('clients'));
        $this->set(compact('markets'));
        $this->set('_serialize', ['attribute', 'flags', 'clients', 'markets']);
    }
	
	public function edit($id = null)
    {
	    
	    $this->Clients->setConnection(ConnectionManager::get('BR_KWP'));
	    $this->Markets->setConnection(ConnectionManager::get('BR_KWP'));
	    
	    // Obtener entidad
        $attribute = $this->Attributes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $attribute = $this->Attributes->patchEntity($attribute, $this->request->getData());
            if ($this->Attributes->save($attribute)) {
                $this->Flash->success(__('El atributo ha sido actualizado.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('El atributo no pudo ser actualizado. Favor intente más tarde.'));
        }
        
        // Listado de FlgDesativacao
        $flags = $this->Flags->find('list', [
	        'keyField' => 'code',
	        'valueField' => 'description',
	        'conditions' => [
		        'active' => true
	        ]
        ]);
        
        // Listado de Clientes
        $clients = $this->Clients->find('list', [
	        'keyField' => 'IdClient',
	        'valueField' => 'Descricao',
	        'conditions' => [
		        'FlgDesativacao' => 'AT'
	        ]
        ]);
        
        // Listado de Mercados
        $markets = $this->Markets->find('list', [
	        'keyField' => 'IdMarket',
	        'valueField' => 'Descricao',
	        'conditions' => [
		        'FlgDesativacao' => 'AT'
	        ]
        ]);
        
        $this->set(compact('attribute'));
        $this->set(compact('flags'));
        $this->set(compact('clients'));
        $this->set(compact('markets'));
        $this->set('_serialize', ['attribute', 'flags', 'clients', 'markets']);
    }
    
    public function detail($id = null)
    {
	    
	    $this->Clients->setConnection(ConnectionManager::get('BR_KWP'));
	    $this->Markets->setConnection(ConnectionManager::get('BR_KWP'));
	    $this->Dimensions->setConnection(ConnectionManager::get('BR_KWP'));
	    
	    $attribute = $this->Attributes->get($id, [
            'contain' => ['Clients', 'Markets', 'Dimensions']
        ]);
        
        $dimensions = $this->Dimensions->find('list', [
	        'keyField' => 'IdDimension',
	        'valueField' => 'Descricao',
	        'conditions' => [
		        'FlgDesativacao' => 'AT'
	        ]
        ]);
	    
        $this->set('dimensions', $dimensions);
        $this->set('attribute', $attribute);
	    
    }
    
    public function order($id = null) {
	    
	    $multiplier = 100000;
	    
	    if(is_null($id)) {
		    throw new NotFoundException(__('Debe indicar identificador'));
	    }
	    
	    if($this->request->is('post') or $this->request->is('put')) {
		    
		    $data = $this->request->data();
		    
		    // Update de ORDER
		    foreach($data['item'] as $key => $item) {
			    $update = $this->connection->execute("UPDATE dbo.A_ClientAttribute_Dimensions SET Orden = :orden WHERE IdClientAttribute = :IdClientAttribute AND IdDimension = :IdDimension", ['orden' => ($key+1) + $multiplier, 'IdClientAttribute' => $id, 'IdDimension' => $item]);
		    }
		    
		    // Update Final
		    $confirm = $this->connection->execute("UPDATE dbo.A_ClientAttribute_Dimensions SET Orden = (Orden - :multiplier) WHERE IdClientAttribute = :IdClientAttribute", ['multiplier' => $multiplier, 'IdClientAttribute' => $id]);
		    
		    $this->set('error', false);
		    $this->set('_serialize', ['error']);		    
		    
	    }
	    
    }

}