<?php
namespace App\Controller\ClientAttribute;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Datasource\ConnectionManager;

class SubsController extends AppController
{
	public function initialize()
	{
		parent::initialize();
		$this->loadComponent('RequestHandler');
		
		// Definir base de datos a utilizar
		$connection = ConnectionManager::get('BR_KWP');
		$this->Subs->setConnection($connection);
		
		// Tablas externas a utilizar
	}
	
	public function beforeFilter(Event $event) {
		
		parent::beforeFilter($event);
    }
    
	public function getbyidproduto() {
		
		$data = $this->request->data();
		
		if($this->request->is('post')) {
			
			$subs = $this->Subs->find('list', [
				        'valueField' => function ($row) {
				            return $row['Descricao'] . ' (' . $row['IdSub'] . ')';
				        },
				        'keyField' => 'IdSub'
				    ])
				    ->where(['FlgDesativacao' => 'AT', 'IdProduto' => $data['IdProduto']])
				    ->order(['Descricao' => 'ASC']);
				    
			$this->set('subs', $subs);
			$this->set('_serialize', ['subs']);
			
		}
		
	}

}