<?php
namespace App\Controller\ClientAttribute;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\I18n\Time;
use Cake\Datasource\ConnectionManager;
use Cake\Network\Exception\NotFoundException;

class MarketsController extends AppController
{
	
	var $connection = null;
	
	public function initialize()
	{
		parent::initialize();
		// Definir base de datos a utilizar
		$this->connection = ConnectionManager::get('BR_KWP');
		$this->Markets->setConnection($this->connection);
		
		// Tablas externas a utilizar
		$this->loadModel('Flags');
		$this->loadModel('Subs');
		$this->loadModel('Products');
	}
	
	public function beforeFilter(Event $event) {
		
		parent::beforeFilter($event);
    }
    
	public function index()
	{
		
		// Listar mercados
		$markets = $this->Markets->find('all', [
			'contain' => ['Flags']
		]);
        $this->set('markets', $markets);	   
         
	}
	
	public function add()
    {
        $market = $this->Markets->newEntity();
        if ($this->request->is('post')) {
            $market = $this->Markets->patchEntity($market, $this->request->getData());
            if ($this->Markets->save($market)) {
                $this->Flash->success(__('El mercado ha sido creado.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('El mercado no pudo ser creado. Favor intente más tarde.'));
        }
        
        // Listado de FlgDesativacao
        $flags = $this->Flags->find('list', [
	        'keyField' => 'code',
	        'valueField' => 'description',
	        'conditions' => [
		        'active' => true
	        ]
        ]);
                
        $this->set(compact('market'));
        $this->set(compact('flags'));
        $this->set('_serialize', ['market', 'flags']);
    }
    
    public function edit($id = null)
    {
        $market = $this->Markets->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $market = $this->Markets->patchEntity($market, $this->request->getData());
            if ($this->Markets->save($market)) {
                $this->Flash->success(__('El mercado ha sido actualizado.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('El mercado no pudo ser actualizado. Favor intente más tarde.'));
        }
        
        // Listado de FlgDesativacao
        $flags = $this->Flags->find('list', [
	        'keyField' => 'code',
	        'valueField' => 'description',
	        'conditions' => [
		        'active' => true
	        ]
        ]);
        
        $this->set(compact('market'));
        $this->set(compact('flags'));
        $this->set('_serialize', ['market', 'flags']);
    }
    
    public function detail($id = null)
    {
	    
	    $this->Subs->setConnection(ConnectionManager::get('BR_KWP'));
	    $this->Products->setConnection(ConnectionManager::get('BR_KWP'));
	    
	    $market = $this->Markets->get($id, [
            'contain' => ['Subs', 'Subs.Products']
        ]);
	    
	    if($this->request->is('put')) {
		    
		    $data = $this->request->data();
		    
		    $pairs = [];
		    if(is_array($data['IdSub'])) {
			    foreach($data['IdSub'] as $IdSub) {
				    if(!empty($IdSub) or $IdSub == 0) {
					    $now = Time::now();
					    $now = $now->format('Y-m-d\TH:i:s');
					    $pairs[] = ['IdProduto' => $data['IdProduto'], 'IdSub' => $IdSub, '_joinData' => ['DtCriacao' => $now, 'CreatedBy' => $this->Auth->User()['cn'][0]]];
				    } else {
					    $this->Flash->set('Sebe seleccionar al menos un Subproduto!', [
						    'element' => 'danger'
						]);
						return $this->redirect(['controller' => 'markets', 'action' => 'detail', $id]);
				    }
			    }
			} else {
				$this->Flash->set('Debe completar todos los campos!', [
				    'element' => 'danger'
				]);
				return $this->redirect(['controller' => 'markets', 'action' => 'detail', $id]);
			}
		    
		    $entity = [
			    'IdMarket' => $data['IdMarket'],
			    'Descricao' => $market['Descricao'],
			    'subs' => $pairs
		    ];
		    
		    $newEntity 	= $this->Markets->patchEntity($market, $entity, ['associated' => 'Subs']);
		    $save		= $this->Markets->save($newEntity);
		    
		    $market = $this->Markets->get($id, [
	            'contain' => ['Subs', 'Subs.Products']
	        ]);
		    
		    if($save) {
		        $this->Flash->set('Market modificado exitosamente!', [
				    'element' => 'success'
				]);
		    } else {
			    $this->Flash->set('Problemas al modificar mercado, favor intente mas tarde!', [
				    'element' => 'error'
				]);
		    }
		    
	    }
	    
	    // Listado de Produtos
	    $products = $this->Products->find('list', [
				        'valueField' => function ($row) {
				            return $row['Descricao'] . ' (' . $row['IdProduto'] . ')';
				        }
				    ])
				    ->where(['FlgDesativacao' => 'AT'])
				    ->order(['Descricao' => 'ASC']);
	    
        
        $this->set('market', $market);
        $this->set('products', $products);
	    
    }
    
    public function remove($id = null) {
	    
	    if($this->request->is('post') or $this->request->is('put')) {
		    
		    $data = $this->request->data();
		    
		    if(is_array($data['check-sub']) and !empty($data['check-sub'])) {
			    foreach($data['check-sub'] as $relation) {
			    	$exists = $this->connection->execute("DELETE FROM dbo.A_Market_Products WHERE Id = (:id)", ['id' => $relation]);
		    	}
		    }
		    
		    $this->Flash->set('Relaciones eliminadas con éxito!', [
			    'element' => 'success'
			]);
		    return $this->redirect($this->referer());
		    
	    }
	    
	    if(!is_null($id)) {
		    
		    $exists = $this->connection->execute("SELECT * FROM dbo.A_Market_Products WHERE Id = :id", ['id' => $id])->fetch('assoc');
		    if($exists) {
			    $result = $this->connection->execute("DELETE dbo.A_Market_Products WHERE Id = :id", ['id' => $id]);
			    $this->Flash->set('Relación eliminada con éxito!', [
				    'element' => 'success'
				]);
		    } else {
			    $this->Flash->set('Ha ocurrido un problema al eliminar la relación, favor intente mas tarde', [
				    'element' => 'danger'
				]);
		    }
		    
		    return $this->redirect(['controller' => 'markets', 'action' => 'detail', $exists['IdMarket']]);
		    
	    } else {
		    throw new NotFoundException(__('Error al solicitar instrucción'));
	    }
	    
    }

}