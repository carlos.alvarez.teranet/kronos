<?php
namespace App\Controller\ClientAttribute;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\I18n\Time;
use Cake\Datasource\ConnectionManager;
use Cake\Network\Exception\NotFoundException;

class DimensionsController extends AppController
{
	
	var $connection = null;
	
	public function initialize()
	{
		parent::initialize();
		// Definir base de datos a utilizar
		$this->connection = ConnectionManager::get('BR_KWP');
		$this->Dimensions->setConnection($this->connection);
		
		// Tablas externas a utilizar
		$this->loadModel('Flags');
	}
	
	public function beforeFilter(Event $event) {
		
		parent::beforeFilter($event);
    }
    
    public function index()
	{
		
		// Listar dimensiones
		$dimensions = $this->Dimensions->find('all', [
			'contain' => ['Flags']
		]);
        $this->set('dimensions', $dimensions);	   
         
	}
	
	public function add()
    {
        $dimension = $this->Dimensions->newEntity();
        if ($this->request->is('post')) {
            $dimension = $this->Dimensions->patchEntity($dimension, $this->request->getData());
            if ($this->Dimensions->save($dimension)) {
                $this->Flash->success(__('La dimensión ha sido creada.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('La dimensión no pudo ser creada. Favor intente más tarde.'));
        }
        
        // Listado de FlgDesativacao
        $flags = $this->Flags->find('list', [
	        'keyField' => 'code',
	        'valueField' => 'description',
	        'conditions' => [
		        'active' => true
	        ]
        ]);
                
        $this->set(compact('dimension'));
        $this->set(compact('flags'));
        $this->set('_serialize', ['dimension', 'flags']);
    }
	
	public function edit($id = null)
    {
        $dimension = $this->Dimensions->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $dimension = $this->Dimensions->patchEntity($dimension, $this->request->getData());
            if ($this->Dimensions->save($dimension)) {
                $this->Flash->success(__('La dimensión ha sido actualizada.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('La dimensión no pudo ser actualizada. Favor intente más tarde.'));
        }
        
        // Listado de FlgDesativacao
        $flags = $this->Flags->find('list', [
	        'keyField' => 'code',
	        'valueField' => 'description',
	        'conditions' => [
		        'active' => true
	        ]
        ]);
        
        $this->set(compact('dimension'));
        $this->set(compact('flags'));
        $this->set('_serialize', ['dimension', 'flags']);
    }

}