<?php
namespace App\Controller\ClientAttribute;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Datasource\ConnectionManager;

class FlagsController extends AppController
{
	public function initialize()
	{
		parent::initialize();
	}
	
	public function beforeFilter(Event $event) {
		
		parent::beforeFilter($event);
    }
    
	public function index()
	{
		
		// Listar Flags
		$flags = $this->Flags->find('all');
        $this->set('flags', $flags);	   
         
	}
	
	public function add()
    {
        $flag = $this->Flags->newEntity();
        if ($this->request->is('post')) {
            $flag = $this->Flags->patchEntity($flag, $this->request->getData());
            if ($this->Flags->save($flag)) {
                $this->Flash->success(__('El flag (status) ha sido creado.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('El flag (status) no pudo ser creado. Favor intente más tarde.'));
        }
        
        // Listado de FlgDesativacao
        $flags = $this->Flags->find('all');
                
        $this->set(compact('flag'));
        $this->set('_serialize', ['flag']);
    }
    
    public function edit($id = null)
    {
        $flag = $this->Flags->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $flag = $this->Flags->patchEntity($flag, $this->request->getData());
            if ($this->Flags->save($flag)) {
                $this->Flash->success(__('El flag (status) ha sido actualizado.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('El flag (status) no pudo ser actualizado. Favor intente más tarde.'));
        }
        
        
        $this->set(compact('flag'));
        $this->set('_serialize', ['flag']);
    }

}