<?php
namespace App\Controller\ClientAttribute;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\I18n\Time;
use Cake\Datasource\ConnectionManager;
use Cake\Network\Exception\ForbiddenException;
use Cake\Console\ShellDispatcher;
use PHPExcel;
use PHPExcel_IOFactory;
use Cake\Network\Response;

class AssignmentsCountriesController extends AppController
{
	public $fields;
	
	public function initialize()
	{
		parent::initialize();
		$this->loadModel('Attributes');
		$this->loadModel('Owners');
		$this->loadModel('Markets');
		$this->loadModel('Dimensions');
		$this->loadModel('Subs');
		$this->loadModel('Products');
		$this->loadModel('Clients');
		$this->loadModel('AttributesValues');
		$this->loadModel('AttributesConfig');
		$this->loadModel('AttributesFiles');
		$this->loadModel('AttributesAdmin');
		
		$this->loadModel('AttributesCountries');
		$this->loadModel('AttributesPendantsDetails');
		$this->loadModel('Flags');
		
		$this->loadComponent('Arrays');
		$this->loadComponent('Cookie');
		
		$this->fields = [
			'IdProduto' => 'Produto',
			'IdSub' => 'Sub',
			'IdFabricante' => 'Fabricante',
			'IdMarca' => 'Marca',
			'IdConteudo' => 'Conteudo',
			'CdC01' => 'Clas01',
			'CdC02' => 'Clas02',
			'CdC03' => 'Clas03',
			'CdC04' => 'Clas04',
			'CdC05' => 'Clas05',
			'CdC06' => 'Clas06',
			'CdC07' => 'Clas07',
			'CdC08' => 'Clas08',
			'CdC09' => 'Clas09',
			'CdC10' => 'Clas10',
			'IdArtigo' => 'CodBar'
		];

	}
	
	public function beforeFilter(Event $event) {
		
		parent::beforeFilter($event);
		
		$this->connection = ConnectionManager::get('IT_WORKFLOW');
		$this->AttributesCountries->setConnection($this->connection);
		$this->AttributesPendantsDetails->setConnection($this->connection);
		
		// Chequeo de Permisos
		$session 	= $this->Auth->User('admins');
		$prefix		= $this->request->getParam('prefix');
		$controller	= strtolower($this->request->getParam('controller'));
		$level		= 0;
		
		foreach($session as $element) {
			if(strtolower($element['prefix']) == strtolower($prefix) and strtolower($element['controller']) == strtolower($controller)) {
				$level = $element['level'];
			}
		}
		
		$this->set('level', $level);
		
    }
    
    public function index()
    {		
		// Listar paises
		$countries = $this->AttributesCountries->find('all', [
			'contain' => ['Flags'],
			'conditions' => [
				'FlgDesativacao' => 'AT'
			],
			'order' => [
				'Descricao' => 'ASC'
			]
		]);
		
		$files = [];
		$sprend = [];
		$availables = [];
		$update= [];
		
		// Calculo de Asignaciones habilitadas
		foreach($countries as $key => $country) {
			
			if(file_exists(LOGS . DS . "refresh" . DS . $country['Descricao'] . DS . $country['Id'] . ".lock")) {
				$files[] = $country['Id'];
			}
			
			$country['lu'] = array_reverse(glob(LOGS . DS . "refresh" . DS . $country['Descricao'] . DS . "*.update"));
			
			$this->Owners->setConnection(ConnectionManager::get($country['DbConfig']));
			$this->Attributes->setConnection(ConnectionManager::get($country['DbConfig']));
			$this->AttributesAdmin->setConnection(ConnectionManager::get($country['DbConfig']));
			
			// Listado de administradores
			$admins = $this->AttributesAdmin->find('list', [
				'conditions' => [
					'Admin' => strtolower($this->Auth->User('mail'))
				]
			]);
			
			$enable = $this->AttributesAdmin->find('list', [
				'conditions' => [
					'Admin' => strtolower($this->Auth->User('mail')),
					'AllowUpdate' => true
				]
			]);
			
			// Listado de propietarios			
			$auths = $this->Owners->find('all', [
				'conditions' => [
					'Owner' => $this->Auth->User('mail')
				]
			]);
			
			if($admins->isEmpty()) {
				$availables[$country['Id']] = ($auths->isEmpty()) ? false : true;
			} else {
				$availables[$country['Id']] = true;
			}
			
			$list = [];
			foreach($auths as $auth) {
				if(!in_array($auth['IdClientAttribute'], $list)) {
					$list[] = $auth['IdClientAttribute'];
				}
			}			
			
			if(!$admins->isEmpty()) {
				$data = $this->AttributesPendantsDetails->find('all', [
					'conditions' => [
						'Country_id' => $country['Id'],
						'FlgDesativacao' => 'AT'
					]
				]);
				if(!$enable->isEmpty()) {
					$update[] = $country['Id'];
				}
			} else {
				$data = $this->AttributesPendantsDetails->find('all', [
					'conditions' => [
						'Country_id' => $country['Id'],
						'FlgDesativacao' => 'AT',
						'ClientAttribute_id IN' => ($list ? $list : [-1])
					]
				]);
			}
			
			$pendants[$country['Id']] = $data->count();
			
			// Fecha de SPREND
			$this->connection = ConnectionManager::get($country->DbConfig);
			$query 	= "SELECT CONVERT(VARCHAR(8),MAX(DATEVALUE),112) as Date FROM dbo.SPREnd";
			$sprend[$country['Id']] = $this->connection->execute($query)->fetch('assoc');
			
		}
		
		
		// Titulo de Pagina
		$pageTitle = 'Listado de países';
		
        $this->set('countries', $countries);
        $this->set('pendants', $pendants);
        $this->set('pageTitle', $pageTitle);
        $this->set('sprend', $sprend);
        $this->set('files', $files);
        $this->set('availables', $availables);
        $this->set('update', $update);
	    
	}
    
	public function display($country_id = null)
	{
		
		// Asignar bases de datos
		$country = $this->setDb($country_id);
		
		// Asignaciones de permisos
		$auths = $this->Owners->find('all', [
			'conditions' => [
				'Owner' => $this->Auth->User('mail')
			]
		]);
		
		// Listado de administradores
		$admins = $this->AttributesAdmin->find('list', [
			'conditions' => [
				'Admin' => strtolower($this->Auth->User('mail'))
			]
		]);
		
		$list = [];
		foreach($auths as $auth) {
			if(!in_array($auth['IdClientAttribute'], $list)) {
				$list[] = $auth['IdClientAttribute'];
			}
		}
		
		// Chequeo de Administracion
		if(!$admins->isEmpty()) {
			$attributes = $this->Attributes->find('all', [
			    'conditions' => [
				    'Attributes.FlgDesativacao' => 'AT',
				    'UpdateCount' => 1
			    ],
			    'contain' => ['Owners', 'Markets', 'Clients'],
			    'sort' => ['Attributes.Client' => 'ASC']
		    ])->toArray();
		} else {
			$attributes = $this->Attributes->find('all', [
			    'conditions' => [
				    'Attributes.FlgDesativacao' => 'AT',
				    'UpdateCount' => 1,
				    'Attributes.IdClientAttribute IN ' => ($list ? $list : [-1])
			    ],
			    'contain' => ['Owners', 'Markets', 'Clients'],
			    'sort' => ['Attributes.Client' => 'ASC']
		    ])->toArray();
		}
		
		// Calculo de pendientes
		foreach($attributes as $attribute) {
			$pendants[$attribute['IdClientAttribute']] = $this->AttributesPendantsDetails->find('all', [
				'conditions' => [
					'Country_id' => $country_id,
					'FlgDesativacao' => 'AT',
					'ClientAttribute_id' => $attribute['IdClientAttribute']
				]
			])->count();
		}
		
		// Fecha de SPREND
		$query 	= "SELECT CONVERT(VARCHAR(8),MAX(DATEVALUE),112) as Date FROM dbo.SPREnd";
		$sprend = $this->connection->execute($query)->fetch('assoc');
			
	    
	    // Titulo de Pagina
		$pageTitle = 'Listado de Client Attributes para <strong>'.$country->Descricao.'</strong>';
		
		// Archivo de actualizacion
		if(file_exists(LOGS . DS . "refresh" . DS . $country['Descricao'] . DS . $country['Id'] . ".lock")) {
			$updating = true;
		}
	    
	    $this->set(compact('attributes'));
	    $this->set(compact('pendants'));
	    $this->set(compact('country'));
	    $this->set(compact('pageTitle'));
	    $this->set(compact('sprend'));
	    $this->set(compact('updating'));
	    $this->set('_serialize', ['attributes', 'pendants', 'country', 'pageTitle', 'sprend', 'updating']);
	    
	}
	
	public function readFile($country_id = null) {
		
		$this->viewBuilder()->layout(false);
		$this->autoRender = false;
		
		// Listado de Paises
		if(is_null($country_id)) {
			return false;
		}
        $country = $this->AttributesCountries->find('all', [
	        'conditions' => [
		    	'Id' => $country_id
	        ]
        ])->first();
        
        $files = scandir(LOGS . DS . "refresh" . DS . $country['Descricao'] . DS, SCANDIR_SORT_DESCENDING);
		$newest_file = $files[0];
		
		$cmd = "tail -n 50 " . LOGS . DS . "refresh" . DS . $country['Descricao'] . DS . $newest_file;
		$output = shell_exec($cmd);
		$output = str_replace(PHP_EOL, "<br>", $output);
		
		if(file_exists(LOGS . DS . "refresh" . DS . $country['Descricao'] . DS . $country_id . ".lock")) {
			echo $output;
		} else {
			echo "END";
		}	
		
	}
	
	public function refresh ($country_id = null) {
		
		// Seleccionar país
		$country = $this->AttributesCountries->get($country_id);
		$this->AttributesAdmin->setConnection(ConnectionManager::get($country['DbConfig']));
		
		// Listado de administradores
		$admins = $this->AttributesAdmin->find('list', [
			'conditions' => [
				'Admin' => $this->Auth->User('mail')
			]
		]);
		
		$output = false;
		if(!$admins->isEmpty()) {
			$output = exec(ROOT . DS . 'bin/cake client_attribute_pendants ' . $country_id . ' > /dev/null 2>&1 & echo $!;', $o);
		}
		
		$response = [];
        if ($output) {
	        $response = [
		        'status' => true,
		        'message' => 'Comando de ejecución en proceso.',
		        'pid' => $output
	        ];
        } else {
            $response = [
		        'status' => false,
		        'message' => 'Ha ocurrido un problema con la ejecución del comando, favor intente mas tarde o comuníquese con su administrador.',
		        'output' => $output,
		        'command' => ROOT . DS . 'bin/cake client_attribute_pendants ' . $country_id . ' > /dev/null 2>&1 & echo $!;'
	        ];
        }
        
        $this->set(compact('response'));
	    $this->set('_serialize', ['response']);
		
	}
	
	public function update ($country_id = null, $id = null) {
		
		// Asignar bases de datos
		$country = $this->setDb($country_id);
		
		// Validar si el atributo puede ser modificado por el usuario logeado
		$list = $this->Owners->find('list', [
			'valueField' => 'IdClientAttribute',
			'conditions' => [
				'Owner' => $this->Auth->User('mail')
			]
		])->toArray();
		
		$admins = $this->AttributesAdmin->find('list', [
			'conditions' => [
				'Admin' => $this->Auth->User('mail')
			]
		]);
		
		if(!in_array($id, $list) and $admins->isEmpty()) {
			throw new ForbiddenException(__('No tienes permiso para ingresar a este atributo'));
		}
		
		if($this->request->is('post') or $this->request->is('put')) {
			
			$requests = $this->request->data('Config');
						
			$data = [];
			$pendant_details = [];
			foreach($requests as $key => $request) {
				if(empty($request['IdValue'])) {
					continue;
				}
				$request['IdClientAttribute'] = $id;
				$request['createdBy'] = $this->Auth->User('mail');
				$data[] = $request;
				$pendant_details[] = [
					'Id' => $key,
					'UpdatedBy' => $this->Auth->User('mail'),
					'FlgDesativacao' => 'AR'
				];
			}
			
			if(empty($pendant_details)) {
				$response = [
					'error' => true,
					'type' => 'danger',
					'message' => 'No se han especificado cambios a realizar'
				];
			} else {
				
				$entities 	= $this->AttributesConfig->newEntities($data);
				$details	= $this->AttributesPendantsDetails->newEntities($pendant_details);
				
				$input = [];
				foreach($entities as $entity) {
					for($i=1; $i<=10; $i++) {
						if(!array_key_exists('Dim'.str_pad($i, 2, 0, STR_PAD_LEFT), $request)) {
							$entity['Dim'.str_pad($i, 2, 0, STR_PAD_LEFT)] = 0;
						}
					}
					$input[] = $entity;
				}
				
				
				if ($this->AttributesConfig->saveMany($input) and $this->AttributesPendantsDetails->saveMany($details)) {
					$response = [
						'error' => false,
						'type' => 'success',
						'message' => 'Las modificaciones han sido efectuadas'
					];
					$this->Flash->success(__('Las asociaciones han sido creadas.'));
	            } else {
		            $response = [
						'error' => true,
						'type' => 'danger',
						'message' => 'Ha ocurrido un error al intentar guardar los cambios, favor intente mas tarde.'
					]; 
	            }
				
			}
			
		}
		
		$this->set(compact('response'));
		$this->set(compact('country'));
		$this->set('_serialize', ['response', 'country']);
		
	}
	
	public function detail ($country_id = null, $id = null) {
		
		// Asignar bases de datos
		$country = $this->setDb($country_id);
		
		// Validar si el atributo puede ser modificado por el usuario logeado
		$list = $this->Owners->find('list', [
			'valueField' => 'IdClientAttribute',
			'conditions' => [
				'Owner' => $this->Auth->User('mail')
			]
		])->toArray();
		
		$admins = $this->AttributesAdmin->find('list', [
			'conditions' => [
				'Admin' => $this->Auth->User('mail')
			]
		]);
		
		if(!in_array($id, $list) and $admins->isEmpty()) {
			throw new ForbiddenException(__('No tienes permiso para ingresar a este atributo'));
		}
		
		// Transforma los datos de SQL a un array
		$database = $this->databaseToArray($id, $country_id);
			
		$attribute 	= $database['attribute'];
		$results	= $database['results'];
		$forms		= $database['forms'];
		$select		= $database['select'];
		$function	= $database['function'];
		$values		= $database['values'];
		$pendants	= $database['pendants'];
		
		// Titulo de Pagina
		$pageTitle = 'Detalle de client attribute';
		
		$this->set(compact('attribute', 'query', 'results', 'forms',  'select', 'values', 'function', 'pageTitle', 'country', 'pendants'));
	    $this->set('_serialize', ['attribute', 'query', 'results', 'forms',  'select', 'values', 'function', 'pageTitle', 'country', 'pendants']);
		
	}
	
	private function setDb($id = null) {
		
		$country = $this->AttributesCountries->get($id);
		
		$this->connection = ConnectionManager::get($country->DbConfig);
		$this->Attributes->setConnection($this->connection);
		$this->Owners->setConnection($this->connection);
		$this->Markets->setConnection($this->connection);
		$this->Dimensions->setConnection($this->connection);
		$this->Subs->setConnection($this->connection);
		$this->Products->setConnection($this->connection);
		$this->Clients->setConnection($this->connection);
		$this->AttributesValues->setConnection($this->connection);
		$this->AttributesConfig->setConnection($this->connection);
		$this->AttributesAdmin->setConnection($this->connection);
		
		return $country;
		
	}
	
	public function upload($country_id = null, $id = null) {
		
		$country = $this->setDb($country_id);
		
		$attribute = $this->Attributes->get($id, [
			'contain' => ['Owners', 'Markets.Subs.Products', 'Clients', 'Dimensions']
		]);
		
		if($this->request->is('post')) {
			
			$data = $this->request->data();
			$file = $this->AttributesFiles->uploadRead($data['excel']);
			$comp = $this->compareDatabase($file['data'], $attribute->IdClientAttribute);
			
			// Registrar la accion de subir archivos
			$insert = [
				'Descricao' => $file['destFile'],
				'Country_id' => $country_id,
				'ClientAttribute_id' => $id,
				'Changes' => count($comp),
				'DtCriacao' => date('Ymd H:i:s'),
				'CreatedBy' => $this->Auth->User('mail')
			];
			
			$entity = $this->AttributesFiles->newEntity($insert);
			$save = $this->AttributesFiles->save($entity);
			
			$data = $comp;
			
			// Titulo de Pagina
			$pageTitle = 'Upload de excel para client attribute';
			
			$this->set(compact('data', 'attribute', 'save', 'country', 'rowData', 'pageTitle'));
			$this->set('_serialize', ['data', 'attribute', 'save', 'country', 'rowData', 'pageTitle']);
			
		}
		
	}
	
	public function process($id = null) {
		
		$workflow = ConnectionManager::get('IT_WORKFLOW');
		
		$entity = $this->AttributesFiles->get($this->toNumbers($id), [
			'conditions' => [
				'Done' => false
			]
		]);
		
		$country = $this->setDb($entity->Country_id);
		$ClientAttribute_id = $entity->ClientAttribute_id;
		
		$file = $this->AttributesFiles->uploadRead($entity->Descricao, true);
		$comp = $this->compareDatabase($file['data'], $entity->ClientAttribute_id);
		
		// Listado de campos
		$fields = $this->fields;
		
		// Listado de posibles asignaciones
		$values = $this->AttributesValues->find('list', [
			'keyField' => 'IdValue',
			'valueField' => 'Descricao',
			'conditions' => [
				'FlgDesativacao' => 'AT',
				'IdClientAttribute' => $ClientAttribute_id
			],
			'order' => ['Descricao' => 'ASC']
		])->toArray();
		
		// Campos de dimensiones
		$attribute = $this->Attributes->get($ClientAttribute_id, [
			'contain' => ['Owners', 'Markets.Subs.Products', 'Clients', 'Dimensions']
		]);
		
		// Generar UPDATE
		$this->connection->begin();
		try {
			foreach($comp as $item) {
				
				$dims = [];
				$bind = [];
				
				$bind['IdValue'] = (array_search($item['Value'], $values)) ? array_search($item['Value'], $values) : -1;
				$bind['createdBy'] = $this->Auth->User('mail');
				$bind['IdClientAttribute'] = $ClientAttribute_id;
				
				$ins['IdValue'] = (array_search($item['Value'], $values)) ? array_search($item['Value'], $values) : -1;
				$ins['createdBy'] = "'".$this->Auth->User('mail')."'";
				$ins['IdClientAttribute'] = $ClientAttribute_id;
				
				foreach($attribute->dimensions as $dimension) {
					$dims[] = "Dim0".$dimension['_joinData']['Orden']." = :".$dimension['Descricao'];
					$bind[$dimension['Descricao']] = $item[$dimension['Descricao']];
					$ins["Dim0".$dimension['_joinData']['Orden']] = (empty($item[$dimension['Descricao']]) ? 0 : $item[$dimension['Descricao']]);
				}
				
				$query = 'UPDATE dbo.A_ClientAttribute_Config SET IdValue = :IdValue, createdBy = :createdBy OUTPUT INSERTED.* WHERE IdClientAttribute = :IdClientAttribute AND '.implode(' AND ', $dims);
				$updated = $this->connection->execute($query, $bind)->fetchAll('assoc');
				
				if(count($updated) == 0) {
					$query = 'INSERT INTO dbo.A_ClientAttribute_Config ('.implode(",", array_keys($ins)).') VALUES ('.implode(",", array_values($ins)).')';
					$inserted = $this->connection->execute($query);
				}
		    	
		    }
		} catch (\Exception $e) {
			$this->connection->rollback();
			$this->Flash->error('No se ha podido procesar el archivo. Error detectado: '.$query);
			$this->redirect(['action' => 'detail', $entity->Country_id, $entity->ClientAttribute_id, 'prefix' => 'client_attribute']);
			return null;
		}
		
		// Generar UPDATE de Pendats Detail
		$workflow->begin();
		try {
			foreach($comp as $item) {
				
				$dims = [];
				$bind = [];
				
				$bind['Country_id'] = $entity->Country_id;
				$bind['ClientAttribute_id'] = $ClientAttribute_id;
				
				foreach($attribute->dimensions as $dimension) {
					if(is_null($item[$dimension['Descricao']])) {
						$dims[] = $dimension['Descricao']." IS NULL";
					} else {
						$dims[] = $dimension['Descricao']." = :".$dimension['Descricao'];
						$bind[$dimension['Descricao']] = $item[$dimension['Descricao']];
					}
				}
				$query = 'UPDATE dbo.ClientAttribute_Pendants_Details SET FlgDesativacao = \'AR\' WHERE Country_id = :Country_id AND ClientAttribute_id = :ClientAttribute_id AND '.implode(' AND ', $dims);
				
				$workflow->execute($query, $bind);
		    }
		} catch (\Exception $e) {
			$workflow->rollback();
			$this->Flash->error('No se ha podido procesar el archivo. Error detectado: '.$query);
			$this->redirect(['action' => 'detail', $entity->Country_id, $entity->ClientAttribute_id, 'prefix' => 'client_attribute']);
			return null;
		}
		
		$workflow->commit();
		$this->connection->commit();
		$this->Flash->success('El archivo ha sido procesado exitosamente');
		$entity->Done = true;
		$this->AttributesFiles->save($entity);
		$this->redirect(['action' => 'detail', $entity->Country_id, $entity->ClientAttribute_id, 'prefix' => 'client_attribute']);
	}
	
	public function download ($country_id = null, $id = null, $pendant = false) {
		
		set_time_limit(300);
		
		$this->Cookie->configKey('fileDownloadToken', 'encryption', false);
		$this->Cookie->write('fileDownloadToken', 'pendant');
		
		// Asignar bases de datos
		$country = $this->setDb($country_id);
		
		// Validar si el atributo puede ser modificado por el usuario logeado
		$list = $this->Owners->find('list', [
			'valueField' => 'IdClientAttribute',
			'conditions' => [
				'Owner' => $this->Auth->User('mail')
			]
		])->toArray();
		
		$admins = $this->AttributesAdmin->find('list', [
			'conditions' => [
				'Admin' => $this->Auth->User('mail')
			]
		]);
		
		if(!in_array($id, $list) and $admins->isEmpty()) {
			throw new ForbiddenException(__('No tienes permiso para ingresar a este atributo'));
		}
		
		// Transforma los datos de SQL a un array
		$data = $this->AllToArray($id, $pendant);
				
		$attribute 	= $data['attribute'];
		$results	= $data['results'];
		$forms		= $data['forms'];
		$select		= $data['select'];
		$function	= $data['function'];
		$values		= $data['values'];
		
		$this->set(compact('attribute'));
		$this->set(compact('results'));
		$this->set(compact('forms'));
		$this->set(compact('select'));
		$this->set(compact('function'));
		$this->set(compact('values'));
		$this->set(compact('country'));
	    $this->set('_serialize', ['attribute', 'results', 'forms', 'select', 'values', 'function', 'country']);
	    
	    $this->Cookie->write('fileDownloadToken', 'finish');
	    
	    //$this->response->type(['xlsx' => 'application/vnd.ms-excel']);
		
	}
	
	private function databaseToArray($id = null, $country_id = null) {
		
		$attribute = $this->Attributes->get($id, [
			'contain' => ['Owners', 'Markets.Subs.Products', 'Clients', 'Dimensions']
		]);
		
		$values = $this->AttributesValues->find('list', [
			'keyField' => 'IdValue',
			'valueField' => 'Descricao',
			'conditions' => [
				'FlgDesativacao' => 'AT',
				'IdClientAttribute' => $attribute->IdClientAttribute
			],
			'order' => ['Descricao' => 'ASC']
		])->toArray();
		
		$fields = $this->fields;
		
		$select = [];
		$function = [];
		$forms = [];
		$flag = 0;
		
		// Validar si existe Fabricante
		foreach($attribute->dimensions as $dimension) {
			if($dimension['Descricao'] == 'IdFabricante') {
				$flag = 1;
			}
		}
		
		foreach($attribute->dimensions as $dimension) {
			
			if($dimension['Descricao'] == 'IdMarca' and $flag == 0) {
				$select[] 	= 'IdFabricante';
				$select[] 	= 'Fabricante';
				$function[]	= 'IdFabricante';
			}
				
			$forms[] = [
				'Descricao' => $dimension['Descricao'],
				'Orden' => $dimension['_joinData']['Orden']
			];
			
			$select[] 	= $dimension['Descricao'];
			$select[] 	= $fields[$dimension['Descricao']];
			$function[]	= $dimension['Descricao'];	
		}
		
		// Build Query		
		$results = $this->AttributesPendantsDetails->find('all', [
			'conditions' => [
				'FlgDesativacao' => 'AT',
				'Country_id' => $country_id,
				'ClientAttribute_id' => $id
			]
		]);
		
		return [
			'attribute' => $attribute,
			'results' => $results->toArray(),
			'select' => $select,
			'function' => $function,
			'forms' => $forms,
			'values' => $values,
			'pendants' => $results->count()
		];
		
	}
	
	private function AllToArray($id = null, $pendant = false) {
		
		$attribute = $this->Attributes->get($id, [
			'contain' => ['Owners', 'Markets.Subs.Products', 'Clients', 'Dimensions']
		]);
		
		$values = $this->AttributesValues->find('list', [
			'keyField' => 'IdValue',
			'valueField' => 'Descricao',
			'conditions' => [
				'FlgDesativacao' => 'AT',
				'IdClientAttribute' => $attribute->IdClientAttribute
			],
			'order' => ['Descricao' => 'ASC']
		])->toArray();
		
		$fields = $this->fields;
		
		// Build Query
		$select = [];
		$function = [];
		$forms = [];
		$flag = 0;
		// Validar si existe Fabricante
		foreach($attribute->dimensions as $dimension) {
			if($dimension['Descricao'] == 'IdFabricante') {
				$flag = 1;
			}
		}
		
		foreach($attribute->dimensions as $dimension) {
			
			if($dimension['Descricao'] == 'IdMarca' and $flag == 0) {
				$select[] 	= 'IdFabricante';
				$select[] 	= 'Fabricante';
			}
			
			if($dimension['Descricao'] == 'IdArtigo') {
				$select[] 	= 'IdProduto';
				$select[] 	= 'Produto';
				$select[] 	= 'IdSub';
				$select[] 	= 'Sub';
				$select[] 	= 'IdFabricante';
				$select[] 	= 'Fabricante';
				$select[] 	= 'IdMarca';
				$select[] 	= 'Marca';
				$select[] 	= 'IdConteudo';
				$select[] 	= 'Conteudo';
				$select[] 	= 'CdC01';
				$select[] 	= 'Clas01';
				$select[] 	= 'CdC02';
				$select[] 	= 'Clas02';
				$select[] 	= 'CdC03';
				$select[] 	= 'Clas03';
				$select[] 	= 'CdC04';
				$select[] 	= 'Clas04';
				$select[] 	= 'CdC05';
				$select[] 	= 'Clas05';
				$select[] 	= 'CdC06';
				$select[] 	= 'Clas06';
				$select[] 	= 'CdC07';
				$select[] 	= 'Clas07';
				$select[] 	= 'CdC08';
				$select[] 	= 'Clas08';
				$select[] 	= 'CdC09';
				$select[] 	= 'Clas09';
			}
			
			$forms[]	= $dimension['Descricao'];
			$select[] 	= $dimension['Descricao'];
			$select[] 	= $fields[$dimension['Descricao']];
			$function[]	= $dimension['Descricao'];	
		}
		
		$query = "SELECT ".implode(", ", $select).", dbo.fc_ClientAttribute(".$attribute->IdClientAttribute.", ".implode(", ", array_pad($function, 10, 0)).") as Value";
		
		$query .= " FROM dbo.VW_ArtigoCA (nolock) WHERE DtCriacao < (SELECT CONVERT(VARCHAR(8),MAX(DATEVALUE),112) FROM dbo.SPREnd) AND ";
		
		if($pendant) {
			$query .= " dbo.fc_ClientAttribute(".$attribute->IdClientAttribute.", ".implode(", ", array_pad($function, 10, 0)).") < 0 AND ";
		}
		
		$where = [];
		foreach($attribute->market->subs as $sub) {
			$where[] = "(IdProduto = ".$sub['IdProduto']." AND IdSub = ".$sub['IdSub'].")";	
		}
		$query .= "(" . implode(" OR ", $where) . ")";
		
		// Filtro adicional de Mercado (Market)
		if($attribute->market->Filtro) {
			$query .= " AND ( " . $attribute->market->Filtro . ") ";
		}
		
		$group = [];
		foreach($attribute->dimensions as $dimension) {
			
			if($dimension['Descricao'] == 'IdMarca') {
				$group[] 	= 'IdFabricante';
				$group[] 	= 'Fabricante';
			}
			
			if($dimension['Descricao'] == 'IdArtigo') {
				$group[] 	= 'IdProduto';
				$group[] 	= 'Produto';
				$group[] 	= 'IdSub';
				$group[] 	= 'Sub';
				$group[] 	= 'IdFabricante';
				$group[] 	= 'Fabricante';
				$group[] 	= 'IdMarca';
				$group[] 	= 'Marca';
				$group[] 	= 'IdConteudo';
				$group[] 	= 'Conteudo';
				$group[] 	= 'CdC01';
				$group[] 	= 'Clas01';
				$group[] 	= 'CdC02';
				$group[] 	= 'Clas02';
				$group[] 	= 'CdC03';
				$group[] 	= 'Clas03';
				$group[] 	= 'CdC04';
				$group[] 	= 'Clas04';
				$group[] 	= 'CdC05';
				$group[] 	= 'Clas05';
				$group[] 	= 'CdC06';
				$group[] 	= 'Clas06';
				$group[] 	= 'CdC07';
				$group[] 	= 'Clas07';
				$group[] 	= 'CdC08';
				$group[] 	= 'Clas08';
				$group[] 	= 'CdC09';
				$group[] 	= 'Clas09';
			}
					
			$group[] = $dimension['Descricao'];
			$group[] = $fields[$dimension['Descricao']];	
		}
		$query .= " GROUP by ".implode(", ", $group);
		
		$results = $this->connection->execute($query)->fetchAll('assoc');
		
		$pendants = 0;
		foreach($results as $result) {
			if($result['Value'] < 0) {
				$pendants++;
			}
		}
		
		return [
			'attribute' => $attribute,
			'results' => $results,
			'forms' => $forms,
			'select' => $select,
			'function' => $function,
			'values' => $values,
			'pendants' => $pendants
		];
		
	}
	
	private function compareDatabase($file = null, $id = null) {
		
		$database 	= $this->AllToArray($id);
		
		$rows		= $database['results'];
		$dimensions	= $database['attribute']->dimensions;
		$values		= $database['values'];
		
		return $this->Arrays->array_diff_assoc_recursive($file, $rows, $dimensions, $values);

	}
	
	private function toNumbers($letters = null) {
	    
	    if($letters) {
		    
		    $numbers 	= array_combine(range('a', 'z'), range(0,25));
		    $array		= str_split($letters);
		    
		    $result = [];
		    foreach($array as $letter) {
			    $result[] = $numbers[$letter];
		    }
		    
		    return (((int)implode('', $result)) - 2354) / 189;
		    		    
	    } else {
		    return false;
	    }
	    
    }

}