<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Datasource\ConnectionManager;
use \TheNetworg\OAuth2\Client\Provider\Azure;

class UsersController extends AppController
{
	public function initialize()
	{
		parent::initialize();
		
		$this->connection = ConnectionManager::get('IT_WORKFLOW');
		$this->Users->setConnection($this->connection);
		
		$this->loadModel('Permissions');
		$this->loadModel('Admins');
		$this->loadModel('PortalLogins');
		
		$this->Auth->allow(['logout', 'login', 'oauth']);
	}
	
	public function beforeFilter(Event $event) {
		
		parent::beforeFilter($event);
    }
    
    public function oauth() {
	    
	    $this->autoRender = false;
	    
	    if($_SERVER['HTTP_HOST'] == '172.28.232.191') {
		    /* Microsoft Application Production */
		    $provider = new Azure([
			    'clientId'          => '9a2fd35d-104b-4a3a-8d38-8c36281fc200',
			    'clientSecret'      => 'szrwLAL3253+?wfuGAKL7*;',
			    'redirectUri'       => 'https://172.28.232.191/~production/users/oauth',
			    'scope' 			=> 'user.read'
			]);
		} else if ($_SERVER['HTTP_HOST'] == 'latam.kantarworldpanel.com') {
			/* Microsoft Application Production */
		    $provider = new Azure([
			    'clientId'          => '069d0ce9-56d4-4ac6-843d-71d0671ac996',
			    'clientSecret'      => 'zfzkYDT2{]-fqfLLNC9380]',
			    'redirectUri'       => 'https://latam.kantarworldpanel.com/users/oauth',
			    'scope' 			=> 'user.read'
			]);
	    } else if($_SERVER['HTTP_HOST'] == '172.28.232.200') {
		    /* Microsoft Application Development */
		    $provider = new Azure([
			    'clientId'          => '4fc6105a-c96d-414b-93b8-07f7bf694972',
			    'clientSecret'      => 'vwlJQO0736]*{~uaxeEBWZ7',
			    'redirectUri'       => 'https://172.28.232.200/~production/users/oauth',
			    'scope' 			=> 'user.read'
			]);
	    }
	    
		$logged = false;
		
		if (!$this->request->query('code')) {
			// If we don't have an authorization code then get one
		    $authUrl = $provider->getAuthorizationUrl();
		    $this->request->session()->write('oauth2state', $provider->getState());
		    header('Location: '.$authUrl);
		    exit;
		// Check given state against previously stored one to mitigate CSRF attack
		} elseif (empty($this->request->query('state')) || ($this->request->query('state') !== $this->request->session()->read('oauth2state'))) {
			
			$this->request->session()->delete('oauth2state');
		    exit('Invalid state');
		
		} else {
		
		    // Try to get an access token (using the authorization code grant)
		    $token = $provider->getAccessToken('authorization_code', [
		        'code' => $this->request->query('code'),
		        //'scope' => "https://graph.microsoft.com/.default",
		        //'resource' => 'https://graph.microsoft.com/'
		    ]);
		
		    // Optional: Now you have a token you can look up a users profile data
		    try {
		
		        // We got an access token, let's now get the user's details
		        $me = $provider->get("me", $token);
		        $image = null;
		        try {
			        $image = $provider->get('me/photo/$value', $token);
		        } catch(\Exception $e) {
			        $image = file_get_contents(WWW_ROOT . 'img' . DS . 'profiles' . DS . 'no-image.jpg');
		        }
			    file_put_contents(WWW_ROOT . 'img' . DS . 'profiles' . DS . $me['id'].'.jpg', $image);
		        
		        
		        $logged = true;
		        
		        // Capturar permisos
		        $me['permissions'] = $this->Permissions->find('all', [
			        'conditions' => [
				        'mail' => $me['mail']
			        ]
		        ])->toArray();
		        
		        // Leer administracion
		        $me['admins'] = $this->Admins->find('all', [
			        'conditions' => [
				        'mail' => $me['mail']
			        ]
		        ])->toArray();
		
		        // Use these details to create a new profile
		        $this->Auth->setUser($me);
		        
		        $redirect = $this->request->session()->read('redirect');
		        $this->request->session()->delete('redirect');
		        
		        if(is_null($redirect)) {
			        $redirect = ['controller' => 'stats', 'action' => 'index', 'prefix' => false];
		        }
		        
		        $this->redirect($redirect);
		
		    } catch (Exception $e) {
		
		        // Failed to get user details
		        //exit('Oh dear...');
		    }
		
		    // Use this to interact with an API on the users behalf
		    //echo $token->getToken();
		}
		
		$this->set('logged', true);
		
		$login = ['mail' => $this->Auth->User('mail')];
		    
	    $entity = $this->PortalLogins->newEntity($login);
	    $save = $this->PortalLogins->save($entity);
	    
    }

	public function login()
    {
	    $this->viewBuilder()->layout('auth');
	    
	    if($this->Auth->User()) {
		    $this->set('logged', true);
		    
	    } else {
		    $this->request->session()->write('redirect', $this->request->query('redirect'));
		    $this->set('logged', false);
	    }
		
    }
    
	public function logout()
	{
	    return $this->redirect($this->Auth->logout());
	}

}