<?php
namespace App\Controller\Fusion;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\I18n\Time;
use Cake\Datasource\ConnectionManager;
use Cake\Network\Exception\NotFoundException;
use Cake\Core\Exception\Exception;

class FieldsController extends AppController
{
	
	var $connection = null;
	
	public function initialize()
	{
		parent::initialize();
		// Definir base de datos a utilizar
		$this->connection = ConnectionManager::get('IT_WORKFLOW');

		// Tablas externas a utilizar
		$this->loadModel('Countries');
		$this->loadModel('Flags');
		$this->loadModel('FieldsGroups');
		$this->loadModel('Fields');
		$this->loadModel('Parameters');
	}

	public function index()
    {
	    $this->Countries->setConnection($this->connection);
	    
		// Listar paises
		$countries = $this->Countries->find('all', [
			'contain' => ['Flags']
		]);
        $this->set('countries', $countries);
    }
    
    public function detail($country_id = null) {
	    
	    $this->Countries->setConnection($this->connection);
	    $this->FieldsGroups->setConnection($this->connection);
	    $this->Fields->setConnection($this->connection);
	    $this->Parameters->setConnection($this->connection);
	    
	    // Instanciar pais
	    $country = $this->Countries->get($country_id, [
		    'contain' => ['Flags', 'FieldsGroups', 'FieldsGroups.Fields', 'Parameters' => [
	            'sort' => ['Group_Order', 'Element_Order']
            ]]
	    ]);
	    
	    if($this->request->is('post') or $this->request->is('put')) {
		    
		    $data = $this->request->data();
		    $fields = $this->Fields->newEntities($data);
		    
		    $save = $this->Fields->saveMany($fields);
		    
		    if($save) {
			    $this->Flash->success(__('Los campos han sido actualizados correctamente.'));
		    } else {
			    $this->Flash->error(__('Ha ocurrido un error al intentar actualizar los campos, favor intente mas tarde.'));
		    }
		    
	    }
	    
	    // Listados de seleccion
	    // Campos de Select
        $table = [];
        foreach($country->parameters as $parameter) {
	        $table[$parameter['Code']] = $parameter['Value'];
        }
        
        $temporal_list = [];
        $connector = $this->connector('IndOOH', $table);
        $lists['OOH']['homo'] = $connector->execute(sprintf("SELECT * FROM sys.columns WHERE object_id = OBJECT_ID('dbo.%s')", $table['TbIndOOH']))->fetchAll('assoc');
        foreach($lists['OOH']['homo'] as $item) {
	        $temporal_list[$item['name']] = $item['name'];
        }
        $lists['OOH']['homo'] = $temporal_list;
        
        $temporal_list = [];
        $connector = $this->connector('IndUS', $table);
        $lists['US']['homo'] = $connector->execute(sprintf("SELECT * FROM sys.columns WHERE object_id = OBJECT_ID('dbo.%s')", $table['TbIndUS']))->fetchAll('assoc');
        foreach($lists['US']['homo'] as $item) {
	        $temporal_list[$item['name']] = $item['name'];
        }
        $lists['US']['homo'] = $temporal_list;
        $lists['US']['individual'] = $temporal_list;
        $lists['OOH']['individual'] = [];
        
        $temporal_list = [];
        $connector = $this->connector('BuyOOH', $table);
        $lists['OOH']['buy'] = $connector->execute(sprintf("SELECT * FROM sys.columns WHERE object_id = OBJECT_ID('dbo.%s')", $table['TbBuyOOH']))->fetchAll('assoc');
        foreach($lists['OOH']['buy'] as $item) {
	        $temporal_list[$item['name']] = $item['name'];
        }
        $lists['OOH']['buy'] = $temporal_list;
        
        $temporal_list = [];
        $connector = $this->connector('BuyUS', $table);
        $lists['US']['buy'] = $connector->execute(sprintf("SELECT * FROM sys.columns WHERE object_id = OBJECT_ID('dbo.%s')", $table['TbBuyUS']))->fetchAll('assoc');
        foreach($lists['US']['buy'] as $item) {
	        $temporal_list[$item['name']] = $item['name'];
        }
        $lists['US']['buy'] = $temporal_list;
        
        $temporal_list = [];
        $connector = $this->connector('ProOOH', $table);
        $lists['OOH']['products'] = $connector->execute(sprintf("SELECT * FROM sys.columns WHERE object_id = OBJECT_ID('dbo.%s')", $table['TbProOOH']))->fetchAll('assoc');
        foreach($lists['OOH']['products'] as $item) {
	        $temporal_list[$item['name']] = $item['name'];
        }
        $lists['OOH']['products'] = $temporal_list;
        
        $temporal_list = [];
        $connector = $this->connector('ProUS', $table);
        $lists['US']['products'] = $connector->execute(sprintf("SELECT * FROM sys.columns WHERE object_id = OBJECT_ID('dbo.%s')", $table['TbProUS']))->fetchAll('assoc');
        foreach($lists['US']['products'] as $item) {
	        $temporal_list[$item['name']] = $item['name'];
        }
        $lists['US']['products'] = $temporal_list;
        
        $temporal_list = [];
        $connector = $this->connector('UsrUS', $table);
        $lists['US']['user'] = $connector->execute(sprintf("SELECT * FROM sys.columns WHERE object_id = OBJECT_ID('dbo.%s')", $table['TbUsrUS']))->fetchAll('assoc');
        foreach($lists['US']['user'] as $item) {
	        $temporal_list[$item['name']] = $item['name'];
        }
        $lists['US']['user'] = $temporal_list;
        $lists['OOH']['user'] = [];
        
        $temporal_list = [];
        $connector = $this->connector('DomUS', $table);
        $lists['US']['household'] = $connector->execute(sprintf("SELECT * FROM sys.columns WHERE object_id = OBJECT_ID('dbo.%s')", $table['TbDomUS']))->fetchAll('assoc');
        foreach($lists['US']['household'] as $item) {
	        $temporal_list[$item['name']] = $item['name'];
        }
        $lists['US']['household'] = $temporal_list;
        $lists['OOH']['household'] = [];
	    
	    
	    $this->set(compact('country'));
	    $this->set(compact('lists'));
	    $this->set(compact('fields'));
        $this->set('_serialize', ['country', 'lists', 'fields']);
	    
    }
    
    //!Procedimientos privados
    
    private function connector($string = null, $table = null) {
		
		$config = ConnectionManager::config($string, [
        	'className' => 'Cake\Database\Connection',
            'driver' => 'Cake\Database\Driver\Sqlserver',
            'persistent' => false,
            'host' => $table['Ip'.$string],
            'username' => $table['Us'.$string],
            'password' => $table['Pw'.$string],
            'database' => $table['Db'.$string],
            'timezone' => 'UTC',
            'cacheMetadata' => true,
            'log' => false,
            'flags' => [],
            'quoteIdentifiers' => false,
            'url' => env('DATABASE_URL', null),
        ]);
        
        $connector = ConnectionManager::get($string);
        
        return $connector;
	
	}
    
}