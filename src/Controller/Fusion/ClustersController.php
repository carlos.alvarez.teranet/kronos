<?php
namespace App\Controller\Fusion;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\I18n\Time;
use Cake\Datasource\ConnectionManager;
use Cake\Network\Exception\NotFoundException;
use Cake\Core\Exception\Exception;

class ClustersController extends AppController
{
	
	var $connection = null;
	
	public function initialize()
	{
		parent::initialize();
		// Definir base de datos a utilizar
		$this->connection = ConnectionManager::get('IT_WORKFLOW');
		$this->Clusters->setConnection($this->connection);
		
		// Tablas a utilizar
		$this->loadModel('Parameters');
		$this->loadModel('Associations');
		$this->loadModel('Oohs');
		$this->loadModel('Uss');
		$this->loadModel('Countries');
	}
	
	public function beforeFilter(Event $event) {
		
		parent::beforeFilter($event);
    }

	public function index()
    {
		$this->Countries->setConnection($this->connection);
		
		// Listar clusteres
		$clusters = $this->Clusters->find('all', [
			'contain' => ['Countries']
		]);
        $this->set('clusters', $clusters);	
	    
	}
	
	public function add()
	{
		$this->Countries->setConnection($this->connection);
		
		// Obtener entidad
        $cluster = $this->Clusters->newEntity();
        if ($this->request->is('post')) {
            $cluster = $this->Clusters->patchEntity($cluster, $this->request->getData());
            if ($this->Clusters->save($cluster)) {
                $this->Flash->success(__('El cluster ha sido creado.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('El cluster no pudo ser creado. Favor intente más tarde.'));
        }
        
        // Listado de Paises
        $countries = $this->Countries->find('list', [
	       'keyField' => 'Id',
	       'valueField' => 'Descricao',
	       'conditions' => [
		       'FlgDesativacao' => 'AT'
	       ]
        ]);
        
        $this->set('cluster', $cluster);
        $this->set('countries', $countries);
		
	}
	
	public function edit($id = null) 
	{
		$this->Countries->setConnection($this->connection);
		
		// Obtener entidad
        $cluster = $this->Clusters->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $cluster = $this->Clusters->patchEntity($cluster, $this->request->getData());
            if ($this->Clusters->save($cluster)) {
                $this->Flash->success(__('El cluster ha sido actualizado.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('El cluster no pudo ser actualizado. Favor intente más tarde.'));
        }
        
        // Listado de Paises
        $countries = $this->Countries->find('list', [
	       'keyField' => 'Id',
	       'valueField' => 'Descricao',
	       'conditions' => [
		       'FlgDesativacao' => 'AT'
	       ]
        ]);
        
        $this->set('cluster', $cluster);
        $this->set('countries', $countries);
		
	}
	
	public function detail($id = null) {
		
		$this->Oohs->setConnection($this->connection);
		$this->Uss->setConnection($this->connection);
		$this->Countries->setConnection($this->connection);
		$this->Parameters->setConnection($this->connection);
		
		// Info de cluster seleccionado
		$cluster = $this->Clusters->get($id, [
            'contain' => ['Oohs', 'Uss', 'Countries', 'Countries.Parameters']
        ]);

        // Post Method
		if($this->request->is('post') or $this->request->is('put')) {
			
			$data 	= $this->request->data();
			$extra = [];
			
			foreach($data['values'] as $key => $value) {
				$extra[$key]['IdCluster']	= $data['IdCluster'];
				$extra[$key]['IdType']		= $data['submit'];
				$extra[$key]['NGroup'] 		= $data['group'];
				$extra[$key]['Field'] 		= $data['field'];
				$extra[$key]['Valor'] 		= $value;
			}
			
			$append[strtolower($data['submit']).'s'] = $extra;
			$entity = $this->Clusters->patchEntity($cluster, $append, ['associated' => ['Oohs', 'Uss']]);
			
			$save = $this->Clusters->save($entity);
			if($save) {
				$this->Flash->success(__('El cluster ha sido actualizado.'));
				return $this->redirect(['action' => 'detail', $data['IdCluster']]);
			} else {
				$this->Flash->danger(__('El cluster no pudo ser actualizado. Favor intente más tarde.'));
			}			
			
		}
        
        // Listado de parametros
        $table = [];
        foreach($cluster->country->parameters as $parameter) {
	        $table[$parameter['Code']] = $parameter['Value'];
        }
        
        // Listados de opciones de seleccion
        $temporal_list = [];
        $connector = $this->connector('PanUS', $table);
        $lists['ColsUS'] = $connector->execute(sprintf("SELECT * FROM sys.columns WHERE object_id = OBJECT_ID('dbo.%s')", $table['TbPanUS']))->fetchAll('assoc');
        foreach($lists['ColsUS'] as $item) {
	        $temporal_list[$item['name']] = $item['name'];
        }
        $lists['ColsUS'] = $temporal_list;
        
        $temporal_list = [];
        $connector = $this->connector('PanOOH', $table);
        $lists['ColsOOH'] = $connector->execute(sprintf("SELECT * FROM sys.columns WHERE object_id = OBJECT_ID('dbo.%s')", $table['TbPanOOH']))->fetchAll('assoc');
        foreach($lists['ColsOOH'] as $item) {
	        $temporal_list[$item['name']] = $item['name'];
        }
        $lists['ColsOOH'] = $temporal_list;
        
        // Retorno de variables
        $this->set('cluster', $cluster);
        $this->set('table', $table);
        $this->set('lists', $lists);
		
	}
	
	private function connector($string = null, $table = null) {
		
		$config = ConnectionManager::config($string, [
        	'className' => 'Cake\Database\Connection',
            'driver' => 'Cake\Database\Driver\Sqlserver',
            'persistent' => false,
            'host' => $table['Ip'.$string],
            'username' => $table['Us'.$string],
            'password' => $table['Pw'.$string],
            'database' => $table['Db'.$string],
            'timezone' => 'UTC',
            'cacheMetadata' => true,
            'log' => false,
            'flags' => [],
            'quoteIdentifiers' => false,
            'url' => env('DATABASE_URL', null),
        ]);
        
        $connector = ConnectionManager::get($string);
        
        return $connector;
		
	}
	
	public function fields() {
		
		$this->Parameters->setConnection($this->connection);
		
		if($this->request->is('post') or $this->request->is('put')) {
			
			// Obtener datos de POST
			$data = $this->request->data();
			
			// Parametros generales
	        $parameters = $this->Parameters->find('all', [
		        'contain' => [],
		        'order' => [
			        'Code' => 'ASC'
		        ],
		        'conditions' => [
			        'Country_id' => $data['country_id']
		        ]
	        ]);
	        $table = [];
	        foreach($parameters as $parameter) {
		        $table[$parameter['Code']] = $parameter['Value'];
	        }
			
			// Leer tabla de valores
			$connector = $this->connector('Pan'.$data['group'], $table);
			try {
				$response = $connector->execute(sprintf("SELECT campo as campo, CASE WHEN campo IN (%s) THEN 1 ELSE 0 END as selected FROM (SELECT DISTINCT %s as campo FROM dbo.%s) a ORDER by campo ASC", $table['ClDat'.$data['group']], $data['field'], $table['TbPan'.$data['group']]));	
			} catch(\PDOException $e) {
				$response = $connector->execute(sprintf("SELECT campo as campo, 0 as selected FROM (SELECT DISTINCT %s as campo FROM dbo.%s) a ORDER by campo ASC", $data['field'], $table['TbPan'.$data['group']]));
			}
			$response = $response->fetchAll();
						
		} else {
			
			$response = ['' => 'Error en buscar valores'];
			
		}
		
		$this->set('response', $response);
		$this->set('field', $data['field']);
		$this->set('_serialize', ['response', 'field']);
		
	}
	
	public function removeAssoc($id = null) {
		
		$this->Associations->setConnection($this->connection);
		
		$association = $this->Associations->get($id);
		
		if($association) {
			
			$result = $this->Associations->delete($association);
			
			if($result) {
				$this->Flash->success(__('Parámetro eliminado con éxito!'));
				return $this->redirect($this->referer());
			} else {
				$this->Flash->danger(__('Problemas al eliminar el parámetro'));
				return $this->redirect($this->referer());
			}
			
		} else {
			$this->Flash->danger(__('Problemas al eliminar el parámetro'));
			return $this->redirect($this->referer());
		}
		
	}
	
}