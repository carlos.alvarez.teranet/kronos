<?php
namespace App\Controller\Fusion;

require_once(ROOT .DS. "vendor" . DS  . "jdorn" . DS . "sql-formatter" . DS . "lib" . DS . "SqlFormatter.php");

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\I18n\Time;
use Cake\Datasource\ConnectionManager;
use Cake\Network\Exception\NotFoundException;
use Cake\Core\Exception\Exception;
use Cake\Console\ShellDispatcher;


class GenerationsController extends AppController
{
	
	var $connection = null;
	
	public function initialize()
	{
		parent::initialize();
		// Definir base de datos a utilizar
		$this->connection = ConnectionManager::get('IT_WORKFLOW');
		
		// Tablas a utilizar
		$this->loadModel('Countries');
		$this->loadModel('Flags');
		$this->loadModel('Executions');
		$this->loadModel('FieldsGroups');
		$this->loadModel('Fields');
		$this->loadModel('Clusters');
		$this->loadModel('Oohs');
		$this->loadModel('Uss');
		$this->loadModel('Parameters');
		$this->loadModel('Joins');
		$this->loadModel('FusionLogs');
		$this->loadModel('FusionCategories');
		
		$this->loadComponent('Queries');
		$this->loadComponent('RequestHandler');
	}
	
	public function beforeFilter(Event $event) {
		
		parent::beforeFilter($event);
    }

	public function index()
    {
		$this->Countries->setConnection($this->connection);
		
		// Listar paises
		$countries = $this->Countries->find('all', [
			'contain' => ['Flags']
		]);
        $this->set('countries', $countries);	
	    
	}
	
	public function da2($id = null) {
		
		$this->Countries->setConnection($this->connection);
		$this->Executions->setConnection($this->connection);
		$this->FusionLogs->setConnection($this->connection);
		
		$country = $this->Countries->get($id, [
			'contain' => ['Flags', 'Executions']
		]);
		
		// Listado de categorias
		$categories = $this->FusionCategories->find('all', [
			'conditions' => [
				'Country_id' => $id
			]
		]);
		
		// Logs de ejecucion
		$logs = $this->FusionLogs->find('all', [
			'fields' => [
				'Executions.Descricao',
				'Executions.Ano',
				'Executions.Mes'
			],
			'conditions' => [
				'FusionLogs.Country_id' => $id,
				'FusionLogs.FlgDesativacao' => 'AT',
				'FusionLogs.Group_id' => 'consume'
			],
			'group' => ['Executions.Descricao', 'Executions.Ano', 'Executions.Mes'],
			'contain' => ['Executions'],
			'order' => ['Executions.Ano', 'Executions.Mes']
		]);
		
		// Retorno de variables
		$this->set(compact('country', 'logs', 'categories'));
        $this->set('_serialize', ['country', 'logs', 'categories']);
        
	} 
	
	public function country($id = null) {
		
		$this->Countries->setConnection($this->connection);
		$this->Executions->setConnection($this->connection);
		
		$country = $this->Countries->get($id, [
			'contain' => ['Flags', 'Executions']
		]);
		
		$execution = $this->Executions->newEntity();
		if ($this->request->is('post')) {
            $execution = $this->Executions->patchEntity($execution, $this->request->getData());
            if ($this->Executions->save($execution)) {
                $this->Flash->success(__('El código de ejecución ha sido creado.'));

                return $this->redirect(['action' => 'country', $id]);
            }
            $this->Flash->danger(__('El código de ejecución no pudo ser creado. Favor intente más tarde.'));
        }
		
		// Retorno de variables
		$this->set(compact('country'));
		$this->set(compact('execution'));
        $this->set('_serialize', ['country', 'execution']);
		
	}
	
	public function detail($id = null) {
		
		$this->Countries->setConnection($this->connection);
		$this->Executions->setConnection($this->connection);
		$this->FieldsGroups->setConnection($this->connection);
		$this->Fields->setConnection($this->connection);
		$this->Clusters->setConnection($this->connection);
		$this->Oohs->setConnection($this->connection);
		$this->Uss->setConnection($this->connection);
		$this->Parameters->setConnection($this->connection);
		$this->Joins->setConnection($this->connection);
		$this->FusionLogs->setConnection($this->connection);
		
		$execution = $this->Executions->get($id, [
			'contain' => [
				'Countries', 
				'Countries.Joins', 
				'Countries.Parameters', 
				'Countries.FieldsGroups', 
				'Countries.FieldsGroups.Fields', 
				'Countries.Clusters.OOhs', 
				'Countries.Clusters.Uss', 
				'Countries.FusionLogs' => [
					'conditions' => [
						'FusionLogs.FlgDesativacao' => 'AT',
						'FusionLogs.Execution_id' => $id
					]
				]
			]
		]);
		
		$execution->country->homologation 	= $this->extraction($execution->country->fields_groups, ['Code' => 'homo']);
		$execution->country->buy		 	= $this->extraction($execution->country->fields_groups, ['Code' => 'buy']);
		$execution->country->articles	 	= $this->extraction($execution->country->fields_groups, ['Code' => 'products']);
		$execution->country->user	 		= $this->extraction($execution->country->fields_groups, ['Code' => 'user']);
		$execution->country->household 		= $this->extraction($execution->country->fields_groups, ['Code' => 'household']);
		$execution->country->individual		= $this->extraction($execution->country->fields_groups, ['Code' => 'individual']);
		$execution->country->parameters		= $this->rebuildKeys($execution->country->parameters, 'Code');
		$execution->country->logs			= $this->orderLogs($execution->country->fusion_logs);
		
		$queries['homologation']['ooh'] 	= \SqlFormatter::format($this->Queries->homologation($execution, 'OOH'));
		$queries['homologation']['us'] 		= \SqlFormatter::format($this->Queries->homologation($execution, 'US'));
		$queries['imputar']['ooh'] 			= \SqlFormatter::format($this->Queries->imputar($execution, 'OOH'));
		$queries['consume']['us'][0] 		= \SqlFormatter::format($this->Queries->consume($execution, 'US')[0]);
		$queries['consume']['us'][1] 		= \SqlFormatter::format($this->Queries->consume($execution, 'US')[1]);
		$queries['consume']['us'][2] 		= \SqlFormatter::format($this->Queries->consume($execution, 'US')[2]);
		$queries['consume']['us'][3] 		= \SqlFormatter::format($this->Queries->consume($execution, 'US')[3]);
		$queries['consume']['us'][4] 		= \SqlFormatter::format($this->Queries->consume($execution, 'US')[4]);
		$queries['consume']['us'][5] 		= \SqlFormatter::format($this->Queries->consume($execution, 'US')[5]);
		
		// Retorno de variables
		$this->set(compact('execution'));
		$this->set(compact('queries'));
        $this->set('_serialize', ['execution', 'queries']);
        
	}
	
	public function run() {
		
		$this->Countries->setConnection($this->connection);
		$this->Executions->setConnection($this->connection);
		$this->FieldsGroups->setConnection($this->connection);
		$this->Fields->setConnection($this->connection);
		$this->Clusters->setConnection($this->connection);
		$this->Oohs->setConnection($this->connection);
		$this->Uss->setConnection($this->connection);
		$this->Parameters->setConnection($this->connection);
		$this->Joins->setConnection($this->connection);
		$this->FusionLogs->setConnection($this->connection);
		
		if($this->request->is('post') or $this->request->is('put')) {
			
			$data = $this->request->data();
			
			$execution = $this->Executions->get($data['execution_id'], [
				'contain' => ['Countries', 'Countries.Joins', 'Countries.Parameters', 'Countries.FieldsGroups', 'Countries.FieldsGroups.Fields', 'Countries.Clusters.OOhs', 'Countries.Clusters.Uss']
			]);
			
			if($execution) {
				
				// Seccion de homologacion
				if($data['section'] == 'homologacion') {
				
					$execution->country->homologation 	= $this->extraction($execution->country->fields_groups, ['Code' => 'homo']);
					$parameters 						= $this->listArray($execution->country->parameters);
					$execution->country->parameters		= $this->rebuildKeys($execution->country->parameters, 'Code');
					
					
					$connector = $this->connector('Ind'.strtoupper($data['type']), $parameters);
					$query = $this->Queries->homologation($execution, strtoupper($data['type']));
					
					// Inicio de ejecucion (LOG)
					$this->inactivateLog($data);
					$this->insertLog($data, ['Descricao' => 'Inicio de ejecución']);
					
					$connector->begin();
					$result = $connector->execute($query);
					$connector->commit();
					
					if($result->errorCode() == '00000') {
						
						$this->insertLog($data, ['Descricao' => 'Término exitoso de ejecución']);
						
						$response = [
							'error' => false,
							'message' => 'Executado correctamente.'
						];
					} else {
						
						$this->insertLog($data, ['Descricao' => 'Término erróneo de ejecución']);
						$this->insertLog($data, ['Descricao' => 'Error: '.$result->errorInfo()]);
						
						$response = [
							'error' => true,
							'message' => 'Ha ocurrido un problema al ejecutar el script, favor intente mas tarde.'
						];
					}
				
				}
				
				// Seccion de fusionar
				if($data['section'] == 'fusionar') {
					
					$array = $this->rebuildKeys($execution->country->parameters, 'Code');
					
					$shell = new ShellDispatcher();
					$shell->run(['cake', 'fusion', $array['DbIndOOH']['Value'], $data['execution_id'], $data['country_id'], $data['section'], $data['type']]);
					
					$response = [
						'error' => false,
						'message' => 'Executado correctamente.'
					];
					
				}
				
				// Seccion Imputar
				if($data['section'] == 'imputar') {
					
					$execution->country->buy		 	= $this->extraction($execution->country->fields_groups, ['Code' => 'buy']);
					$execution->country->articles	 	= $this->extraction($execution->country->fields_groups, ['Code' => 'products']);
					$parameters 						= $this->listArray($execution->country->parameters);
					$execution->country->parameters		= $this->rebuildKeys($execution->country->parameters, 'Code');
					
					
					$connector = $this->connector('Ind'.strtoupper($data['type']), $parameters);
					$query = $this->Queries->imputar($execution, strtoupper($data['type']));
					
					// Inicio de ejecucion (LOG)
					$this->inactivateLog($data);
					$this->insertLog($data, ['Descricao' => 'Inicio de ejecución']);
					
					$connector->begin();
					$result = $connector->execute($query);
					$connector->commit();
					
					if($result->errorCode() == '00000') {
						
						$this->insertLog($data, ['Descricao' => 'Término exitoso de ejecución']);
						
						$response = [
							'error' => false,
							'message' => 'Executado correctamente.'
						];
					} else {
						
						$this->insertLog($data, ['Descricao' => 'Término erroneo de ejecución']);
						$this->insertLog($data, ['Descricao' => 'Error: '.$result->errorInfo()]);
						
						$response = [
							'error' => true,
							'message' => 'Ha ocurrido un problema al ejecutar el script, favor intente mas tarde.'
						];
					}
					
				}
				
				// Seccion Consumo
				
				if($data['section'] == 'consume') {
					
					$execution->country->buy		 	= $this->extraction($execution->country->fields_groups, ['Code' => 'buy']);
					$execution->country->user	 		= $this->extraction($execution->country->fields_groups, ['Code' => 'user']);
					$execution->country->household 		= $this->extraction($execution->country->fields_groups, ['Code' => 'household']);
					$execution->country->individual		= $this->extraction($execution->country->fields_groups, ['Code' => 'individual']);
					$parameters 						= $this->listArray($execution->country->parameters);
					$execution->country->parameters		= $this->rebuildKeys($execution->country->parameters, 'Code');
					
					$connector = $this->connector('Ind'.strtoupper($data['type']), $parameters);
					$query = $this->Queries->consume($execution, strtoupper($data['type']));
					
					// Inicio de ejecucion (LOG)
					$this->inactivateLog($data);
					$this->insertLog($data, ['Descricao' => 'Inicio de ejecución']);
					
					$result = $connector->transactional(function ($connector) use ($query) {
					    foreach($query as $q) {
							$connector->execute($q);
						}
					});
					
					if($result !== false) {
						
						$this->insertLog($data, ['Descricao' => 'Término exitoso de ejecución']);
						
						$response = [
							'error' => false,
							'message' => 'Executado correctamente.'
						];
					} else {
						
						$this->insertLog($data, ['Descricao' => 'Término erroneo de ejecución']);
						$this->insertLog($data, ['Descricao' => 'Error: '.json_encode($result)]);
						
						$response = [
							'error' => true,
							'message' => 'Ha ocurrido un problema al ejecutar el script, favor intente mas tarde.'
						];
					}
					
				}
				
				
			} else {
				$response = [
					'error' => true,
					'message' => 'Ha ocurrido un problema obteniendo datos de ejecución, favor intente mas tarde.'
				];
			}
			
		} else {
			$response = [
				'error' => true,
				'message' => 'No existe método post.'
			];
		}
		
		$this->set(compact('response'));
		$this->set('_serialize', ['response']);
		
	}
	
	//! Seccion de funciones privadas
	
	private function extraction($elements = null, $array = []) {
		
		$return = [];
		foreach($elements as $element) {
			if($element[key($array)] == $array[key($array)]) {
				$return = $element;
			}
		}
		
		return $return;
		
	}
	
	private function rebuildKeys($elements = null, $field = null) {
		
		$return = [];
		foreach($elements as $element) {
			$return[$element[$field]] = $element;
		}
		
		return $return;
		
	}
	
	private function listArray($elements = null) {
		
		$return = [];
		foreach($elements as $element) {
			$return[$element['Code']] = $element['Value'];
		}
		
		return $return;
		
	}
	
	private function connector($string = null, $table = null) {
		
		$config = ConnectionManager::config($string, [
        	'className' => 'Cake\Database\Connection',
            'driver' => 'Cake\Database\Driver\Sqlserver',
            'persistent' => false,
            'host' => $table['Ip'.$string],
            'username' => $table['Us'.$string],
            'password' => $table['Pw'.$string],
            'database' => $table['Db'.$string],
            'timezone' => 'UTC',
            'cacheMetadata' => true,
            'log' => true,
            'flags' => [],
            'quoteIdentifiers' => false,
            'url' => env('DATABASE_URL', null),
        ]);
        
        $connector = ConnectionManager::get($string);
        
        return $connector;
		
	}
	
	private function insertLog($data = null, $fields = null) {
		
		$this->FusionLogs->setConnection($this->connection);
		
		$log = [
			'Country_id' => $data['country_id'],
			'Group_id' => $data['section'],
			'Type_id' => $data['type'],
			'Execution_id' => $data['execution_id'],
			'Descricao' => null
		];
		
		foreach($fields as $key => $field) {
			if(array_key_exists($key, $log)) {
				$log[$key] = $field;
			}
		}
		
		$init = $this->FusionLogs->newEntity($log);
		$save = $this->FusionLogs->save($init);
		
		return $save;
		
	}
	
	private function orderLogs($logs = null) {
		
		$array = [
			'homologacion' => [
				'us' => [],
				'ooh' => []
			],
			'fusionar' => [
				'both' => []
			],
			'imputar' => [
				'ooh' => []
			]
		];
		
		foreach($logs as $log) {
			$array[$log['Group_id']][$log['Type_id']][] = $log;
		}
		
		return $array;
		
	}
	
	private function inactivateLog($data) {
		
		$this->FusionLogs->setConnection($this->connection);
		
		$update = $this->FusionLogs->updateAll(
	        ['FlgDesativacao' => 'AR'], // fields
	        ['Execution_id' => $data['execution_id'], 'Group_id' => $data['section'], 'Type_id' => $data['type']] // conditions
	    );
	    
	    return $update;
		
	}
	
}