<?php
namespace App\Controller\Fusion;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\I18n\Time;
use Cake\Datasource\ConnectionManager;
use Cake\Network\Exception\NotFoundException;
use Cake\Core\Exception\Exception;

class ParametersController extends AppController
{
	
	var $connection = null;
	
	public function initialize()
	{
		parent::initialize();
		// Definir base de datos a utilizar
		$this->connection = ConnectionManager::get('IT_WORKFLOW');
		$this->Parameters->setConnection($this->connection);
	}
	
	public function beforeFilter(Event $event) {
		
		parent::beforeFilter($event);
    }

	public function index()
    {
		// Chequeo de base de datos
		$exists = $this->checkExists();
		if(!$exists) {
			$this->createTable();
			$this->populateTable();
		}
		
		if($this->request->is('post') or $this->request->is('put')) {
			
			$data = $this->request->data();
			
			foreach($data as $code => $value) {
				if($value) {
					$update = $this->connection->execute("UPDATE dbo.Production_Parameters SET Value = :value, DtAtualizacao = GETDATE() WHERE Code = :code", ['value' => $value, 'code' => $code]);
				}
			}
			
			$this->Flash->success(__('Los parámetros han sido actualizados.'));
			
		}
		
		$parameters = $this->Parameters->find('all', [
			'conditions' => [
				'Project' => 'Fusion'
			]
		]);
		
		$this->set('parameters', $parameters);
    }
    
    private function checkExists() {
	    
	    $exists = $this->connection->execute("SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'Production_Parameters'")->fetchAll('assoc');
	    if($exists) {
		    return true;
	    } else {
		    return false;
	    }
	    
    }
    
    private function createTable() {
	    
	    try {	    
		    $table = $this->connection->execute("CREATE TABLE Production_Parameters (
			    									Id INT NOT NULL IDENTITY (1,1),
			    									Code VARCHAR(50) NOT NULL,
			    									Country_id INT NULL,
			    									Description VARCHAR(MAX) NOT NULL,
			    									Value VARCHAR(50) NULL,
			    									Project VARCHAR(50) NOT NULL,
			    									DtCriacao datetime NOT NULL,
			    									DtAtualizacao datetime NULL
		    									);
		    									
		    									ALTER TABLE dbo.Production_Parameters ADD CONSTRAINT PK_Production_Parameters PRIMARY KEY (Id);
		    									");
		} catch (Exception $e) {
			return false;
		}
		
		return true;
	    
    }
    
    private function populateTable() {
	    
	    $table = $this->connection->execute("INSERT INTO dbo.Production_Parameters (Code, Description, DtCriacao, Project) 
	    									VALUES 
	    									('TbIndOOH', 'Tabla de individuos OOH', GETDATE(), 'Fusion'),
	    									('TbIndUS', 'Tabla de individuos PNC U&S', GETDATE(), 'Fusion'),
	    									('TbPanOOH', 'Tabla de panel OOH', GETDATE(), 'Fusion'),
	    									('TbPanUS', 'Tabla de panel PNC U&S', GETDATE(), 'Fusion')");

	    
    }

}