<?php
namespace App\Controller\Fusion;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\I18n\Time;
use Cake\Datasource\ConnectionManager;
use Cake\Network\Exception\NotFoundException;
use Cake\Core\Exception\Exception;

class CountriesController extends AppController
{
	
	var $connection = null;
	
	public function initialize()
	{
		parent::initialize();
		// Definir base de datos a utilizar
		$this->connection = ConnectionManager::get('IT_WORKFLOW');
		$this->Countries->setConnection($this->connection);

		// Tablas externas a utilizar
		$this->loadModel('Flags');
		$this->loadModel('Parameters');
		$this->loadModel('Joins');
	}
	
	public function beforeFilter(Event $event) {
		
		parent::beforeFilter($event);
    }

	public function index()
    {
		// Listar paises
		$countries = $this->Countries->find('all', [
			'contain' => ['Flags']
		]);
        $this->set('countries', $countries);
    }
    
    public function add()
    {
        $country = $this->Countries->newEntity();
        if ($this->request->is('post')) {
            $country = $this->Countries->patchEntity($country, $this->request->getData());
            if ($this->Countries->save($country)) {
                $this->Flash->success(__('El país ha sido creado.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('El país no pudo ser creado. Favor intente más tarde.'));
        }
        
        // Listado de FlgDesativacao
        $flags = $this->Flags->find('list', [
	        'keyField' => 'code',
	        'valueField' => 'description',
	        'conditions' => [
		        'active' => true
	        ]
        ]);
                
        $this->set(compact('country'));
        $this->set(compact('flags'));
        $this->set('_serialize', ['country', 'flags']);
    }
    
    public function edit($id = null)
    {
        $country = $this->Countries->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $country = $this->Countries->patchEntity($country, $this->request->getData());
            if ($this->Countries->save($country)) {
                $this->Flash->success(__('El país ha sido actualizado.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('El país no pudo ser actualizado. Favor intente más tarde.'));
        }
        
        // Listado de FlgDesativacao
        $flags = $this->Flags->find('list', [
	        'keyField' => 'code',
	        'valueField' => 'description',
	        'conditions' => [
		        'active' => true
	        ]
        ]);
        
        $this->set(compact('country'));
        $this->set(compact('flags'));
        $this->set('_serialize', ['country', 'flags']);
    }
    
    public function parameters($id = null) {
	    
		$this->Parameters->setConnection($this->connection);
		
		// En caso de actualizar los campos
		if($this->request->is('post') or $this->request->is('put')) {
			
			$data = $this->request->data();
			
			foreach($data as $code => $value) {
				
				if(!empty($value)) {
					if(is_array($value)) {
						$update = $this->connection->execute("UPDATE dbo.Fusion_Parameters SET Value = :value, DtAtualizacao = GETDATE() WHERE Code = :code AND Country_id = :Country_id", ['value' => implode(",", $value), 'code' => $code, 'Country_id' => $id]);
					} else {
						$update = $this->connection->execute("UPDATE dbo.Fusion_Parameters SET Value = :value, DtAtualizacao = GETDATE() WHERE Code = :code AND Country_id = :Country_id", ['value' => $value, 'code' => $code, 'Country_id' => $id]);
					}
				}
			}
			
			$this->Flash->success(__('Los parámetros han sido actualizados.'));
			
		}
	    
	    $country = $this->Countries->get($id, [
            'contain' => ['Parameters' => [
	            'sort' => ['Group_Order', 'Element_Order']
            ]]
        ]);
        
        // Listado de parametros
        $table = [];
        $lists = ['ColsUS' => [], 'ColsOOH' => []];
        foreach($country['parameters'] as $parameter) {
	        $table[$parameter['Code']] = $parameter['Value'];
        }
        // Listados de opciones de seleccion
        if(!empty($table['IpPanUS']) and !empty($table['UsPanUS']) and !empty($table['PwPanUS']) and !empty($table['DbPanUS'])) {
	        $temporal_list = [];
			$connector = $this->connector('PanUS', $table);
	        $lists['ColsUS'] = $connector->execute(sprintf("SELECT * FROM sys.columns WHERE object_id = OBJECT_ID('dbo.%s')", $table['TbPanUS']))->fetchAll('assoc');
	        foreach($lists['ColsUS'] as $item) {
		        $temporal_list[$item['name']] = $item['name'];
	        }
	        $lists['ColsUS'] = $temporal_list;
        }
        
        if(!empty($table['IpPanOOH']) and !empty($table['UsPanOOH']) and !empty($table['PwPanOOH']) and !empty($table['DbPanOOH'])) {
	        $temporal_list = [];
	        $connector = $this->connector('PanOOH', $table);
	        $lists['ColsOOH'] = $connector->execute(sprintf("SELECT * FROM sys.columns WHERE object_id = OBJECT_ID('dbo.%s')", $table['TbPanOOH']))->fetchAll('assoc');
	        foreach($lists['ColsOOH'] as $item) {
		        $temporal_list[$item['name']] = $item['name'];
	        }
	        $lists['ColsOOH'] = $temporal_list;
	    }
        /////////////////////////////////////////////////////
        
        $parameters = [];
        foreach($country['parameters'] as $parameter) {
	        $parameters[$parameter['Group_Description']][] = $parameter;
        }
        $country->parameters = $parameters;
        
        // Chequeo de base de datos
		if(!$this->checkExists()) {
			$this->createTable();
		}
		if(!$this->checkData($id)) {
			$this->populateTable($id);
			$this->redirect(['action' => 'parameters', $id]);
		}
		
		$this->set(compact('country'));
		$this->set(compact('lists'));
		$this->set(compact('table'));
        $this->set('_serialize', ['country', 'lists', 'table']);
	    
    }
    
    public function remove($country_id = null, $id = null) {
	    
		$this->Joins->setConnection($this->connection);
		
		$country = $this->Countries->get($country_id);
		$join = $this->Joins->get($id);
		
		$delete = $this->Joins->delete($join);
		
		if($delete) {
			$this->Flash->success(__('El atributo ha sido eliminado correctamente.'));
		} else {
			$this->Flash->error(__('Problemas al intentar eliminar el atributo, favor intente nuevamente.'));
		}
	    return $this->redirect(['action' => 'join', $country_id]);
    }
    
    public function join($id = null) {
	    
	    $this->Parameters->setConnection($this->connection);
	    $this->Joins->setConnection($this->connection);
	    
	    if($this->request->is('post') or $this->request->is('put')) {
		    
		    $data = $this->request->data();
		    
		    $entity = $this->Joins->newEntity($data);
		    $save = $this->Joins->save($entity);
		    
		    if($save) {
			    $this->Flash->success(__('Los argumentos han sido ingresados correctamente.'));
			    return $this->redirect(['controller' => 'countries', 'action' => 'join', $id, 'prefix' => 'fusion']);
		    } else {
			    $this->Flash->error(__('Ha ocurrido un problema al intentar guardar los parámetros. Favor intente mas tarde.'));
		    }
		    
	    }
	    
	    $country = $this->Countries->get($id, [
            'contain' => ['Joins', 'Parameters' => [
	            'sort' => ['Group_Order', 'Element_Order']
            ]]
        ]);
        
        $joins = ['US' => [], 'OOH' => []];
        foreach($country->joins as $join) {
	        $joins[$join['IdType']][] = $join;
        }
        
        // Campos de Select
        $table = [];
        foreach($country->parameters as $parameter) {
	        $table[$parameter['Code']] = $parameter['Value'];
        }
        
        // Listados de opciones de seleccion
        $temporal_list = [];
        $connector = $this->connector('PanUS', $table);
        $lists['US']['Pan'] = $connector->execute(sprintf("SELECT * FROM sys.columns WHERE object_id = OBJECT_ID('dbo.%s')", $table['TbPanUS']))->fetchAll('assoc');
        foreach($lists['US']['Pan'] as $item) {
	        $temporal_list[$item['name']] = $item['name'];
        }
        $lists['US']['Pan'] = $temporal_list;
        
        $temporal_list = [];
        $connector = $this->connector('IndUS', $table);
        $lists['US']['Ind'] = $connector->execute(sprintf("SELECT * FROM sys.columns WHERE object_id = OBJECT_ID('dbo.%s')", $table['TbIndUS']))->fetchAll('assoc');
        foreach($lists['US']['Ind'] as $item) {
	        $temporal_list[$item['name']] = $item['name'];
        }
        $lists['US']['Ind'] = $temporal_list;
        
        $temporal_list = [];
        $connector = $this->connector('PanOOH', $table);
        $lists['OOH']['Pan'] = $connector->execute(sprintf("SELECT * FROM sys.columns WHERE object_id = OBJECT_ID('dbo.%s')", $table['TbPanOOH']))->fetchAll('assoc');
        foreach($lists['OOH']['Pan'] as $item) {
	        $temporal_list[$item['name']] = $item['name'];
        }
        $lists['OOH']['Pan'] = $temporal_list;
        
        $temporal_list = [];
        $connector = $this->connector('IndOOH', $table);
        $lists['OOH']['Ind'] = $connector->execute(sprintf("SELECT * FROM sys.columns WHERE object_id = OBJECT_ID('dbo.%s')", $table['TbIndOOH']))->fetchAll('assoc');
        foreach($lists['OOH']['Ind'] as $item) {
	        $temporal_list[$item['name']] = $item['name'];
        }
        $lists['OOH']['Ind'] = $temporal_list;
        
        $lists['US']['Op'] = ['=' => '='];
        $lists['OOH']['Op'] = ['=' => '='];
        
        // Objetos JOIN para Formularios
        $join_US = $this->Joins->newEntity();
        $join_OOH = $this->Joins->newEntity();

        // Retorno de variables
		$this->set(compact('country'));
		$this->set(compact('joins'));
		$this->set(compact('lists'));
		$this->set(compact('join_US'));
		$this->set(compact('join_OOH'));
        $this->set('_serialize', ['country', 'joins', 'lists', 'join_US', 'join_OOH']);
	    
    }
    
    //!Procedimientos privados
    
    private function connector($string = null, $table = null) {
		
		$config = ConnectionManager::config($string, [
        	'className' => 'Cake\Database\Connection',
            'driver' => 'Cake\Database\Driver\Sqlserver',
            'persistent' => false,
            'host' => $table['Ip'.$string],
            'username' => $table['Us'.$string],
            'password' => $table['Pw'.$string],
            'database' => $table['Db'.$string],
            'timezone' => 'UTC',
            'cacheMetadata' => true,
            'log' => false,
            'flags' => [],
            'quoteIdentifiers' => false,
            'url' => env('DATABASE_URL', null),
        ]);
        
        $connector = ConnectionManager::get($string);
        
        return $connector;
	
	}

    
    private function checkData($id) {
	    
	    $valid = $this->connection->execute("SELECT * FROM dbo.Fusion_Parameters WHERE Country_id = :id", ['id' => $id])->fetchAll('assoc');
	    if($valid) {
		    return true;
	    } else {
		    return false;
	    }
	    
    }
    
    private function checkExists() {
	    
	    $exists = $this->connection->execute("SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'Fusion_Parameters'")->fetchAll('assoc');
	    if($exists) {
		    return true;
	    } else {
		    return false;
	    }
	    
    }
    
    private function createTable() {
	    
	    try {	    
		    $table = $this->connection->execute("CREATE TABLE Fusion_Parameters (
			    									Id INT NOT NULL IDENTITY (1,1),
			    									Code VARCHAR(50) NOT NULL,
			    									Country_id INT NULL,
			    									Description VARCHAR(MAX) NOT NULL,
			    									Group_Description VARCHAR(255) NOT NULL,
			    									Group_Order INT NOT NULL,
			    									Element_Order INT NOT NULL,
			    									Value VARCHAR(50) NULL,
			    									Project VARCHAR(50) NOT NULL,
			    									DtCriacao datetime NOT NULL,
			    									DtAtualizacao datetime NULL
		    									);
		    									
		    									ALTER TABLE dbo.Fusion_Parameters ADD CONSTRAINT PK_Production_Parameters PRIMARY KEY (Id);
		    									");
		} catch (Exception $e) {
			return false;
		}
		
		return true;
	    
    }
    
    private function populateTable($id = null) {
	    
	    $table = $this->connection->execute("INSERT INTO dbo.Fusion_Parameters (Code, Description, Group_Description, Group_Order, Element_Order, Country_id, DtCriacao, Project) 
	    									VALUES 
	    									('IpIndOOH', 'IP servidor de individuos', 'Individuos OOH', 1, 1, $id, GETDATE(), 'Fusion'),
	    									('DbIndOOH', 'BD de individuos', 'Individuos OOH', 1, 2, $id, GETDATE(), 'Fusion'),
	    									('UsIndOOH', 'Usuario base de datos', 'Individuos OOH', 1, 3, $id, GETDATE(), 'Fusion'),
	    									('PwIndOOH', 'Password base de datos', 'Individuos OOH', 1, 4, $id, GETDATE(), 'Fusion'),
	    									('TbIndOOH', 'Tabla de individuos', 'Individuos OOH', 1, 5, $id, GETDATE(), 'Fusion'),
	    									
	    									('IpPanOOH', 'IP servidor de panel', 'Panel OOH', 2, 1, $id, GETDATE(), 'Fusion'),
	    									('DbPanOOH', 'BD de panel', 'Panel OOH', 2, 2, $id, GETDATE(), 'Fusion'),
	    									('UsPanOOH', 'Usuario base de datos', 'Panel OOH', 2, 3, $id, GETDATE(), 'Fusion'),
	    									('PwPanOOH', 'Password base de datos', 'Panel OOH', 2, 4, $id, GETDATE(), 'Fusion'),
	    									('TbPanOOH', 'Tabla de panel', 'Panel OOH', 2, 5, $id, GETDATE(), 'Fusion'),
	    									
	    									('EdDatOOH', 'Campo edad', 'Parámetros OOH', 3, 1, $id, GETDATE(), 'Fusion'),
	    									('E1DatOOH', 'Edad mínima de individuo', 'Parámetros OOH', 3, 2, $id, GETDATE(), 'Fusion'),
											('E2DatOOH', 'Edad máxima de individuo', 'Parámetros OOH', 3, 3, $id, GETDATE(), 'Fusion'),
											('CiDatOOH', 'Campo ciudad', 'Parámetros OOH', 3, 4, $id, GETDATE(), 'Fusion'),
											('ClDatOOH', 'Ciudades habilitadas', 'Parámetros OOH', 3, 5, $id, GETDATE(), 'Fusion'),
	    									
	    									('IpIndUS', 'IP servidor de individuos', 'Individuos PNC U&S', 4, 1, $id, GETDATE(), 'Fusion'),
	    									('DbIndUS', 'BD de individuos', 'Individuos PNC U&S', 4, 2, $id, GETDATE(), 'Fusion'),
	    									('UsIndUS', 'Usuario base de datos', 'Individuos PNC U&S', 4, 3, $id, GETDATE(), 'Fusion'),
	    									('PwIndUS', 'Password base de datos', 'Individuos PNC U&S', 4, 4, $id, GETDATE(), 'Fusion'),
	    									('TbIndUS', 'Tabla de individuos', 'Individuos PNC U&S', 4, 5, $id, GETDATE(), 'Fusion'),
	    									
	    									('IpPanUS', 'IP servidor de panel', 'Panel PNC U&S', 5, 1, $id, GETDATE(), 'Fusion'),
	    									('DbPanUS', 'BD de panel', 'Panel PNC U&S', 5, 2, $id, GETDATE(), 'Fusion'),
	    									('UsPanUS', 'Usuario base de datos', 'Panel PNC U&S', 5, 3, $id, GETDATE(), 'Fusion'),
	    									('PwPanUS', 'Password base de datos', 'Panel PNC U&S', 5, 4, $id, GETDATE(), 'Fusion'),
	    									('TbPanUS', 'Tabla de panel', 'Panel PNC U&S', 5, 5, $id, GETDATE(), 'Fusion'),
	    									
	    									('EdDatUS', 'Campo edad', 'Parámetros PNC U&S', 6, 1, $id, GETDATE(), 'Fusion'),
	    									('E1DatUS', 'Edad mínima de individuo', 'Parámetros PNC U&S', 6, 2, $id, GETDATE(), 'Fusion'),
											('E2DatUS', 'Edad máxima de individuo', 'Parámetros PNC U&S', 6, 3, $id, GETDATE(), 'Fusion'),
											('CiDatUS', 'Campo ciudad', 'Parámetros PNC U&S', 6, 4, $id, GETDATE(), 'Fusion'),
											('ClDatUS', 'Ciudades habilitadas', 'Parámetros PNC U&S', 6, 5, $id, GETDATE(), 'Fusion'),
											
											('IpBuyOOH', 'IP servidor de compras', 'Compras OOH', 7, 1, $id, GETDATE(), 'Fusion'),
	    									('DbBuyOOH', 'BD de compras', 'Compras OOH', 7, 2, $id, GETDATE(), 'Fusion'),
	    									('UsBuyOOH', 'Usuario base de datos', 'Compras OOH', 7, 3, $id, GETDATE(), 'Fusion'),
	    									('PwBuyOOH', 'Password base de datos', 'Compras OOH', 7, 4, $id, GETDATE(), 'Fusion'),
	    									('TbBuyOOH', 'Tabla de compras', 'Compras OOH', 7, 5, $id, GETDATE(), 'Fusion'),
	    									
	    									('IpBuyUS', 'IP servidor de compras', 'Compras PNC U&S', 8, 1, $id, GETDATE(), 'Fusion'),
	    									('DbBuyUS', 'BD de compras', 'Compras PNC U&S', 8, 2, $id, GETDATE(), 'Fusion'),
	    									('UsBuyUS', 'Usuario base de datos', 'Compras PNC U&S', 8, 3, $id, GETDATE(), 'Fusion'),
	    									('PwBuyUS', 'Password base de datos', 'Compras PNC U&S', 8, 4, $id, GETDATE(), 'Fusion'),
	    									('TbBuyUS', 'Tabla de compras', 'Compras PNC U&S', 8, 5, $id, GETDATE(), 'Fusion'),
	    									
	    									('IpProOOH', 'IP servidor de artigos', 'Artigos OOH', 9, 1, $id, GETDATE(), 'Fusion'),
	    									('DbProOOH', 'BD de artigos', 'Artigos OOH', 9, 2, $id, GETDATE(), 'Fusion'),
	    									('UsProOOH', 'Usuario base de datos', 'Artigos OOH', 9, 3, $id, GETDATE(), 'Fusion'),
	    									('PwProOOH', 'Password base de datos', 'Artigos OOH', 9, 4, $id, GETDATE(), 'Fusion'),
	    									('TbProOOH', 'Tabla de artigos', 'Artigos OOH', 9, 5, $id, GETDATE(), 'Fusion'),
	    									
	    									('IpProUS', 'IP servidor de artigos', 'Artigos PNC U&S', 10, 1, $id, GETDATE(), 'Fusion'),
	    									('DbProUS', 'BD de artigos', 'Artigos PNC U&S', 10, 2, $id, GETDATE(), 'Fusion'),
	    									('UsProUS', 'Usuario base de datos', 'Artigos PNC U&S', 10, 3, $id, GETDATE(), 'Fusion'),
	    									('PwProUS', 'Password base de datos', 'Artigos PNC U&S', 10, 4, $id, GETDATE(), 'Fusion'),
	    									('TbProUS', 'Tabla de artigos', 'Artigos PNC U&S', 10, 5, $id, GETDATE(), 'Fusion'),
	    									
	    									('IpBfuOOH', 'IP servidor de compras fusion', 'Compras Fusion OOH', 11, 1, $id, GETDATE(), 'Fusion'),
	    									('DbBfuOOH', 'BD de compras fusion', 'Compras Fusion OOH', 11, 2, $id, GETDATE(), 'Fusion'),
	    									('UsBfuOOH', 'Usuario base de datos', 'Compras Fusion OOH', 11, 3, $id, GETDATE(), 'Fusion'),
	    									('PwBfuOOH', 'Password base de datos', 'Compras Fusion OOH', 11, 4, $id, GETDATE(), 'Fusion'),
	    									('TbBfuOOH', 'Tabla de compras fusion', 'Compras Fusion OOH', 11, 5, $id, GETDATE(), 'Fusion'),
	    									
	    									('IpUsrUS', 'IP servidor de usuarios', 'Usuarios PNC U&S', 12, 1, $id, GETDATE(), 'Fusion'),
	    									('DbUsrUS', 'BD de usuarios', 'Usuarios PNC U&S', 12, 2, $id, GETDATE(), 'Fusion'),
	    									('UsUsrUS', 'Usuario base de datos', 'Usuarios PNC U&S', 12, 3, $id, GETDATE(), 'Fusion'),
	    									('PwUsrUS', 'Password base de datos', 'Usuarios PNC U&S', 12, 4, $id, GETDATE(), 'Fusion'),
	    									('TbUsrUS', 'Tabla de usuarios', 'Usuarios PNC U&S', 12, 5, $id, GETDATE(), 'Fusion'),
	    									
	    									('IpDomUS', 'IP servidor de domicilios', 'Domicilios PNC U&S', 13, 1, $id, GETDATE(), 'Fusion'),
	    									('DbDomUS', 'BD de domicilios', 'Domicilios PNC U&S', 13, 2, $id, GETDATE(), 'Fusion'),
	    									('UsDomUS', 'Usuario base de datos', 'Domicilios PNC U&S', 13, 3, $id, GETDATE(), 'Fusion'),
	    									('PwDomUS', 'Password base de datos', 'Domicilios PNC U&S', 13, 4, $id, GETDATE(), 'Fusion'),
	    									('TbDomUS', 'Tabla de domicilios', 'Domicilios PNC U&S', 13, 5, $id, GETDATE(), 'Fusion'),
	    									
	    									('IpResFUS', 'IP servidor de fusion resultado', 'Resultado FUSION', 8, 1, 5, GETDATE(), 'Fusion'),
	    									('DbResFUS', 'BD de fusion resultado', 'Resultado FUSION', 8, 2, 5, GETDATE(), 'Fusion'),
	    									('UsResFUS', 'Usuario base de datos', 'Resultado FUSION', 8, 3, 5, GETDATE(), 'Fusion'),
	    									('PwResFUS', 'Password base de datos', 'Resultado FUSION', 8, 4, 5, GETDATE(), 'Fusion'),
	    									('TbResFUS', 'Tabla de fusion resultado', 'Resultado FUSION', 8, 5, 5, GETDATE(), 'Fusion')");

	    
    }

}