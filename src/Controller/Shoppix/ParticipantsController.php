<?php
namespace App\Controller\Shoppix;

use App\Controller\AppController;
use Cake\Datasource\ConnectionManager;

/**
 * Participants Controller
 *
 */
class ParticipantsController extends AppController
{
	
	public function initialize()
	{
		parent::initialize();
		$this->loadModel('ShoppixUsers');
		
	}
	
	public function index($country = null)
	{
		
		$this->_setConnection($country);
		$pageTitle = 'Participants summary';
		
		// Summary
		$participants = $this->ShoppixUsers->summary();
		// Total Graph
		$total_graph = $this->ShoppixUsers->total_chart();
		
		$this->set(compact('country', 'pageTitle', 'participants', 'total_graph'));
        $this->set('_serialize', ['country', 'pageTitle', 'participants', 'total_graph']);
	}
	
	public function _setConnection($country = null) {
		
		$this->connection = ConnectionManager::get('shoppix-profile-'.strtolower($country));
		$this->ShoppixUsers->setConnection($this->connection);
		
	}
}
