<?php
namespace App\Controller\Uroboros;

use App\Controller\AppController;

/**
 * Cases Controller
 *
 *
 * @method \App\Model\Entity\Case[] paginate($object = null, array $settings = [])
 */
class CasesController extends AppController
{

	public function initialize($config = null) {
		parent::initialize($config);
		$this->loadModel('UroborosProcessControls');
		$this->loadModel('UroborosResolutions');
	}

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $pageTitle = 'Listado de mis casos detectados por Uroboros';
        
        // Leer pendientes y finalizados
        $pendants 	= $this->UroborosProcessControls->loadPendants($this->Auth->user('mail'));
        $finishes 	= $this->UroborosProcessControls->loadFinishes($this->Auth->user('mail'));
        
        $aux = [];
        foreach($pendants as $pendant) {
	        if(count($pendant['uroboros_logs']) > 0 ) {
		    	$aux[] = $pendant;
		    }
        }
        $pendants = $aux;
        
        $aux = [];
        foreach($finishes as $finish) {
	        if(count($finish['uroboros_logs']) > 0 ) {
		    	$aux[] = $finish;
		    }
        }
        $finishes = $aux;
        
        
        $this->set(compact('pageTitle', 'pendants', 'finishes'));
        $this->set('_serialize', ['pageTitle', 'pendants', 'finishes']);
    }
    
    public function all()
    {
        $pageTitle = 'Listado de todos los casos detectados por Uroboros';
        
        // Leer pendientes y finalizados
        $pendants 	= $this->UroborosProcessControls->loadPendants();
        $finishes 	= $this->UroborosProcessControls->loadFinishes();
        
        
        $this->set(compact('pageTitle', 'pendants', 'finishes'));
        $this->set('_serialize', ['pageTitle', 'pendants', 'finishes']);
    }
    
    public function detail($id = null) {
	    
	    $pageTitle = 'Detalle de ejecución y sus errores detectados';
        
        // Leer pendientes y finalizados
        $detail 	= $this->UroborosProcessControls->loadDetail($id);
        $resolution = $this->UroborosResolutions->newEntity();
        
        if($this->request->is('post')) {
		    
		    $data = $this->request->getData();
			$data['eid'] = $id;
			$data['gid'] = uniqid();
			$data['created'] = date('Ymd h:i:s');
			$entity = [
				'eid' => $id,
				'done' => true,
				'done_date' => date('Y-m-d'),
				'done_user' => $this->Auth->user('mail')
			];
			
			$resolution = $this->UroborosResolutions->patchEntity($resolution, $data);
			$process = $this->UroborosProcessControls->patchEntity($detail, $entity);
			
			// Transactional UPDATE
			$update = $this->UroborosProcessControls->getConnection()->transactional(function () use ($resolution, $process) {
			    
			    try {
				    $this->UroborosResolutions->save($resolution, ['atomic' => false]);
				    $this->UroborosProcessControls->save($process, ['atomic' => false]);
			    } catch (\Exception $e) {
				    return false;
			    }
			    return true;
			    
			});
					    
		    if($update) {
			    $this->Flash->success('Registro actualizado exitosamente!');
				return $this->redirect(['controller' => 'cases', 'action' => 'index', 'prefix' => 'uroboros']);
		    } else {
			    $this->Flash->error('No se ha podido actualizar el registro, favor intente mas tarde.');
		    }
		    
	    }        
        
        $this->set(compact('pageTitle', 'detail', 'resolution'));
        $this->set('_serialize', ['pageTitle', 'detail'. 'resolution']);
	    
    }
}
