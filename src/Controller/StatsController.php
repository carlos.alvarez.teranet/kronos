<?php
namespace App\Controller;

use App\Controller\AppController;

class StatsController extends AppController
{
	
	public function initialize()
	{
		parent::initialize();
		
		$this->loadModel('PortalLogins');
	}

	public function index() 
	{
		$pageTitle = 'Mi panel';
		
		// Lista de logins
		$logins = $this->PortalLogins->find('all', [
			'conditions' => [
				'mail' => $this->Auth->User('mail')
			],
			'order' => [
				'created' => 'DESC'
			],
			'limit' => 5			
		]);
		
		// Lista de dias
		$days = [
			'Mon' => 'Lunes',
			'Tue' => 'Martes',
			'Wed' => 'Miércoles',
			'Thu' => 'Jueves',
			'Fri' => 'Viernes',
			'Sat' => 'Sábado',
			'Sun' => 'Domingo'
		];
		
		// Listado de sistemas
		$links = [];
		$links[] = [
			'name' => 'Aranda',
			'link' => [
				'internal' => 'http://172.28.232.250/USDK/Login.aspx',
				'external' => 'http://servicedesk.kantarworldpanel.cl/USDK/Login.aspx'
			]
		];
		$links[] = [
			'name' => 'Kronos',
			'link' => [
				'internal' => 'http://172.28.232.191/panel/',
				'external' => 'http://kronos.kantarworldpanel.com/'
			]
		];
		$links[] = [
			'name' => 'Delivery',
			'link' => [
				'internal' => 'https://ktglbuc.sharepoint.com/sites/ROCLatAm/DELIVERY/Forms/AllItems.aspx',
				'external' => 'https://ktglbuc.sharepoint.com/sites/ROCLatAm/DELIVERY/Forms/AllItems.aspx'
			]
		];
		$links[] = [
			'name' => 'Workplace',
			'link' => [
				'internal' => 'https://kantar.facebook.com',
				'external' => 'https://kantar.facebook.com'
			]
		];
		
		$iptype = 'external';
		if(strpos($_SERVER['REMOTE_ADDR'], '172.28.232') >= 0) {
			$iptype = 'internal';
		}
		//
		
		$this->set(compact('pageTitle', 'logins', 'days', 'links', 'iptype'));
        $this->set('_serialize', ['pageTitle', 'logins', 'days', 'links', 'iptype']);
	}

}