<?php
namespace App\Controller\Operations;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Datasource\ConnectionManager;

/**
 * Distributions Controller
 *
 */
class DistributionsController extends AppController
{
	
	public function initialize()
	{
		parent::initialize();
		$this->loadModel('AttributesCountries');
	}
	
	public function beforeFilter(Event $event) {
		
		parent::beforeFilter($event);
    }
    
    public function index()
	{   
		$pageTitle = 'Distribuciones';
		
		// Ciclo de Países
		$countries = $this->AttributesCountries->find('all', [
			'order' => 'Descricao ASC'
		]);
		
		$this->set(compact('pageTitle', 'countries'));
        $this->set('_serialize', ['pageTitle', 'countries']);
         
	}
	
}
