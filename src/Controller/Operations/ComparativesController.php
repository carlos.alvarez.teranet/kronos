<?php
namespace App\Controller\Operations;

use Cake\Network\Session\DatabaseSession;
use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Datasource\ConnectionManager;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;
use PHPExcel;
use PHPExcel_IOFactory;
use Cake\Console\ShellDispatcher;

/**
 * Weighings Controller
 *
 */
class ComparativesController extends AppController
{
	
	public function initialize()
	{
		parent::initialize();
		$this->loadComponent('Excel');
	}
	
	public function beforeFilter(Event $event) {
		
		parent::beforeFilter($event);
    }
    
    public function index($country = null)
	{
		$pageTitle = 'Comparativos';
		
		// Archivo configuración
		$string = file_get_contents( CONFIG . DS . "platform" . DS . "comparatives.json" );
		$config = json_decode($string, true);
		
		$connection = ConnectionManager::get($config['countries'][$country]['dbname']);
		$code		= $config['countries'][$country]['code'];
		
		$dir 			= new Folder('/mnt/procesos/ZIbanz/Atos');
        $files 			= $dir->find('^ato*\w+('.$code.')\w+.*');
        
        $elements = [];
        foreach($files as $file) {
	        $base 	= explode(".", $file)[0];
	        $ext	= explode(".", $file)[1];
	        
	        if(strlen($ext) <> 4) {
		        continue;
	        }
	        
	        $base	= str_replace("ato", "", $base);
	        $base	= str_replace("cs", "", $base);
	        
	        $ext	= $ext[2].$ext[3].$ext[0].$ext[1];
	        
	       $elements[$base][] = $ext;
	        
        }
		
		$databases	= $connection->execute("SELECT 	a.Nombre,
													a.Descripcion,
													a.Grupo,
													b.Nombre as Corte,
													c.Descripcion as Panel
											FROM 	dbo.DBM_DA2 a
											JOIN	dbo.DBM_Cortes b ON b.IdCorte = a.IdCorte
											JOIN	dbo.DBM_DA2_Tipos c ON c.IdTipo = a.IdTipo
											WHERE	a.FlgActivo = 'AT'
											AND		c.Descripcion = 'PNC'
											ORDER	by a.Grupo ASC, a.Nombre")->fetchAll('assoc');
		
		$this->set(compact('pageTitle', 'config', 'databases', 'files', 'elements', 'country', 'dir'));
        $this->set('_serialize', ['pageTitle', 'config', 'databases', 'files', 'elements', 'country', 'dir']);
	}
	
	public function detail($country = null, $database = null) {
		
		$pageTitle = 'Comparativos';
		
		// Archivo configuración
		$string = file_get_contents( CONFIG . DS . "platform" . DS . "comparatives.json" );
		$config = json_decode($string, true);
		
		$connection = ConnectionManager::get($config['countries'][$country]['dbname']);
		
		$dir 			= new Folder('/mnt/procesos/ZIbanz/Atos');
        $files 			= $dir->find('^ato'.$database.'\w+.*');
        
        $tmp = [];
        foreach($files as $r => $file) {
	        $file = new File($dir->pwd() . DS . $file, true, 0644);
	        $tmp[($file->ext()[2].$file->ext()[3].$file->ext()[0].$file->ext()[1]).'_'.$r] = $file;
        }
        $files = $tmp;
        ksort($files);
        $files = array_reverse($files);
        
        $info	= $connection->execute("SELECT 	a.Nombre,
												a.Descripcion,
												a.Grupo,
												b.Nombre as Corte,
												c.Descripcion as Panel
										FROM 	dbo.DBM_DA2 (nolock) a
										JOIN	dbo.DBM_Cortes (nolock) b ON b.IdCorte = a.IdCorte
										JOIN	dbo.DBM_DA2_Tipos (nolock) c ON c.IdTipo = a.IdTipo
										WHERE	a.FlgActivo = 'AT'
										AND		a.Nombre = '$database'
										AND		c.Descripcion = 'PNC'
										ORDER	by a.Grupo ASC, a.Nombre")->fetch('assoc');
										
										
		$folder 		= new Folder(TMP . "acts");
        $acts 			= $folder->find('^'.$database.'\w+.*');
        
        $tmp = [];
        foreach($acts as $r => $file) {
	        $file = new File($folder->pwd() . DS . $file, true, 0644);
	        if($file->size() == 0) {
		        continue;
	        }
	        $tmp[$r] = $file;
	        
	        $stats[] = ['u' => 0, 'd' => 0, 'n' => 0];
	        $archivo = fopen($file->pwd(), 'r');
	        while(!feof($archivo)) {
		        $dato = fgets($archivo);
		        if(explode(";", $dato)[4] == 'Acto nuevo') { 
			        $stats[$r]['n']++; 
			    } else if (explode(";", $dato)[4] == 'Acto eliminado') {
				    $stats[$r]['d']++;
			    } else {
				    $stats[$r]['u']++;
			    }
		    }
	        
        }
        $acts = $tmp;
        ksort($acts);
		
        
		$this->set(compact('pageTitle', 'config', 'database', 'files', 'country', 'info', 'acts', 'stats'));
        $this->set('_serialize', ['pageTitle', 'config', 'database', 'files', 'country', 'info', 'acts', 'stats']);
        
	}
	
	public function process($country = null, $database = null) {
		
		$this->autoRender = false;
		
		// Archivo configuración
		$string = file_get_contents( CONFIG . DS . "platform" . DS . "comparatives.json" );
		$config = json_decode($string, true);
		
		$connection = ConnectionManager::get($config['countries'][$country]['dbname']);
		$info		= $connection->execute("SELECT 	a.Nombre,
												a.Descripcion,
												a.Grupo,
												b.Nombre as Corte,
												c.Descripcion as Panel
										FROM 	dbo.DBM_DA2 a
										JOIN	dbo.DBM_Cortes b ON b.IdCorte = a.IdCorte
										JOIN	dbo.DBM_DA2_Tipos c ON c.IdTipo = a.IdTipo
										WHERE	a.FlgActivo = 'AT'
										AND		a.Nombre = '$database'
										AND		c.Descripcion = 'PNC'
										ORDER	by a.Grupo ASC, a.Nombre")->fetch('assoc');
		
		if ($this->request->is('post')) {
			$data = $this->request->getData();
			
			$periods = array_filter($data['compare']);
			$periods = array_keys($periods);
			
			// Ejecutar Shell de comparador de actos
			$output = exec(ROOT . DS . 'bin/cake compare_acts ' . $database . ' ' . $periods[1] . ' ' . $periods[0] . ' ' . $config['countries'][$country]['code'] . ' > /dev/null 2>&1 & echo $!;', $o);
			//$output = exec(ROOT . DS . 'bin/cake compare_acts ' . $database . ' ' . $periods[1] . ' ' . $periods[0] . ' > /home/production/public_html/logs/compare_acts;', $o);
					
			return $this->redirect($this->referer());
			
		}		
		
	}
	
}