<?php
namespace App\Controller\Operations;

use Cake\Network\Session\DatabaseSession;
use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Datasource\ConnectionManager;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;
use PHPExcel;
use PHPExcel_IOFactory;
use Cake\Console\ShellDispatcher;

/**
 * Weighings Controller
 *
 */
class WeighingsController extends AppController
{
	
	public function initialize()
	{
		parent::initialize();
		$this->loadComponent('Excel');
	}
	
	public function beforeFilter(Event $event) {
		
		parent::beforeFilter($event);
    }
    
    public function index()
	{   
		$pageTitle = 'Pesajes';
		
		// Archivo configuración
		$string = file_get_contents( CONFIG . DS . "platform" . DS . "weighings.json" );
		$config = json_decode($string, true);
		
		$this->set(compact('pageTitle', 'config'));
        $this->set('_serialize', ['pageTitle', 'config']);
         
	}
	
	public function w($c_sel = null, $p_sel = null, $year = null, $month = null) {
		
		$this->viewBuilder()->layout('ajax');	
		$pageTitle = 'Pesajes Especiales';
		
		// Archivo configuración
		$string = file_get_contents( CONFIG . DS . "platform" . DS . "weighings.json" );
		$config = json_decode($string, true);
		
		// Summary
		$connection = ConnectionManager::get($config['countries'][$c_sel]['panels'][$p_sel]['weights']['db']);
		$table 		= $config['countries'][$c_sel]['panels'][$p_sel]['csv']['table'];
		
		$specials	= $connection->execute("SELECT	idPeso,
													COUNT(*) as Samples,
													SUM(Valor) as Weights
											FROM	$table
											WHERE	Ano = $year
											AND		MesSem = $month
											GROUP	by idPeso
											ORDER	by idPeso")->fetchAll('assoc');
											
		$this->set(compact('pageTitle', 'config', 'c_sel', 'p_sel', 'year', 'month', 'specials'));
        $this->set('_serialize', ['pageTitle', 'config', 'c_sel', 'p_sel', 'year', 'month', 'specials']);
		
	}
	
	public function c($c_sel = null)
    {
	    
		$pageTitle = 'Pesajes';
		
		// Archivo configuración
		$string = file_get_contents( CONFIG . DS . "platform" . DS . "weighings.json" );
		$config = json_decode($string, true);
		
		$this->set(compact('pageTitle', 'config', 'c_sel'));
        $this->set('_serialize', ['pageTitle', 'config', 'c_sel']);
		
    }
    
    public function p($c_sel = null, $p_sel = null)
    {
		$pageTitle = 'Pesajes';
		
		// Archivo configuración
		$string = file_get_contents( CONFIG . DS . "platform" . DS . "weighings.json" );
		$config = json_decode($string, true);
		
		// Summary
		$connection = ConnectionManager::get($config['countries'][$c_sel]['panels'][$p_sel]['weights']['db']);
		$table 		= $config['countries'][$c_sel]['panels'][$p_sel]['weights']['table'];
		$weights	= $connection->execute("SELECT	Ano,
													SUM(CASE WHEN MesSem = 1 THEN Valor ELSE 0 END) as Mes1,
													SUM(CASE WHEN MesSem = 2 THEN Valor ELSE 0 END) as Mes2,
													SUM(CASE WHEN MesSem = 3 THEN Valor ELSE 0 END) as Mes3,
													SUM(CASE WHEN MesSem = 4 THEN Valor ELSE 0 END) as Mes4,
													SUM(CASE WHEN MesSem = 5 THEN Valor ELSE 0 END) as Mes5,
													SUM(CASE WHEN MesSem = 6 THEN Valor ELSE 0 END) as Mes6,
													SUM(CASE WHEN MesSem = 7 THEN Valor ELSE 0 END) as Mes7,
													SUM(CASE WHEN MesSem = 8 THEN Valor ELSE 0 END) as Mes8,
													SUM(CASE WHEN MesSem = 9 THEN Valor ELSE 0 END) as Mes9,
													SUM(CASE WHEN MesSem = 10 THEN Valor ELSE 0 END) as Mes10,
													SUM(CASE WHEN MesSem = 11 THEN Valor ELSE 0 END) as Mes11,
													SUM(CASE WHEN MesSem = 12 THEN Valor ELSE 0 END) as Mes12
											FROM	$table
											WHERE	Ano >= 2017
											AND		idPeso = 1
											GROUP	by Ano
											ORDER	by Ano")->fetchAll('assoc');
											
		$samples	= $connection->execute("SELECT	Ano,
													SUM(CASE WHEN MesSem = 1 THEN 1 ELSE 0 END) as Mes1,
													SUM(CASE WHEN MesSem = 2 THEN 1 ELSE 0 END) as Mes2,
													SUM(CASE WHEN MesSem = 3 THEN 1 ELSE 0 END) as Mes3,
													SUM(CASE WHEN MesSem = 4 THEN 1 ELSE 0 END) as Mes4,
													SUM(CASE WHEN MesSem = 5 THEN 1 ELSE 0 END) as Mes5,
													SUM(CASE WHEN MesSem = 6 THEN 1 ELSE 0 END) as Mes6,
													SUM(CASE WHEN MesSem = 7 THEN 1 ELSE 0 END) as Mes7,
													SUM(CASE WHEN MesSem = 8 THEN 1 ELSE 0 END) as Mes8,
													SUM(CASE WHEN MesSem = 9 THEN 1 ELSE 0 END) as Mes9,
													SUM(CASE WHEN MesSem = 10 THEN 1 ELSE 0 END) as Mes10,
													SUM(CASE WHEN MesSem = 11 THEN 1 ELSE 0 END) as Mes11,
													SUM(CASE WHEN MesSem = 12 THEN 1 ELSE 0 END) as Mes12
											FROM	$table
											WHERE	Ano >= 2017
											AND		idPeso = 1
											GROUP	by Ano
											ORDER	by Ano")->fetchAll('assoc');
		
		// Logs
		$log	= LOGS . "weights" . DS . $config['countries'][$c_sel]['code'] . "_" . $config['countries'][$c_sel]['panels'][$p_sel]['code'] . ".json";
		$file 	= new File($log, true, 0644);
		$logs	= json_decode($file->read(), true);
		$logs['items'] = (empty($logs['items'])) ? [] : array_reverse($logs['items'], true);
		
		$years = [
			date('Y') - 4 => date('Y') - 4,
			date('Y') - 3 => date('Y') - 3,
			date('Y') - 2 => date('Y') - 2,
			date('Y') - 1 => date('Y') - 1,
			date('Y') => date('Y'),
			date('Y') + 1 => date('Y') + 1,
			date('Y') + 2 => date('Y') + 2
		];
		
		$months = [
			1 => 'Enero',
			2 => 'Febrero',
			3 => 'Marzo',
			4 => 'Abril',
			5 => 'Mayo',
			6 => 'Junio',
			7 => 'Julio',
			8 => 'Agosto',
			9 => 'Septiembre',
			10 => 'Octubre',
			11 => 'Noviembre',
			12 => 'Diciembre'
		];
		
		$fist_day = new \DateTime("first day of last month");
		$selected_year = $fist_day->format('Y');
		$selected_month = $fist_day->format('m');
		
		$help = $config['countries'][$c_sel]['panels'][$p_sel];
		
		$this->set(compact('pageTitle', 'config', 'c_sel', 'p_sel', 'weights', 'samples', 'logs', 'years', 'months', 'selected_year', 'selected_month', 'help'));
        $this->set('_serialize', ['pageTitle', 'config', 'c_sel', 'p_sel', 'weights', 'samples', 'logs', 'years', 'months', 'selected_year', 'selected_month', 'help']);
		
    }
    
    public function upload($c_sel = null, $p_sel = null)
    {
	    $this->autoRender = false;
	    
	    // Archivo configuración
		$string = file_get_contents( CONFIG . DS . "platform" . DS . "weighings.json" );
		$config = json_decode($string, true);
		$log	= LOGS . "weights" . DS . $config['countries'][$c_sel]['code'] . "_" . $config['countries'][$c_sel]['panels'][$p_sel]['code'] . ".json";
		$file 	= new File($log, true, 0644);
		
		// General
		$connection = ConnectionManager::get($config['countries'][$c_sel]['panels'][$p_sel]['weights']['db']);
		
		// Subir archivo
		if($this->request->is('post')) {
			
			// Validacion de periodos
			$data = $this->request->data();
			if(empty($data['p_year']) or empty($data['p_year'])) {
				$this->Flash->error('Debe seleccionar Año y Periodo/Mes');
				return $this->redirect(
		            ['action' => 'p', $c_sel, $p_sel]
		        );
			}
			
			// Validacion de extensión de archivo
			if($data['excel']['type'] != 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
				$this->Flash->error('El archivo debe ser tipo EXCEL y extensión .xlsx');
				return $this->redirect(
		            ['action' => 'p', $c_sel, $p_sel]
		        );
			}
			
			if(!is_uploaded_file($data['excel']['tmp_name'])) {
		    	return false;
		    }
		    
		    $destFile = WWW_ROOT . 'excel' . DS . uniqid() . '_' . $data['excel']['name'];
		    if (!move_uploaded_file($data['excel']['tmp_name'], $destFile)){
		    	return false;
		    }
		    
		    if(!$file->exists()) {
			    $file->write('{"items":[]}');
		    }
		    $log_data = json_decode($file->read());
		    $log_data->items[] = [
			    'date' => date('d-m-Y H:i:s'),
			   // 'period' => $this->Excel->weight_period($destFile),
			    'year' => $data['p_year'],
			    'month' => $data['p_month'],
			    'user' => $this->Auth->User('mail'),
			    'name' => $data['excel']['name'],
			    'file' => $destFile,
			    'type' => $data['excel']['type'],
			    'size' => $data['excel']['size']
		    ];
		    $jsonData = json_encode($log_data);
		    $file->write($jsonData);
		    
		}
		
		return $this->redirect(
            ['action' => 'p', $c_sel, $p_sel]
        );
    }
    
    public function process($c_sel = null, $p_sel = null)
    {
	    $this->autoRender = false;
	    set_time_limit(120);
	    
	    // Archivo configuración
		$string = file_get_contents( CONFIG . DS . "platform" . DS . "weighings.json" );
		$config = json_decode($string, true);
		$log	= LOGS . "weights" . DS . $config['countries'][$c_sel]['code'] . "_" . $config['countries'][$c_sel]['panels'][$p_sel]['code'] . ".json";
		$file 	= new File($log, true, 0644);
		$jsonData = json_decode($file->read(), true);
		
		$position = $this->request->data();			
		$inputFileName	= $jsonData['items'][$position['key']]['file'];
		
		//  Leer archivo guardado previamente en las carpetas de webroot
		try {
		    $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
		    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
		    $objPHPExcel = $objReader->load($inputFileName);
		} catch(Exception $e) {
		    die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
		}
		
		$sheet = $objPHPExcel->getSheet(0);
		
		$initialRow = 2;
		$highestRow = $sheet->getHighestRow(); 
		$initialColumn = 1;
		$highestColumn = $sheet->getHighestColumn(); 
		
		/*
		$rowData = $sheet->rangeToArray('A' . $initialRow . ':' . $highestColumn . $highestRow,
		                                    NULL,
		                                    TRUE,
		                                    FALSE);
		*/
		                                    
		$headerData = $sheet->rangeToArray('A' . ($initialRow - 1) . ':' . $highestColumn . ($initialRow - 1),
		                                    NULL,
		                                    TRUE,
		                                    FALSE)[0];
		
		$validate = [];                                    
		// Validación de columnas
		$headerData = array_map('strtolower', $headerData);
		$headerData = array_map('trim', $headerData);
		$columns = $config['countries'][$c_sel]['panels'][$p_sel]['csv']['columns'];
		foreach($columns as $key => $column) {
			$validate['columns'][$column['from']] = array_search($column['from'], $headerData);
		}
		
		$data = [];
		if(!array_search(false, $validate['columns'], true)) {
			//$jsonData['items'][$position['key']]['last_execution'] = date('d-m-Y H:i:s');
			$jsonData['items'][$position['key']]['start_execution'] = date('d-m-Y H:i:s');
			
			// Ejecutar Shell de carga de datos
			$output = exec(ROOT . DS . 'bin/cake upload_excel_weight ' . $c_sel . ' ' . $p_sel . ' ' . $position['key'] . ' > /dev/null 2>&1 & echo $!;', $o);
			
			//$shell = new ShellDispatcher();
			//$output = $shell->run(['cake', 'UploadExcelWeight', $c_sel, $p_sel, $position['key']]);
			//exec(ROOT . "bin/cake upload_excel_weight " . $c_sel . " " . $p_sel . " " . $position['key'] . " --verbose > " . LOGS . "test");
		} else {
			$this->Flash->error('El archivo cargado no contiene todas las columnas necesarias para procesar.');
			$jsonData['items'][$position['key']]['error'] = [
				'header' => $headerData,
				'validate' => $validate
			];
		}
		
		$jsonData = json_encode($jsonData);
		$file->write($jsonData);
		
		return $this->redirect(
            ['action' => 'p', $c_sel, $p_sel]
        );
    }
    
    public function delete($c_sel = null, $p_sel = null)
    {
	    $this->autoRender = false;
	    
	    // Archivo configuración
		$string = file_get_contents( CONFIG . DS . "platform" . DS . "weighings.json" );
		$config = json_decode($string, true);
		$log	= LOGS . "weights" . DS . $config['countries'][$c_sel]['code'] . "_" . $config['countries'][$c_sel]['panels'][$p_sel]['code'] . ".json";
		$file 	= new File($log, true, 0644);
		$jsonData = json_decode($file->read(), true);
		
		$position = $this->request->data();			
		$inputFileName	= $jsonData['items'][$position['key']]['file'];
		
		try {
			$jsonData['items'][$position['key']]['visible'] = false;
			unlink($inputFileName);
			$this->Flash->success('El archivo fue eliminado con éxito.');
			
			$jsonData = json_encode($jsonData);
			$file->write($jsonData);
		
		} catch(Exception $e) {
			$this->Flash->error('Ocurrió un problema al eliminar el archivo. Favor contactarse con el administrador');
		}
		
		return $this->redirect(
            ['action' => 'p', $c_sel, $p_sel]
        );
	}
	
	public function status($c_sel = null, $p_sel = null, $key = null)
	{
		$string 	= file_get_contents( CONFIG . DS . "platform" . DS . "weighings.json" );
		$config 	= json_decode($string, true);
		
		$file 	= LOGS . "weights" . DS . $config['countries'][$c_sel]['code'] . "_" . $config['countries'][$c_sel]['panels'][$p_sel]['code'] . "_file_" . $key . ".json";
		$output	= shell_exec("tail -n 1 " . $file);
		
		
		$status = json_decode($output);
		$this->set(compact('status'));
		$this->set('_serialize', ['status']);
		
	}
	
	public function download($c_sel = null, $p_sel = null, $key = null) 
	{
		$this->autoRender = false;
		
		$string 	= file_get_contents( CONFIG . DS . "platform" . DS . "weighings.json" );
		$config 	= json_decode($string, true);
		
		$log	= LOGS . "weights" . DS . $config['countries'][$c_sel]['code'] . "_" . $config['countries'][$c_sel]['panels'][$p_sel]['code'] . ".json";
		
		$file 	= new File($log, true, 0644);
		$jsonData = json_decode($file->read(), true);
		
		debug($jsonData);
		
		$this->response->file($jsonData['items'][$key]['file'], ['download' => true, 'name' => $jsonData['items'][$key]['name']]);
		
	}
	
}
