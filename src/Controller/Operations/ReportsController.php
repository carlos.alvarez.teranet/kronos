<?php
namespace App\Controller\Operations;

use Cake\Network\Session\DatabaseSession;
use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Datasource\ConnectionManager;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;
use Cake\Console\ShellDispatcher;

/**
 * Weighings Controller
 *
 */
class ReportsController extends AppController
{
	public function initialize()
	{
		parent::initialize();
	}
	
	public function beforeFilter(Event $event) {
		
		parent::beforeFilter($event);
    }
    
    public function index() {
	    
	    $pageTitle = 'Seguimiento Producción';
	    
	    $periodo = date('Ym', strtotime('-3 month'));
	    
	    $connection = ConnectionManager::get('IT_WORKFLOW_33');
	    $periods	= $connection->execute("SELECT	d.Period,
													COUNT(DISTINCT(CASE
														WHEN (d.Grupo LIKE '%_PNC' OR d.Grupo IN ('PE_TBC', 'PE_RIPLEY', 'BR_PNI', 'BR_TBC', 'CL_DG')) THEN d.Nombre
													END)) as [Individuales],
													COUNT(DISTINCT(CASE
														WHEN (d.Grupo LIKE '%_Cross' AND d.Grupo <> 'BR_WPO_Cross') THEN d.Nombre
													END)) as [Cross],
													COUNT(DISTINCT(CASE
														WHEN (d.Grupo LIKE '%_OOH') THEN d.Nombre
													END)) as [OOH],
													COUNT(DISTINCT(CASE
														WHEN (d.Grupo LIKE '%_US') AND d.Grupo <> 'BR_WPO_US' THEN d.Nombre
													END)) as [US]
											FROM 	dbo.DBM_Status_General d
											WHERE	d.FlgActivo = 'AT'
											AND		d.Period >= $periodo
											GROUP 	by d.Period 
											ORDER 	by d.Period")->fetchAll('assoc');
	    
	    $this->set(compact('pageTitle', 'periods'));
        $this->set('_serialize', ['pageTitle', 'periods']);
	    
    }
    
    public function individuals($period = null)
	{
		$pageTitle = 'Seguimiento Producción Individuales';
		
		$connection = ConnectionManager::get('IT_WORKFLOW_33');
		$inds		= $connection->execute("SELECT * FROM dbo.DBM_Status_Level_Individuals WHERE Period = ? ORDER by [Sort] ASC, 1 ASC", [$period], ['integer'])->fetchAll('assoc');
		
		$details	= $connection->execute("SELECT * FROM dbo.DBM_Status_Detail_Individuals WHERE Period = ?", [$period], ['integer'])->fetchAll('assoc');
		
		$pendants	= $connection->execute("SELECT * FROM dbo.DBM_Status_Pendants_Individuals WHERE Period = ?", [$period], ['integer'])->fetchAll('assoc');
		
		$connection = ConnectionManager::get('ARANDA');
		$aranda		= $connection->execute("SELECT
											CASE
												WHEN SUBSTRING(e.NAME, 0, 4) = 'CAM' THEN 'CAM'
												ELSE co.DESCRIPTION
											END as DESCRIPTION, 
											c.serv_id_by_project AS ID,
											CASE
												WHEN SUBSTRING(e.NAME, 0, 4) = 'CAM' THEN SUBSTRING(e.NAME,5,8)
											ELSE SUBSTRING(e.NAME,4,8)END AS BASE,
											c.serv_opened_date as FECHA_REGISTRO,
											c.serv_colsed_date as FECHA_CIERRE,
											stat.stat_name AS STATUS,
											us.UNAME AS RESPONSABLE,
											G.GRP_NAME AS ÁREA, 
											CASE WHEN serv_category_id = 74 THEN '3.1' ELSE '3.2' END AS TIPO_CUESTIONAMIENTO
											FROM ASDK_SERVICE_CALL c
											INNER JOIN ASDK_SERVICE e ON c.serv_service_id = e.FL_INT_SERVICE_ID
											INNER JOIN USUARIOS u ON u.CODUSUARIO = c.serv_customer_id
											INNER JOIN USUARIOS us ON us.CODUSUARIO = c.serv_responsible_id
											INNER JOIN COUNTRY co ON co.countryid = u.COUNTRYID
											INNER JOIN AFW_STATUS stat ON stat.stat_id = c.serv_status_id
											INNER JOIN GROUPHD g ON g.GRP_id = c.serv_responsible_group_id
											AND c.serv_category_id IN (74, 75)
											WHERE stat.stat_name NOT IN ('CERRADO','CANCELADO/RECHAZADO','SOLUCIONADO','RE-SOLUCIONADO', 'CERRADO CON OBSERVACIONES')
											AND	CASE WHEN serv_category_id = 74 THEN '3.1' ELSE '3.2' END = '3.1'
											ORDER by c.serv_opened_date")->fetchAll('assoc');
			
		$casos = [];							
		foreach($aranda as $detalle) {
			$casos[$detalle['BASE']][] = [
				'Fecha' => $detalle['BASE'],
				'Codigo' => $detalle['ID'],
				'Status' => $detalle['STATUS'],
				'Responsable' => $detalle['RESPONSABLE'],
				'Area' => $detalle['ÁREA'],
				'Tipo' => $detalle['TIPO_CUESTIONAMIENTO']
			];
		}
		
		
		$this->set(compact('pageTitle', 'inds', 'details', 'pendants', 'casos'));
        $this->set('_serialize', ['pageTitle', 'inds', 'details', 'pendants', 'casos']);
	}
	
	public function ooh($period = null)
	{
		$pageTitle = 'Seguimiento Producción Out of Home';
		
		$connection = ConnectionManager::get('IT_WORKFLOW_33');
		$inds		= $connection->execute("SELECT * FROM dbo.DBM_Status_Level_OOH WHERE Period = ? ORDER by [Sort] ASC, 1 ASC", [$period], ['integer'])->fetchAll('assoc');
		
		$details	= $connection->execute("SELECT * FROM dbo.DBM_Status_Detail_OOH WHERE Period = ?", [$period], ['integer'])->fetchAll('assoc');
		
		$pendants	= $connection->execute("SELECT * FROM dbo.DBM_Status_Pendants_OOH WHERE Period = ?", [$period], ['integer'])->fetchAll('assoc');
		
		
		$this->set(compact('pageTitle', 'inds', 'details', 'pendants'));
        $this->set('_serialize', ['pageTitle', 'inds', 'details', 'pendants']);
	}
	
	 public function us($period = null)
	{
		$pageTitle = 'Seguimiento Producción User & Shopper';
		
		$connection = ConnectionManager::get('IT_WORKFLOW_33');
		$inds		= $connection->execute("SELECT * FROM dbo.DBM_Status_Level_US WHERE Period = ? ORDER by [Sort] ASC, 1 ASC", [$period], ['integer'])->fetchAll('assoc');
		
		$details	= $connection->execute("SELECT * FROM dbo.DBM_Status_Detail_US WHERE Period = ?", [$period], ['integer'])->fetchAll('assoc');
		
		$pendants	= $connection->execute("SELECT * FROM dbo.DBM_Status_Pendants_US WHERE Period = ?", [$period], ['integer'])->fetchAll('assoc');
		
		
		$this->set(compact('pageTitle', 'inds', 'details', 'pendants'));
        $this->set('_serialize', ['pageTitle', 'inds', 'details', 'pendants']);
	}
	
	public function cross($period = null)
	{
		$pageTitle = 'Seguimiento Producción Cross';
		
		$connection = ConnectionManager::get('IT_WORKFLOW_33');
		
		$multies	= $connection->execute("SELECT * FROM dbo.DBM_Status_Level_Cross WHERE Period = ? ORDER by 1 ASC", [$period], ['integer'])->fetchAll('assoc');
		
		$details	= $connection->execute("SELECT * FROM dbo.DBM_Status_Detail_Cross WHERE Period = ?", [$period], ['integer'])->fetchAll('assoc');
		
		$pendants	= $connection->execute("SELECT * FROM dbo.DBM_Status_Pendants_Cross WHERE Period = ?", [$period], ['integer'])->fetchAll('assoc');
		
		
		$this->set(compact('pageTitle', 'details', 'pendants', 'multies'));
        $this->set('_serialize', ['pageTitle', 'details', 'pendants', 'multies']);
	}
	
	public function dependents($period = null, $dbname = null) {
		
		$pageTitle = 'Bases dependientes';
		
		$connection = ConnectionManager::get('IT_WORKFLOW_33');
		
		$list = $connection->execute("	SELECT	DISTINCT Nombre, Descripcion, Grupo, CONVERT(VARCHAR, k.liberation_created, 23) as liberation_created, a.Item_Filtro
										FROM	dbo.DBM_Status_General a
										JOIN
										(
										SELECT	DISTINCT Item_Filtro, Country, [Date], Period
										FROM	dbo.DBM_Status_General
										WHERE	Nombre = ?
										AND		[Date] = (SELECT MAX([Date]) FROM dbo.DBM_Status_General)
										AND		Period = ?
										AND		Grupo LIKE '%_Cross'
										) x ON x.Item_Filtro = a.Item_Filtro AND x.Country = a.Country AND x.[Date] = a.[Date] AND x.Period = a.Period
										LEFT JOIN	OPENQUERY(MYSQL_SERVER, 'SELECT maestros.dbname, maestros.clasification, (periods.year * 100 + periods.month_id) as period, tasks.* FROM tasks JOIN periods ON periods.id = tasks.period_id JOIN maestros ON maestros.id = tasks.master_id WHERE periods.status = 1') k ON k.dbname = a.Nombre COLLATE Modern_Spanish_CI_AS AND k.period = a.Period
										WHERE	a.Grupo LIKE '%_PNC'", [$dbname, $period], ['string', 'integer'])->fetchAll('assoc');
										
		$this->set(compact('pageTitle', 'list'));
        $this->set('_serialize', ['pageTitle', 'list']);
		
	}
}