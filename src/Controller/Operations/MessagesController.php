<?php
namespace App\Controller\Operations;

use Cake\Network\Session\DatabaseSession;
use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Datasource\ConnectionManager;

/**
 * Messages Controller
 *
 */
class MessagesController extends AppController
{
	public function initialize()
	{
		parent::initialize();
	}
	
	public function beforeFilter(Event $event) {
		
		parent::beforeFilter($event);
    }
    
    public function index($type = null, $reference = null, $period = null) {
	    
	    $this->viewBuilder()->layout('ajax');
	    
	    $messages = $this->Messages->find('all', [
		    'conditions' => [
			    'type' => $type,
		    	'reference' => $reference,
		    	'period' => $period
		    ],
		    'order' => ['date' => 'asc']
	    ]);
	    
	    $this->set(compact('type', 'reference', 'period', 'messages'));
        $this->set('_serialize', ['type', 'reference', 'period', 'messages']);
	    
	}
	
	public function content($type = null, $reference = null, $period = null) {
	    
	    $this->viewBuilder()->layout('ajax');
	    
	    $messages = $this->Messages->find('all', [
		    'conditions' => [
		    	'type' => $type,
		    	'reference' => $reference,
		    	'period' => $period
		    ],
		    'order' => ['date' => 'asc']
	    ]);
	    
	    $this->set(compact('messages'));
        $this->set('_serialize', ['messages']);
	    
	}
	
	public function create() {
		
		$this->viewBuilder()->layout('ajax');
		$this->request->allowMethod(['post', 'ajax']);
		
		$message = $this->Messages->newEntity();
		$message->email = $this->Auth->User('mail');
		$message->owner = $this->Auth->User('displayName');
		$message->userId = $this->Auth->User('id');
		
        if ($this->request->is('post') AND strlen($this->request->getData()['message']) > 1) {
            $message = $this->Messages->patchEntity($message, $this->request->getData());
            if ($this->Messages->save($message)) {
            }
        }
        $this->set(compact('message'));
        
	}

}