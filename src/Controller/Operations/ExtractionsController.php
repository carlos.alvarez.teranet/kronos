<?php
namespace App\Controller\Operations;

use Cake\Network\Session\DatabaseSession;
use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Datasource\ConnectionManager;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;


class ExtractionsController extends AppController
{
	
	public function initialize()
	{
		parent::initialize();
		$this->loadComponent('RequestHandler');
	}
	
	public function beforeFilter(Event $event) {
		
		parent::beforeFilter($event);
    }
    
    public function index()
	{ 
		$pageTitle = 'Descarga de DA1';
		
		$route 	= TMP . DS . "da1";
		$dir 	= new Folder($route);
		
		$files 	= $dir->find('.*\.da1');
		
		$folder = [];
		foreach ($files as $key => $file) {
		    $file = new File($dir->pwd() . DS . $file);
		    $info = $file->info();
		    
		    array_push($folder, $info);
		    
		}

		
		$this->set(compact('pageTitle', 'folder'));
        $this->set('_serialize', ['pageTitle', 'folder']);
	}
}