<?php
namespace App\Controller\Operations;

use Cake\Network\Session\DatabaseSession;
use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Datasource\ConnectionManager;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;


class ImagesController extends AppController
{
	
	public function initialize()
	{
		parent::initialize();
		$this->countries = [
			'ECUADOR' => 'ECU_KWP'
		];
	}
	
	public function beforeFilter(Event $event) {
		
		parent::beforeFilter($event);
    }
    
    public function index()
	{
		$pageTitle = 'Banco de imágenes';
		$countries = $this->countries;
		
		if($this->request->is('post')) {
		
			$data = $this->request->data();
			
			if(empty($data['country'])) {
				$this->Flash->error('Debe seleccionar un País para la búsqueda');
				return $this->redirect(
		            ['action' => 'index']
		        );
			}
			
			if(!empty($data['country']) and empty($data['idartigo']) and empty($data['codbar'])) {
				$this->Flash->error('Debe indicar un parámetro de búsqueda');
				return $this->redirect(
		            ['action' => 'index']
		        );
			}
			
			$country 	= $data['country'];
			$file		= TMP . 'images' . DS . 'config' . DS . 'FILES_' . $country . '.txt';
			$database	= TMP . 'images' . DS . 'config' . DS . 'NACFOTO_' . $country . '.csv';
			$array		= [];
			
			$file_d		= fopen($file, 'r');
			$database_d	= fopen($database, 'r');
			
			$header		= trim(fgets($database_d));	
			$header		= explode(";", $header);
			
			while(!feof($database_d)) {
				
				$line	= trim(fgets($database_d));	
				$line	= explode(";", $line);
				
				if($data['idartigo'] == $line[7]) {
					
					while(!feof($file_d)) {
					
						$ruta	= trim(fgets($file_d));	
						if(strpos($ruta, $line[5]) !== false) {
							$images[] = [
								'ruta' => $ruta,
								'archivo' => $line[5]
							];
						}
						
					}
					
				}
			}
			
			$connection = ConnectionManager::get($this->countries[$data['country']]);
			$rows	= $connection->execute("SELECT * FROM dbo.VW_ArtigoZ WHERE idArtigo = :idArtigo", ['idArtigo' => $data['idartigo']])->fetchAll('assoc');
			
		}
		
		$this->set(compact('pageTitle', 'countries', 'header', 'country', 'images', 'rows'));
        $this->set('_serialize', ['pageTitle', 'countries', 'header', 'country', 'images', 'rows']);
	}
	
}	