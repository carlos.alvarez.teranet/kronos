<?php
$baseDir = dirname(dirname(__FILE__));
return [
    'plugins' => [
        'Bake' => $baseDir . '/vendor/cakephp/bake/',
        'CakeDC/Mixer' => $baseDir . '/vendor/cakedc/mixer/',
        'CakeExcel' => $baseDir . '/vendor/dakota/cake-excel/',
        'DebugKit' => $baseDir . '/vendor/cakephp/debug_kit/',
        'Ldap' => $baseDir . '/vendor/queencitycodefactory/ldap/',
        'Migrations' => $baseDir . '/vendor/cakephp/migrations/',
        'Muffin/OAuth2' => $baseDir . '/vendor/muffin/oauth2/',
        'WyriHaximus/TwigView' => $baseDir . '/vendor/wyrihaximus/twig-view/'
    ]
];