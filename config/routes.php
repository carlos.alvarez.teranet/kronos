<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

use Cake\Core\Plugin;
use Cake\Routing\RouteBuilder;
use Cake\Routing\Router;
use Cake\Routing\Route\DashedRoute;
use Cake\Core\Configure;

/**
 * The default class to use for all routes
 *
 * The following route classes are supplied with CakePHP and are appropriate
 * to set as the default:
 *
 * - Route
 * - InflectedRoute
 * - DashedRoute
 *
 * If no call is made to `Router::defaultRouteClass()`, the class used is
 * `Route` (`Cake\Routing\Route\Route`)
 *
 * Note that `Route` does not do any inflections on URLs which will result in
 * inconsistently cased URLs when used with `:plugin`, `:controller` and
 * `:action` markers.
 *
 */
Router::defaultRouteClass(DashedRoute::class);

Router::scope('/', function (RouteBuilder $routes) {
	$routes->setExtensions(['json', 'xml']);
    $routes->connect('/', ['controller' => 'Stats', 'action' => 'index']);
    $routes->connect('/pages/*', ['controller' => 'Pages', 'action' => 'display']);
    $routes->fallbacks(DashedRoute::class);
});

Router::prefix('fusion', function ($routes) {
    // All routes here will be prefixed with `/admin`
    // And have the prefix => admin route element added.
    $routes->setExtensions(['json', 'xml']);
    $routes->fallbacks(DashedRoute::class);
});

Router::prefix('client_attribute', function ($routes) {
    // All routes here will be prefixed with `/admin`
    // And have the prefix => admin route element added.
    $routes->setExtensions(['json', 'xml', 'xlsx']);
    $routes->fallbacks(DashedRoute::class);
});

Router::prefix('client_services', function ($routes) {
    // All routes here will be prefixed with `/admin`
    // And have the prefix => admin route element added.
    $routes->setExtensions(['json', 'xml', 'xlsx']);
    $routes->fallbacks(DashedRoute::class);
});

Router::prefix('wpo', function ($routes) {
    // All routes here will be prefixed with `/admin`
    // And have the prefix => admin route element added.
    $routes->setExtensions(['json', 'xml', 'xlsx']);
    $routes->fallbacks(DashedRoute::class);
});

Router::prefix('uroboros', function ($routes) {
    // All routes here will be prefixed with `/admin`
    // And have the prefix => admin route element added.
    $routes->setExtensions(['json', 'xml']);
    $routes->fallbacks(DashedRoute::class);
});

Router::prefix('shoppix', function ($routes) {
    // All routes here will be prefixed with `/admin`
    // And have the prefix => admin route element added.
    $routes->setExtensions(['json', 'xml']);
    $routes->fallbacks(DashedRoute::class);
});

Router::prefix('fcm', function ($routes) {
    // All routes here will be prefixed with `/admin`
    // And have the prefix => admin route element added.
    $routes->setExtensions(['json', 'xml']);
    $routes->fallbacks(DashedRoute::class);
});

Router::prefix('mcm', function ($routes) {
    // All routes here will be prefixed with `/admin`
    // And have the prefix => admin route element added.
    $routes->setExtensions(['json', 'xml']);
    $routes->fallbacks(DashedRoute::class);
});

Router::prefix('information', function ($routes) {
    // All routes here will be prefixed with `/admin`
    // And have the prefix => admin route element added.
    $routes->setExtensions(['json', 'xml']);
    $routes->fallbacks(DashedRoute::class);
});

Router::prefix('operations', function ($routes) {
    // All routes here will be prefixed with `/admin`
    // And have the prefix => admin route element added.
    $routes->setExtensions(['json', 'xml']);
    $routes->fallbacks(DashedRoute::class);
});

Router::prefix('validation', function ($routes) {
    // All routes here will be prefixed with `/admin`
    // And have the prefix => admin route element added.
    $routes->setExtensions(['json', 'xml']);
    $routes->fallbacks(DashedRoute::class);
});

Router::prefix('mirror', function ($routes) {
    // All routes here will be prefixed with `/admin`
    // And have the prefix => admin route element added.
    $routes->setExtensions(['json', 'xml']);
    $routes->fallbacks(DashedRoute::class);
});

Router::prefix('changes', function ($routes) {
    // All routes here will be prefixed with `/admin`
    // And have the prefix => admin route element added.
    $routes->setExtensions(['json', 'xml']);
    $routes->fallbacks(DashedRoute::class);
});


Plugin::routes();
